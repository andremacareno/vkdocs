package daogenerator;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

/**
 * Created by Andrew on 08.01.2016.
 */
public class DAOGenerator {

    public static void main(String[] args) throws Exception {
        Schema schema = new Schema(1, "com.andremacareno.vkdocs.dao");

        folderEntity(schema);
        docEntity(schema);

        new DaoGenerator().generateAll(schema, "app/src/main/java/");
    }

    private static void folderEntity(Schema schema)
    {
        Entity folder = schema.addEntity("Folder");
        folder.setHasKeepSections(true);
        folder.addIdProperty().autoincrement();
        folder.addStringProperty("folderName").notNull();
    }
    private static void docEntity(Schema schema)
    {
        Entity doc = schema.addEntity("Document");
        doc.setHasKeepSections(true);
        doc.addIdProperty();
        doc.addStringProperty("title");
        doc.addLongProperty("size");
        doc.addStringProperty("ext");
        doc.addStringProperty("url");
        doc.addStringProperty("preview_url");
        doc.addLongProperty("date");
        doc.addLongProperty("folderId");
        doc.addStringProperty("path");
    }
}
