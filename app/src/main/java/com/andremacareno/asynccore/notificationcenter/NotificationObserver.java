package com.andremacareno.asynccore.notificationcenter;

/**
 * Created by andremacareno on 24/04/15.
 */
public abstract class NotificationObserver {
    private int id;
    private Object key;
    public NotificationObserver(int id)
    {
        this.id = id;
        this.key = null;
    }
    public NotificationObserver(int id, Object key)
    {
        this.id = id;
        this.key = key;
    }
    public int getId() { return this.id; }
    public Object getKey() { return this.key; }
    public void setKey(Object k) { this.key = k; }
    public abstract void didNotificationReceived(Notification notification);

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NotificationObserver that = (NotificationObserver) o;

        if (id != that.id) return false;
        if (key != null ? !key.equals(that.key) : that.key != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (key != null ? key.hashCode() : 0);
        return result;
    }
}
