package com.andremacareno.asynccore.notificationcenter;

import com.andremacareno.vkdocs.AppLoader;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by andremacareno on 24/04/15.
 */
public class NotificationCenter {
    private static final String TAG = "NotificationCenter";
    private static int totalEvents = 1;
    public static final int didLoggedOut = totalEvents++;
    public static final int didDocumentsHandled = totalEvents++;
    public static final int didFileProgressUpdated = totalEvents++;
    public static final int didFileDownloadFinished = totalEvents++;
    public static final int didfileDownloadCancelled = totalEvents++;
    public static final int didImagePathRetrieveFailed = totalEvents++;
    public static final int didImagePathRetrieved = totalEvents++;
    public static final int didFriendsHandled = totalEvents++;
    public static final int didImageProcessingFailed = totalEvents++;
    public static final int didImageProcessed = totalEvents++;
    public static final int didImageCached = totalEvents++;
    public static final int didGalleryPhotoThumbnailCached = totalEvents++;
    public static final int didRecentFromGalleryLoaded = totalEvents++;
    public static final int didQuickSendModelsCreated = totalEvents++;
    public static final int didNavDrawerClosed = totalEvents++;
    public static final int didNavDrawerActionSelected = totalEvents++;
    public static final int didUploadFailed = totalEvents++;
    public static final int didOfflineListReceived = totalEvents++;
    public static final int didSavedOffline = totalEvents++;
    public static final int didRemovedOffline = totalEvents++;
    public static final int didFolderInfoRetrieved = totalEvents++;
    public static final int didNewFolderSaved = totalEvents++;
    public static final int didFolderListLoaded = totalEvents++;
    public static final int didFolderContentLoaded = totalEvents++;
    public static final int didAppInitialized = totalEvents++;
    public static final int didAppInitFailed = totalEvents++;
    public static final int didOfflineTotalSizeComputed = totalEvents++;
    public static final int didLocalSearchFinished = totalEvents++;
    public static final int didUploadFinished = totalEvents++;
    public static final int didNetworkConnectionStateChanged = totalEvents++;
    public static final int didBookOpened = totalEvents++;
    public static final int didBookOpenFailed = totalEvents++;
    public static final int didTextLoaded = totalEvents++;
    public static final int didTextLoadingFailed = totalEvents++;
    public static final int didPageNumbersCalculated = totalEvents++;
    public static final int didPageNumbersFailed = totalEvents++;
    public static final int didPreloadFailed = totalEvents++;
    public static final int didPreloadFinished = totalEvents++;
    public static final int didBooksDashboardLoaded = totalEvents++;
    public static final int didBookCoverRetrieved = totalEvents++;
    public static final int didBookCoverRetrieveFailed = totalEvents++;

    private ConcurrentHashMap<Integer, CopyOnWriteArrayList<NotificationObserver>> observers = new ConcurrentHashMap<Integer, CopyOnWriteArrayList<NotificationObserver>>();
    private static volatile NotificationCenter _instance;

    private NotificationCenter() {}
    public static NotificationCenter getInstance()
    {
        if(_instance == null)
        {
            synchronized(NotificationCenter.class)
            {
                if(_instance == null)
                    _instance = new NotificationCenter();
            }
        }
        return _instance;
    }
    public static void stop()
    {
        if(_instance != null)
        {
            synchronized(NotificationCenter.class)
            {
                if(_instance != null) {
                    _instance.removeAllObservers();
                    _instance = null;
                }
            }
        }
    }

    public void postNotification(int id, Object obj)
    {
        Notification notification = new Notification(id, obj);
        CopyOnWriteArrayList<NotificationObserver> observersOfId = observers.get(id);
        if(observersOfId == null)
            return;
        for(NotificationObserver obs : observersOfId)
        {
            obs.didNotificationReceived(notification);
        }
    }
    public void postNotification(int id)
    {
        postNotification(id, null);
    }
    public void postNotificationDelayed(final int id, final Object obj, long delay)
    {
        try
        {
            AppLoader.applicationHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    postNotification(id, obj);
                }
            }, delay);
        }
        catch(Exception ignored) {}
    }
    public void addObserver(NotificationObserver obs) {
        if(obs == null)
            return;
        int id = obs.getId();
        CopyOnWriteArrayList<NotificationObserver> idObservers = observers.get(id);
        if(idObservers == null)
            observers.put(id, (idObservers = new CopyOnWriteArrayList<NotificationObserver>()));
        idObservers.add(obs);
    }

    public void removeObserver(NotificationObserver obs) {
        if(obs == null)
            return;
        int id = obs.getId();
        CopyOnWriteArrayList<NotificationObserver> idObservers = observers.get(id);
        if(idObservers != null)
        {
            idObservers.remove(obs);
            if(idObservers.size() == 0)
                observers.remove(id);
        }
    }
    public void removeAllObservers()
    {
        for(Integer i : observers.keySet())
            observers.get(i).clear();
        observers.clear();
    }

}
