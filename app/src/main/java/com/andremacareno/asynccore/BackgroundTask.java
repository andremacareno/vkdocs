package com.andremacareno.asynccore;

/**
 * Created by Andrew on 09.06.2014.
 */
public abstract class BackgroundTask {

    protected OnTaskCompleteListener onTaskCompleteListener = null;
    protected OnTaskFailureListener onTaskFailureListener = null;
    private final Object pauseObject = new Object();
    private long _id;
    public Object getNotificationObject() { return null; }
    public abstract String getTaskKey();
    public abstract int getTaskFailedNotificationId();
    public abstract int getTaskCompletedNotificationId();
    public void onComplete(OnTaskCompleteListener l)
    {
        this.onTaskCompleteListener = l;
    }
    public void onFailure(OnTaskFailureListener l)
    {
        this.onTaskFailureListener = l;
    }

    public void addToQueue()
    {
        BackgroundTaskManager.sharedInstance().addTask(this);
    }
    protected abstract void work() throws Exception;
    public OnTaskCompleteListener getTaskCompleteListener() { return this.onTaskCompleteListener; }
    public OnTaskFailureListener getTaskFailureListener() { return this.onTaskFailureListener; }

    protected void pauseIfNeed()
    {
        while(BackgroundTaskManager.sharedInstance().checkPauseRequested())
        {
            try
            {
                synchronized(pauseObject)
                {
                    pauseObject.wait();
                }
            }
            catch(Exception ignored) {}
        }
    }
    public void unpause()
    {
        try
        {
            synchronized(pauseObject)
            {
                pauseObject.notifyAll();
            }
        }
        catch(Exception ignored) {}
    }

    public void assignTaskId(long id)
    {
        this._id = id;
    }
    public long getTaskId() { return this._id; }
    protected boolean isCritical() { return false; }
}
