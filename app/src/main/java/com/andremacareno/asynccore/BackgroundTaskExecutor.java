package com.andremacareno.asynccore;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by Andrew on 09.06.2014.
 */
public class BackgroundTaskExecutor {
    BlockingQueue<BackgroundTask> tasks;
    CountDownLatch cdl;
    public final AtomicBoolean paused = new AtomicBoolean(false);
    public final Object lockObject = new Object();
    public static final int maxThreads = Runtime.getRuntime().availableProcessors() <= 1 ? 1 : Runtime.getRuntime().availableProcessors() - 1;
    public BackgroundTaskExecutor()
    {
        tasks = new LinkedBlockingQueue<BackgroundTask>();
        cdl = new CountDownLatch(maxThreads);
    }

    public void run()
    {
        for(long i = 0; !Thread.interrupted();)
        {
            try
            {
                BackgroundTask task = tasks.take();
                if(!task.isCritical())
                {
                    if(i != 0 && (i % maxThreads) == 0) {
                        i = 0;
                        cdl.await();
                    }
                    i++;
                }

                WorkerThread wtr = new WorkerThread(task, cdl);
                wtr.start();
            }
            catch (InterruptedException e) {
            }
        }
    }
    public void addTask(BackgroundTask task)
    {
        tasks.add(task);
    }
}
