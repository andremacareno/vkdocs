package com.andremacareno.vkdocs;

/**
 * Created by Andrew on 13.01.2016.
 */
public interface BackPressProcessor {
    void removeBackPressInterceptor(BackPressListener interceptor);
    void interceptBackPress(BackPressListener interceptor);
}
