package com.andremacareno.vkdocs.objs;

import nl.siegmann.epublib.domain.Book;

/**
 * Created by andremacareno on 26/03/16.
 */
public class BookWrap {
    public String path;
    public Book book;
    public BookWrap(String path, Book bk) { this.path = path; this.book = bk; }
}
