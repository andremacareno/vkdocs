package com.andremacareno.vkdocs.objs;

/**
 * Created by Andrew on 30.01.2016.
 */
public interface BottomSheetListener {
    void onActionSelect(int bottomSheetAction, Object arg);
}
