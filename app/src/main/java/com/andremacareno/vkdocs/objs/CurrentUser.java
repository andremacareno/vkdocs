package com.andremacareno.vkdocs.objs;

/**
 * Created by andremacareno on 29/02/16.
 */
public class CurrentUser {
    private static volatile CurrentUser _instance;
    private String photo;
    private String displayName;
    private long id;
    private CurrentUser()
    {
        photo = displayName = null;
        id = 0;
    }
    private CurrentUser(long id, String displayName, String photo)
    {
        this.photo = photo;
        this.displayName = displayName;
        this.id = id;
    }

    public static void createInstance(long id, String displayName, String photo)
    {
        if(_instance == null)
        {
            synchronized(CurrentUser.class)
            {
                if(_instance == null)
                    _instance = new CurrentUser(id, displayName, photo);
            }
        }
    }
    public static CurrentUser getInstance()
    {
        if(_instance == null)
        {
            synchronized(CurrentUser.class)
            {
                return _instance;
            }
        }
        return _instance;
    }
    public String getPhoto() { return this.photo; }
    public String getDisplayName() { return this.displayName; }
    public long getId() { return this.id; }
    public static void didLoggedOut() {
        synchronized(CurrentUser.class)
        {
            _instance = null;
        }
    }

}
