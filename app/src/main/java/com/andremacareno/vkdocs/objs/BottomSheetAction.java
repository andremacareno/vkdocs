package com.andremacareno.vkdocs.objs;

/**
 * Created by Andrew on 30.01.2016.
 */
public class BottomSheetAction {
    private static int actionCount = 0;
    public static final int NOTHING = actionCount++;
    public static final int CHOOSE_FROM_GALLERY = actionCount++;
    public static final int CHOOSE_A_FILE = actionCount++;
    public static final int NEW_FOLDER = actionCount++;
    public static final int SHARE_WITH_A_FRIEND = actionCount++;
    public static final int MOVE_TO_FOLDER = actionCount++;
    public static final int MAKE_OFFLINE = actionCount++;
    public static final int REVOKE_OFFLINE = actionCount++;
    public static final int DELETE = actionCount++;
    public static final int RENAME = actionCount++;
    public static final int REMOVE_FROM_FOLDER = actionCount++;
    public static final int SELECT = actionCount++;
    public static final int DIVIDER = actionCount++;

    private int action;
    private int iconRes;
    private String actionString;
    public BottomSheetAction(int actId, int icon, String actString)
    {
        this.action = actId;
        this.iconRes = icon;
        this.actionString = actString;
    }
    public int getAction() { return this.action; }
    public int getIconRes() { return this.iconRes; }
    public String getActionString() { return this.actionString; }
    public void setText(String txt) { this.actionString = txt; }
}
