package com.andremacareno.vkdocs.objs;

/**
 * Created by andremacareno on 15/03/16.
 */
public class DocsListSection {
    private String title;
    public DocsListSection(String t)
    {
        this.title = t;
    }
    public String getTitle() { return this.title; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DocsListSection that = (DocsListSection) o;

        return title.equals(that.title);

    }

    @Override
    public int hashCode() {
        return title.hashCode();
    }
}
