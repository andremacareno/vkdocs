package com.andremacareno.vkdocs.objs;

import com.andremacareno.vkdocs.dao.Document;
import com.andremacareno.vkdocs.dao.Folder;

/**
 * Created by andremacareno on 15/03/16.
 */
public class DocListItemWrap {
    public enum Type {DOCUMENT, FOLDER, SECTION};
    private Type type;
    private Object obj;
    private DocListItemWrap(Document doc)
    {
        this.type = Type.DOCUMENT;
        this.obj = doc;
    }
    public DocListItemWrap(Folder folder)
    {
        this.type = Type.FOLDER;
        this.obj = folder;
    }
    public DocListItemWrap(DocsListSection section)
    {
        this.type = Type.SECTION;
        this.obj = section;
    }
    public Document castDoc() { return (Document) this.obj; }
    public Folder castFolder() { return (Folder) this.obj; }
    public DocsListSection castSection() { return (DocsListSection) this.obj; }
    public Type getType() { return this.type; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DocListItemWrap that = (DocListItemWrap) o;

        if (type != that.type) return false;
        return !(obj != null ? !obj.equals(that.obj) : that.obj != null);

    }

    @Override
    public int hashCode() {
        int result = type.hashCode();
        result = 31 * result + (obj != null ? obj.hashCode() : 0);
        return result;
    }
    public static DocListItemWrap createInstance(Document doc)
    {
        return new DocListItemWrap(doc);
    }
}
