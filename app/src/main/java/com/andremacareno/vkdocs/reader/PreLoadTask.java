package com.andremacareno.vkdocs.reader;

import android.text.Spannable;

import com.andremacareno.asynccore.BackgroundTask;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.vkdocs.reader.epub.PageTurnerSpine;

import nl.siegmann.epublib.domain.Resource;

/**
 * Created by alex on 10/14/14.
 */
public class PreLoadTask extends BackgroundTask {

    private PageTurnerSpine spine;
    private TextLoader textLoader;

    public PreLoadTask( PageTurnerSpine spine, TextLoader textLoader ) {
        this.spine = spine;
        this.textLoader = textLoader;
    }

    @Override
    public String getTaskKey() {
        return "preload";
    }

    @Override
    public int getTaskFailedNotificationId() {
        return NotificationCenter.didPreloadFailed;
    }

    @Override
    public int getTaskCompletedNotificationId() {
        return NotificationCenter.didPreloadFinished;
    }

    @Override
    protected void work() throws Exception {
        if ( spine == null ) {
            return;
        }

        Resource res = spine.getNextResource();

        Spannable cachedText = textLoader.getCachedTextForResource( res );

        if (cachedText == null) {
            try {
                textLoader.getText(res, null);
            } catch ( Exception | OutOfMemoryError ignored ) {}
        }
    }
}


