/*
 * Copyright (C) 2012 Alex Kuiper
 *
 * This file is part of PageTurner
 *
 * PageTurner is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PageTurner is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PageTurner.  If not, see <http://www.gnu.org/licenses/>.*
 */

package com.andremacareno.vkdocs.reader;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Debug;
import android.preference.PreferenceManager;

import com.andremacareno.vkdocs.AndroidUtilities;
import com.andremacareno.vkdocs.AppLoader;

import net.nightwhistler.htmlspanner.FontFamily;

import java.util.Locale;
public class Configuration {

	private SharedPreferences settings;
	private Context context;

	private FontFamily font;

	public static enum ColourProfile {
		DAY, NIGHT
	}

    public static enum LongShortPressBehaviour {
        NORMAL, REVERSED
    }


	public static final String KEY_POS = "offset:";
	public static final String KEY_NAV_TAP_V = "nav_tap_v";
	public static final String KEY_NAV_TAP_H = "nav_tap_h";
	public static final String KEY_NAV_SWIPE_H = "nav_swipe_h";
	public static final String KEY_SCROLLING = "scrolling";
	public static final String KEY_TEXT_SIZE = "itext_size";

	public static final String KEY_MARGIN_H = "margin_h";
	public static final String KEY_MARGIN_V = "margin_v";

	public static final String KEY_LINE_SPACING = "line_spacing";

    public static final String KEY_LONG_SHORT = "long_short";

	private static Configuration _instance;
	public static Configuration sharedInstance()
	{
		if(_instance == null)
		{
			synchronized(Configuration.class)
			{
				_instance = new Configuration(AppLoader.sharedInstance().getApplicationContext());
			}
		}
		return _instance;
	}
	private Configuration(Context context) {
		this.settings = PreferenceManager.getDefaultSharedPreferences(context);
		this.context = context;
	}

    public LongShortPressBehaviour getLongShortPressBehaviour() {
        String value = settings.getString(KEY_LONG_SHORT,
                LongShortPressBehaviour.NORMAL.name());
        return LongShortPressBehaviour.valueOf(value.toUpperCase(Locale.US));
    }

	public int getTextSize() {
		return settings.getInt(KEY_TEXT_SIZE, 18);
	}

	public int getHorizontalMargin() {
		return settings.getInt(KEY_MARGIN_H, 30);
	}

	public int getVerticalMargin() {
		return settings.getInt(KEY_MARGIN_V, 25);
	}

	public int getLineSpacing() {
		return settings.getInt(KEY_LINE_SPACING, 0);
	}

	public ColourProfile getColourProfile() {
			return ColourProfile.DAY;
	}


	public FontFamily getFontFamily() {

		if(font == null) {
			font = new FontFamily("literata", AndroidUtilities.getTypeface("literata-regular.otf"));
			font.setBoldTypeface(AndroidUtilities.getTypeface("literata-bold.otf"));
			font.setBoldItalicTypeface(AndroidUtilities.getTypeface("literata-bold-italic.otf"));
			font.setItalicTypeface(AndroidUtilities.getTypeface("literata-italic.otf"));
		}
		return font;
	}
	public static double getMemoryUsage() {
		long max = Runtime.getRuntime().maxMemory();
		long used = Runtime.getRuntime().totalMemory();

		return (double) used / (double) max;
	}

	public static double getBitmapMemoryUsage() {

		if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) {
			return getMemoryUsage();
		}

		long max = Runtime.getRuntime().maxMemory();
		long used = Debug.getNativeHeapAllocatedSize();

		return (double) used / (double) max;
	}
}
