package com.andremacareno.vkdocs.reader;
/*
    Based on the source code of PageTurner app: https://github.com/NightWhistler/PageTurner
*/

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.os.Build;
import android.os.Handler;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.SpannedString;
import android.text.style.BackgroundColorSpan;
import android.text.style.ClickableSpan;
import android.text.style.ImageSpan;
import android.util.Base64;
import android.util.Log;
import android.util.Pair;
import android.view.ActionMode;
import android.view.MotionEvent;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.andremacareno.asynccore.BackgroundTask;
import com.andremacareno.asynccore.OnTaskCompleteListener;
import com.andremacareno.asynccore.OnTaskFailureListener;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.vkdocs.BuildConfig;
import com.andremacareno.vkdocs.R;
import com.andremacareno.vkdocs.reader.dto.TocEntry;
import com.andremacareno.vkdocs.reader.epub.PageTurnerSpine;
import com.andremacareno.vkdocs.reader.epub.ResourceLoader;

import net.nightwhistler.htmlspanner.FontFamily;
import net.nightwhistler.htmlspanner.HtmlSpanner;
import net.nightwhistler.htmlspanner.SpanStack;
import net.nightwhistler.htmlspanner.TagNodeHandler;
import net.nightwhistler.htmlspanner.handlers.TableHandler;
import net.nightwhistler.htmlspanner.spans.CenterSpan;

import org.htmlcleaner.TagNode;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import nl.siegmann.epublib.Constants;
import nl.siegmann.epublib.domain.Book;
import nl.siegmann.epublib.domain.Resource;
import nl.siegmann.epublib.domain.TOCReference;
import nl.siegmann.epublib.util.IOUtil;
import nl.siegmann.epublib.util.StringUtil;

import static java.util.Arrays.asList;

public class BookView extends ScrollView implements LinkTagHandler.LinkCallBack {


    private int storedIndex;
    private String storedAnchor;

    private InnerView childView;

    private Set<BookViewListener> listeners;

    private TableHandler tableHandler;

    private PageTurnerSpine spine;

    private String fileName;
    private Book book;

    private int prevIndex = -1;
    private int prevPos = -1;

    private PageChangeStrategy strategy;
    private ResourceLoader loader;

    private int horizontalMargin = 0;
    private int verticalMargin = 0;
    private int lineSpacing = 0;
    private boolean cancelled = false;
    private final Runnable restoreRunnable = new Runnable() {
        @Override
        public void run() {
            restorePosition();
            strategy.updateGUI();
            progressUpdate();
            parseEntryComplete(spine.getCurrentTitle());
            performPreload();
        }
    };
    private final Runnable calculatePagesRunnable = new Runnable() {
        @Override
        public void run() {
            performCalculatePageNumbers();
        }
    };

    private Handler scrollHandler;

    private TextLoader textLoader;

    private EpubFontResolver fontResolver;
    private static final HashSet<Character> boundaryChars = new HashSet<Character>() {{
        add(' ');
        add('.');
        add(',');
        add('\"');
        add('\'');
        add('\n');
        add('\t');
        add(':');
        add('!');
    }};

    public BookView(Context context) {
        super(context);
        this.scrollHandler = new Handler();
    }

    public void init() {
        this.listeners = new HashSet<>();
        this.childView = new InnerView(getContext());
        childView.setBookView(this);
        LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        childView.setLayoutParams(lp);
        addView(childView);
        this.childView.setBookView(this);

        childView.setCursorVisible(false);
        childView.setLongClickable(true);
        this.setVerticalFadingEdgeEnabled(false);
        childView.setFocusable(true);
        childView.setLinksClickable(true);

        //childView.setTextIsSelectable(true);
        this.strategy = new FixedPagesStrategy();
        this.strategy.setBookView(this);
        this.setSmoothScrollingEnabled(false);
        this.textLoader = new TextLoader();
        this.fontResolver = new EpubFontResolver(textLoader, getContext());
        this.tableHandler = new TableHandler();
        this.textLoader.registerTagNodeHandler("table", tableHandler);

        ImageTagHandler imgHandler = new ImageTagHandler(false);
        this.textLoader.registerTagNodeHandler("img", imgHandler);
        this.textLoader.registerTagNodeHandler("image", imgHandler);
        this.textLoader.setLinkCallBack(this);
        int pos = this.strategy.getTopLeftPosition();
        this.strategy.clearText();

        strategy.setBookView(this);
        this.strategy.setPosition(pos);
        loadText();
    }

    private void onInnerViewResize() {
        restorePosition();

        if ( this.tableHandler != null ) {
            int tableWidth = (int) (childView.getWidth() * 0.9);
            tableHandler.setTableWidth(tableWidth);
        }
    }

    public String getFileName() {
        return fileName;
    }
    @Override
    public void linkClicked(String href) {
        navigateTo(spine.resolveHref(href));
    }

    public PageChangeStrategy getStrategy() {
        return this.strategy;
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        progressUpdate();
    }

    /**
     * Returns if we're at the start of the book, i.e. displaying the title
     * page.
     *
     * @return
     */
    public boolean isAtStart() {

        if (spine == null) {
            return true;
        }

        return spine.getPosition() == 0 && strategy.isAtStart();
    }

    public boolean isAtEnd() {
        if (spine == null) {
            return false;
        }

        return spine.getPosition() >= spine.size() - 1 && strategy.isAtEnd();
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
        this.loader = new ResourceLoader(fileName);
    }

    @Override
    public void setOnTouchListener(OnTouchListener l) {
        super.setOnTouchListener(l);
        this.childView.setOnTouchListener(l);
    }


    /*public List<HighlightSpan> getHighlightsAt( float x, float y ) {
        return getSpansAt(x, y, HighlightSpan.class );
    }*/

    public List<ClickableSpan> getLinkAt(float x, float y) {
        return getSpansAt(x, y, ClickableSpan.class);
    }

    /**
     * Returns all the spans of a specific class at a specific location.
     *
     * @param x the X coordinate
     * @param y the Y coordinate
     * @param spanClass the class of span to filter for
     * @param <A>
     * @return a List of spans of type A, may be empty.
     */
    private <A> List<A> getSpansAt( float x, float y, Class<A> spanClass) {

        Integer offsetOption = findOffsetForPosition(x, y);

        CharSequence text = childView.getText();

        if (offsetOption == null || ! (text instanceof Spanned) ) {
            return new ArrayList<>();
        }


        return asList( ((Spanned) text).getSpans(offsetOption, offsetOption, spanClass));
    }

    /**
     * Blocks the inner-view from creating action-modes for a given amount of time.
     *
     * @param time
     */
    public void blockFor( long time ) {
        this.childView.setBlockUntil( System.currentTimeMillis() + time );
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {

        if (strategy.isScrolling()) {
            return super.onTouchEvent(ev);
        } else {
            return childView.onTouchEvent(ev);
        }
    }

    public boolean hasPrevPosition() {
        return this.prevIndex != -1 && this.prevPos != -1;
    }

    public void setLineSpacing(int lineSpacing) {
        if (lineSpacing != this.lineSpacing) {
            this.lineSpacing = lineSpacing;
            this.childView.setLineSpacing(lineSpacing, 1);

            if (strategy != null) {
                strategy.updatePosition();
            }
        }
    }


    public int getLineSpacing() {
        return lineSpacing;
    }

    public void setHorizontalMargin(int horizontalMargin) {

        if (horizontalMargin != this.horizontalMargin) {
            this.horizontalMargin = horizontalMargin;
            setPadding(this.horizontalMargin, this.verticalMargin,
                    this.horizontalMargin, this.verticalMargin);
            if (strategy != null) {
                strategy.updatePosition();
            }
        }
    }

    public void releaseResources() {
        this.strategy.clearText();
        this.textLoader.closeCurrentBook();
        cancelled = true;
    }

    public void setLinkColor(int color) {
        this.childView.setLinkTextColor(color);
    }

    public void setVerticalMargin(int verticalMargin) {
        if (verticalMargin != this.verticalMargin) {
            this.verticalMargin = verticalMargin;
            setPadding(this.horizontalMargin, this.verticalMargin,
                    this.horizontalMargin, this.verticalMargin);
            if (strategy != null) {
                strategy.updatePosition();
            }
        }
    }

    public int getVerticalMargin() {
        return verticalMargin;
    }

    public int getSelectionStart() {
        return childView.getSelectionStart();
    }

    public int getSelectionEnd() {
        return childView.getSelectionEnd();
    }

    public void goBackInHistory() {

        if (this.prevIndex == this.getIndex()) {
            strategy.setPosition(prevPos);

            this.storedAnchor = null;
            this.prevIndex = -1;
            this.prevPos = -1;

            restorePosition();

        } else {
            this.strategy.clearText();
            this.spine.navigateByIndex(this.prevIndex);
            strategy.setPosition(this.prevPos);

            this.storedAnchor = null;
            this.prevIndex = -1;
            this.prevPos = -1;

            loadText();
        }
    }

    public void clear() {
        this.childView.setText("");
        this.storedAnchor = null;
        this.storedIndex = -1;
        this.book = null;
        this.fileName = null;

        this.strategy.reset();
    }

    /**
     * Loads the text and saves the restored position.
     */
    public void restore() {
        strategy.clearText();
        loadText();
    }

    public void setIndex(int index) {
        this.storedIndex = index;
    }

    void loadText() {

        if ( spine == null && ! textLoader.hasCachedBook( this.fileName ) ) {
            performOpen();
        } else {
            if ( spine == null ) {
                try {
                    Book book = initBookAndSpine();

                    if ( book != null ) {
                        bookOpened( book );
                    }

                } catch ( IOException io ) {
                    return;
                } catch ( OutOfMemoryError e ) {
                    return;
                }
            }
            loadText(spine.getCurrentResource());
        }
    }

    private void loadText( Resource resource ) {


        Spannable cachedText = textLoader.getCachedTextForResource( resource );

        //TODO cancel other loading tasks

        if (cachedText != null && getInnerView().getWidth() > 0  ) {
            loadText(cachedText);
        } else {
            performLoadText(resource);
        }
        performPreload();
    }

    private void loadText( Spanned text ) {

        strategy.loadText( text );

        restorePosition();
        strategy.updateGUI();
        progressUpdate();
        parseEntryComplete(spine.getCurrentTitle());
    }

    private Book initBookAndSpine() throws IOException {

        Book book = textLoader.initBook(fileName);
        textLoader.setFontResolver(fontResolver);
        textLoader.initHtmlSpanner();
        this.book = book;
        this.spine = new PageTurnerSpine(book);

        this.spine.navigateByIndex(BookView.this.storedIndex);
        return book;
    }

    public void setFontFamily(FontFamily family) {
        this.childView.setTypeface(family.getDefaultTypeface());
        this.tableHandler.setTypeFace(family.getDefaultTypeface());
    }

    public void pageDown() {
        strategy.pageDown();
        progressUpdate();
    }

    public void pageUp() {
        strategy.pageUp();
        progressUpdate();
    }

    TextView getInnerView() {
        return childView;
    }

    PageTurnerSpine getSpine() {
        return this.spine;
    }

    private Integer findOffsetForPosition(float x, float y) {

        if (childView == null || childView.getLayout() == null) {
            return null;
        }

        Layout layout = this.childView.getLayout();
        int line = layout.getLineForVertical((int) y);

        return layout.getOffsetForHorizontal(line, x);
    }


    private static boolean isBoundaryCharacter(char c) {
        return boundaryChars.contains(c);
    }

    public void navigateTo( TocEntry tocEntry ) {
        navigateTo( tocEntry.getHref() );
    }

    public void navigateTo(String rawHref) {

        this.prevIndex = this.getIndex();
        this.prevPos = this.getProgressPosition();

        // Default Charset for android is UTF-8
        // http://developer.android.com/reference/java/nio/charset/Charset.html#defaultCharset()
        String charsetName = Charset.defaultCharset().name();

        if (!Charset.isSupported(charsetName)) {
            charsetName = "UTF-8";
        }

        // URLDecode the href, so it does not contain %20 etc.
        String href;
        try {
            href = URLDecoder.decode(StringUtil.substringBefore(rawHref,
                    Constants.FRAGMENT_SEPARATOR_CHAR), charsetName);

        } catch (UnsupportedEncodingException e) {
            // Won't ever be reached
            throw new AssertionError(e);
        }

        // Don't decode the anchor.
        String anchor = StringUtil.substringAfterLast(rawHref,
                Constants.FRAGMENT_SEPARATOR_CHAR);

        if (!"".equals(anchor)) {
            this.storedAnchor = anchor;
        }

        // Just an anchor and no href; resolve it on this page
        if (href.length() == 0) {
            restorePosition();
        } else {

            this.strategy.clearText();
            this.strategy.setPosition(0);

            if (this.spine.navigateByHref(href)) {
                loadText();
            } else {

                Resource resource = book.getResources().getByHref(href);

                if ( resource != null ) {
                    loadText( resource );
                } else {
                    loadText( new SpannedString( getContext().getString(R.string.dead_link ) ) );
                }
            }
        }
    }

    public void navigateToPercentage(int percentage) {

        if (spine == null) {
            return;
        }

        int index = 0;

        if ( percentage > 0 ) {

            double targetPoint = (double) percentage / 100d;
            List<Double> percentages = this.spine.getRelativeSizes();

            if (percentages == null || percentages.isEmpty()) {
                return;
            }

            double total = 0;

            for (; total < targetPoint && index < percentages.size(); index++) {
                total = total + percentages.get(index);
            }

            index--;

            // Work-around for when we get multiple events.
            if (index < 0 || index >= percentages.size()) {
                return;
            }

            double partBefore = total - percentages.get(index);
            double progressInPart = (targetPoint - partBefore)
                    / percentages.get(index);

            this.strategy.setRelativePosition(progressInPart);
        } else {

            //Simply jump to titlepage
            this.strategy.setPosition(0);
        }

        this.prevPos = this.getProgressPosition();
        doNavigation(index);
    }

    private void doNavigation(int index) {

        // Check if we're already in the right part of the book
        if (index == this.getIndex()) {
            restorePosition();
            progressUpdate();
            return;
        }

        this.prevIndex = this.getIndex();

        this.storedIndex = index;
        this.strategy.clearText();
        this.spine.navigateByIndex(index);

        loadText();
    }

    public void navigateTo(int index, int position) {

        this.prevPos = this.getProgressPosition();
        this.strategy.setPosition(position);

        doNavigation(index);
    }

    public List<TocEntry> getTableOfContents() {

        if ( this.book != null ) {
            List<TocEntry> result = new ArrayList<>();
            flatten(book.getTableOfContents().getTocReferences(), result, 0);
            return result;
        } else {
            return null;
        }
    }

    private void flatten(List<TOCReference> refs, List<TocEntry> entries,
                         int level) {

        if (spine == null || refs == null || refs.isEmpty()) {
            return;
        }

        for (TOCReference ref : refs) {

            String title = "";

            for (int i = 0; i < level; i++) {
                title += "  ";
            }

            title += ref.getTitle();

            if (ref.getResource() != null) {
                entries.add(new TocEntry(title, spine.resolveTocHref(ref
                        .getCompleteHref())));
            }

            flatten(ref.getChildren(), entries, level + 1);
        }
    }


    @Override
    public void fling(int velocityY) {
        strategy.clearStoredPosition();
        super.fling(velocityY);
    }

    public int getIndex() {
        if (this.spine == null) {
            return storedIndex;
        }

        return this.spine.getPosition();
    }

    public int getStartOfCurrentPage() {
        return strategy.getTopLeftPosition();
    }

    public int getProgressPosition() {
        return strategy.getProgressPosition();
    }

    public void setPosition(int pos) {
        this.strategy.setPosition(pos);
    }

    public void update() {
        strategy.updateGUI();
    }

    public String getFirstLine() {
        int topLeft = strategy.getTopLeftPosition();

        String plainText = "";

        if ( getStrategy().getText() != null ) {
            plainText = getStrategy().getText().toString();
        }

        if ( plainText.length() == 0 ) {
            return plainText;
        }

        plainText = plainText.substring( topLeft, plainText.length() ).trim();

        int firstNewLine = plainText.indexOf( '\n' );

        if ( firstNewLine == -1 ) {
            return plainText;
        }

        return plainText.substring(0, firstNewLine);
    }

    /**
     * Scrolls to a previously stored point.
     *
     * Call this after setPosition() to actually go there.
     */
    private void restorePosition() {

        if (this.storedAnchor != null  ) {
            String href = spine.getCurrentHref();
            Integer anchorValue = this.textLoader.getAnchor(
                    href, storedAnchor );

            if (anchorValue != null) {
                strategy.setPosition(anchorValue);
                this.storedAnchor = null;
            }
        }
        this.strategy.updatePosition();
    }

    private void setImageSpan(SpannableStringBuilder builder,
                              Drawable drawable, int start, int end) {
        builder.setSpan(new ImageSpan(drawable), start, end,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        if (spine != null && spine.isCover()) {
            builder.setSpan(new CenterSpan(), start, end,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
    }

    private class ImageCallback implements ResourceLoader.ResourceCallback {

        private SpannableStringBuilder builder;
        private int start;
        private int end;

        private String storedHref;

        private boolean fakeImages;

        public ImageCallback(String href, SpannableStringBuilder builder,
                             int start, int end, boolean fakeImages) {
            this.builder = builder;
            this.start = start;
            this.end = end;
            this.storedHref = href;
            this.fakeImages = fakeImages;
        }

        @Override
        public void onLoadResource(String href, InputStream input) {

            if ( fakeImages ) {
                setFakeImage(input);
            } else {
                setBitmapDrawable(input);
            }

        }

        private void setFakeImage(InputStream input) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(input, null, options);

            Pair<Integer, Integer> sizes = calculateSize(options.outWidth, options.outHeight );

            ShapeDrawable draw = new ShapeDrawable( new RectShape() );
            draw.setBounds(0, 0, sizes.first, sizes.second);

            setImageSpan(builder, draw, start, end);
        }

        private void setBitmapDrawable(InputStream input) {
            Bitmap bitmap = null;
            try {
                bitmap = getBitmap(input);

                if (bitmap == null || bitmap.getHeight() < 1
                        || bitmap.getWidth() < 1) {
                    return;
                }

            } catch (OutOfMemoryError outofmem) {
            }

            if (bitmap != null) {

                FastBitmapDrawable drawable = new FastBitmapDrawable(bitmap);

                drawable.setBounds(0, 0, bitmap.getWidth() - 1,
                        bitmap.getHeight() - 1);
                setImageSpan(builder, drawable, start, end);


                textLoader.storeImageInChache(storedHref, drawable);
            }
        }


        private Bitmap getBitmap(InputStream input) {
            Bitmap originalBitmap = BitmapFactory.decodeStream(input);

            if (originalBitmap != null) {
                int originalWidth = originalBitmap.getWidth();
                int originalHeight = originalBitmap.getHeight();

                Pair<Integer, Integer> targetSizes = calculateSize(originalWidth, originalHeight);
                int targetWidth = targetSizes.first;
                int targetHeight = targetSizes.second;

                if ( targetHeight != originalHeight || targetWidth != originalWidth ) {
                    return Bitmap.createScaledBitmap(originalBitmap,
                            targetWidth, targetHeight, true);
                }
            }

            return originalBitmap;
        }
    }

    private Pair<Integer,Integer> calculateSize(int originalWidth, int originalHeight ) {

        int screenHeight = getHeight() - (verticalMargin * 2);
        int screenWidth = getWidth() - (horizontalMargin * 2);

        // We scale to screen width for the cover or if the image is too
        // wide.
        if (originalWidth > screenWidth
                || originalHeight > screenHeight || spine.isCover()) {

            float ratio = (float) originalWidth
                    / (float) originalHeight;

            int targetHeight = screenHeight - 1;
            int targetWidth = (int) (targetHeight * ratio);

            if (targetWidth > screenWidth - 1) {
                targetWidth = screenWidth - 1;
                targetHeight = (int) (targetWidth * (1 / ratio));
            }

            if (targetWidth > 0 || targetHeight > 0) {
                return new Pair<>(targetWidth, targetHeight);
            }
        }

        return new Pair<>(originalWidth, originalHeight);
    }

    private class ImageTagHandler extends TagNodeHandler {

        private boolean fakeImages;

        public ImageTagHandler(boolean fakeImages) {
            this.fakeImages = fakeImages;
        }

        @TargetApi(Build.VERSION_CODES.FROYO)
        @Override
        public void handleTagNode(TagNode node, SpannableStringBuilder builder,
                                  int start, int end, SpanStack span) {

            String src = node.getAttributeByName("src");

            if (src == null) {
                src = node.getAttributeByName("href");
            }

            if (src == null) {
                src = node.getAttributeByName("xlink:href");
            }

            if ( src == null ) {
                return;
            }

            builder.append("\uFFFC");

            if (src.startsWith("data:image")) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {

                    try {
                        String dataString = src.substring(src
                                .indexOf(',') + 1);

                        byte[] binData = Base64.decode(dataString,
                                Base64.DEFAULT);

                        setImageSpan(builder, new BitmapDrawable(
                                        getContext().getResources(),
                                        BitmapFactory.decodeByteArray(binData, 0, binData.length )),
                                start, builder.length());
                    } catch ( OutOfMemoryError | IllegalArgumentException ia ) {
                        //Out of memory or invalid Base64, ignore
                    }

                }

            } else if ( spine != null ) {

                String resolvedHref = spine.resolveHref(src);

                if ( textLoader.hasCachedImage(resolvedHref) && ! fakeImages ) {
                    Drawable drawable = textLoader.getCachedImage(resolvedHref);
                    setImageSpan(builder, drawable, start, builder.length());
                } else {
                    this.registerCallback(resolvedHref, new ImageCallback(
                            resolvedHref, builder, start, builder.length(), fakeImages));
                }
            }
        }

        protected void registerCallback(String resolvedHref, ImageCallback callback ) {
            BookView.this.loader.registerCallback(resolvedHref, callback);
        }

    }

    @Override
    public void setBackgroundColor(int color) {
        super.setBackgroundColor(color);

        if (this.childView != null) {
            this.childView.setBackgroundColor(color);
        }
    }

    /*public void highlightClicked( HighLight highLight ) {
        for ( BookViewListener listener: listeners ) {
            listener.onHighLightClick(highLight);
        }
    }*/

    public void setTextColor(int color) {
        if (this.childView != null) {
            this.childView.setTextColor(color);
        }

        this.tableHandler.setTextColor(color);
    }

    public Book getBook() {
        return book;
    }

    public void setTextSize(float textSize) {
        this.childView.setTextSize(textSize);
        this.tableHandler.setTextSize(textSize);
    }

    public void addListener(BookViewListener listener) {
        this.listeners.add(listener);
    }

    private void bookOpened(Book book) {
        for (BookViewListener listener : this.listeners) {
            listener.bookOpened(book);
        }
    }

    private void errorOnBookOpening(String errorMessage) {
        for (BookViewListener listener : this.listeners) {
            listener.errorOnBookOpening(errorMessage);
        }
    }

    private void parseEntryStart(int entry) {
        for (BookViewListener listener : this.listeners) {
            listener.parseEntryStart(entry);
        }
    }

    private void parseEntryComplete( String name) {
        for (BookViewListener listener : this.listeners) {
            listener.parseEntryComplete(name);
        }
    }

    private void fireOpenFile() {
        for (BookViewListener listener : this.listeners) {
            listener.readingFile();
        }
    }

    private void fireRenderingText() {
        for (BookViewListener listener : this.listeners) {
            listener.renderingText();
        }
    }

    public int getPercentageFor( int index, int offset ) {
        if ( spine != null ) {
            return spine.getProgressPercentage(index, offset);
        }

        return -1;
    }

    private void progressUpdate() {

        if ( this.spine == null ) {
            return;
        }

        this.strategy.getText(); /*.filter( t -> t.length() > 0 ).forEach( text -> {

            double progressInPart = (double) this.getProgressPosition()
                    / (double) text.length();

            if (text.length() > 0 && strategy.isAtEnd()) {
                progressInPart = 1d;
            }

            int progress = spine.getProgressPercentage(progressInPart);

            if (progress != -1) {

                int pageNumber = getPageNumberFor(getIndex(),
                        getProgressPosition());

                for (BookViewListener listener : this.listeners) {
                    listener.progressUpdate(progress, pageNumber,
                            spine.getTotalNumberOfPages());
                }
            }
        });*/
    }


    public int getTotalNumberOfPages() {
        if ( spine != null ) {
            return spine.getTotalNumberOfPages();
        }

        return -1;
    }

    public int getPageNumberFor( int index, int position ) {

        if ( spine == null ) {
            return -1;
        }

        int pageNum = -1;

        List<List<Integer>> pageOffsets = spine.getPageOffsets();

        if ( pageOffsets == null || index >= pageOffsets.size() ) {
            return -1;
        }

        for ( int i=0; i < index; i++ ) {

            int pages = pageOffsets.get(i).size();

            pageNum += pages;

        }

        List<Integer> offsets = pageOffsets.get(index);

        if ( this.strategy instanceof FixedPagesStrategy) {
            List<Integer> strategyOffsets = ( (FixedPagesStrategy) this.strategy ).getPageOffsets();
        }

        for ( int i=0; i < offsets.size() && offsets.get(i) <= position; i++ ) {
            pageNum++;
        }

        return pageNum;
    }

    private static String asString( List<Integer> offsets ) {

        StringBuilder stringBuilder = new StringBuilder("[ ");
        for(Integer o : offsets)
            stringBuilder.append(o).append(" ");
        stringBuilder.append(" ]");

        return stringBuilder.toString();
    }

    public static class InnerView extends TextView {

        private BookView bookView;

        private long blockUntil = 0l;

        public InnerView(Context context) {
            super(context);
        }

        protected void onSizeChanged(int w, int h, int oldw, int oldh) {
            super.onSizeChanged(w, h, oldw, oldh);
            bookView.onInnerViewResize();
        }

        @Override
        public void onWindowFocusChanged(boolean hasWindowFocus) {
            /*
            We override this method to do nothing, since the base
            implementation closes the ActionMode.
            This means that when the user clicks the overflow menu,
            the ActionMode is stopped and text selection is ended.
             */
        }

        public void setBookView(BookView bookView) {
            this.bookView = bookView;
        }

        public void setBlockUntil( long blockUntil ) {
            this.blockUntil = blockUntil;
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);
        }

        @TargetApi( Build.VERSION_CODES.HONEYCOMB )
        @Override
        public ActionMode startActionMode(ActionMode.Callback callback) {

            if ( System.currentTimeMillis() > blockUntil ) {

                return super.startActionMode(callback);

            } else {
                clearFocus();
                return null;
            }
        }
    }

    private static class SearchResultSpan extends BackgroundColorSpan {
        public SearchResultSpan() {
            super( Color.YELLOW );
        }
    }
    private void performOpen()
    {
        fireOpenFile();
        OpenFileBackgroundTask task = new OpenFileBackgroundTask();
        task.onComplete(new OnTaskCompleteListener() {
            @Override
            public void taskComplete(final BackgroundTask task) {
                try {
                    post(new Runnable() {
                        @Override
                        public void run() {
                            bookOpened(((OpenFileBackgroundTask) task).getResult());
                            performLoadText(null);
                        }
                    });
                }
                catch(Exception ignored) {}
            }
        });
        task.onFailure(new OnTaskFailureListener() {
            @Override
            public void taskFailed(BackgroundTask task) {
                try {
                    post(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getContext(), R.string.error_opening_book, Toast.LENGTH_SHORT).show();
                        }
                    });
                } catch (Exception ignored) {
                }
            }
        });
        task.addToQueue();
    }
    private void performLoadText(Resource res)
    {
        LoadTextBackgroundTask loadTxtTask = new LoadTextBackgroundTask(res == null ? spine.getCurrentResource() : res);
        loadTxtTask.onComplete(new OnTaskCompleteListener() {
            @Override
            public void taskComplete(BackgroundTask task) {
                scrollHandler.post(restoreRunnable);
            }
        });
        loadTxtTask.addToQueue();
    }
    private void performPreload()
    {
        PreLoadTask task = new PreLoadTask(spine, textLoader);
        task.onComplete(new OnTaskCompleteListener() {
            @Override
            public void taskComplete(BackgroundTask task) {
                try {
                    post(calculatePagesRunnable);
                }
                catch(Exception ignored) {}
            }
        });
        task.addToQueue();
    }
    private void performCalculatePageNumbers()
    {
        for ( BookViewListener listener: listeners ) {
            listener.onStartCalculatePageNumbers();
        }
        CalculatePageNumbersBackgroundTask t = new CalculatePageNumbersBackgroundTask();
        t.onComplete(new OnTaskCompleteListener() {
            @Override
            public void taskComplete(BackgroundTask task) {
                for (BookViewListener listener : listeners) {
                    listener.onCalculatePageNumbersComplete();
                }
                List<List<Integer>> result = ((CalculatePageNumbersBackgroundTask) task).getResult();
                spine.setPageOffsets(result);
                progressUpdate();
            }
        });
        t.addToQueue();
    }

    private class OpenFileBackgroundTask extends BackgroundTask
    {
        private Book bk;
        @Override
        public String getTaskKey() {
            return "open_book";
        }

        @Override
        public int getTaskFailedNotificationId() {
            return NotificationCenter.didBookOpenFailed;
        }

        @Override
        public int getTaskCompletedNotificationId() {
            return NotificationCenter.didBookOpened;
        }

        @Override
        protected void work() throws Exception {
            bk = initBookAndSpine();
            if(bk == null)
                throw new NullPointerException("book = null");
        }
        public Book getResult() { return this.bk; }
    }
    private class LoadTextBackgroundTask extends BackgroundTask
    {
        private Resource resource;
        private Spannable txt;
        public LoadTextBackgroundTask(Resource res) { this.resource = res; }
        @Override
        public String getTaskKey() {
            return "load_text";
        }

        @Override
        public int getTaskFailedNotificationId() {
            return NotificationCenter.didTextLoadingFailed;
        }

        @Override
        public int getTaskCompletedNotificationId() {
            return NotificationCenter.didTextLoaded;
        }

        @Override
        protected void work() throws Exception {
            if (loader != null) {
                loader.clear();
            }

            try {
                Spannable result = textLoader.getText(resource, null);
                if(BuildConfig.DEBUG)
                    Log.d("BookView", result.toString());
                loader.load(); // Load all image resources.

                //Clear any old highlighting spans

                SearchResultSpan[] spans = result.getSpans(0, result.length(), SearchResultSpan.class);
                for ( BackgroundColorSpan span: spans ) {
                    result.removeSpan(span);
                }

                // Highlight search results (if any)
                //If the view isn't ready yet, wait a bit.
                while ( getInnerView().getWidth() == 0 ) {
                    Thread.sleep(100);
                }

                strategy.loadText(result);

                this.txt = result;
            } catch (Exception | OutOfMemoryError ignored ) {
                this.txt = null;
            }
        }
        public Spannable getText() { return this.txt; }
    }
    private class CalculatePageNumbersBackgroundTask extends BackgroundTask
    {
        private List<List<Integer>> result;
        @Override
        public String getTaskKey() {
            return "calculate_page_numbers";
        }

        @Override
        public int getTaskFailedNotificationId() {
            return NotificationCenter.didPageNumbersFailed;
        }

        @Override
        public int getTaskCompletedNotificationId() {
            return NotificationCenter.didPageNumbersCalculated;
        }

        @Override
        protected void work() throws Exception {
            try {

                result = getOffsets();
            } catch ( OutOfMemoryError | Exception e ) {
            }
        }
        public List<List<Integer>> getResult() { return this.result; }
        private List<List<Integer>> getOffsets()
                throws IOException {

            List<List<Integer>> result = new ArrayList<>();
            final ResourceLoader imageLoader = new ResourceLoader(fileName);
            final ResourceLoader textResourceLoader = new ResourceLoader(fileName);


            //Image loader which only loads image dimensions
            ImageTagHandler tagHandler = new ImageTagHandler(true) {
                protected void registerCallback(String resolvedHref, ImageCallback callback) {
                    imageLoader.registerCallback(resolvedHref, callback);
                }
            };

            //Private spanner
            final HtmlSpanner mySpanner = new HtmlSpanner();

            mySpanner.setAllowStyling( true );
            mySpanner.setFontResolver( fontResolver );

            mySpanner.registerHandler("table", tableHandler );
            mySpanner.registerHandler("img", tagHandler);
            mySpanner.registerHandler("image", tagHandler);
            mySpanner.registerHandler("link", new CSSLinkHandler(textLoader));

            final Map<String, List<Integer>> offsetsPerSection = new HashMap<>();

            //We use the ResourceLoader here to load all the text in the book in 1 pass,
            //but we only keep a single section in memory at each moment
            ResourceLoader.ResourceCallback callback = new ResourceLoader.ResourceCallback() {
                @Override
                public void onLoadResource(String href, InputStream stream) {
                    try {
                        InputStream input = new ByteArrayInputStream(IOUtil.toByteArray(stream));
                        Spannable text = mySpanner.fromHtml(input, null);
                        imageLoader.load();

                        FixedPagesStrategy fixedPagesStrategy = getFixedPagesStrategy();
                        fixedPagesStrategy.setBookView(BookView.this);
                        offsetsPerSection.put(href, fixedPagesStrategy.getPageOffsets(text));
                    } catch ( IOException io ) {
                    }
                }
            };

            //Do first pass: grab either cached text, or schedule a callback
            for ( PageTurnerSpine.SpineEntry spineEntry: spine ) {
                Resource res = spineEntry.getResource();

                Spannable cachedText = textLoader.getCachedTextForResource( res );

                if (cachedText != null) {

                    FixedPagesStrategy fixedPagesStrategy = getFixedPagesStrategy();
                    fixedPagesStrategy.setBookView(BookView.this);

                    offsetsPerSection.put(res.getHref(), fixedPagesStrategy.getPageOffsets(cachedText));
                } else {
                    textResourceLoader.registerCallback( res.getHref(), callback );
                }
            }

            //Load callbacks, this will fill renderedText
            textResourceLoader.load();
            imageLoader.load();

            //Do a second pass and order the offsets correctly
            for (PageTurnerSpine.SpineEntry spineEntry: spine ) {

                Resource res = spineEntry.getResource();

                List<Integer> offsets = null;

                //Scan for the full href
                for ( String href: offsetsPerSection.keySet() ) {
                    if ( href.endsWith( res.getHref() )) {
                        offsets = offsetsPerSection.get(href);
                        break;
                    }
                }

                if (offsets == null) {
                    return null;
                }
                else
                    result.add(offsets);
            }
            return result;
        }

        private FixedPagesStrategy getFixedPagesStrategy() {
            FixedPagesStrategy fixedPagesStrategy =  new FixedPagesStrategy();
            fixedPagesStrategy.setLayoutFactory(new StaticLayoutFactory());
            return fixedPagesStrategy;
        }
    }

}
