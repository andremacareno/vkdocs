package com.andremacareno.vkdocs.cache;

import android.os.Message;
import android.support.v4.util.LongSparseArray;

import com.andremacareno.asynccore.BackgroundTask;
import com.andremacareno.asynccore.OnTaskCompleteListener;
import com.andremacareno.screencore.PauseHandler;
import com.andremacareno.vkdocs.dao.Document;
import com.andremacareno.vkdocs.tasks.GetFolderInfoTask;

/**
 * Created by andremacareno on 15/03/16.
 */
public class FolderInfoCache {
    public interface FolderInfoDelegate
    {
        public void onFolderInfoReceived(long folder, int fileCount, String totalStringSize);
    }
    private static volatile FolderInfoCache _instance;
    private final LongSparseArray<Integer> fileCount = new LongSparseArray<>();         //key: folder id
    private final LongSparseArray<Long> folderTotalSize = new LongSparseArray<>();
    private final LongSparseArray<String> folderStringSize = new LongSparseArray<>();
    private final LongSparseArray<FolderInfoDelegate> pendingDelegates = new LongSparseArray<>();
    private final LongSparseArray<Long> docToFolder = new LongSparseArray<>();
    public PauseHandler queryFinishReceiver = new QueryFinishReceiver();
    public String totalOfflineSize = null;
    private FolderInfoCache() {
    }

    public static FolderInfoCache sharedInstance() {
        if(_instance == null)
        {
            synchronized(FolderInfoCache.class)
            {
                if(_instance == null)
                {
                    _instance = new FolderInfoCache();
                }
            }
        }
        return _instance;
    }

    public void getFolderInfo(final long folderId, final FolderInfoDelegate delegate)
    {
        synchronized(pendingDelegates)
        {
            pendingDelegates.put(folderId, delegate);
        }
        int count;
        long totalSize;
        String totalStrSize;
        boolean cached = false;
        synchronized(fileCount)
        {
            Integer obj = fileCount.get(folderId);
            count = obj == null ? -1 : obj;
            synchronized(folderStringSize)
            {
                synchronized(folderTotalSize)
                {
                    Long sizeObj = folderTotalSize.get(folderId);
                    totalSize = sizeObj == null ? -1 : sizeObj;
                    totalStrSize = totalSize == -1 ? null : Document.sizeToString(totalSize);
                    if(totalStrSize != null) {
                        folderStringSize.put(folderId, totalStrSize);
                        cached = true;
                    }
                }
            }
        }
        if(cached)
        {
            delegate.onFolderInfoReceived(folderId, count, totalStrSize);
        }
        else
        {
            GetFolderInfoTask t = new GetFolderInfoTask(folderId);
            t.onComplete(new OnTaskCompleteListener() {
                @Override
                public void taskComplete(BackgroundTask task) {
                    GetFolderInfoTask cast = (GetFolderInfoTask) task;
                    synchronized(fileCount)
                    {
                        fileCount.put(folderId, cast.getFilesCount());
                        synchronized(folderStringSize)
                        {
                            synchronized(folderTotalSize)
                            {
                                folderTotalSize.put(folderId, cast.getFolderSize());
                            }
                            folderStringSize.put(folderId, cast.getStringSize());
                        }
                    }
                    Message msg = Message.obtain(queryFinishReceiver);
                    msg.obj = folderId;
                    msg.sendToTarget();
                }
            });
            t.addToQueue();
        }
    }
    public void removeDelegate(long folderId)
    {
        synchronized(pendingDelegates)
        {
            pendingDelegates.remove(folderId);
        }
    }
    public void didDocAddedToFolder(long folder, Document doc)
    {
        if(doc == null)
            return;
        synchronized(docToFolder)
        {
            Long savedFolder = docToFolder.get(doc.getId());
            if(savedFolder != null && savedFolder == folder)
                return;
            docToFolder.put(doc.getId(), folder);
        }
        Integer count;
        String strSize = null;
        synchronized(fileCount)
        {
            count = fileCount.get(folder);
            if(count != null) {
                fileCount.put(folder, count + 1);
            }
            if(count != null)
            {
                try
                {
                    synchronized(folderStringSize)
                    {
                        synchronized(folderTotalSize)
                        {
                            Long totalSize = folderTotalSize.get(folder);
                            totalSize += doc.getSize();
                            folderTotalSize.put(folder, totalSize);
                            strSize = Document.sizeToString(totalSize);
                            folderStringSize.put(folder, strSize);
                        }
                    }
                }
                catch(Exception ignored) {}
            }
        }
        synchronized(pendingDelegates)
        {
            FolderInfoDelegate delegate = pendingDelegates.get(folder);
            if(delegate != null && count != null && strSize != null)
                delegate.onFolderInfoReceived(folder, count+1, strSize);
        }
    }
    public void didDocRemovedFromFolder(long folder, Document doc)
    {
        if(doc == null)
            return;
        synchronized(docToFolder)
        {
            docToFolder.remove(doc.getId());
        }
        Integer count;
        String strSize = null;
        synchronized(fileCount)
        {
            count = fileCount.get(folder);
            if(count != null) {
                fileCount.put(folder, count >= 1 ? count - 1 : 0);
            }
            if(count != null)
            {
                try
                {
                    synchronized(folderStringSize)
                    {
                        synchronized(folderTotalSize)
                        {
                            Long totalSize = folderTotalSize.get(folder);
                            totalSize -= doc.getSize();
                            folderTotalSize.put(folder, totalSize);
                            strSize = Document.sizeToString(totalSize);
                            folderStringSize.put(folder, strSize);
                        }
                    }
                }
                catch(Exception ignored) {}
            }
        }
        synchronized(pendingDelegates)
        {
            FolderInfoDelegate delegate = pendingDelegates.get(folder);
            if(delegate != null && count != null && strSize != null)
                delegate.onFolderInfoReceived(folder, count >= 1 ? count - 1 : 0, strSize);
        }
    }

    private static class QueryFinishReceiver extends PauseHandler
    {
        @Override
        protected boolean storeMessage(Message message) {
            return true;
        }

        @Override
        protected void processMessage(Message message) {
            try
            {
                long folderId = (Long) message.obj;
                int fileCount;
                long totalSize;
                String totalStrSize;
                synchronized(FolderInfoCache.sharedInstance().fileCount)
                {
                    fileCount = FolderInfoCache.sharedInstance().fileCount.get(folderId);
                    synchronized(FolderInfoCache.sharedInstance().folderStringSize)
                    {
                        totalStrSize = FolderInfoCache.sharedInstance().folderStringSize.get(folderId);
                        if(totalStrSize == null)
                        {
                            synchronized(FolderInfoCache.sharedInstance().folderTotalSize)
                            {
                                totalSize = FolderInfoCache.sharedInstance().folderTotalSize.get(folderId);
                                totalStrSize = Document.sizeToString(totalSize);
                                FolderInfoCache.sharedInstance().folderStringSize.put(folderId, totalStrSize);
                            }
                        }
                    }
                }
                synchronized(FolderInfoCache.sharedInstance().pendingDelegates)
                {
                    FolderInfoDelegate delegate = FolderInfoCache.sharedInstance().pendingDelegates.get(folderId);
                    if(delegate != null)
                    {
                        delegate.onFolderInfoReceived(folderId, fileCount, totalStrSize);
                    }
                }
            }
            catch(Exception e) { e.printStackTrace(); }
        }
    }
    public void clear() {
        synchronized(fileCount)
        {
            synchronized(folderStringSize)
            {
                synchronized(folderTotalSize)
                {
                    folderTotalSize.clear();
                }
                folderStringSize.clear();
            }
            fileCount.clear();
        }
    }
    public boolean checkMovingToSelf(Long document, Long folder)
    {
        try
        {
            synchronized(docToFolder)
            {
                return docToFolder.get(document) == folder;
            }
        }
        catch(Exception ignored) { return false; }

    }
    public void addDocFolderLink(Long document, Long folder)
    {
        if(document == null || folder == null)
            return;
        synchronized(docToFolder)
        {
            docToFolder.put(document, folder);
        }
    }
}
