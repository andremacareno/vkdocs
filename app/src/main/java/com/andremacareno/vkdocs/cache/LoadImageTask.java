package com.andremacareno.vkdocs.cache;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.util.Log;

import com.andremacareno.asynccore.BackgroundTask;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.vkdocs.AppLoader;
import com.andremacareno.vkdocs.BuildConfig;
import com.andremacareno.vkdocs.imageprocessors.BlurImage;
import com.andremacareno.vkdocs.imageprocessors.CircleImage;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class LoadImageTask extends BackgroundTask {
    private static final String TAG = "LoadImageTask";
    private String key;
    private String url;
    private int postProcessingMask;
    private int scaleWidth, scaleHeight;

    public LoadImageTask(String url, int postProcessing, int scaleWidth, int scaleHeight)
    {
        this.key = ImageCache.hashKeyForDisk(url);
        this.url = url;
        this.postProcessingMask = postProcessing;
        this.scaleWidth = scaleWidth;
        this.scaleHeight = scaleHeight;
    }
    @Override
    public String getTaskKey() {
        return this.key;
    }

    @Override
    public int getTaskFailedNotificationId() {
        return NotificationCenter.didImageProcessingFailed;
    }

    @Override
    public int getTaskCompletedNotificationId() {
        return NotificationCenter.didImageProcessed;
    }

    @Override
    protected void work() throws Exception {

        //check cache
        if(BuildConfig.DEBUG)
            Log.d(TAG, "work started");
        Bitmap bmp = ImageCache.getInstance().getBitmapFromDiskCache(url);
        if(bmp == null)
        {
            FileDescriptor fileDescriptor = null;
            FileInputStream fileInputStream = null;
            DiskLruCache.Snapshot snapshot;
            synchronized (ImageCache.getInstance().mDiskCacheLock) {
                // Wait for disk cache to initialize
                while (ImageCache.getInstance().mDiskCacheStarting) {
                    try {
                        ImageCache.getInstance().mDiskCacheLock.wait();
                    } catch (InterruptedException e) {}
                }

                if (ImageCache.getInstance().mDiskLruCache != null) {
                    try {
                        snapshot = ImageCache.getInstance().mDiskLruCache.get(key);
                        if (snapshot == null) {
                            if (BuildConfig.DEBUG) {
                                Log.d(TAG, "processBitmap, not found in http cache, downloading...");
                            }
                            DiskLruCache.Editor editor = ImageCache.getInstance().mDiskLruCache.edit(key);
                            if (editor != null) {
                                if (loadFromURL(editor.newOutputStream(ImageLoader.DISK_CACHE_INDEX))) {
                                    editor.commit();
                                } else {
                                    editor.abort();
                                }
                            }
                            snapshot = ImageCache.getInstance().mDiskLruCache.get(key);
                        }
                        if (snapshot != null) {
                            fileInputStream =
                                    (FileInputStream) snapshot.getInputStream(ImageLoader.DISK_CACHE_INDEX);
                            fileDescriptor = fileInputStream.getFD();
                        }
                    } catch (IOException e) {
                        Log.e(TAG, "processBitmap - " + e);
                    } catch (IllegalStateException e) {
                        Log.e(TAG, "processBitmap - " + e);
                    } finally {
                        if (fileDescriptor == null && fileInputStream != null) {
                            try {
                                fileInputStream.close();
                            } catch (IOException e) {}
                        }
                    }
                }
            }

            if (fileDescriptor != null) {
                pauseIfNeed();
                bmp = decodeSampledBitmapFromDescriptor(fileDescriptor, scaleWidth,
                        scaleHeight, ImageCache.getInstance());
            }
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {}
            }
            if(bmp == null)
                throw new NullPointerException("task failed");
            if(BuildConfig.DEBUG)
                Log.d(TAG, "loaded bitmap");
        }
        if((postProcessingMask & ImageLoader.POST_PROCESSING_CIRCLE) != 0)
        {
            CircleImage circleImage = new CircleImage();
            pauseIfNeed();
            Bitmap newBitmap = circleImage.process(bmp);
            bmp.recycle();
            System.gc();
            bmp = newBitmap;
        }
        if((postProcessingMask & ImageLoader.POST_PROCESSING_BLUR) != 0)
        {
            BlurImage blurImage = new BlurImage();
            pauseIfNeed();
            Bitmap newBitmap = blurImage.process(bmp);
            bmp.recycle();
            System.gc();
            bmp = newBitmap;
        }
        ImageCache.getInstance().addBitmapToCache(url, new RecyclingBitmapDrawable(AppLoader.sharedInstance().getResources(), bmp));
    }
    public static Bitmap decodeSampledBitmapFromDescriptor(
            FileDescriptor fileDescriptor, int reqWidth, int reqHeight, ImageCache cache) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        // If we're running on Honeycomb or newer, try to use inBitmap
        if (Utils.hasHoneycomb()) {
            addInBitmapOptions(options, cache);
        }

        return BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options);
    }
    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight) {
        // BEGIN_INCLUDE (calculate_sample_size)
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }

            // This offers some additional logic in case the image has a strange
            // aspect ratio. For example, a panorama may have a much larger
            // width than height. In these cases the total pixels might still
            // end up being too large to fit comfortably in memory, so we should
            // be more aggressive with sample down the image (=larger inSampleSize).

            long totalPixels = width * height / inSampleSize;

            // Anything more than 2x the requested pixels we'll sample down further
            final long totalReqPixelsCap = reqWidth * reqHeight * 2;

            while (totalPixels > totalReqPixelsCap) {
                inSampleSize *= 2;
                totalPixels /= 2;
            }
        }
        return inSampleSize;
        // END_INCLUDE (calculate_sample_size)
    }
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static void addInBitmapOptions(BitmapFactory.Options options, ImageCache cache) {
        //BEGIN_INCLUDE(add_bitmap_options)
        // inBitmap only works with mutable bitmaps so force the decoder to
        // return mutable bitmaps.
        options.inMutable = true;

        if (cache != null) {
            // Try and find a bitmap to use for inBitmap
            Bitmap inBitmap = cache.getBitmapFromReusableSet(options);

            if (inBitmap != null) {
                options.inBitmap = inBitmap;
            }
        }
        //END_INCLUDE(add_bitmap_options)
    }
    private boolean loadFromURL(OutputStream outputStream)
    {
        if(BuildConfig.DEBUG)
            Log.d(TAG, "loading from url");
        disableConnectionReuseIfNecessary();
        HttpURLConnection urlConnection = null;
        BufferedOutputStream out = null;
        BufferedInputStream in = null;

        try {
            final URL urlObj = new URL(url);
            urlConnection = (HttpURLConnection) urlObj.openConnection();
            in = new BufferedInputStream(urlConnection.getInputStream(), ImageLoader.IO_BUFFER_SIZE);
            out = new BufferedOutputStream(outputStream, ImageLoader.IO_BUFFER_SIZE);

            int b;
            while ((b = in.read()) != -1) {
                pauseIfNeed();
                out.write(b);
            }
            return true;
        } catch (final IOException e) {
            Log.e(TAG, "Error in downloadBitmap - " + e);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            try {
                if (out != null) {
                    out.close();
                }
                if (in != null) {
                    in.close();
                }
            } catch (final IOException e) {}
        }
        return false;
    }
    public static void disableConnectionReuseIfNecessary() {
        // HTTP connection reuse which was buggy pre-froyo
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.FROYO) {
            System.setProperty("http.keepAlive", "false");
        }
    }
}