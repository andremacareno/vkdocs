/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andremacareno.vkdocs.cache;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.ImageView;

import com.andremacareno.asynccore.BackgroundTask;
import com.andremacareno.asynccore.OnTaskFailureListener;
import com.andremacareno.asynccore.notificationcenter.Notification;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.asynccore.notificationcenter.NotificationObserver;
import com.andremacareno.vkdocs.AppLoader;
import com.andremacareno.vkdocs.BuildConfig;
import com.andremacareno.vkdocs.R;
import com.andremacareno.vkdocs.objs.GalleryImageEntry;
import com.andremacareno.vkdocs.tasks.LoadBookCoverTask;
import com.andremacareno.vkdocs.tasks.LoadThumbnailTask;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;

import nl.siegmann.epublib.domain.Book;

/**
 * This class wraps up completing some arbitrary long running work when loading a bitmap to an
 * ImageView. It handles things like using a memory and disk cache, running the work in a background
 * thread and setting a placeholder image.
 */

public class ImageLoader {
    private static final String TAG = "ImageLoader";
    public static final int POST_PROCESSING_NOTHING = 0;
    public static final int POST_PROCESSING_CIRCLE = 1;
    public static final int POST_PROCESSING_SCALE = 2;
    public static final int POST_PROCESSING_BLUR = 4;

    private static final int MESSAGE_CLEAR = 0;
    private static final int MESSAGE_INIT_DISK_CACHE = 1;
    private static final int MESSAGE_FLUSH = 2;
    private static final int MESSAGE_CLOSE = 3;
    public static final int HTTP_CACHE_SIZE = 10 * 1024 * 1024; // 10MB
    public static final String IMG_CACHE_DIR = "img_cache";
    public static final int IO_BUFFER_SIZE = 8 * 1024;

    private DiskLruCache mHttpDiskCache;
    private File mHttpCacheDir;
    private boolean mHttpDiskCacheStarting = true;
    private final Object mHttpDiskCacheLock = new Object();
    public static final int DISK_CACHE_INDEX = 0;

    private static volatile ImageLoader _instance;

    private ImageLoader()
    {
        mHttpCacheDir = ImageCache.getDiskCacheDir(AppLoader.sharedInstance().getApplicationContext(), IMG_CACHE_DIR);
        initCache();
    }

    public static ImageLoader getInstance()
    {
        if(_instance == null)
        {
            synchronized(ImageLoader.class)
            {
                if(_instance == null)
                    _instance = new ImageLoader();
            }
        }
        return _instance;
    }
    public void loadImage(String url, ImageView imageView, int postProcessingMask, int scaleWidth, int scaleHeight, boolean forceUpdate)
    {
        try
        {
            if (url == null) {
                return;
            }
            BitmapDrawable value = ImageCache.getInstance().getBitmapFromMemCache(url);

            if (value != null && !forceUpdate) {
                // Bitmap found in memory cache
                imageView.setImageDrawable(value);
            } else if (cancelPotentialWork(url, imageView)) {
                final ImageObserver obs = new ImageObserver(url, imageView);
                final AsyncDrawable asyncDrawable = new AsyncDrawable(AppLoader.sharedInstance().getResources(), null, obs);
                imageView.setImageDrawable(asyncDrawable);
                NotificationCenter.getInstance().addObserver(obs);
                LoadImageTask task = new LoadImageTask(url, postProcessingMask, scaleWidth, scaleHeight);
                task.addToQueue();
            }
        }
        catch(Exception ignored) {}
    }
    public void loadGalleryThumb(GalleryImageEntry entry, ImageView imageView)
    {
        try {
            if(entry == null)
                return;
            BitmapDrawable value = ImageCache.getInstance().getBitmapFromMemCache(entry.getCacheKey());
            if(value != null)
                imageView.setImageDrawable(value);
            else if(cancelPotentialWork(entry.getCacheKey(), imageView))
            {
                final ImageObserver obs = new ImageObserver(entry.getCacheKey(), imageView);
                final AsyncDrawable asyncDrawable = new AsyncDrawable(AppLoader.sharedInstance().getResources(), null, obs);
                imageView.setImageDrawable(asyncDrawable);
                NotificationCenter.getInstance().addObserver(obs);
                LoadThumbnailTask task = new LoadThumbnailTask(entry);
                task.addToQueue();
            }
        }
        catch(Exception ignored) {}
    }
    public void loadBookCover(final Book book, final ImageView imageView)
    {
        try {
            if(book == null)
                return;
            BitmapDrawable value = ImageCache.getInstance().getBitmapFromMemCache(book.getTitle());
            if(value != null)
                imageView.setImageDrawable(value);
            else if(cancelPotentialWork(book.getTitle(), imageView))
            {
                final ImageObserver obs = new ImageObserver(book.getTitle(), imageView);
                final AsyncDrawable asyncDrawable = new AsyncDrawable(AppLoader.sharedInstance().getResources(), null, obs);
                imageView.setImageDrawable(asyncDrawable);
                NotificationCenter.getInstance().addObserver(obs);
                final LoadBookCoverTask task = new LoadBookCoverTask(book);
                task.onFailure(new OnTaskFailureListener() {
                    @Override
                    public void taskFailed(BackgroundTask t) {
                        if(BuildConfig.DEBUG)
                            Log.d(TAG, String.format("fail; keys: %s %s", task.getTaskKey(), t.getTaskKey()));
                        try
                        {
                            imageView.post(new Runnable() {
                                @Override
                                public void run() {
                                    try
                                    {
                                        ImageObserver obs = getImageObserver(imageView);
                                        if(obs != null && obs.getStringKey().equals(book.getTitle()))
                                            imageView.setImageResource(R.drawable.book_no_cover);
                                    }
                                    catch(Exception e) { e.printStackTrace(); }
                                }
                            });
                        }
                        catch(Exception e) { e.printStackTrace(); }
                    }
                });
                task.addToQueue();
            }
        }
        catch(Exception ignored) {}
    }

    /**
     * A custom Drawable that will be attached to the imageView while the work is in progress.
     * Contains a reference to the actual notification observer, so that it can be removed if a new binding is
     * required, and makes sure that only the last observer can bind its result,
     * independently of the finish order.
     */
    private static class AsyncDrawable extends BitmapDrawable {
        private final WeakReference<ImageObserver> observerRef;

        public AsyncDrawable(Resources res, Bitmap bitmap, ImageObserver obs) {
            super(res, bitmap);
            observerRef =
                    new WeakReference<ImageObserver>(obs);
        }

        public ImageObserver getImageObserver() {
            return observerRef.get();
        }
    }

    private static class ImageObserver extends NotificationObserver {
        private final WeakReference<ImageView> imageViewReference;
        private Runnable setImageDrawableRunnable;
        public ImageObserver(String k, ImageView imageView) {
            super(NotificationCenter.didImageCached, k);
            imageViewReference = new WeakReference<ImageView>(imageView);
            setImageDrawableRunnable = new Runnable() {
                @Override
                public void run() {
                    BitmapDrawable dr = ImageCache.getInstance().getBitmapFromMemCache(getStringKey());
                    ImageView attachedIV = getAttachedImageView();
                    if(attachedIV != null) {
                        setImageDrawable(attachedIV, dr);
                    }
                }
            };
        }
        private void setImageDrawable(ImageView imageView, Drawable drawable) {
            try
            {
                if(imageView == null)
                    return;
                if(drawable == null) {
                    imageView.setImageDrawable(null);
                    return;
                }
                final TransitionDrawable td = new TransitionDrawable(new Drawable[] {
                        new ColorDrawable(ContextCompat.getColor(AppLoader.sharedInstance().getApplicationContext(), android.R.color.transparent)),
                        drawable
                });
                imageView.setImageDrawable(td);
                td.startTransition(200);
            }
            catch(Exception e) { e.printStackTrace(); }
        }
        @Override
        public void didNotificationReceived(Notification notification) {
            if(BuildConfig.DEBUG)
                Log.d(TAG, "ImageObserver: didNotificationReceived");
            if(getStringKey().equals(notification.getObject()))
            {
                ImageView attachedIV = getAttachedImageView();
                if(attachedIV != null) {
                    attachedIV.post(setImageDrawableRunnable);
                    NotificationCenter.getInstance().removeObserver(this);
                }
            }
        }
        public String getStringKey() { return (String) getKey(); }
        /**
         * Returns the ImageView associated with this task as long as the ImageView's task still
         * points to this task as well. Returns null otherwise.
         */
        private ImageView getAttachedImageView() {
            final ImageView imageView = imageViewReference.get();
            final ImageObserver bitmapWorkerTask = getImageObserver(imageView);

            if (this == bitmapWorkerTask) {
                return imageView;
            }

            return null;
        }
    }
    private static ImageObserver getImageObserver(ImageView imageView) {
        if (imageView != null) {
            final Drawable drawable = imageView.getDrawable();
            if (drawable instanceof AsyncDrawable) {
                final AsyncDrawable asyncDrawable = (AsyncDrawable) drawable;
                return asyncDrawable.getImageObserver();
            }
        }
        return null;
    }
    public static boolean cancelPotentialWork(String key, ImageView imageView) {
        final ImageObserver observer = getImageObserver(imageView);

        if (observer != null) {
            final String bmpKey = observer.getStringKey();
            if (bmpKey == null || !bmpKey.equals(key)) {
                NotificationCenter.getInstance().removeObserver(observer);
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "cancelPotentialWork - removed observer for " + key);
                }
            } else {
                // The same work is already in progress.
                return false;
            }
        }
        return true;
    }
    protected class CacheAsyncTask extends AsyncTask<Object, Void, Void> {

        @Override
        protected Void doInBackground(Object... params) {
            switch ((Integer)params[0]) {
                case MESSAGE_CLEAR:
                    clearCacheInternal();
                    break;
                case MESSAGE_INIT_DISK_CACHE:
                    initDiskCacheInternal();
                    break;
                case MESSAGE_FLUSH:
                    flushCacheInternal();
                    break;
                case MESSAGE_CLOSE:
                    closeCacheInternal();
                    break;
            }
            return null;
        }
    }

    protected void initDiskCacheInternal() {
        ImageCache.getInstance().initDiskCache();
        initHttpDiskCache();
    }

    protected void clearCacheInternal() {
        ImageCache.getInstance().clearCache();
        synchronized (mHttpDiskCacheLock) {
            if (mHttpDiskCache != null && !mHttpDiskCache.isClosed()) {
                try {
                    mHttpDiskCache.delete();
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "HTTP cache cleared");
                    }
                } catch (IOException e) {
                    Log.e(TAG, "clearCacheInternal - " + e);
                }
                mHttpDiskCache = null;
                mHttpDiskCacheStarting = true;
                initHttpDiskCache();
            }
        }
    }

    protected void flushCacheInternal() {
        ImageCache.getInstance().flush();
        synchronized (mHttpDiskCacheLock) {
            if (mHttpDiskCache != null) {
                try {
                    mHttpDiskCache.flush();
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "HTTP cache flushed");
                    }
                } catch (IOException e) {
                    Log.e(TAG, "flush - " + e);
                }
            }
        }
    }

    protected void closeCacheInternal() {
        ImageCache.getInstance().close();
        synchronized (mHttpDiskCacheLock) {
            if (mHttpDiskCache != null) {
                try {
                    if (!mHttpDiskCache.isClosed()) {
                        mHttpDiskCache.close();
                        mHttpDiskCache = null;
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "HTTP cache closed");
                        }
                    }
                } catch (IOException e) {
                    Log.e(TAG, "closeCacheInternal - " + e);
                }
            }
        }
    }

    public void initCache() {
        new CacheAsyncTask().execute(MESSAGE_INIT_DISK_CACHE);
    }
    public void clearCache() {
        new CacheAsyncTask().execute(MESSAGE_CLEAR);
    }

    public void flushCache() {
        new CacheAsyncTask().execute(MESSAGE_FLUSH);
    }

    public void closeCache() {
        new CacheAsyncTask().execute(MESSAGE_CLOSE);
    }

    private void initHttpDiskCache() {
        if (!mHttpCacheDir.exists()) {
            mHttpCacheDir.mkdirs();
        }
        synchronized (mHttpDiskCacheLock) {
            if (ImageCache.getUsableSpace(mHttpCacheDir) > HTTP_CACHE_SIZE) {
                try {
                    mHttpDiskCache = DiskLruCache.open(mHttpCacheDir, 1, 1, HTTP_CACHE_SIZE);
                    if (BuildConfig.DEBUG) {
                        Log.d(TAG, "HTTP cache initialized");
                    }
                } catch (IOException e) {
                    mHttpDiskCache = null;
                }
            }
            mHttpDiskCacheStarting = false;
            mHttpDiskCacheLock.notifyAll();
        }
    }
}
