package com.andremacareno.vkdocs.cache;

import com.andremacareno.vkdocs.dao.Document;
import com.andremacareno.vkdocs.objs.DocListItemWrap;

import java.util.HashMap;

/**
 * Created by andremacareno on 24/04/15.
 */
public class FileCache {
    private static volatile FileCache _instance;
    private final HashMap<Long, DocListItemWrap> files = new HashMap<>();
    private FileCache() {
    }
    public static FileCache getInstance()
    {
        if(_instance == null)
        {
            synchronized(FileCache.class)
            {
                if(_instance == null)
                    _instance = new FileCache();
            }
        }
        return _instance;
    }
    public void clear()
    {
        synchronized(files)
        {
            files.clear();
        }
    }

    public DocListItemWrap getDocument(Document doc)
    {
        if(doc == null)
            return null;
        Long docId = doc.getId();
        DocListItemWrap f;
        synchronized(files)
        {
            if(files.containsKey(docId))
                f = files.get(docId);
            else {
                f = DocListItemWrap.createInstance(doc);
                files.put(docId, f);
            }
        }
        return f;
    }
    public DocListItemWrap getDocumentById(Long id)
    {
        if(id == null)
            return null;
        synchronized(files)
        {
            if(files.containsKey(id))
                return files.get(id);
        }
        return null;
    }
    public void didIdChanged(Long oldId, Long newId)
    {
        DocListItemWrap f;
        synchronized(files)
        {
            if(files.containsKey(oldId))
            {
                f = files.get(oldId);
                files.remove(oldId);
                files.put(newId, f);
            }
        }
    }
}
