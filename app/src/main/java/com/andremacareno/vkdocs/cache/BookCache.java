package com.andremacareno.vkdocs.cache;

import java.util.HashSet;

/**
 * Created by andremacareno on 21/03/16.
 */
public class BookCache {
    public final HashSet<Long> ids = new HashSet<>();
    private static volatile BookCache _instance;
    private BookCache() {}

    public static BookCache sharedInstance()
    {
        if(_instance == null)
        {
            synchronized(BookCache.class)
            {
                if(_instance == null)
                    _instance = new BookCache();
            }
        }
        return _instance;
    }

    public void addBook(Long id)
    {
        synchronized(ids)
        {
            ids.add(id);
        }
    }
    public void notifyBookRemoved(Long id)
    {
        synchronized(ids)
        {
            ids.remove(id);
        }
    }
    public void clear()
    {
        synchronized(ids)
        {
            ids.clear();
        }
    }

}
