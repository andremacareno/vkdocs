package com.andremacareno.vkdocs;

/**
 * Created by Andrew on 08.01.2016.
 */
public interface OfflineAccessDelegate {
    void didOfflineAccessRevoked();
}