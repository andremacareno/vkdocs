package com.andremacareno.vkdocs;

/**
 * Created by Andrew on 11.01.2016.
 */
public enum DownloadState {
    NOT_DOWNLOADED,
    DOWNLOADING,
    DOWNLOADED
}
