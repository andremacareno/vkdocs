package com.andremacareno.vkdocs;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;

import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.vkdocs.dao.DaoMaster;
import com.andremacareno.vkdocs.vk.VKAuthObserver;
import com.crashlytics.android.Crashlytics;
import com.vk.sdk.VKSdk;

import java.io.File;
import java.util.concurrent.atomic.AtomicBoolean;

import io.fabric.sdk.android.Fabric;


/**
 * Created by andremacareno on 03/04/15.
 */
public class AppLoader extends Application {
    private final String TAG = "AppLoader";
    private static volatile AppLoader sharedApp;

    private volatile SharedPreferences prefs;
    private VKAuthObserver authObserver;
    private final AtomicBoolean isWifi = new AtomicBoolean(false);
    private final AtomicBoolean hasConnection = new AtomicBoolean(false);
    public static AppLoader sharedInstance() {
        return sharedApp;
    }
    public static volatile Handler applicationHandler = new Handler();
    private File cacheDir;
    private String pathToCache;
    private String docsCache;
    private String tmpCache;
    public DaoMaster daoMaster;
    IntentFilter connectivityFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
    NetworkChangeReceiver networkChangeReceiver = new NetworkChangeReceiver();

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        cacheDir = getApplicationContext().getExternalCacheDir() == null ? getApplicationContext().getCacheDir() : getApplicationContext().getExternalCacheDir();
        pathToCache = cacheDir.getPath();
        docsCache = pathToCache.concat("/docs/");
        tmpCache = pathToCache.concat("/tmp/");
        authObserver = new VKAuthObserver();
        authObserver.startTracking();
        VKSdk.initialize(this);
        sharedApp = this;
    }

    public SharedPreferences sharedPreferences() {
        if (prefs == null) {
            synchronized (SharedPreferences.class) {
                if (prefs == null)
                    prefs = getSharedPreferences(Constants.APP_PREFS, MODE_PRIVATE);
            }
        }
        return prefs;
    }


    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        if (level >= TRIM_MEMORY_MODERATE) {
            /*ImageCache.sharedInstance().evictAll();
            FileCache.getInstance().clear();
            UserCache.getInstance().clear();
            GroupChatFullCache.getInstance().clear();*/
            //writeThatTDLibMightBeReloaded();
        } else if (level >= TRIM_MEMORY_BACKGROUND) {
            //ImageCache.sharedInstance().trimToSize(ImageCache.sharedInstance().size() / 2);
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        /*ImageCache.sharedInstance().evictAll();
        FileCache.getInstance().clear();
        UserCache.getInstance().clear();
        GroupChatFullCache.getInstance().clear();*/
    }
    public File cacheLocation() { return this.cacheDir; }
    public String stringCacheLocation() { return this.pathToCache; }
    public String docsCacheLocation() { return this.docsCache; }
    public String tmpCacheLocation() { return this.tmpCache; }
    public void stopConnectionChangeBroadcasting()
    {
        this.unregisterReceiver(networkChangeReceiver);
    }
    public void startConnectionChangeBroadcasting()
    {
        this.registerReceiver(networkChangeReceiver, connectivityFilter);
    }

    private class NetworkChangeReceiver extends BroadcastReceiver {
        public boolean isNetworkAvailable() {
            try {
                ConnectivityManager connectivityManager
                        = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                synchronized(isWifi)
                {
                    isWifi.set(activeNetworkInfo.getType() == ConnectivityManager.TYPE_WIFI);
                }
                return activeNetworkInfo != null && activeNetworkInfo.isConnected() && activeNetworkInfo.isAvailable();
            } catch (NullPointerException e) {
                return false;
            }
        }

        @Override
        public void onReceive(final Context context, final Intent intent) {
            synchronized(hasConnection)
            {
                boolean available = isNetworkAvailable();
                hasConnection.set(available);
                NotificationCenter.getInstance().postNotification(NotificationCenter.didNetworkConnectionStateChanged, available);
            }
        }
    }
    public boolean isConnectedToNetwork() {
        synchronized(hasConnection)
        {
            return hasConnection.get();
        }
    }
    public boolean isConnectedToWifi()
    {
        synchronized(isWifi)
        {
            return isWifi.get();
        }
    }
}
