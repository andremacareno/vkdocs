package com.andremacareno.vkdocs;

import com.andremacareno.vkdocs.dao.Document;

import java.io.File;

/**
 * Created by Andrew on 08.01.2016.
 */
public interface TempDownloadDelegate {
    void didFileProgressUpdated(Document doc, float currentProgress);
    void didFileDownloadFinished(Document doc, File tmpPath);
    void didFileDownloadCancelled(Document doc, DownloadDelegate.CancelReason why);
}