package com.andremacareno.vkdocs.dao;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.DaoConfig;

import com.andremacareno.vkdocs.dao.Document;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table "DOCUMENT".
*/
public class DocumentDao extends AbstractDao<Document, Long> {

    public static final String TABLENAME = "DOCUMENT";

    /**
     * Properties of entity Document.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property Title = new Property(1, String.class, "title", false, "TITLE");
        public final static Property Size = new Property(2, Long.class, "size", false, "SIZE");
        public final static Property Ext = new Property(3, String.class, "ext", false, "EXT");
        public final static Property Url = new Property(4, String.class, "url", false, "URL");
        public final static Property Preview_url = new Property(5, String.class, "preview_url", false, "PREVIEW_URL");
        public final static Property Date = new Property(6, Long.class, "date", false, "DATE");
        public final static Property FolderId = new Property(7, Long.class, "folderId", false, "FOLDER_ID");
        public final static Property Path = new Property(8, String.class, "path", false, "PATH");
    };


    public DocumentDao(DaoConfig config) {
        super(config);
    }
    
    public DocumentDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"DOCUMENT\" (" + //
                "\"_id\" INTEGER PRIMARY KEY ," + // 0: id
                "\"TITLE\" TEXT," + // 1: title
                "\"SIZE\" INTEGER," + // 2: size
                "\"EXT\" TEXT," + // 3: ext
                "\"URL\" TEXT," + // 4: url
                "\"PREVIEW_URL\" TEXT," + // 5: preview_url
                "\"DATE\" INTEGER," + // 6: date
                "\"FOLDER_ID\" INTEGER," + // 7: folderId
                "\"PATH\" TEXT);"); // 8: path
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"DOCUMENT\"";
        db.execSQL(sql);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, Document entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        String title = entity.getTitle();
        if (title != null) {
            stmt.bindString(2, title);
        }
 
        Long size = entity.getSize();
        if (size != null) {
            stmt.bindLong(3, size);
        }
 
        String ext = entity.getExt();
        if (ext != null) {
            stmt.bindString(4, ext);
        }
 
        String url = entity.getUrl();
        if (url != null) {
            stmt.bindString(5, url);
        }
 
        String preview_url = entity.getPreview_url();
        if (preview_url != null) {
            stmt.bindString(6, preview_url);
        }
 
        Long date = entity.getDate();
        if (date != null) {
            stmt.bindLong(7, date);
        }
 
        Long folderId = entity.getFolderId();
        if (folderId != null) {
            stmt.bindLong(8, folderId);
        }
 
        String path = entity.getPath();
        if (path != null) {
            stmt.bindString(9, path);
        }
    }

    /** @inheritdoc */
    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    /** @inheritdoc */
    @Override
    public Document readEntity(Cursor cursor, int offset) {
        Document entity = new Document( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1), // title
            cursor.isNull(offset + 2) ? null : cursor.getLong(offset + 2), // size
            cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3), // ext
            cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4), // url
            cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5), // preview_url
            cursor.isNull(offset + 6) ? null : cursor.getLong(offset + 6), // date
            cursor.isNull(offset + 7) ? null : cursor.getLong(offset + 7), // folderId
            cursor.isNull(offset + 8) ? null : cursor.getString(offset + 8) // path
        );
        return entity;
    }
     
    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, Document entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setTitle(cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1));
        entity.setSize(cursor.isNull(offset + 2) ? null : cursor.getLong(offset + 2));
        entity.setExt(cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3));
        entity.setUrl(cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4));
        entity.setPreview_url(cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5));
        entity.setDate(cursor.isNull(offset + 6) ? null : cursor.getLong(offset + 6));
        entity.setFolderId(cursor.isNull(offset + 7) ? null : cursor.getLong(offset + 7));
        entity.setPath(cursor.isNull(offset + 8) ? null : cursor.getString(offset + 8));
     }
    
    /** @inheritdoc */
    @Override
    protected Long updateKeyAfterInsert(Document entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    /** @inheritdoc */
    @Override
    public Long getKey(Document entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    /** @inheritdoc */
    @Override    
    protected boolean isEntityUpdateable() {
        return true;
    }
    
}
