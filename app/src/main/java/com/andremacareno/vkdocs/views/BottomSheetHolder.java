package com.andremacareno.vkdocs.views;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by andremacareno on 29/02/16.
 */
public abstract class BottomSheetHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public BottomSheetItem item;
    public BottomSheetHolder(View itemView) {
        super(itemView);
        this.item = (BottomSheetItem) itemView;
        this.item.setOnClickListener(this);
    }
}
