package com.andremacareno.vkdocs.views;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by Andrew on 07.01.2016.
 */
public class BooksGridView extends BaseListView {
    private LayoutManager lm;

    public BooksGridView(Context context) {
        super(context);
        init();
    }

    public BooksGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init()
    {
        if(lm == null)
            lm = new GridLayoutManager(getContext(), 3);
        setLayoutManager(lm);
        addItemDecoration(new ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, State state) {
                outRect.bottom = outRect.top = outRect.left = outRect.right = 0;
            }
        });
    }
}
