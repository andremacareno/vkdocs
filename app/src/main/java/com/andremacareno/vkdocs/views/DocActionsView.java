package com.andremacareno.vkdocs.views;

import android.content.Context;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.andremacareno.vkdocs.AndroidUtilities;
import com.andremacareno.vkdocs.R;
import com.andremacareno.vkdocs.controllers.DocContextMenuListController;
import com.andremacareno.vkdocs.controllers.ViewController;
import com.andremacareno.vkdocs.dao.Document;
import com.andremacareno.vkdocs.dao.Folder;
import com.andremacareno.vkdocs.objs.DocListItemWrap;

/**
 * Created by andremacareno on 16/08/15.
 */
public class DocActionsView extends RelativeLayout implements BottomSheet {

    public interface MenuDelegate
    {
        public void didShareWithFriendRequested(Document doc);
        public void didMoveToFolderRequested(Document doc);
        public void didRemoveFromFolderRequested(Document doc);
        public void didOfflineAccessRequested(Document doc);
        public void didRevokeOfflineAccessRequested(Document doc);
        public void didRemoveRequested(Document doc);
        public void didRemoveRequested(Folder folder);
        public void onBottomSheetClose(boolean dismissed);
        public void onActionChoose();
        public void didRenameRequested(Document doc);
        public void didRenameRequested(Folder folder);
        public void onRenameDonePressed(Document doc, String newTitle);
        public void onRenameDonePressed(Folder folder, String newTitle);
        public void onSelectionModeRequested(DocListItemWrap initialSelection);

    }
    private float yFraction;
    private ViewTreeObserver.OnPreDrawListener preDrawListener;
    private DoneButtonListener doneButtonListener;
    public BottomSheetListView attachActionsListView;
    public EditText title;
    private MenuDelegate delegate;
    private final OnTouchListener ignoreEditTextTap = new OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            return true;
        }
    };

    public DocActionsView(Context context) {
        super(context);
        init();
    }

    public DocActionsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    private void init()
    {
        View v = inflate(getContext(), R.layout.doc_context_menu, this);
        attachActionsListView = (BottomSheetListView) v.findViewById(R.id.doc_context_actions);
        doneButtonListener = new DoneButtonListener();
        title = (EditText) v.findViewById(R.id.title);
        title.setInputType(InputType.TYPE_CLASS_TEXT);
        title.setOnEditorActionListener(doneButtonListener);
        title.setImeOptions(EditorInfo.IME_ACTION_DONE);
        title.setOnTouchListener(ignoreEditTextTap);
    }
    public void setYFraction(float fraction) {

        this.yFraction = fraction;
        if (getHeight() == 0) {
            if (preDrawListener == null) {
                preDrawListener = new ViewTreeObserver.OnPreDrawListener() {
                    @Override
                    public boolean onPreDraw() {
                        getViewTreeObserver().removeOnPreDrawListener(preDrawListener);
                        setYFraction(yFraction);
                        return true;
                    }
                };
                getViewTreeObserver().addOnPreDrawListener(preDrawListener);
            }
            return;
        }
        float translationY = getHeight() * fraction;
        setTranslationY(translationY);
    }
    public float getYFraction() {
        return this.yFraction;
    }
    public void setDelegate(MenuDelegate delegateRef)
    {
        this.delegate = delegateRef;
    }
    public void onRenameEnabled(Document doc)
    {
        doneButtonListener.doc = doc;
        doneButtonListener.folder = null;
        continueInitRename();
    }
    public void onRenameEnabled(Folder f)
    {
        doneButtonListener.doc = null;
        doneButtonListener.folder = f;
        continueInitRename();
    }
    private void continueInitRename()
    {
        title.setOnTouchListener(null);
        title.requestFocus();
        title.setSelection(title.getText().length());
        AndroidUtilities.showKeyboard(title);
    }
    @Override
    public void onClose(boolean dismissed) {
        AndroidUtilities.hideKeyboard(title);
        title.setOnTouchListener(ignoreEditTextTap);
        if(attachActionsListView != null)
        {
            ViewController listController = attachActionsListView.getController();
            if(listController != null && listController instanceof DocContextMenuListController)
                ((DocContextMenuListController) listController).continueProcessing();
        }
        if(delegate != null)
            delegate.onBottomSheetClose(dismissed);
    }
    private class DoneButtonListener implements TextView.OnEditorActionListener
    {
        public Document doc;
        public Folder folder;
        @Override
        public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
            if(i == EditorInfo.IME_ACTION_DONE && delegate != null)
            {
                String newTitle = title.getText().toString();
                if(newTitle.isEmpty() || newTitle.length() > 128)
                    return false;
                if(doc != null)
                    delegate.onRenameDonePressed(this.doc, newTitle);
                else if(folder != null)
                    delegate.onRenameDonePressed(this.folder, newTitle);
                return true;
            }
            return false;
        }
    }
}
