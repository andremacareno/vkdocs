package com.andremacareno.vkdocs.views;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andremacareno.vkdocs.AndroidUtilities;
import com.andremacareno.vkdocs.R;

/**
 * Created by andremacareno on 20/03/16.
 */
public class SelectionInfoView extends RelativeLayout{
    public interface SelectionInfoDelegate
    {
        public void onDeleteRequested();
        public void onMoveToFolderRequested();
    }
    private TextView infoView;
    private ImageView moveToFolder, delete;
    private SelectionInfoDelegate delegate;
    public SelectionInfoView(Context context) {
        super(context);
        init();
    }
    private void init()
    {
        setBackgroundColor(0xFFFAFAFA);
        PorterDuffColorFilter colorFilter = new PorterDuffColorFilter(ContextCompat.getColor(getContext(), R.color.bottom_sheet_mask), PorterDuff.Mode.SRC_ATOP);
        View shadow = new View(getContext());
        shadow.setBackgroundResource(R.drawable.greydivider_top);
        shadow.setId(R.id.shadow);
        RelativeLayout.LayoutParams shadowLP = new LayoutParams(LayoutParams.MATCH_PARENT, AndroidUtilities.dp(4));
        shadowLP.addRule(ALIGN_PARENT_TOP);
        shadow.setLayoutParams(shadowLP);
        int padding = AndroidUtilities.dp(12);
        delete = new ImageView(getContext());
        delete.setImageResource(R.drawable.ic_del);
        delete.setColorFilter(colorFilter);
        delete.setPadding(padding, padding, padding, padding);
        delete.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (delegate != null)
                    delegate.onDeleteRequested();
            }
        });
        delete.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Toast.makeText(getContext(), R.string.delete, Toast.LENGTH_SHORT).show();
                return true;
            }
        });

        moveToFolder = new ImageView(getContext());
        moveToFolder.setImageResource(R.drawable.ic_folder);
        moveToFolder.setColorFilter(colorFilter);
        moveToFolder.setPadding(padding, padding, padding, padding);
        moveToFolder.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (delegate != null)
                    delegate.onMoveToFolderRequested();
            }
        });
        moveToFolder.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Toast.makeText(getContext(), R.string.move_to_folder, Toast.LENGTH_SHORT).show();
                return true;
            }
        });

        infoView = new TextView(getContext());
        infoView.setSingleLine(true);
        infoView.setEllipsize(TextUtils.TruncateAt.END);
        infoView.setTextColor(0xDE000000);
        infoView.setGravity(Gravity.CENTER_VERTICAL);
        infoView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
        infoView.setPadding(AndroidUtilities.dp(24), 0, 0, 0);

        RelativeLayout.LayoutParams deleteLP = new RelativeLayout.LayoutParams(AndroidUtilities.dp(48), AndroidUtilities.dp(48));
        deleteLP.addRule(ALIGN_PARENT_RIGHT);
        deleteLP.addRule(CENTER_VERTICAL);
        deleteLP.addRule(BELOW, R.id.shadow);
        delete.setId(R.id.del_selection);
        delete.setLayoutParams(deleteLP);
        delete.setBackgroundResource(R.drawable.list_selector);

        RelativeLayout.LayoutParams moveLP = new RelativeLayout.LayoutParams(AndroidUtilities.dp(48), AndroidUtilities.dp(48));
        moveLP.addRule(CENTER_VERTICAL);
        moveLP.addRule(LEFT_OF, R.id.del_selection);
        moveLP.addRule(BELOW, R.id.shadow);
        moveToFolder.setId(R.id.move_selection);
        moveToFolder.setLayoutParams(moveLP);
        moveToFolder.setBackgroundResource(R.drawable.list_selector);

        RelativeLayout.LayoutParams tvLP = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, AndroidUtilities.dp(48));
        tvLP.addRule(LEFT_OF, R.id.move_selection);
        tvLP.addRule(BELOW, R.id.shadow);
        infoView.setLayoutParams(tvLP);

        addView(delete);
        addView(moveToFolder);
        addView(infoView);
        addView(shadow);
    }
    public void updateSelectionCount(int count)
    {
        if(count <= 0)
            return;
        infoView.setText(count == 1 ? getResources().getString(R.string.selected_one) : String.format(getResources().getString(R.string.selected_two_or_more), count));
        RelativeLayout.LayoutParams delLP = (RelativeLayout.LayoutParams) delete.getLayoutParams();
        delLP.width = count > 10 ? AndroidUtilities.dp(1) : AndroidUtilities.dp(48);
        delete.setVisibility(count > 10 ? INVISIBLE : VISIBLE);
    }
    public void setDelegate(SelectionInfoDelegate delegate)
    {
        this.delegate = delegate;
    }
}
