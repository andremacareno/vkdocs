package com.andremacareno.vkdocs.views;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;

import com.andremacareno.vkdocs.AppLoader;

/**
 * Created by Andrew on 07.01.2016.
 */
public class FriendsListView extends BaseListView {
    private LayoutManager lm;
    public FriendsListView(Context context) {
        super(context);
        init();
    }
    private void init()
    {
        if(lm == null)
            lm = new LinearLayoutManager(AppLoader.sharedInstance().getApplicationContext());
        setLayoutManager(lm);
    }
}
