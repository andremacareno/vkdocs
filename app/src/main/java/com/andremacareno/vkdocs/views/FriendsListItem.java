package com.andremacareno.vkdocs.views;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.andremacareno.vkdocs.AndroidUtilities;
import com.andremacareno.vkdocs.R;
import com.andremacareno.vkdocs.cache.ImageLoader;
import com.vk.sdk.api.model.VKApiUserFull;

/**
 * Created by Andrew on 08.01.2016.
 */
public class FriendsListItem extends FrameLayout {
    private SelectableAvatarImageView photo;
    private TextView friendName;
    private TextView header;
    private SmallRevealingCheckView check;

    public FriendsListItem(Context context) {
        super(context);
        init();
    }
    private void init()
    {
        setBackgroundResource(R.drawable.list_selector);
        FrameLayout.LayoutParams photoLP = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);

        FrameLayout.LayoutParams tvLP = new FrameLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        tvLP.gravity = Gravity.CENTER_VERTICAL;
        tvLP.leftMargin = AndroidUtilities.dp(120);
        tvLP.rightMargin = AndroidUtilities.dp(16);
        photo = new SelectableAvatarImageView(getContext());
        photo.setScaleType(ImageView.ScaleType.CENTER_CROP);
        FrameLayout photoContainer = new FrameLayout(getContext());
        photoContainer.setBackgroundResource(R.drawable.avatar_bg_selection);
        FrameLayout.LayoutParams containerLP = new LayoutParams(AndroidUtilities.dp(40), AndroidUtilities.dp(40));
        containerLP.gravity = Gravity.CENTER_VERTICAL | Gravity.LEFT;
        containerLP.leftMargin = AndroidUtilities.dp(72);
        containerLP.rightMargin = AndroidUtilities.dp(8);
        photoContainer.setLayoutParams(containerLP);

        photo.setLayoutParams(photoLP);

        friendName = new TextView(getContext());
        if(Build.VERSION.SDK_INT < 23)
            friendName.setTextAppearance(friendName.getContext(), R.style.GeneralText);
        else
            friendName.setTextAppearance(R.style.GeneralText);
        friendName.setLayoutParams(tvLP);
        friendName.setPadding(0, AndroidUtilities.dp(16), AndroidUtilities.dp(8), AndroidUtilities.dp(16));
        friendName.setSingleLine(true);
        friendName.setEllipsize(TextUtils.TruncateAt.END);

        header = new TextView(getContext());
        header.setTextColor(0xFF808080);
        header.setBackgroundColor(0x00000000);
        header.setTypeface(Typeface.DEFAULT_BOLD);
        header.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
        header.setVisibility(GONE);

        FrameLayout.LayoutParams headerLP = new FrameLayout.LayoutParams(AndroidUtilities.dp(48), AndroidUtilities.dp(40));
        headerLP.gravity = Gravity.CENTER_VERTICAL | Gravity.LEFT;
        headerLP.leftMargin = AndroidUtilities.dp(24);
        header.setLayoutParams(headerLP);

        check = new SmallRevealingCheckView(getContext());
        FrameLayout.LayoutParams checkLP = new FrameLayout.LayoutParams(AndroidUtilities.dp(16), AndroidUtilities.dp(16));
        checkLP.gravity = Gravity.BOTTOM | Gravity.RIGHT;
        check.setLayoutParams(checkLP);
        photoContainer.addView(photo);
        photoContainer.addView(check);
        addView(friendName);
        addView(photoContainer);
        addView(header);
    }
    public void fill(VKApiUserFull user)
    {
        if(user == null)
            return;
        String url = AndroidUtilities.isLowDPIScreen() ? user.photo_50 : user.photo_100;
        friendName.setText(user.first_name.concat(" ").concat(user.last_name));
        ImageLoader.getInstance().loadImage(url, photo, ImageLoader.POST_PROCESSING_CIRCLE, AndroidUtilities.dp(40), AndroidUtilities.dp(40), false);
    }
    public void check(boolean animate)
    {
        this.photo.select(animate);
        this.check.check(animate);
    }
    public void uncheck(boolean animate)
    {
        this.photo.deselect(animate);
        this.check.uncheck(animate);
    }
    public void insertHeaderIfNeed(VKApiUserFull currentUser, VKApiUserFull previousUser)
    {
        if(currentUser == null)
            return;
        boolean show = false;
        char firstChar = currentUser.first_name.charAt(0);
        if(previousUser == null)
            show = true;
        else if(firstChar != previousUser.first_name.charAt(0))
            show = true;
        if(!show)
        {
            if(header.getVisibility() == VISIBLE)
                header.setVisibility(GONE);
            return;
        }
        else
        {
            if(header.getVisibility() == GONE)
                header.setVisibility(VISIBLE);
            header.setText(new char[] {firstChar}, 0, 1);
        }
    }
}
