package com.andremacareno.vkdocs.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.RelativeLayout;

import com.andremacareno.vkdocs.R;
import com.andremacareno.vkdocs.listadapters.GalleryImagesListAdapter;

/**
 * Created by andremacareno on 16/08/15.
 */
public class NewDocView extends RelativeLayout implements BottomSheet {

    public interface MenuDelegate
    {
        public void createNewFolder();
        public void sendRecentGallery();
        public void chooseFileFromGallery();
        public void chooseFileFromAll();
        public void onBottomSheetClose(boolean dismissed);
        public void onActionChoose();
    }
    private float yFraction;
    private ViewTreeObserver.OnPreDrawListener preDrawListener;
    public View attachListDivider;
    public BottomSheetListView attachActionsListView;
    public RecentFromGalleryListView recentFromGallery;
    private MenuDelegate delegate;
    private GalleryImagesListAdapter.QuickSelectDelegate quickSelectDelegate;

    public NewDocView(Context context) {
        super(context);
        init();
    }

    public NewDocView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    private void init()
    {
        View v = inflate(getContext(), R.layout.new_doc_menu, this);
        attachListDivider = v.findViewById(R.id.item_divider);
        attachActionsListView = (BottomSheetListView) v.findViewById(R.id.newdoc_actions);
        recentFromGallery = (RecentFromGalleryListView) v.findViewById(R.id.recent_from_gallery);
    }
    public void setYFraction(float fraction) {

        this.yFraction = fraction;
        if (getHeight() == 0) {
            if (preDrawListener == null) {
                preDrawListener = new ViewTreeObserver.OnPreDrawListener() {
                    @Override
                    public boolean onPreDraw() {
                        getViewTreeObserver().removeOnPreDrawListener(preDrawListener);
                        setYFraction(yFraction);
                        return true;
                    }
                };
                getViewTreeObserver().addOnPreDrawListener(preDrawListener);
            }
            return;
        }
        float translationY = getHeight() * fraction;
        setTranslationY(translationY);
    }
    public float getYFraction() {
        return this.yFraction;
    }
    public void setDelegate(MenuDelegate delegateRef)
    {
        this.delegate = delegateRef;
    }
    @Override
    public void onClose(boolean dismissed) {
        if(delegate != null)
            delegate.onBottomSheetClose(dismissed);
    }
}
