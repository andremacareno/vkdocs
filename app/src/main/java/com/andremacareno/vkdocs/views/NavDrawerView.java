package com.andremacareno.vkdocs.views;

import android.content.Context;
import android.support.design.widget.NavigationView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.andremacareno.vkdocs.AndroidUtilities;
import com.andremacareno.vkdocs.R;
import com.andremacareno.vkdocs.cache.ImageLoader;
import com.andremacareno.vkdocs.objs.CurrentUser;


/**
 * Created by Andrew on 10.05.2015.
 */
public class NavDrawerView extends NavigationView {
    private static int totalActions = 0;
    public static final int GOTO_RECENT = totalActions++;
    public static final int GOTO_FILES = totalActions++;
    public static final int GOTO_BOOKS = totalActions++;
    public static final int GOTO_OFFLINE = totalActions++;
    public static final int GOTO_SETTINGS = totalActions++;
    private View layout;
    private RecyclingImageView avatar;
    public NavDrawerView(Context context) {
        super(context);
        init();
    }

    public NavDrawerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public NavDrawerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    private void init()
    {
        layout = inflateHeaderView(R.layout.navdrawer_header);
    }
    public void initHeader()
    {
        TextView username = (TextView) layout.findViewById(R.id.appdrawer_username);
        avatar = (RecyclingImageView) layout.findViewById(R.id.appdrawer_avatar);
        if(CurrentUser.getInstance() != null)
        {
            ImageLoader.getInstance().loadImage(CurrentUser.getInstance().getPhoto(), avatar, ImageLoader.POST_PROCESSING_CIRCLE | ImageLoader.POST_PROCESSING_SCALE, AndroidUtilities.dp(56), AndroidUtilities.dp(56), false);
            username.setText(CurrentUser.getInstance().getDisplayName());
        }
    }
    public void refreshAvatar()
    {
        if(CurrentUser.getInstance() == null)
            return;
        ImageLoader.getInstance().loadImage(CurrentUser.getInstance().getPhoto(), avatar, ImageLoader.POST_PROCESSING_CIRCLE|ImageLoader.POST_PROCESSING_SCALE, AndroidUtilities.dp(56), AndroidUtilities.dp(56), false);
    }
}
