package com.andremacareno.vkdocs.views.actionviews;

import android.content.Context;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.andremacareno.vkdocs.AndroidUtilities;
import com.andremacareno.vkdocs.R;
import com.andremacareno.vkdocs.views.ActionView;

import java.lang.reflect.Field;

/**
 * Created by andremacareno on 13/03/16.
 */
public class FileScreenActionView extends ActionView {
    public ImageButton drawerButton, searchButton;
    public EditText searchEditText;
    public TextView title;
    private static final String TAG = "FileScreenAV";

    public FileScreenActionView(Context context) {
        super(context);
    }

    @Override
    public Object getTag() {
        return TAG;
    }

    @Override
    protected void initView() {
        setBackgroundColor(0xFF5181B8);
        setLayoutParams(new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, AndroidUtilities.dp(56)));
        LayoutParams drawerButtonLP = new LayoutParams(AndroidUtilities.dp(56), AndroidUtilities.dp(56));
        LayoutParams searchButtonLP = new LayoutParams(AndroidUtilities.dp(56), AndroidUtilities.dp(56));
        LayoutParams editTextLP = new LayoutParams(LayoutParams.MATCH_PARENT, AndroidUtilities.dp(56));
        editTextLP.gravity = Gravity.CENTER_VERTICAL | Gravity.LEFT;
        LayoutParams tvLP = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        tvLP.gravity = Gravity.CENTER_VERTICAL | Gravity.LEFT;
        drawerButtonLP.gravity = Gravity.TOP | Gravity.LEFT;
        searchButtonLP.gravity = Gravity.TOP | Gravity.RIGHT;
        editTextLP.gravity = Gravity.TOP | Gravity.LEFT;
        drawerButton = new ImageButton(getContext());
        drawerButton.setPadding(AndroidUtilities.dp(16), AndroidUtilities.dp(16), AndroidUtilities.dp(16), AndroidUtilities.dp(16));
        drawerButton.setBackgroundResource(R.drawable.action_button_bg);
        drawerButton.setLayoutParams(drawerButtonLP);
        drawerButton.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        //backButton.setImageResource(R.drawable.ic_drawer);

        searchButton = new ImageButton(getContext());
        searchButton.setPadding(AndroidUtilities.dp(16), AndroidUtilities.dp(16), AndroidUtilities.dp(16), AndroidUtilities.dp(16));
        searchButton.setBackgroundResource(R.drawable.action_button_bg);
        searchButton.setLayoutParams(searchButtonLP);
        searchButton.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        searchButton.setImageResource(R.drawable.ic_search);

        searchEditText = new EditText(getContext());
        searchEditText.setBackgroundColor(0x00000000);
        searchEditText.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 17);
        searchEditText.setHint(R.string.search);
        searchEditText.setTextColor(0xFFFFFFFF);
        searchEditText.setHintTextColor(0xffb0cce1);
        searchEditText.setSingleLine(true);
        searchEditText.setPadding(AndroidUtilities.dp(72), AndroidUtilities.dp(8), AndroidUtilities.dp(8), AndroidUtilities.dp(8));
        try {
            Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
            f.setAccessible(true);
            f.set(searchEditText, R.drawable.search_cursor);
        } catch (Exception ignored) {
        }
        searchEditText.setLayoutParams(editTextLP);
        searchEditText.setVisibility(GONE);
        title = new TextView(getContext());
        title.setPadding(AndroidUtilities.dp(72), 0, 0, 0);
        title.setLayoutParams(tvLP);
        title.setText(R.string.files);
        title.setTextColor(0xFFFFFFFF);
        title.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
        title.setTypeface(null, Typeface.BOLD);
        title.setSingleLine(true);
        title.setEllipsize(TextUtils.TruncateAt.END);
        addView(title);
        addView(searchEditText);
        addView(drawerButton);
        addView(searchButton);
    }
    public void setTitle(int resId)
    {
        this.title.setText(resId);
    }
    public void setTitle(String str)
    {
        this.title.setText(str);
    }
}
