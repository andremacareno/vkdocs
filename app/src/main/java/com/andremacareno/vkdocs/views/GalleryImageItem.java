package com.andremacareno.vkdocs.views;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.andremacareno.vkdocs.AndroidUtilities;

/**
 * Created by andremacareno on 16/08/15.
 */
public class GalleryImageItem extends FrameLayout {
    private AccelerateInterpolator revealInterpolator = new AccelerateInterpolator(1.8f);
    private DecelerateInterpolator hideInterpolator = new DecelerateInterpolator(1.8f);
    private RecyclingImageView img;
    private GalleryCheckView selectionMark;
    public GalleryImageItem(Context context) {
        super(context);
        init();
    }
    private void init()
    {
        img = new RecyclingImageView(getContext());
        selectionMark = new GalleryCheckView(getContext());
        LayoutParams imgLayoutParams = new LayoutParams(AndroidUtilities.dp(100), AndroidUtilities.dp(100), Gravity.CENTER);
        LayoutParams selectionMarkLayoutParams = new LayoutParams(AndroidUtilities.dp(26), AndroidUtilities.dp(26), Gravity.BOTTOM | Gravity.LEFT);
        int selectionMarkPadding = AndroidUtilities.dp(4);
        selectionMarkLayoutParams.setMargins(selectionMarkPadding, selectionMarkPadding, selectionMarkPadding, selectionMarkPadding);
        img.setLayoutParams(imgLayoutParams);
        selectionMark.setLayoutParams(selectionMarkLayoutParams);
        img.setPadding(selectionMarkPadding, selectionMarkPadding, selectionMarkPadding, selectionMarkPadding);
        img.setScaleType(ImageView.ScaleType.CENTER_CROP);
        img.setVisibility(View.VISIBLE);
        addView(img);
        addView(selectionMark);
    }
    /*@Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        setMeasuredDimension(AndroidUtilities.dp(104), AndroidUtilities.dp(104));
    }*/
    public RecyclingImageView getImageView() { return this.img; }
    public void markSelected(boolean animated)
    {
        selectionMark.setChecked(true);
        /*if(!animated)
            shadow.setVisibility(View.VISIBLE);
        else
        {
            if(Build.VERSION.SDK_INT >= 21)
                reveal(false);
            else
               animatePreLollipop(false);
        }*/
    }
    public void markUnselected(boolean animated)
    {
        selectionMark.setChecked(false);
        /*if(!animated)
            shadow.setVisibility(View.INVISIBLE);
        else
        {
            if(Build.VERSION.SDK_INT >= 21)
                reveal(true);
            else
                animatePreLollipop(true);
        }*/
    }
}
