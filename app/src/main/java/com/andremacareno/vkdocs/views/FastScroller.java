package com.andremacareno.vkdocs.views;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.andremacareno.vkdocs.AndroidUtilities;
import com.andremacareno.vkdocs.BuildConfig;
import com.andremacareno.vkdocs.R;

public class FastScroller extends LinearLayout {
    public interface FastScrollHelper
    {
        public char getCharIndex(int position);
        public int getItemHeight();
    }
    private static final int BUBBLE_ANIMATION_DURATION = 250;
    private static final int TRACK_SNAP_RANGE = AndroidUtilities.dp(24);

    private RecyclerView recyclerView;
    private TextView bubble;
    private ImageView handle;
    private int height;
    private ObjectAnimator bubbleAnimator = null;
    private RecyclerView.OnScrollListener scrollListener;
    private float itemHeight = 0.0f;
    private boolean fastScrolling = false;

    // CONSTRUCTORS ________________________________________________________________________________
    public FastScroller(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public FastScroller(final Context context) {
        super(context);
        init();
    }

    public FastScroller(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        setOrientation(HORIZONTAL);
        setClipChildren(false);
        bubble = new TextView(getContext());
        handle = new ImageView(getContext());
        LinearLayout.LayoutParams bubbleLP = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        bubbleLP.gravity = Gravity.RIGHT;

        LinearLayout.LayoutParams handleLP = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        int margin = AndroidUtilities.dp(8);
        setPadding(margin, 0, margin, 0);
        handleLP.setMargins(margin, 0, margin, 0);
        bubbleLP.rightMargin = margin*3;

        bubble.setBackgroundResource(R.drawable.fastscroll_bubble);
        bubble.setGravity(Gravity.CENTER);
        bubble.setTextColor(0xFFFFFFFF);
        bubble.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 48);
        bubble.setVisibility(INVISIBLE);
        bubble.setAllCaps(true);
        handle.setImageResource(R.drawable.fastscroll_handle);
        bubble.setLayoutParams(bubbleLP);
        handle.setLayoutParams(handleLP);

        scrollListener = new ScrollListener();
        addView(bubble);
        addView(handle);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        height = h;
    }

    @Override
    public boolean onTouchEvent(@NonNull MotionEvent event) {
        final int action = event.getAction();

        switch (action) {
            case MotionEvent.ACTION_DOWN:
                if (event.getX() < handle.getX()) {
                    return false;
                }

                if (bubbleAnimator != null) {
                    bubbleAnimator.cancel();
                }
                fastScrolling = true;
                if (bubble.getVisibility() == INVISIBLE) {
                    showBubble();
                }

                handle.setSelected(true);

            case MotionEvent.ACTION_MOVE:
                final float y = event.getY();

                //setBubbleAndHandlePosition(y);
                setRecyclerViewPosition(y);

                return true;

            case MotionEvent.ACTION_UP:

            case MotionEvent.ACTION_CANCEL:
                fastScrolling = false;
                handle.setSelected(false);
                hideBubble();

                return true;
        }

        return super.onTouchEvent(event);
    }

    public void setRecyclerView(RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
        this.recyclerView.addOnScrollListener(scrollListener);
    }

    private void setRecyclerViewPosition(float y) {
        if(BuildConfig.DEBUG)
            Log.d("FastScroller", String.format("setRecyclerViewPosition: y = %f", y));
        if (recyclerView != null) {
            int itemCount = recyclerView.getAdapter().getItemCount();
            float proportion;

            if (y <= TRACK_SNAP_RANGE) {
                proportion = 0f;
            } else if (y + handle.getHeight() >= height - TRACK_SNAP_RANGE) {
                proportion = 1f;
            } else {
                proportion = y / (float) height;
            }

            int targetPos = getValueInRange(0, itemCount - 1, (int) (proportion * (float) itemCount));

            ((LinearLayoutManager) recyclerView.getLayoutManager()).scrollToPositionWithOffset(targetPos, 0);
            char[] bubbleText = new char[] {((FastScrollHelper) recyclerView.getAdapter()).getCharIndex(targetPos)};

            bubble.setText(bubbleText, 0, 1);
        }
    }

    private int getValueInRange(int min, int max, int value) {
        int minimum = Math.max(min, value);
        return Math.min(minimum, max);
    }

    public void setBubbleAndHandlePosition(float y) {
        int bubbleHeight = bubble.getHeight();
        int handleHeight = handle.getHeight();

        handle.setY(getValueInRange(TRACK_SNAP_RANGE, height - handleHeight - TRACK_SNAP_RANGE, (int) (y - handleHeight / 2 - TRACK_SNAP_RANGE)));
        bubble.setY(getValueInRange(TRACK_SNAP_RANGE, height - bubbleHeight - handleHeight / 2 - TRACK_SNAP_RANGE, (int) (y - bubbleHeight - TRACK_SNAP_RANGE)));
    }

    public int getFastScrollHeight() {
        return height;
    }


    private void showBubble() {
        bubble.setVisibility(VISIBLE);

        if (bubbleAnimator != null)
            bubbleAnimator.cancel();

        bubbleAnimator = ObjectAnimator.ofFloat(bubble, "alpha", 0f, 1f).setDuration(BUBBLE_ANIMATION_DURATION);
        bubbleAnimator.start();
    }

    private void hideBubble() {
        if (bubbleAnimator != null)
            bubbleAnimator.cancel();

        bubbleAnimator = ObjectAnimator.ofFloat(bubble, "alpha", 1f, 0f).setDuration(BUBBLE_ANIMATION_DURATION);
        bubbleAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                bubble.setVisibility(INVISIBLE);
                bubbleAnimator = null;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                super.onAnimationCancel(animation);
                bubble.setVisibility(INVISIBLE);
                bubbleAnimator = null;
            }
        });

        bubbleAnimator.start();
    }
    private class ScrollListener extends RecyclerView.OnScrollListener
    {
        private float additional = 0.0f;
        private float previousRect = 0.0f;
        private float lastScrolled = -1.0f;
        private float previousPosition = -1.0f;
        private int visibleRange = -1;
        private final Rect resultRect = new Rect();
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            try {
                boolean directionUp = dy < 0;
                View firstVisibleView=recyclerView.getChildAt(0);
                int firstVisiblePosition=recyclerView.getChildAdapterPosition(firstVisibleView);
                recyclerView.getChildVisibleRect(firstVisibleView, resultRect, null);
                if(itemHeight == 0.0f)
                    itemHeight = (float) ((FastScrollHelper) recyclerView.getAdapter()).getItemHeight();
                float part = (float) Math.abs(resultRect.top);
                float scrolled = Math.abs(part - previousRect);
                if(scrolled > lastScrolled && lastScrolled >= 0.0f)
                    scrolled = Math.min(itemHeight - scrolled, scrolled);
                additional += directionUp ? -scrolled : scrolled;
                if(additional < 0.0f)
                    additional = itemHeight + additional;
                else if(additional >= itemHeight)
                    additional -= itemHeight;
                float delta = additional / itemHeight;
                previousRect = part;
                lastScrolled = scrolled;
                if(visibleRange == -1)
                    visibleRange=recyclerView.getChildCount();
                int itemCount=recyclerView.getAdapter().getItemCount();

                float position;

                int pos = firstVisiblePosition; //Math.round((((float) firstVisiblePosition / (((float) itemCount - (float) visibleRange))) * (float) itemCount));
                position = (float) pos + delta;
                float dp = position - previousPosition;
                if(previousPosition != -1.0f)
                {
                    if(dp > 1.0f ) {
                        position = pos;
                        additional = 0;
                    }
                    else if(dp < -1.0f && directionUp)
                    {
                        if(firstVisiblePosition == 0)
                            position = 0;
                        else
                            position += 1.0f;
                    }
                }
                //}
                previousPosition = position;
                float proportion= (position + visibleRange) / (float)itemCount;
                setBubbleAndHandlePosition(getFastScrollHeight() * proportion);
            }
            catch(Exception ignored) {}
        }
    }
}