package com.andremacareno.vkdocs.views;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.andremacareno.vkdocs.AndroidUtilities;
import com.andremacareno.vkdocs.AppLoader;
import com.andremacareno.vkdocs.BuildConfig;
import com.andremacareno.vkdocs.Constants;
import com.andremacareno.vkdocs.DownloadDelegate;
import com.andremacareno.vkdocs.DownloadState;
import com.andremacareno.vkdocs.OfflineAccessDelegate;
import com.andremacareno.vkdocs.R;
import com.andremacareno.vkdocs.UploadDelegate;
import com.andremacareno.vkdocs.cache.FolderInfoCache;
import com.andremacareno.vkdocs.cache.ImageLoader;
import com.andremacareno.vkdocs.dao.Document;
import com.andremacareno.vkdocs.dao.Folder;

/**
 * Created by Andrew on 08.01.2016.
 */
public class DocumentView extends RelativeLayout {
    private DocumentIconView extension;
    private DocPreviewImageView docPreview;
    private RevealingCheckView checkView;
    private TextView documentName;
    private TextView downloadText;
    private RadialProgressView radialProgress;
    private SpannableString txtSize;
    private ImageView overflowMenu;


    private Document boundDocument;
    private Folder boundFolder;
    private DownloadDelegate dlDelegate;
    private UploadDelegate ulDelegate;
    private String boundDocumentDelegateKey = null;
    private String boundDocumentUploadDelegateKey = null;

    private Runnable updateProgressRunnable;
    private Runnable dlStartRunnable, dlFinishRunnable, ulFinishRunnable, dlCancelRunnable;
    private OfflineAccessDelegate offlineAccessDelegate;
    private FolderInfoCache.FolderInfoDelegate folderDelegate;
    private boolean checked = false;
    public DocumentView(Context context) {
        super(context);
        init();
    }
    private void init()
    {
        PorterDuffColorFilter grayFilter = new PorterDuffColorFilter(Color.rgb(214, 214, 214), PorterDuff.Mode.SRC_ATOP);
        View v = LayoutInflater.from(getContext()).inflate(R.layout.docslist_item, this);
        setBackgroundResource(R.drawable.list_selector);
        extension = (DocumentIconView) v.findViewById(R.id.extension);
        docPreview = (DocPreviewImageView) v.findViewById(R.id.doc_preview);
        radialProgress = (RadialProgressView) v.findViewById(R.id.radial_progress);
        checkView = (RevealingCheckView) v.findViewById(R.id.doc_check);
        documentName = (TextView) v.findViewById(R.id.fileName);
        downloadText = (TextView) v.findViewById(R.id.downloadState);
        overflowMenu = (ImageView) v.findViewById(R.id.doc_action);
        overflowMenu.setColorFilter(grayFilter);
        setPadding(AndroidUtilities.dp(16), 0, 0, 0);
        updateProgressRunnable = new Runnable() {
            @Override
            public void run() {
                if(boundDocument == null)
                    return;
                if(radialProgress.getVisibility() != View.VISIBLE)
                    radialProgress.setVisibility(View.VISIBLE);
                float progress = (float) boundDocument.getDownloadedSize() / boundDocument.getSize();
                if(BuildConfig.DEBUG)
                    Log.d("DocumentView", String.format("progress: %f; view = %s", progress, DocumentView.this.toString()));
                radialProgress.setProgress(progress, true);
                txtSize = getSuitableSizeString();
                downloadText.setText(txtSize);
            }
        };
        offlineAccessDelegate = new OfflineAccessDelegate() {
            @Override
            public void didOfflineAccessRevoked() {
                extension.updateOfflineIconState(DownloadState.NOT_DOWNLOADED);
            }
        };
        dlStartRunnable = new Runnable() {
            @Override
            public void run() {
                onDownloadStart();
            }
        };
        dlFinishRunnable = new Runnable() {
            @Override
            public void run() {
                onDownloadFinish();
            }
        };
        dlCancelRunnable = new Runnable() {
        @Override
        public void run() {
            onDownloadCancel();
        }
    };
        ulFinishRunnable = new Runnable() {
            @Override
            public void run() {
                onDownloadFinish();
                try {
                    String previewURL = boundDocument.getPreview_url();
                    if(previewURL != null && !previewURL.isEmpty())
                        adjustPreview();
                }
                catch(Exception ignored) {}
            }
        };
        int DOWNLOAD_BTN_SIZE = AndroidUtilities.dp(40);
        int twoDp = AndroidUtilities.dp(2);
        radialProgress.setProgressRect(0, 0, DOWNLOAD_BTN_SIZE + twoDp, DOWNLOAD_BTN_SIZE + twoDp);
        radialProgress.setProgressColor(ContextCompat.getColor(getContext(), R.color.loading_color));
        radialProgress.setBackground(ContextCompat.getDrawable(getContext(), android.R.color.transparent), true, true);
        dlDelegate = new DownloadDelegate() {
            @Override
            public String getDelegateKey() {
                return boundDocumentDelegateKey;
            }

            @Override
            public void didFileDownloadStarted(Document doc) {
                if(boundDocument == null || !doc.getId().equals(boundDocument.getId())) {
                    return;
                }
                try {
                    AppLoader.applicationHandler.post(dlStartRunnable);
                }
                catch(Exception ignored) {}
            }

            @Override
            public void didFileProgressUpdated(Document doc) {
                if(boundDocument == null || !doc.getId().equals(boundDocument.getId()))
                    return;
                try {
                    AppLoader.applicationHandler.post(updateProgressRunnable);
                }
                catch(Exception ignored) {}
            }

            @Override
            public void didFileDownloadFinished(Document doc) {
                if(boundDocument == null || !doc.getId().equals(boundDocument.getId()))
                    return;
                try {
                    AppLoader.applicationHandler.post(dlFinishRunnable);
                }
                catch(Exception ignored) {}
            }

            @Override
            public void didFileDownloadCancelled(Document doc, CancelReason why) {
                if(boundDocument == null || !doc.getId().equals(boundDocument.getId()))
                    return;
                try {
                    AppLoader.applicationHandler.post(dlCancelRunnable);
                }
                catch(Exception ignored) {}
            }
        };
        ulDelegate = new UploadDelegate() {
            @Override
            public String getDelegateKey() {
                return boundDocumentUploadDelegateKey;
            }
            @Override
            public void didFileUploadStarted(Document doc) {
                if(boundDocument == null || !doc.getId().equals(boundDocument.getId()))
                    return;
                try {
                    AppLoader.applicationHandler.post(dlStartRunnable);
                }
                catch(Exception ignored) {}
            }

            @Override
            public void didFileProgressUpdated(Document doc) {
                if(BuildConfig.DEBUG)
                    Log.d("DocumentView", "handling upload progress event");
                if(boundDocument == null || !doc.getId().equals(boundDocument.getId())) {
                    if(BuildConfig.DEBUG)
                        Log.d("DocumentView", "ids are different");
                    return;
                }
                try {
                    if(BuildConfig.DEBUG)
                        Log.d("DocumentView", "posting to UI thread");
                    AppLoader.applicationHandler.post(updateProgressRunnable);
                }
                catch(Exception ignored) {}
            }
            @Override
            public void didFileUploadFinished(Document doc) {
                if(boundDocument == null || !doc.getId().equals(boundDocument.getId()))
                    return;
                try {
                    AppLoader.applicationHandler.post(ulFinishRunnable);
                }
                catch(Exception ignored) {}
            }

            @Override
            public void didFileUploadCancelled(Document doc, DownloadDelegate.CancelReason why) {
                if(boundDocument == null || !doc.getId().equals(boundDocument.getId()))
                    return;
                try {
                    AppLoader.applicationHandler.post(dlCancelRunnable);
                }
                catch(Exception ignored) {}
            }
        };
        folderDelegate = new FolderInfoCache.FolderInfoDelegate() {
            @Override
            public void onFolderInfoReceived(long folder, int fileCount, String totalStringSize) {
                if(boundFolder != null && boundFolder.getId().equals(Long.valueOf(folder)))
                {
                    if(BuildConfig.DEBUG)
                        Log.d("DocumentView", String.format("received: folder = %d; fileCount = %d; totalStringSize = %s", folder, fileCount, totalStringSize));
                    String generalFmt = getResources().getString(R.string.filesize_format);
                    txtSize = new SpannableString(fileCount <= 0 ? getResources().getString(R.string.empty_folder) : fileCount == 1 ? String.format(generalFmt, getResources().getString(R.string.one_file), totalStringSize) : String.format(generalFmt, String.format(getResources().getString(R.string.n_files), fileCount), totalStringSize));
                    downloadText.setText(txtSize);
                }
            }
        };
        View divider = new View(getContext());
        divider.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.grey));
        RelativeLayout.LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, AndroidUtilities.dp(1));
        lp.leftMargin = AndroidUtilities.dp(56);
        lp.addRule(ALIGN_PARENT_BOTTOM);
        divider.setLayoutParams(lp);
        addView(divider);
    }
    public void bindDocument(Document doc)
    {
        if(BuildConfig.DEBUG)
            Log.d("DocumentView", "bindDoc requested");
        if(doc == null)
            return;
        if(BuildConfig.DEBUG)
            Log.d("DocumentView", String.format("id: %d", doc.getId()));
        if(boundFolder != null)
            FolderInfoCache.sharedInstance().removeDelegate(boundFolder.getId());
        if(boundDocument != null && !doc.getId().equals(boundDocument.getId())) {
            if(BuildConfig.DEBUG)
                Log.d("DocumentView", String.format("ids: %d %d", doc.getId(), boundDocument.getId()));
            unbindDoc(boundDocument);
        }
        boundDocument = doc;
        adjustPreview();
        boundDocumentDelegateKey = String.format("document%d_dl_delegate", doc.getId());
        boundDocumentUploadDelegateKey = String.format("document%d_ul_delegate", doc.getId());
        boundDocument.setOfflineAccessDelegate(offlineAccessDelegate);
        if(boundDocument.isUploading())
            boundDocument.addDelegate(ulDelegate);
        else
            boundDocument.addDelegate(dlDelegate);
        documentName.setText(doc.getTitle());
        txtSize = getSuitableSizeString();
        downloadText.setText(txtSize);
        if(boundDocument.getDownloadState() != DownloadState.DOWNLOADING)
        {
            overflowMenu.setImageResource(R.drawable.ic_overflow);
            radialProgress.setVisibility(View.GONE);
        }
        else
        {
            overflowMenu.setImageResource(R.drawable.ic_dl_cancel);
            radialProgress.setVisibility(View.VISIBLE);
            radialProgress.setProgress((float) boundDocument.getDownloadedSize() / boundDocument.getSize(), false);
        }
    }
    public void bindFolder(Folder folder)
    {
        if(boundDocument != null)
            unbindDoc(boundDocument);
        if(boundFolder != null)
            FolderInfoCache.sharedInstance().removeDelegate(boundFolder.getId());
        boundDocument = null;
        overflowMenu.setImageResource(R.drawable.ic_overflow);
        radialProgress.setVisibility(View.GONE);
        this.boundFolder = folder;
        adjustPreview();
        documentName.setText(folder.getFolderName());
        downloadText.setText(R.string.computing);
        FolderInfoCache.sharedInstance().getFolderInfo(folder.getId(), folderDelegate);
    }
    public void unbindDoc(Document doc)
    {
        if(BuildConfig.DEBUG)
            Log.d("DocumentView", "unbindDoc requested");
        if(boundDocument == null || !boundDocument.getId().equals(doc.getId()))
            return;
        radialProgress.setProgress(0, false);
        radialProgress.setVisibility(View.GONE);
        try {
            doc.removeDelegate(dlDelegate);
        }
        catch(Exception ignored) {}
        try {
            doc.removeDelegate(ulDelegate);
        }
        catch(Exception ignored) {}
        doc.setOfflineAccessDelegate(null);
        downloadText.setText("");
        boundDocument = null;
    }

    private void onDownloadStart()
    {
        if(BuildConfig.DEBUG)
            Log.d("DocumentView", "onDownloadStart");
        try {
            radialProgress.setVisibility(View.VISIBLE);
            radialProgress.setProgress((float) boundDocument.getDownloadedSize() / boundDocument.getSize(), false);
            radialProgress.setProgressColor(ContextCompat.getColor(getContext(), R.color.loading_color));
            txtSize = getSuitableSizeString();
            downloadText.setText(txtSize);
            overflowMenu.setImageResource(R.drawable.ic_dl_cancel);
        }
        catch(Exception ignored) {}
    }
    private void onDownloadFinish()
    {
        if(BuildConfig.DEBUG)
            Log.d("DocumentView", "onDownloadFinish");
        try {
            if(boundDocument != null) {
                extension.updateOfflineIconState(boundDocument.getDownloadState());
            }
            radialProgress.setVisibility(View.GONE);
            txtSize = getSuitableSizeString();
            downloadText.setText(txtSize);
            overflowMenu.setImageResource(R.drawable.ic_overflow);
        }
        catch(Exception ignored) {ignored.printStackTrace();}
    }
    private void onDownloadCancel()
    {
        try {
            radialProgress.setVisibility(View.GONE);
            txtSize = getSuitableSizeString();
            downloadText.setText(txtSize);
            overflowMenu.setImageResource(R.drawable.ic_overflow);
        }
        catch(Exception ignored) {}
    }
    public View getOverflowMenu() { return this.overflowMenu; }
    public void check(boolean animate)
    {
        if(checked)
            return;
        checked = true;
        checkView.check(animate);
    }
    public void uncheck(boolean animate)
    {
        if(!checked)
            return;
        checked = false;
        checkView.uncheck(animate);
    }
    /*public void setSelectionMode(boolean enabled)
    {
        if(checked )
        checkView.setVisibility(enabled ? VISIBLE : GONE);
    }*/
    private SpannableString getSuitableSizeString()
    {
        if(boundFolder != null)
            return null;
        if(boundDocument == null)
            return null;
        String generalFmt = getResources().getString(R.string.filesize_format);
        String sizeString = boundDocument.getDownloadState() == DownloadState.DOWNLOADING ? getContext().getString(R.string.downloading_size_format, Document.sizeToString(boundDocument.getDownloadedSize()), Document.sizeToString(boundDocument.getSize())) : boundDocument.getTxtSize();
        Document.Type type = Document.getTypeByExtension(boundDocument.getExt());
        String fmt = getResources().getString(Constants.extensionsStrings.get(type));

        SpannableString result = new SpannableString(type == Document.Type.PDF ? String.format(generalFmt, fmt, sizeString) : String.format(generalFmt, String.format(fmt, boundDocument.getExt().toUpperCase()), sizeString));
        if(boundDocument.getDownloadState() == DownloadState.DOWNLOADING)
            result.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.loading_color)), result.length()-sizeString.length(), result.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return result;
    }
    private void adjustPreview()
    {
        if(boundFolder != null)
        {
            docPreview.setVisibility(View.GONE);
            extension.drawFolder();
            return;
        }
        if(boundDocument == null)
            return;
        String previewURL = boundDocument.getPreview_url();
        extension.bindDocument(boundDocument);
        if(previewURL != null && !previewURL.isEmpty()) {
            docPreview.setVisibility(View.VISIBLE);
            ImageLoader.getInstance().loadImage(previewURL, docPreview, ImageLoader.POST_PROCESSING_CIRCLE|ImageLoader.POST_PROCESSING_SCALE, AndroidUtilities.dp(40), AndroidUtilities.dp(40), false);
        }
        else
            docPreview.setVisibility(View.GONE);
    }
}
