package com.andremacareno.vkdocs.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ScaleDrawable;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.andremacareno.vkdocs.AndroidUtilities;
import com.andremacareno.vkdocs.R;

/**
 * Created by andremacareno on 19/03/16.
 */
public class GalleryCheckView extends View {
    private Drawable check, uncheck;
    private boolean checked = false;

    private Rect checkRect = new Rect();
    public GalleryCheckView(Context context) {
        super(context);
        check = new ScaleDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_attach_check), 0, AndroidUtilities.dp(26), AndroidUtilities.dp(26));
        uncheck = new ScaleDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_attach_uncheck), 0, AndroidUtilities.dp(26), AndroidUtilities.dp(26));
        check.setLevel(10000);
        uncheck.setLevel(10000);
    }

    @Override
    public void onMeasure(int measureWidthSpec, int measureHeightSpec)
    {
        setMeasuredDimension(AndroidUtilities.dp(26), AndroidUtilities.dp(26));
    }
    public void setChecked(boolean checked) {
        this.checked = checked;
        postInvalidate();
    }

    @Override
    public void onDraw(Canvas canvas)
    {
        canvas.getClipBounds(checkRect);
        check.setBounds(checkRect);
        uncheck.setBounds(checkRect);
        if(checked)
            check.draw(canvas);
        else
            uncheck.draw(canvas);
    }
    @Override
    public boolean hasOverlappingRendering() {
        return false;
    }

}
