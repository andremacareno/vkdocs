package com.andremacareno.vkdocs.views;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatEditText;
import android.view.ContextThemeWrapper;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.andremacareno.vkdocs.AndroidUtilities;
import com.andremacareno.vkdocs.R;

/**
 * Created by andremacareno on 06/12/15.
 */
public class NewFolderConfirmView extends LinearLayout {
    private EditText title;
    private final Runnable kbdRunnable = new Runnable() {
        @Override
        public void run() {
            if(title != null)
            {
                title.requestFocus();
                ((InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE)).showSoftInput(title, InputMethodManager.SHOW_FORCED);
            }
        }
    };
    public NewFolderConfirmView(Context context) {
        super(context);
        init();
    }
    private void init()
    {
        final int margin = AndroidUtilities.dp(16);
        ViewGroup.LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        setLayoutParams(lp);
        setPadding(margin, margin, margin, margin);
        setOrientation(VERTICAL);
        title = new AppCompatEditText(new ContextThemeWrapper(getContext(), R.style.AppCompatEditTextTheme));
        title.setImeOptions(EditorInfo.IME_ACTION_DONE);
        title.setSingleLine(true);
        title.setTextColor(ContextCompat.getColor(getContext(), R.color.general_text));
        title.setMaxLines(1);
        int eightDp = AndroidUtilities.dp(8);
        title.setPadding(eightDp, eightDp, eightDp, eightDp);
        title.setLayoutParams(lp);
        /*try {
            Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
            f.setAccessible(true);
            f.set(title, R.drawable.default_cursor);
        } catch (Exception ignored) {
        }*/
        addView(title);
    }
    public String getEnteredTitle()
    {
        try {
            return title.getText().toString();
        }
        catch(Exception e) {
            return "";
        }
    }
    public EditText getEditText() { return this.title; }
    public void showKeyboard()
    {
        title.postDelayed(kbdRunnable, 400);
    }
}
