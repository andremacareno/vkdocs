package com.andremacareno.vkdocs.views;

import android.content.Context;
import android.util.AttributeSet;

import com.andremacareno.vkdocs.R;


/**
 * Created by Andrew on 25.04.2015.
 */
public class BigAvatarImageView extends RecyclingImageView {
    public BigAvatarImageView(Context context) {
        super(context);
    }

    public BigAvatarImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    @Override
    public void onMeasure(int measureWidthSpec, int heightMeasureSpec)
    {
        setMeasuredDimension(getResources().getDimensionPixelSize(R.dimen.big_avatar_size), getResources().getDimensionPixelSize(R.dimen.big_avatar_size));
    }
}
