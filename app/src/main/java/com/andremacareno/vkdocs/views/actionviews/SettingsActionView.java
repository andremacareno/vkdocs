package com.andremacareno.vkdocs.views.actionviews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.andremacareno.vkdocs.AndroidUtilities;
import com.andremacareno.vkdocs.R;
import com.andremacareno.vkdocs.views.ActionView;

/**
 * Created by andremacareno on 13/03/16.
 */
public class SettingsActionView extends ActionView {
    public ImageButton backButton;
    private static final String TAG = "SettingsScreenAV";

    public SettingsActionView(Context context) {
        super(context);
    }

    @Override
    public Object getTag() {
        return TAG;
    }

    @Override
    protected void initView() {
        setBackgroundColor(0xFF5181B8);
        setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, AndroidUtilities.dp(56)));
        LayoutParams backButtonLP = new LayoutParams(AndroidUtilities.dp(56), AndroidUtilities.dp(56));
        LayoutParams tvLP = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        tvLP.gravity = Gravity.CENTER_VERTICAL | Gravity.LEFT;
        backButtonLP.gravity = Gravity.TOP | Gravity.LEFT;

        backButton = new ImageButton(getContext());
        backButton.setPadding(AndroidUtilities.dp(16), AndroidUtilities.dp(16), AndroidUtilities.dp(16), AndroidUtilities.dp(16));
        backButton.setBackgroundResource(R.drawable.action_button_bg);
        backButton.setLayoutParams(backButtonLP);
        backButton.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        backButton.setImageResource(R.drawable.ic_back);

        TextView title = new TextView(getContext());
        title.setPadding(AndroidUtilities.dp(72), 0, 0, 0);
        title.setLayoutParams(tvLP);
        title.setText(R.string.settings);
        title.setTextColor(0xFFFFFFFF);
        title.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
        title.setTypeface(null, Typeface.BOLD);
        addView(title);
        addView(backButton);
    }
}
