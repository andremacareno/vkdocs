package com.andremacareno.vkdocs.views.settings;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.andremacareno.vkdocs.AndroidUtilities;
import com.andremacareno.vkdocs.R;

/**
 * Created by andremacareno on 17/03/16.
 */
public class SettingsSingleLineItem extends RelativeLayout {
    private TextView title;
    public SettingsSingleLineItem(Context context) {
        super(context);
        init();
    }

    public SettingsSingleLineItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    private void init()
    {
        setBackgroundResource(R.drawable.list_selector);
        int sixteenDp = AndroidUtilities.dp(16);
        LayoutParams tvLP = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        tvLP.addRule(RelativeLayout.CENTER_VERTICAL);
        tvLP.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        title = new TextView(getContext());
        title.setSingleLine(true);
        title.setEllipsize(TextUtils.TruncateAt.END);
        title.setPadding(sixteenDp, 0, sixteenDp, 0);
        title.setLayoutParams(tvLP);
        title.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);
        title.setTextColor(0xFF000000);
        addView(title);
        View divider = new View(getContext());
        LayoutParams dividerLP = new LayoutParams(LayoutParams.MATCH_PARENT, AndroidUtilities.dp(1));
        dividerLP.addRule(ALIGN_PARENT_BOTTOM);
        divider.setLayoutParams(dividerLP);
        divider.setBackgroundResource(R.color.grey);
        addView(divider);
    }

    /*@Override
    public void onMeasure(int measureWidthSpec, int measureHeightSpec)
    {
        setMeasuredDimension(AppLoader.sharedInstance().getResources().getDisplayMetrics().widthPixels, AndroidUtilities.dp(56));
    }*/
    public void setTextColor(int color) { this.title.setTextColor(color); }
    public void setText(int res) { this.title.setText(res); }
}
