package com.andremacareno.vkdocs.views;

import android.content.Context;
import android.util.AttributeSet;

import com.andremacareno.vkdocs.AndroidUtilities;


/**
 * Created by Andrew on 25.04.2015.
 */
public class SmallAvatarImageView extends RecyclingImageView {
    public SmallAvatarImageView(Context context) {
        super(context);
    }

    public SmallAvatarImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    @Override
    public void onMeasure(int measureWidthSpec, int heightMeasureSpec)
    {
        setMeasuredDimension(AndroidUtilities.dp(32), AndroidUtilities.dp(32));
    }
}
