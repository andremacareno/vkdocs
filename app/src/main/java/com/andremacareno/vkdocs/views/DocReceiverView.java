package com.andremacareno.vkdocs.views;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.TransitionDrawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.andremacareno.vkdocs.AndroidUtilities;
import com.andremacareno.vkdocs.BuildConfig;
import com.andremacareno.vkdocs.R;
import com.vk.sdk.api.model.VKApiUserFull;

/**
 * Created by andremacareno on 23/03/16.
 */
public class DocReceiverView extends FrameLayout {
    public interface OnRemoveListener
    {
        public void onRemove(VKApiUserFull user);
    }
    private ImageView avatar;
    private View del;
    private TextView username;
    private TransitionDrawable transition;
    private Animation ccwRotate, show, cwRotate, hide;
    private Animation.AnimationListener showListener, hideListener;
    private static final int DURATION = 90;
    private VKApiUserFull boundUser = null;
    private OnRemoveListener onRemoveListener;
    public DocReceiverView(Context context) {
        super(context);
        init();
    }
    private void init()
    {
        transition = (TransitionDrawable) ContextCompat.getDrawable(getContext(), R.drawable.rounded_rect_transition);
        if(Build.VERSION.SDK_INT >= 16)
            setBackground(transition);
        else
            setBackgroundDrawable(transition);

        ccwRotate = new RotateAnimation(0, -30, Animation.RELATIVE_TO_SELF,
                0.5f,  Animation.RELATIVE_TO_SELF, 0.5f);
        cwRotate = new RotateAnimation(-30, 0, Animation.RELATIVE_TO_SELF,
                0.5f,  Animation.RELATIVE_TO_SELF, 0.5f);
        show = new AlphaAnimation(0.0f, 1.0f);
        hide = new AlphaAnimation(1.0f, 0.0f);

        ccwRotate.setDuration(DURATION);
        cwRotate.setDuration(DURATION);
        show.setDuration((int) (DURATION*1.5));
        hide.setDuration((int) (DURATION*1.5));
        showListener = new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                try
                {
                    avatar.setVisibility(VISIBLE);
                    del.setVisibility(View.VISIBLE);
                }
                catch(Exception ignored) {}
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                try {
                    avatar.setVisibility(View.GONE);
                }
                catch(Exception ignored) {}
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        };
        hideListener = new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                try {
                    avatar.setVisibility(View.VISIBLE);
                    del.setVisibility(VISIBLE);
                }
                catch(Exception ignored) {}
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                try {
                    del.setVisibility(GONE);
                }
                catch(Exception ignored) {}
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        };
        show.setAnimationListener(showListener);
        hide.setAnimationListener(hideListener);

        avatar = new SmallAvatarImageView(getContext());
        del = new View(getContext());
        del.setBackgroundResource(R.drawable.remove_from_receivers);
        username = new TextView(getContext());
        username.setTextColor(0xFFFFFFFF);
        username.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
        username.setTypeface(Typeface.DEFAULT);
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        lp.gravity = Gravity.CENTER_VERTICAL;
        lp.rightMargin = AndroidUtilities.dp(8);
        lp.leftMargin = AndroidUtilities.dp(40);
        username.setSingleLine(true);
        username.setEllipsize(TextUtils.TruncateAt.END);
        username.setPadding(0, 0, 0, 0);
        FrameLayout.LayoutParams avatarLP = new FrameLayout.LayoutParams(AndroidUtilities.dp(32), AndroidUtilities.dp(32));
        avatar.setLayoutParams(avatarLP);
        del.setLayoutParams(avatarLP);
        del.setVisibility(View.GONE);
        username.setLayoutParams(lp);
        addView(avatar);
        addView(username);
        addView(del);
    }
    @Override
    public boolean onTouchEvent(MotionEvent ev)
    {
        if(ev.getAction() == MotionEvent.ACTION_DOWN)
        {
            transition.startTransition(DURATION);
            avatar.startAnimation(ccwRotate);
            del.startAnimation(show);
            return true;
        }
        else if(ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_CANCEL) {
            transition.reverseTransition(DURATION);
            avatar.startAnimation(cwRotate);
            del.startAnimation(hide);
            if(ev.getAction() == MotionEvent.ACTION_UP && onRemoveListener != null && boundUser != null)
                onRemoveListener.onRemove(boundUser);
            return true;
        }
        return super.onTouchEvent(ev);
    }
    @Override
    public void onDetachedFromWindow()
    {
        if(BuildConfig.DEBUG)
            Log.d("DocReceiverView", "clearing animations");
        try {
            avatar.clearAnimation();
        }
        catch(Exception ignored) {}
        try {
            del.clearAnimation();
        }
        catch(Exception ignored) {}
        super.onDetachedFromWindow();
    }
    public ImageView getAvatar() { return this.avatar; }
    public void bindUser(VKApiUserFull u) {
        this.boundUser = u;
        if(boundUser != null) {
            if(boundUser.first_name.isEmpty())
                username.setText(boundUser.last_name);
            else
                username.setText(String.format("%c. %s", boundUser.first_name.charAt(0), boundUser.last_name));
            del.setVisibility(View.GONE);
            avatar.setVisibility(View.VISIBLE);
        }
    }
    public void setOnRemoveListener(OnRemoveListener listener) { this.onRemoveListener = listener;}
}
