package com.andremacareno.vkdocs.views.settings;

import android.content.Context;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.andremacareno.vkdocs.AndroidUtilities;
import com.andremacareno.vkdocs.AppLoader;
import com.andremacareno.vkdocs.R;

/**
 * Created by andremacareno on 17/03/16.
 */
public class SettingsToggleItem extends RelativeLayout {
    private TextView title;
    private SwitchCompat sw;
    public SettingsToggleItem(Context context) {
        super(context);
        init();
    }

    public SettingsToggleItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    private void init()
    {
        setBackgroundResource(R.drawable.list_selector);
        int sixteenDp = AndroidUtilities.dp(16);
        RelativeLayout.LayoutParams switchLP = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        switchLP.addRule(RelativeLayout.CENTER_VERTICAL);
        switchLP.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        ContextThemeWrapper ctw = new ContextThemeWrapper(AppLoader.sharedInstance().getApplicationContext(), R.style.AppTheme);
        sw = new SwitchCompat(ctw);
        sw.setShowText(false);
        sw.setPadding(sixteenDp, 0, sixteenDp, 0);
        sw.setLayoutParams(switchLP);
        sw.setId(R.id.settings_switch);
        sw.setClickable(false);
        addView(sw);
        RelativeLayout.LayoutParams tvLP = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        tvLP.addRule(RelativeLayout.CENTER_VERTICAL);
        tvLP.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        tvLP.addRule(RelativeLayout.LEFT_OF, R.id.settings_switch);
        title = new TextView(getContext());
        title.setPadding(sixteenDp, 0, sixteenDp, 0);
        title.setSingleLine(true);
        title.setEllipsize(TextUtils.TruncateAt.END);
        title.setLayoutParams(tvLP);
        title.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);
        title.setTextColor(0xFF000000);
        addView(title);
        View divider = new View(getContext());
        LayoutParams dividerLP = new LayoutParams(LayoutParams.MATCH_PARENT, AndroidUtilities.dp(1));
        dividerLP.addRule(ALIGN_PARENT_BOTTOM);
        divider.setLayoutParams(dividerLP);
        divider.setBackgroundResource(R.color.grey);
        addView(divider);
    }

    /*@Override
    public void onMeasure(int measureWidthSpec, int measureHeightSpec)
    {
        setMeasuredDimension(AppLoader.sharedInstance().getResources().getDisplayMetrics().widthPixels, AndroidUtilities.dp(56));
    }*/

    public void setTitle(int res)
    {
        this.title.setText(res);
    }
    public void setSwitchCondition(boolean checked)
    {
        sw.setChecked(checked);
    }
}
