package com.andremacareno.vkdocs.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Build;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;

import com.andremacareno.vkdocs.AndroidUtilities;
import com.andremacareno.vkdocs.AppLoader;
import com.andremacareno.vkdocs.BuildConfig;

/**
 * Created by andremacareno on 15/03/16.
 */
public class ListSectionHeader extends View {
    private TextPaint textPaint;
    private CharSequence title;
    private int textOriginX, textOriginY, textSize;
    public ListSectionHeader(Context c)
    {
        super(c);
        init();
    }
    private void init()
    {
        textOriginX = AndroidUtilities.dp(16);
        textOriginY = AndroidUtilities.dp(48) / 2;
        textPaint = new TextPaint();
        textPaint.setColor(0x99000000);
        textPaint.setStyle(Paint.Style.FILL);
        textPaint.setAntiAlias(true);
        textSize = AndroidUtilities.dp(14);
        textPaint.setTextSize(textSize);
        textPaint.setTextAlign(Paint.Align.LEFT);
        textPaint.setLinearText(true);
        textPaint.setTypeface(Typeface.DEFAULT_BOLD);

        getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT >= 16) {
                    getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
                if(BuildConfig.DEBUG)
                    Log.d("ListHeader", "onLayout");
                postInvalidate();
            }
        });

    }
    public void setTitle(String title)
    {
        if(BuildConfig.DEBUG)
            Log.d("ListHeader", "setTitle");
        this.title = TextUtils.ellipsize(title, textPaint, AppLoader.sharedInstance().getResources().getDisplayMetrics().widthPixels - textOriginX*2, TextUtils.TruncateAt.END);
        postInvalidate();
    }
    @Override
    public boolean hasOverlappingRendering()
    {
        return false;
    }
    @Override
    public void onMeasure(int measureWidthSpec, int measureHeightSpec)
    {
        setMeasuredDimension(AppLoader.sharedInstance().getResources().getDisplayMetrics().widthPixels, AndroidUtilities.dp(48));
    }
    @Override
    public void onDraw(Canvas canvas)
    {
        if(BuildConfig.DEBUG)
            Log.d("ListHeader", "onDraw");
        super.onDraw(canvas);
        canvas.drawText(title, 0, title.length(), textOriginX, textOriginY+textSize/2, textPaint);
    }
}
