package com.andremacareno.vkdocs.views;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.util.AttributeSet;

import com.andremacareno.vkdocs.AppLoader;

/**
 * Created by Andrew on 07.01.2016.
 */
public class DocsListView extends BaseListView {
    private LayoutManager lm;

    public DocsListView(Context context) {
        super(context);
        init();
    }

    public DocsListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init()
    {
        if(lm == null)
            lm = new LinearLayoutManager(AppLoader.sharedInstance().getApplicationContext());
        setLayoutManager(lm);
    }
}
