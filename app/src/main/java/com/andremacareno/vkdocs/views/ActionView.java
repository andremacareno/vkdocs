package com.andremacareno.vkdocs.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import com.andremacareno.vkdocs.controllers.ActionViewController;


/**
 * Created by Andrew on 12.05.2015.
 */
public abstract class ActionView extends FrameLayout {
    private ActionViewController viewController;
    public ActionView(Context context) {
        super(context);
        initView();
    }

    public ActionView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public ActionView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }
    public ActionViewController getController() {
        return this.viewController;
    }
    public abstract Object getTag();
    protected abstract void initView();
    public void setController(ActionViewController controller)
    {
        if(controller == null)
            return;
        this.viewController = controller;
        controller.setActionView(this);
    }

    public boolean drawShadow() { return true; }
    @Override
    public boolean hasOverlappingRendering() {
        return false;
    }
}
