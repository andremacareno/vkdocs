package com.andremacareno.vkdocs.views;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ScaleDrawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;

import com.andremacareno.vkdocs.AndroidUtilities;
import com.andremacareno.vkdocs.DownloadState;
import com.andremacareno.vkdocs.R;
import com.andremacareno.vkdocs.dao.Document;

/**
 * Created by Andrew on 03.02.2016.
 */
public class DocumentIconView extends View {
    private Paint circlePaint, whiteRectPaint;
    private final Drawable pdfIcon, bookIcon, imgIcon, otherIcon, folderIcon;
    private Drawable whatToDraw = null;
    private final Drawable offlineDrawable;
    private final int viewCenter;
    private final int iconColor;
    private boolean drawOffline = false;
    private ColorFilter offlineCF = new PorterDuffColorFilter(0xFF7199C9, PorterDuff.Mode.SRC_ATOP);
    private ColorFilter cf;

    public DocumentIconView(Context context, AttributeSet attrs) {
        super(context, attrs);
        offlineDrawable = new ScaleDrawable(ContextCompat.getDrawable(context, R.drawable.ic_smartphone), Gravity.RIGHT|Gravity.BOTTOM, AndroidUtilities.dp(12), AndroidUtilities.dp(12));
        offlineDrawable.setLevel(10000);
        viewCenter = AndroidUtilities.dp(40) / 2;
        iconColor = ContextCompat.getColor(context, R.color.doc_icon_filter);
        cf = new PorterDuffColorFilter(iconColor, PorterDuff.Mode.SRC_ATOP);
        setBackgroundColor(ContextCompat.getColor(context, android.R.color.transparent));

        pdfIcon = ContextCompat.getDrawable(getContext(), R.drawable.ic_pdf);
        bookIcon = ContextCompat.getDrawable(getContext(), R.drawable.ic_book);
        imgIcon = ContextCompat.getDrawable(getContext(), R.drawable.ic_photo);
        otherIcon = ContextCompat.getDrawable(getContext(), R.drawable.ic_file);
        folderIcon = ContextCompat.getDrawable(getContext(), R.drawable.ic_folder);
        int eightDp = AndroidUtilities.dp(8);
        int twelveDp = AndroidUtilities.dp(12);
        Rect bounds = new Rect(eightDp, eightDp, viewCenter*2-eightDp, viewCenter*2-eightDp);
        Rect offlineBounds = new Rect(viewCenter*2-twelveDp, viewCenter*2-twelveDp, viewCenter*2, viewCenter*2);
        pdfIcon.setBounds(bounds);
        bookIcon.setBounds(bounds);
        imgIcon.setBounds(bounds);
        otherIcon.setBounds(bounds);
        folderIcon.setBounds(bounds);

        circlePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        circlePaint.setColor(ContextCompat.getColor(context, R.color.doc_icon_bg));
        whiteRectPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        whiteRectPaint.setColor(0xFFFFFFFF);
        offlineDrawable.setColorFilter(offlineCF);
        offlineDrawable.setBounds(offlineBounds);
    }

    public void bindDocument(Document doc)
    {
        drawOffline = doc.getDownloadState() == DownloadState.DOWNLOADED;
        if(doc.getPreview_url() != null && !doc.getPreview_url().isEmpty())
        {
            whatToDraw = null;
            postInvalidate();
            return;
        }
        Document.Type type = Document.getTypeByExtension(doc.getExt());
        if(type == Document.Type.PDF)
            whatToDraw = pdfIcon;
        else if(type == Document.Type.BOOK)
            whatToDraw = bookIcon;
        else if(type == Document.Type.IMG)
            whatToDraw = imgIcon;
        else
            whatToDraw = otherIcon;
        whatToDraw.setColorFilter(cf);
        postInvalidate();
    }
    public void drawFolder()
    {
        whatToDraw = folderIcon;
        whatToDraw.setColorFilter(cf);
        drawOffline = false;
        postInvalidate();
    }
    public void updateOfflineIconState(DownloadState newState)
    {
        drawOffline = newState == DownloadState.DOWNLOADED;
        postInvalidate();
    }
    @Override
    public void onDraw(Canvas canvas)
    {
        if(whatToDraw == null) {
            canvas.drawARGB(0, 0, 0, 0);
            if(drawOffline) {
                canvas.drawRect(offlineDrawable.getBounds(), whiteRectPaint);
                offlineDrawable.draw(canvas);
            }
            return;
        }
        canvas.drawARGB(0, 0, 0, 0);
        canvas.drawCircle(viewCenter, viewCenter, viewCenter, circlePaint);
        whatToDraw.draw(canvas);
        if(drawOffline) {
            canvas.drawRect(offlineDrawable.getBounds(), whiteRectPaint);
            offlineDrawable.draw(canvas);
        }
    }

}
