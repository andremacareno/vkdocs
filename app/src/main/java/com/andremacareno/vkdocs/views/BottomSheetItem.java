package com.andremacareno.vkdocs.views;

import android.content.Context;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.andremacareno.vkdocs.AndroidUtilities;
import com.andremacareno.vkdocs.R;
import com.andremacareno.vkdocs.objs.BottomSheetAction;

/**
 * Created by Andrew on 08.01.2016.
 */
public class BottomSheetItem extends LinearLayout {
    private ImageView icon;
    private TextView action;
    private ColorFilter iconFilter;

    public BottomSheetItem(Context context) {
        super(context);
        init();
    }
    private void init()
    {
        setOrientation(HORIZONTAL);
        setBackgroundResource(R.drawable.list_selector);
        setClickable(true);
        setPadding(AndroidUtilities.dp(16), AndroidUtilities.dp(8), AndroidUtilities.dp(16), AndroidUtilities.dp(8));
        iconFilter = new PorterDuffColorFilter(ContextCompat.getColor(getContext(), R.color.bottom_sheet_mask), PorterDuff.Mode.SRC_ATOP);
        View v = inflate(getContext(), R.layout.bottom_sheet_itm, this);
        icon = (ImageView) v.findViewById(R.id.action_icon);
        icon.setColorFilter(iconFilter);
        action = (TextView) v.findViewById(R.id.action_text);
    }
    public void fill(BottomSheetAction action)
    {
        if(action == null)
            return;
        if(action.getIconRes() != 0)
        {
            icon.setVisibility(View.VISIBLE);
            icon.setImageResource(action.getIconRes());
        }
        else
        {
            icon.setVisibility(View.GONE);
        }
        this.action.setText(action.getActionString() != null ? action.getActionString() : "");
    }
}
