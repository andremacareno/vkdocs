package com.andremacareno.vkdocs.views;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.andremacareno.vkdocs.AndroidUtilities;
import com.andremacareno.vkdocs.AppLoader;
import com.andremacareno.vkdocs.BuildConfig;
import com.andremacareno.vkdocs.cache.ImageLoader;
import com.andremacareno.vkdocs.listadapters.FriendsListAdapter;
import com.vk.sdk.api.model.VKApiUserFull;

import java.util.ArrayList;

/**
 * Created by Andrew on 07.01.2016.
 */
public class ShareReceiverListView extends BaseListView {
    public interface SelectionDelegate
    {
        public void didSelectionCountChanged(int selectionCount);
    }
    private LayoutManager lm;
    private int spanCount;
    private final ArrayList<VKApiUserFull> selection = new ArrayList<>();
    private final SparseArray<VKApiUserFull> idsToSelection = new SparseArray<>();
    private SelectionAdapter adapter;
    private SelectionDelegate delegateRef;
    private FriendsListAdapter.FriendsAdapterDelegate bottomAdapterDelegate;
    private LinearLayout.LayoutParams defaultLP, reducedLP;
    private static Handler hideHandler = new Handler(Looper.getMainLooper());
    private final Runnable hideRunnable = new Runnable() {
        @Override
        public void run() {
            setVisibility(GONE);
        }
    };

    public ShareReceiverListView(Context context) {
        super(context);
        init();
    }

    public ShareReceiverListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init()
    {
        defaultLP = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, AndroidUtilities.dp(96));
        reducedLP = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, AndroidUtilities.dp(48));
        setLayoutParams(reducedLP);
        setOverScrollMode(OVER_SCROLL_NEVER);
        if(lm == null) {
            /*lm = new FlowLayoutManager() {
                private Size size = new Size(0, 0);
                private final int height = AndroidUtilities.dp(32);
                private TextPaint textPaint;
                @Override
                protected Size getSizeForItem(int i) {
                    try {
                        String username;
                        synchronized (selection) {
                            VKApiUserFull userFull = selection.get(i);
                            username = String.format("%s %s", userFull.first_name, userFull.last_name);
                        }
                        View v = getChildAt(0);
                        size.upd(((DocReceiverView) v).getFullWidth(username), height);
                    } catch (Exception e) {
                        e.printStackTrace();
                        size.upd(AndroidUtilities.dp(100), height);
                    }
                    return size;
                }
            };*/
            spanCount = AndroidUtilities.screenWidthInDp(AppLoader.sharedInstance().getApplicationContext().getResources().getDisplayMetrics().widthPixels) / 100 - 1;
            if(spanCount <= 0)
                spanCount = 1;
            lm = new GridLayoutManager(getContext(), spanCount);
            adapter = new SelectionAdapter();
        }
        setLayoutManager(lm);
        setAdapter(adapter);
    }
    public void setDelegate(SelectionDelegate delegate) { this.delegateRef = delegate; }
    public void remove(int userId)
    {
        try {
            synchronized(idsToSelection)
            {
                synchronized(selection)
                {
                    VKApiUserFull user = idsToSelection.get(userId);
                    idsToSelection.remove(userId);
                    int index = selection.indexOf(user);
                    selection.remove(index);
                    adapter.notifyItemRemoved(index);
                    int size = selection.size();
                    if(delegateRef != null) {
                        delegateRef.didSelectionCountChanged(size);
                    }
                    if(size <= 0) {
                        hideHandler.removeCallbacksAndMessages(null);
                        hideHandler.postDelayed(hideRunnable, 200);
                    }
                    else if(size <= spanCount)
                        setLayoutParams(reducedLP);
                }
            }
        }
        catch(Exception ignored) {}
    }
    public void add(VKApiUserFull user)
    {
        try {
            synchronized(idsToSelection)
            {
                synchronized(selection)
                {
                    int position = selection.size();
                    if(position == 0) {
                        hideHandler.removeCallbacksAndMessages(null);
                        setVisibility(VISIBLE);
                    }
                    else if(position == spanCount)
                        setLayoutParams(defaultLP);
                    idsToSelection.put(user.id, user);
                    selection.add(user);
                    adapter.notifyItemInserted(position);
                    smoothScrollToPosition(position);
                    if(delegateRef != null)
                        delegateRef.didSelectionCountChanged(position + 1);
                }
            }
        }
        catch(Exception ignored) {}
    }
    public String getIds() {
        String ids = "";
        try {
            synchronized(selection)
            {
                for(VKApiUserFull u : selection)
                {
                    if(ids.isEmpty())
                        ids += String.valueOf(u.id);
                    else
                        ids += String.format(",%d", u.id);
                }
            }
        }
        catch(Exception ignored) { return null; }
        return ids;
    }
    public boolean isSelected(int userId)
    {
        synchronized(idsToSelection)
        {
            return idsToSelection.get(userId) != null;
        }
    }
    public void clearSelection()
    {
        try {
            synchronized(idsToSelection)
            {
                synchronized(selection)
                {
                    idsToSelection.clear();
                    selection.clear();
                    //adapter.notifyDataSetChanged();
                }
            }
        }
        catch(Exception ignored) {}
    }
    public void setBottomAdapterDelegate(FriendsListAdapter.FriendsAdapterDelegate delegate) { this.bottomAdapterDelegate = delegate; }

    private class SelectionAdapter extends RecyclerView.Adapter<SelectionAdapter.ViewHolder>
    {
        private DocReceiverView.OnRemoveListener onRemoveListener;
        public SelectionAdapter()
        {
            onRemoveListener = new DocReceiverView.OnRemoveListener() {
                @Override
                public void onRemove(VKApiUserFull user) {
                    try {
                        remove(user.id);
                        bottomAdapterDelegate.onRemoveFromTopList(user);
                    }
                    catch(Exception ignored) {}
                }
            };
        }
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            DocReceiverView v = new DocReceiverView(getContext());
            RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, AndroidUtilities.dp(32));
            lp.leftMargin = lp.rightMargin = lp.topMargin = lp.bottomMargin = AndroidUtilities.dp(8);

            v.setLayoutParams(lp);
            return new ViewHolder(v);
        }
        @Override
        public void onViewAttachedToWindow(ViewHolder viewHolder)
        {
            int position = viewHolder.getAdapterPosition();
            if(position < 0)
                return;
            synchronized(selection)
            {
                VKApiUserFull itm = selection.get(position);
                String url = AndroidUtilities.isLowDPIScreen() ? itm.photo_50 : itm.photo_100;
                if(BuildConfig.DEBUG)
                    Log.d("FriendsListItem", String.format("url = %s", url));
                ImageLoader.getInstance().loadImage(url, viewHolder.userInfo.getAvatar(), ImageLoader.POST_PROCESSING_CIRCLE, AndroidUtilities.dp(32), AndroidUtilities.dp(32), false);
            }
        }
        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            synchronized(selection)
            {
                VKApiUserFull userFull = selection.get(position);
                holder.userInfo.bindUser(userFull);
            }
        }

        @Override
        public int getItemCount() {
            synchronized(selection)
            {
                return selection.size();
            }
        }

        public class ViewHolder extends RecyclerView.ViewHolder{
            public DocReceiverView userInfo;

            public ViewHolder(View itemView) {
                super(itemView);
                this.userInfo = (DocReceiverView) itemView;
                this.userInfo.setOnRemoveListener(onRemoveListener);
            }
        }
    }
}
