package com.andremacareno.vkdocs.views;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

import com.andremacareno.vkdocs.ThreadUtils;

/**
 * Created by Andrew on 11.06.2014.
 */
public class BaseListView extends RecyclerView {
    //statement variables
    private boolean isScrolling = false;
    private final String TAG = "BaseListView";
    private final int SLOW_SCROLLING = 70;
    private boolean strictPause;
    public BaseListView(Context context) {
        super(context);
        init();
    }

    public BaseListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    public BaseListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }
    private void init()
    {
        if(isInEditMode())
            return;
        setBackgroundResource(0);
        strictPause = Runtime.getRuntime().availableProcessors() <= 2;
        RecyclerView.OnScrollListener l = new RecyclerView.OnScrollListener()
        {

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                isScrolling = newState != SCROLL_STATE_IDLE;
                if(!isScrolling)
                    ThreadUtils.unpauseAll();
            }
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                int speed = Math.max(Math.abs(dx), Math.abs(dy));
                //Log.d(TAG, String.format("speed = %d", speed));
                if(isScrolling && (strictPause || speed > SLOW_SCROLLING))
                    ThreadUtils.pauseAll();
                else
                    ThreadUtils.unpauseAll();
            }
        };
        addOnScrollListener(l);
    }
    @Override
    public boolean hasOverlappingRendering()
    {
        return false;
    }
    @Override
    public void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        ThreadUtils.unpauseAll();
    }
    @Override
    public void onDetachedFromWindow()
    {

        super.onDetachedFromWindow();
        ThreadUtils.unpauseAll();
    }
}
