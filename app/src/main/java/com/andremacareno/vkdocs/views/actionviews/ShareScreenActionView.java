package com.andremacareno.vkdocs.views.actionviews;

import android.content.Context;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.andremacareno.vkdocs.AndroidUtilities;
import com.andremacareno.vkdocs.R;
import com.andremacareno.vkdocs.views.ActionView;
import com.andremacareno.vkdocs.views.ShareReceiverListView;

import java.lang.reflect.Field;

/**
 * Created by andremacareno on 13/03/16.
 */
public class ShareScreenActionView extends ActionView {

    public ImageButton backButton;
    public ShareReceiverListView selection;
    public EditText searchEditText;
    private static final String TAG = "FileScreenAV";

    public ShareScreenActionView(Context context) {
        super(context);
    }

    @Override
    public Object getTag() {
        return TAG;
    }

    @Override
    protected void initView() {
        setBackgroundColor(0xFF5181B8);
        setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        LayoutParams backButtonLP = new LayoutParams(AndroidUtilities.dp(56), AndroidUtilities.dp(56));
        FrameLayout.LayoutParams rightContainerLP = new LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        rightContainerLP.gravity = Gravity.LEFT;
        rightContainerLP.leftMargin = AndroidUtilities.dp(64);
        rightContainerLP.topMargin = rightContainerLP.bottomMargin = rightContainerLP.rightMargin = 0;
        backButtonLP.gravity = Gravity.TOP | Gravity.LEFT;

        backButton = new ImageButton(getContext());
        backButton.setPadding(AndroidUtilities.dp(16), AndroidUtilities.dp(16), AndroidUtilities.dp(16), AndroidUtilities.dp(16));
        backButton.setBackgroundResource(R.drawable.action_button_bg);
        backButton.setLayoutParams(backButtonLP);
        backButton.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        backButton.setImageResource(R.drawable.ic_back);

        /*TextView title = new TextView(getContext());
        title.setPadding(AndroidUtilities.dp(72), 0, 0, 0);
        title.setLayoutParams(tvLP);
        title.setText(R.string.share_a_doc);
        title.setTextColor(0xFFFFFFFF);
        title.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
        title.setTypeface(null, Typeface.BOLD);
        addView(title);*/
        LinearLayout rightSide = new LinearLayout(getContext());
        rightSide.setLayoutParams(rightContainerLP);
        rightSide.setOrientation(LinearLayout.VERTICAL);
        selection = new ShareReceiverListView(getContext());
        selection.setVisibility(GONE);

        searchEditText = new EditText(getContext());
        searchEditText.setBackgroundColor(0x00000000);
        searchEditText.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 17);
        searchEditText.setHint(R.string.send_message_to);
        searchEditText.setTextColor(0xFFFFFFFF);
        searchEditText.setHintTextColor(0xffb0cce1);
        searchEditText.setSingleLine(true);
        int padding = AndroidUtilities.dp(8);
        searchEditText.setPadding(padding, padding, padding*10, padding);
        try {
            Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
            f.setAccessible(true);
            f.set(searchEditText, R.drawable.search_cursor);
        } catch (Exception ignored) {
        }

        LinearLayout.LayoutParams editTextLP = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, AndroidUtilities.dp(56));
        editTextLP.gravity = Gravity.CENTER_VERTICAL | Gravity.LEFT;
        searchEditText.setLayoutParams(editTextLP);
        searchEditText.setVisibility(VISIBLE);
        rightSide.addView(selection);
        rightSide.addView(searchEditText);


        addView(rightSide);
        addView(backButton);
    }
}
