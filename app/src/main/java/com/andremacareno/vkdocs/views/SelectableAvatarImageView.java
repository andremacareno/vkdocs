package com.andremacareno.vkdocs.views;

import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

import com.andremacareno.vkdocs.AndroidUtilities;

public class SelectableAvatarImageView extends RecyclingImageView {
    private final int ON_SELECTION_PADDING;
    ValueAnimator.AnimatorUpdateListener updateListener;
    private final DecelerateInterpolator decInterpolator = new DecelerateInterpolator(1.1f);
    private final AccelerateInterpolator accInterpolator = new AccelerateInterpolator(1.1f);
    public SelectableAvatarImageView(Context context) {
        super(context);
        ON_SELECTION_PADDING = AndroidUtilities.dp(6);
        init();
    }

    public SelectableAvatarImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        ON_SELECTION_PADDING = AndroidUtilities.dp(6);
        init();
    }
    private void init()
    {
        updateListener = new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator){
                int currentPadding = (int) valueAnimator.getAnimatedValue();
                setPadding(currentPadding, currentPadding, currentPadding, currentPadding);
            }
        };
    }
    @Override
    public void onMeasure(int measureWidthSpec, int heightMeasureSpec)
    {
        setMeasuredDimension(AndroidUtilities.dp(40), AndroidUtilities.dp(40));
    }

    public void select(boolean animate)
    {
        if(!animate || AndroidUtilities.isLowDPIScreen() || AndroidUtilities.isHdpi())
            setPadding(ON_SELECTION_PADDING, ON_SELECTION_PADDING, ON_SELECTION_PADDING, ON_SELECTION_PADDING);
        else
        {
            ValueAnimator animator = ValueAnimator.ofInt(0, ON_SELECTION_PADDING);
            animator.addUpdateListener(updateListener);
            animator.setDuration(150);
            animator.setInterpolator(decInterpolator);
            animator.start();
        }
    }
    public void deselect(boolean animate)
    {
        if(!animate || AndroidUtilities.isLowDPIScreen() || AndroidUtilities.isHdpi())
            setPadding(0, 0, 0, 0);
        else
        {
            ValueAnimator animator = ValueAnimator.ofInt(ON_SELECTION_PADDING, 0);
            animator.addUpdateListener(updateListener);
            animator.setInterpolator(accInterpolator);
            animator.setDuration(150);
            animator.start();
        }
    }
}