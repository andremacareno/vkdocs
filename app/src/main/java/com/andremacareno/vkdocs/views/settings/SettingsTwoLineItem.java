package com.andremacareno.vkdocs.views.settings;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.andremacareno.vkdocs.AndroidUtilities;
import com.andremacareno.vkdocs.R;

/**
 * Created by andremacareno on 17/03/16.
 */
public class SettingsTwoLineItem extends LinearLayout {
    private TextView title;
    private TextView subtitle;
    public SettingsTwoLineItem(Context context) {
        super(context);
        init();
    }

    public SettingsTwoLineItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    private void init()
    {
        setOrientation(VERTICAL);
        setBackgroundResource(R.drawable.list_selector);
        int sixteenDp = AndroidUtilities.dp(16);
        setPadding(0, sixteenDp, 0, 0);
        LayoutParams tvLP = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        title = new TextView(getContext());
        title.setSingleLine(true);
        title.setEllipsize(TextUtils.TruncateAt.END);
        title.setPadding(sixteenDp, 0, sixteenDp, 0);
        title.setLayoutParams(tvLP);
        title.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);
        title.setTextColor(0xFF000000);
        addView(title);
        subtitle = new TextView(getContext());
        subtitle.setSingleLine(true);
        subtitle.setEllipsize(TextUtils.TruncateAt.END);
        subtitle.setPadding(sixteenDp, 0, sixteenDp, sixteenDp);
        subtitle.setLayoutParams(tvLP);
        subtitle.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
        addView(subtitle);
        View divider = new View(getContext());
        LayoutParams dividerLP = new LayoutParams(LayoutParams.MATCH_PARENT, AndroidUtilities.dp(1));
        dividerLP.gravity = Gravity.BOTTOM;
        divider.setLayoutParams(dividerLP);
        divider.setBackgroundResource(R.color.grey);
        addView(divider);
    }

    /*@Override
    public void onMeasure(int measureWidthSpec, int measureHeightSpec)
    {
        setMeasuredDimension(AppLoader.sharedInstance().getResources().getDisplayMetrics().widthPixels, AndroidUtilities.dp(72));
    }*/
    public void setTitle(int res)
    {
        this.title.setText(res);
    }
    public void setSubtitle(int res)
    {
        this.subtitle.setText(res);
    }
    public void setSubtitle(String str)
    {
        this.subtitle.setText(str);
    }
    public void setSubtitleColor(int color)
    {
        this.subtitle.setTextColor(color);
    }
}
