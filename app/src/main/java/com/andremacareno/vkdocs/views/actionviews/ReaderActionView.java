package com.andremacareno.vkdocs.views.actionviews;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.view.Gravity;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.andremacareno.vkdocs.AndroidUtilities;
import com.andremacareno.vkdocs.R;
import com.andremacareno.vkdocs.views.ActionView;

/**
 * Created by andremacareno on 13/03/16.
 */
public class ReaderActionView extends ActionView {
    public ImageButton backButton;
    private static final String TAG = "ReaderScreenAV";

    public ReaderActionView(Context context) {
        super(context);
    }

    @Override
    public Object getTag() {
        return TAG;
    }

    @Override
    protected void initView() {
        setBackgroundColor(0xFFF5F5F5);
        setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, AndroidUtilities.dp(56)));
        LayoutParams backButtonLP = new LayoutParams(AndroidUtilities.dp(56), AndroidUtilities.dp(56));
        LayoutParams tvLP = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        tvLP.gravity = Gravity.CENTER_VERTICAL | Gravity.LEFT;
        backButtonLP.gravity = Gravity.TOP | Gravity.LEFT;
        backButton = new ImageButton(getContext());
        backButton.setPadding(AndroidUtilities.dp(16), AndroidUtilities.dp(16), AndroidUtilities.dp(16), AndroidUtilities.dp(16));
        backButton.setBackgroundResource(R.drawable.list_selector);
        backButton.setImageResource(R.drawable.ic_back);
        backButton.setLayoutParams(backButtonLP);
        backButton.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        backButton.setColorFilter(new PorterDuffColorFilter(0xFF7A7A7A, PorterDuff.Mode.SRC_ATOP));
        addView(backButton);
    }
    @Override
    public boolean drawShadow() { return false; }
}
