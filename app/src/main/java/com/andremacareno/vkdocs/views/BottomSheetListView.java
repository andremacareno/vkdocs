package com.andremacareno.vkdocs.views;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;

import com.andremacareno.vkdocs.AndroidUtilities;
import com.andremacareno.vkdocs.AppLoader;
import com.andremacareno.vkdocs.R;
import com.andremacareno.vkdocs.controllers.ViewControllable;
import com.andremacareno.vkdocs.controllers.ViewController;
import com.andremacareno.vkdocs.views.additional.ExactlyHeightLayoutManager;

/**
 * Created by andremacareno on 04/04/15.
 */
public class BottomSheetListView extends BaseListView implements ViewControllable {
    private ViewController controller;
    private LayoutManager lm;
    public BottomSheetListView(Context context) {
        super(context);
        init();
    }

    public BottomSheetListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BottomSheetListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }
    private void init()
    {
        if(isInEditMode())
            return;
        setBackgroundColor(ContextCompat.getColor(getContext(), R.color.white));
        if(lm == null)
            lm = new ExactlyHeightLayoutManager(AppLoader.sharedInstance().getApplicationContext(), AndroidUtilities.dp(208));
        setLayoutManager(lm);
        setHasFixedSize(true);
    }
    @Override
    public void setController(ViewController vc)
    {
        this.controller = vc;
    }

    @Override
    public ViewController getController() {
        return controller;
    }
    public void setNewElementCount(int count)
    {
        ((ExactlyHeightLayoutManager) lm).setSpecifiedHeight(count * AndroidUtilities.dp(52));
    }
}
