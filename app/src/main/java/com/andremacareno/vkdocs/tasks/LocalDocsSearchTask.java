package com.andremacareno.vkdocs.tasks;

import com.andremacareno.asynccore.BackgroundTask;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.vkdocs.dao.Document;
import com.andremacareno.vkdocs.objs.DocListItemWrap;

import java.util.ArrayList;

/**
 * Created by Andrew on 07.01.2016.
 */
public class LocalDocsSearchTask extends BackgroundTask {
    private final ArrayList<DocListItemWrap> source;
    private final ArrayList<DocListItemWrap> results = new ArrayList<>();
    private final String query;
    public LocalDocsSearchTask(ArrayList<DocListItemWrap> src, String query)
    {
        this.source = new ArrayList<>(src);
        this.query = query.toLowerCase();
    }
    @Override
    public String getTaskKey() {
        return String.format("local_search_%s", query);
    }

    @Override
    public int getTaskFailedNotificationId() {
        return 0;
    }

    @Override
    public int getTaskCompletedNotificationId() {
        return NotificationCenter.didLocalSearchFinished;
    }

    @Override
    protected void work() throws Exception {
        for(DocListItemWrap wrap : source)
        {
            try
            {
                if(wrap.getType() != DocListItemWrap.Type.DOCUMENT)
                    continue;
                Document doc = wrap.castDoc();
                String title = doc.getTitle().toLowerCase();
                String extension = doc.getExt().toLowerCase();
                String fullName = String.format("%s.%s", title, extension);
                if(title.contains(query) || extension.contains(query) || fullName.contains(query))
                    results.add(wrap);
            }
            catch(Exception ignored) {}
        }
    }
    public ArrayList<DocListItemWrap> getResults() { return this.results; }
}
