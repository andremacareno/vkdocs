package com.andremacareno.vkdocs.tasks;

import com.andremacareno.asynccore.BackgroundTask;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.vkdocs.AppLoader;
import com.andremacareno.vkdocs.cache.BookCache;
import com.andremacareno.vkdocs.cache.FileCache;
import com.andremacareno.vkdocs.cache.FolderInfoCache;
import com.andremacareno.vkdocs.dao.DaoSession;
import com.andremacareno.vkdocs.dao.Document;
import com.andremacareno.vkdocs.dao.DocumentDao;
import com.andremacareno.vkdocs.objs.DocListItemWrap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by andremacareno on 24/04/15.
 */
public class GetOfflineListTask extends BackgroundTask {
    private ArrayList<DocListItemWrap> entries = new ArrayList<>();
    private Map<Long, DocListItemWrap> idsToEntries = new HashMap<>();
    public GetOfflineListTask()
    {
    }

    @Override
    public String getTaskKey() {
        return "retrieve_offline";
    }

    @Override
    public int getTaskFailedNotificationId() {
        return 0;
    }

    @Override
    public int getTaskCompletedNotificationId() {
        return NotificationCenter.didOfflineListReceived;
    }

    @Override
    protected void work() throws Exception {
        DaoSession daoSession = AppLoader.sharedInstance().daoMaster.newSession();
        DocumentDao docsDao = daoSession.getDocumentDao();
        List<Document> docEntries = docsDao.queryBuilder().orderDesc(DocumentDao.Properties.Id).list();
        for(Document doc : docEntries)
        {
            FolderInfoCache.sharedInstance().addDocFolderLink(doc.getId(), doc.getFolderId());
            if(doc.getExt().toLowerCase().equals("epub"))
                BookCache.sharedInstance().addBook(doc.getId());
            if(doc.getPath() == null || doc.getPath().isEmpty())
                continue;
            DocListItemWrap wrap = FileCache.getInstance().getDocument(doc);
            entries.add(wrap);
            idsToEntries.put(doc.getId(), wrap);
        }
    }
    public List<DocListItemWrap> getOfflineDocs() {
        return this.entries;
    }
    public Map<Long, DocListItemWrap> getMap() {
        return this.idsToEntries;
    }
    @Override
    public boolean isCritical() { return true; }
}
