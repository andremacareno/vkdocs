package com.andremacareno.vkdocs.tasks;

import android.util.Log;

import com.andremacareno.asynccore.BackgroundTask;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.vkdocs.BuildConfig;
import com.andremacareno.vkdocs.cache.BookCache;
import com.andremacareno.vkdocs.cache.FileCache;
import com.andremacareno.vkdocs.dao.Document;
import com.andremacareno.vkdocs.objs.BookWrap;
import com.andremacareno.vkdocs.objs.DocListItemWrap;

import java.util.ArrayList;

import nl.siegmann.epublib.domain.Book;
import nl.siegmann.epublib.domain.MediaType;
import nl.siegmann.epublib.epub.EpubReader;
import nl.siegmann.epublib.service.MediatypeService;

/**
 * Created by Andrew on 07.01.2016.
 */
public class LoadBooksDashboardTask extends BackgroundTask {
    private final ArrayList<BookWrap> results = new ArrayList<>();
    private final ArrayList<MediaType> coverMediatypes = new ArrayList<MediaType>() {{
        add(MediatypeService.JPG);
        add(MediatypeService.GIF);
        add(MediatypeService.PNG);
    }};
    @Override
    public String getTaskKey() {
        return "loading_books_dashboard";
    }

    @Override
    public int getTaskFailedNotificationId() {
        return 0;
    }

    @Override
    public int getTaskCompletedNotificationId() {
        return NotificationCenter.didBooksDashboardLoaded;
    }

    @Override
    protected void work() throws Exception {
        try
        {
            EpubReader reader = new EpubReader();
            synchronized(BookCache.sharedInstance().ids)
            {
                for(Long id : BookCache.sharedInstance().ids)
                {
                    DocListItemWrap wr = FileCache.getInstance().getDocumentById(id);
                    if(wr == null || wr.getType() != DocListItemWrap.Type.DOCUMENT)
                        continue;
                    Document doc = wr.castDoc();
                    if(doc.getPath() == null || doc.getPath().isEmpty())
                        continue;
                    try
                    {
                        Book bk = reader.readEpubLazy(doc.getPath(), "UTF-8", coverMediatypes);
                        results.add(new BookWrap(doc.getPath(), bk));
                    }
                    catch(Exception e) { e.printStackTrace(); }
                }
            }
        }
        catch(Exception e) { e.printStackTrace(); }
        if(BuildConfig.DEBUG)
            Log.d("LoadBooks", String.format("count = %d", results.size()));
    }
    @Override
    public boolean isCritical() { return true; }
    public ArrayList<BookWrap> getResults() { return this.results; }
}
