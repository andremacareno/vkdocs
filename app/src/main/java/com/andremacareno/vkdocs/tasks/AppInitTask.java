package com.andremacareno.vkdocs.tasks;

import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.andremacareno.asynccore.BackgroundTask;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.vkdocs.AppLoader;
import com.andremacareno.vkdocs.BuildConfig;
import com.andremacareno.vkdocs.dao.DaoMaster;

import java.io.File;

import de.greenrobot.dao.query.QueryBuilder;

/**
 * Created by andremacareno on 17/03/16.
 */
public class AppInitTask extends BackgroundTask{
    @Override
    public String getTaskKey() {
        return "app_init";
    }

    @Override
    public int getTaskFailedNotificationId() {
        return NotificationCenter.didAppInitFailed;
    }

    @Override
    public int getTaskCompletedNotificationId() {
        return NotificationCenter.didAppInitialized;
    }

    @Override
    protected void work() throws Exception {
        if(BuildConfig.DEBUG)
            Log.d("AppInit", "init started");
        QueryBuilder.LOG_SQL = true;
        QueryBuilder.LOG_VALUES = true;
        SQLiteOpenHelper helper = new DaoMaster.DevOpenHelper(AppLoader.sharedInstance().getApplicationContext(), "vkdocs-db", null);
        AppLoader.sharedInstance().daoMaster = new DaoMaster(helper.getWritableDatabase());
        try
        {
            File tmpDir = new File(AppLoader.sharedInstance().tmpCacheLocation());
            if(tmpDir.exists() && tmpDir.isDirectory())
            {
                for(File f : tmpDir.listFiles())
                    f.delete();
            }
        }
        catch(Exception ignored) {}
        if(BuildConfig.DEBUG)
            Log.d("AppInit", "init finished");
    }
    @Override
    public boolean isCritical() { return true; }
}
