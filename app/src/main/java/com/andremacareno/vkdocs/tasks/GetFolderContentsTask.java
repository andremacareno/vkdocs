package com.andremacareno.vkdocs.tasks;

import com.andremacareno.asynccore.BackgroundTask;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.vkdocs.AppLoader;
import com.andremacareno.vkdocs.cache.FileCache;
import com.andremacareno.vkdocs.dao.DaoSession;
import com.andremacareno.vkdocs.dao.Document;
import com.andremacareno.vkdocs.dao.DocumentDao;
import com.andremacareno.vkdocs.objs.DocListItemWrap;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.dao.query.QueryBuilder;

/**
 * Created by Andrew on 07.05.2015.
 */
public class GetFolderContentsTask extends BackgroundTask {
    private long folder;

    private final ArrayList<DocListItemWrap> folderContents = new ArrayList<>();


    public GetFolderContentsTask(long folder) {
        this.folder = folder;
    }

    @Override
    public String getTaskKey() {
        return String.format("get_folder%d_info", folder);
    }
    @Override
    public int getTaskFailedNotificationId() {
        return 0;
    }

    @Override
    public int getTaskCompletedNotificationId() {
        return NotificationCenter.didFolderContentLoaded;
    }

    @Override
    protected void work() throws Exception {
        try
        {
            DaoSession session = AppLoader.sharedInstance().daoMaster.newSession();
            DocumentDao docDao = session.getDocumentDao();
            QueryBuilder<Document> qb = docDao.queryBuilder();
            List<Document> docsInFolder = qb.where(DocumentDao.Properties.FolderId.eq(folder)).list();
            for(Document doc : docsInFolder)
                folderContents.add(FileCache.getInstance().getDocument(doc));
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
    public ArrayList<DocListItemWrap> getFolderContents() { return this.folderContents; }
}
