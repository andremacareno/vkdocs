package com.andremacareno.vkdocs.tasks;

import com.andremacareno.asynccore.BackgroundTask;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.vkdocs.cache.FileCache;
import com.andremacareno.vkdocs.dao.Document;
import com.andremacareno.vkdocs.objs.DocListItemWrap;
import com.vk.sdk.api.VKResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Andrew on 07.01.2016.
 */
public class HandleDocsTask extends BackgroundTask {
    private VKResponse response;
    private int count;
    private final ArrayList<DocListItemWrap> newDocs = new ArrayList<>();
    public HandleDocsTask(VKResponse response)
    {
        this.response = response;
    }
    @Override
    public String getTaskKey() {
        return "handle_docs_task";
    }

    @Override
    public int getTaskFailedNotificationId() {
        return 0;
    }

    @Override
    public int getTaskCompletedNotificationId() {
        return NotificationCenter.didDocumentsHandled;
    }

    @Override
    protected void work() throws Exception {
        try {
            JSONObject resp = response.json.getJSONObject("response");
            count = resp.getInt("count");
            JSONArray items = resp.getJSONArray("items");
            for(int i = 0; i < items.length(); i++)
            {
                JSONObject obj = items.getJSONObject(i);
                Document model = new Document();
                model.fillFromJSON(obj);
                newDocs.add(FileCache.getInstance().getDocument(model));
            }
        }
        catch(Exception ignored) {}
    }
    public int getTotalCount() { return this.count; }
    public ArrayList<DocListItemWrap> getParsedDocs() { return this.newDocs; }
}
