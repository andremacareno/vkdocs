package com.andremacareno.vkdocs.tasks;

import android.database.Cursor;
import android.provider.MediaStore;
import android.util.Log;

import com.andremacareno.asynccore.BackgroundTask;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.vkdocs.AppLoader;
import com.andremacareno.vkdocs.BuildConfig;
import com.andremacareno.vkdocs.objs.GalleryImageEntry;

import java.util.ArrayList;

/**
 * Created by andremacareno on 16/08/15.
 */
public class LoadImagesFromGalleryTask extends BackgroundTask {
    private volatile boolean cancelled = false;
    private final ArrayList<GalleryImageEntry> photos = new ArrayList<>();
    private static final String[] projection = {
            MediaStore.Images.Media._ID,
            MediaStore.Images.Media.DATA
    };
    @Override
    public String getTaskKey() {
        return "attachmenu_thumbnails_request";
    }

    @Override
    public int getTaskFailedNotificationId() {
        return 0;
    }

    @Override
    public int getTaskCompletedNotificationId() {
        return NotificationCenter.didRecentFromGalleryLoaded;
    }

    @Override
    protected void work() throws Exception {
        Cursor cursor = null;
        try {
            cursor = MediaStore.Images.Media.query(AppLoader.sharedInstance().getApplicationContext().getContentResolver(), MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, null, MediaStore.Images.Media.DATE_TAKEN + " desc");
            if (cursor != null) {
                if(BuildConfig.DEBUG)
                    Log.d("LoadImagesFromGallery", String.format("received %d entries", cursor.getCount()));
                int imageIdColumn = cursor.getColumnIndex(MediaStore.Images.Media._ID);
                int dataColumn = cursor.getColumnIndex(MediaStore.Images.Media.DATA);

                while (cursor.moveToNext() && !cancelled) {
                    pauseIfNeed();
                    int imageId = cursor.getInt(imageIdColumn);
                    String path = cursor.getString(dataColumn);

                    if (path == null || path.length() == 0) {
                        continue;
                    }
                    GalleryImageEntry entry = new GalleryImageEntry(path, imageId);
                    synchronized(photos)
                    {
                        photos.add(entry);
                    }
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                try {
                    cursor.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public boolean isCancelled() { return this.cancelled; }
    public ArrayList<GalleryImageEntry> getEntries() { return this.photos; }
    public void cancel() { this.cancelled = true; }
}
