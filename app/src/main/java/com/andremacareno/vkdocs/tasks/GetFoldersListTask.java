package com.andremacareno.vkdocs.tasks;

import android.util.Log;

import com.andremacareno.asynccore.BackgroundTask;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.vkdocs.AppLoader;
import com.andremacareno.vkdocs.BuildConfig;
import com.andremacareno.vkdocs.dao.DaoSession;
import com.andremacareno.vkdocs.dao.Folder;
import com.andremacareno.vkdocs.dao.FolderDao;
import com.andremacareno.vkdocs.objs.DocListItemWrap;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by andremacareno on 24/04/15.
 */
public class GetFoldersListTask extends BackgroundTask {
    private ArrayList<DocListItemWrap> entries = new ArrayList<>();
    //private Map<Long, DocListItemWrap> idsToEntries = new HashMap<>();
    public GetFoldersListTask()
    {
    }

    @Override
    public String getTaskKey() {
        return "retrieve_offline";
    }

    @Override
    public int getTaskFailedNotificationId() {
        return 0;
    }

    @Override
    public int getTaskCompletedNotificationId() {
        return NotificationCenter.didFolderListLoaded;
    }

    @Override
    protected void work() throws Exception {
        DaoSession daoSession = AppLoader.sharedInstance().daoMaster.newSession();
        FolderDao docsDao = daoSession.getFolderDao();
        List<Folder> folderEntries = docsDao.queryBuilder().orderDesc(FolderDao.Properties.Id).list();
        for(Folder f : folderEntries)
        {
            DocListItemWrap wrap = new DocListItemWrap(f);
            entries.add(wrap);
            //idsToEntries.put(f.getId(), wrap);
        }
        if(BuildConfig.DEBUG)
            Log.d("GetFolders", String.format("found %d folders", folderEntries.size()));
    }
    public List<DocListItemWrap> getOfflineFolders() {
        return this.entries;
    }
    @Override
    public boolean isCritical() { return true; }
    /*public Map<Long, DocListItemWrap> getMap() {
        return this.idsToEntries;
    }*/
}
