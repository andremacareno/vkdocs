package com.andremacareno.vkdocs.tasks;

import com.andremacareno.asynccore.BackgroundTask;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.vkdocs.AppLoader;
import com.andremacareno.vkdocs.dao.Folder;

/**
 * Created by Andrew on 07.05.2015.
 */
public class NewFolderTask extends BackgroundTask {
    private Folder createdFolder;
    public NewFolderTask(Folder f) {
        this.createdFolder = f;
    }

    @Override
    public String getTaskKey() {
        return "new_folder_task";
    }
    @Override
    public int getTaskFailedNotificationId() {
        return 0;
    }

    @Override
    public int getTaskCompletedNotificationId() {
        return NotificationCenter.didNewFolderSaved;
    }

    @Override
    protected void work() throws Exception {
        AppLoader.sharedInstance().daoMaster.newSession().getFolderDao().insert(createdFolder);
    }
    public Folder getResult() { return this.createdFolder; }
    @Override
    public boolean isCritical() { return true; }
}
