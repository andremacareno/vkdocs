package com.andremacareno.vkdocs.tasks;

import android.os.Process;

import com.andremacareno.vkdocs.AppLoader;
import com.andremacareno.vkdocs.dao.DaoSession;
import com.andremacareno.vkdocs.dao.Document;
import com.andremacareno.vkdocs.dao.DocumentDao;
import com.andremacareno.vkdocs.dao.Folder;
import com.andremacareno.vkdocs.dao.FolderDao;

import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class DatabaseWritingThread extends Thread {
    private static volatile DatabaseWritingThread _instance;
    BlockingQueue<Object> tasks;
    private DatabaseWritingThread()
    {
        tasks = new LinkedBlockingQueue<>();
    }
    @Override
    public void run()
    {
        android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_LOWEST);
        while(!isInterrupted())
        {
            Object task;
            try {
                task = tasks.take();
                if(task instanceof Document)
                {
                    DaoSession daoSession = AppLoader.sharedInstance().daoMaster.newSession();
                    DocumentDao documentDao = daoSession.getDocumentDao();
                    documentDao.insertOrReplace((Document) task);
                }
                else if(task instanceof ArrayList)
                {
                    DaoSession daoSession = AppLoader.sharedInstance().daoMaster.newSession();
                    DocumentDao documentDao = daoSession.getDocumentDao();
                    documentDao.insertOrReplaceInTx((ArrayList<Document>) task);
                }
                else if(task instanceof Folder)
                {
                    DaoSession daoSession = AppLoader.sharedInstance().daoMaster.newSession();
                    FolderDao folderDao = daoSession.getFolderDao();
                    folderDao.insertOrReplace((Folder) task);
                }
            } catch (InterruptedException e) {
                return;
            }
            catch(Exception ignored) {}
        }
    }
    public void addTask(Document task) {
        tasks.add(task);
    }
    public void addTask(Folder task) {
        tasks.add(task);
    }
    public static DatabaseWritingThread getInstance()
    {
        if(_instance == null)
        {
            synchronized(DatabaseWritingThread.class)
            {
                if(_instance == null) {
                    _instance = new DatabaseWritingThread();
                    _instance.start();
                }
            }
        }
        return _instance;
    }
}