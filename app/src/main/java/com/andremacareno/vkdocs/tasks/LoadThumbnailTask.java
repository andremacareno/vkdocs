package com.andremacareno.vkdocs.tasks;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.andremacareno.asynccore.BackgroundTask;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.vkdocs.AndroidUtilities;
import com.andremacareno.vkdocs.AppLoader;
import com.andremacareno.vkdocs.BuildConfig;
import com.andremacareno.vkdocs.cache.ImageCache;
import com.andremacareno.vkdocs.cache.RecyclingBitmapDrawable;
import com.andremacareno.vkdocs.objs.GalleryImageEntry;

public class LoadThumbnailTask extends BackgroundTask {
    private GalleryImageEntry entry;
    public LoadThumbnailTask(GalleryImageEntry e)
    {
        this.entry = e;
    }
    @Override
    public String getTaskKey() {
        return this.entry.getCacheKey();
    }

    @Override
    public int getTaskFailedNotificationId() {
        return 0;
    }

    @Override
    public int getTaskCompletedNotificationId() {
        return NotificationCenter.didGalleryPhotoThumbnailCached;
    }

    @Override
    protected void work() throws Exception {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(entry.getPath(), o);
        final int REQUIRED_WIDTH= AndroidUtilities.dp(100);
        final int REQUIRED_HEIGHT=REQUIRED_WIDTH;
        int scale=1;
        while(o.outWidth/scale/2>=REQUIRED_WIDTH && o.outHeight/scale/2>=REQUIRED_HEIGHT)
            scale*=2;

        //Decode with inSampleSize
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inJustDecodeBounds = false;
        opts.inSampleSize=scale;
        pauseIfNeed();
        Bitmap bmp = BitmapFactory.decodeFile(entry.getPath(), opts);
        if(bmp == null)
            return;
        if(BuildConfig.DEBUG)
            Log.d("LoadThumbs", String.format("image size: %d x %d", opts.outWidth, opts.outHeight));
        float scaleFactor = opts.outWidth > opts.outHeight ? opts.outWidth / AndroidUtilities.dp(98) : opts.outHeight / AndroidUtilities.dp(98);
        opts.outWidth /= scaleFactor;
        opts.outHeight /= scaleFactor;
        pauseIfNeed();
        Bitmap finalBmp = Bitmap.createScaledBitmap(bmp, opts.outWidth, opts.outHeight, true);
        ImageCache cache = ImageCache.getInstance();
        cache.addBitmapToCache(this.entry.getCacheKey(), new RecyclingBitmapDrawable(AppLoader.sharedInstance().getResources(), finalBmp), false);
    }
}
