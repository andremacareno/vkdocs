package com.andremacareno.vkdocs.tasks;

import com.andremacareno.asynccore.BackgroundTask;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.vkdocs.AppLoader;
import com.andremacareno.vkdocs.cache.FolderInfoCache;
import com.andremacareno.vkdocs.dao.DaoSession;
import com.andremacareno.vkdocs.dao.Document;
import com.andremacareno.vkdocs.dao.DocumentDao;

import java.util.List;

/**
 * Created by andremacareno on 24/04/15.
 */
public class ComputeOfflineSizeTask extends BackgroundTask {
    private String totalSize = "";
    public ComputeOfflineSizeTask()
    {
    }

    @Override
    public String getTaskKey() {
        return "compute_offline_size";
    }

    @Override
    public int getTaskFailedNotificationId() {
        return 0;
    }

    @Override
    public int getTaskCompletedNotificationId() {
        return NotificationCenter.didOfflineTotalSizeComputed;
    }

    @Override
    protected void work() throws Exception {
        DaoSession daoSession = AppLoader.sharedInstance().daoMaster.newSession();
        DocumentDao docsDao = daoSession.getDocumentDao();
        List<Document> docEntries = docsDao.queryBuilder().orderDesc(DocumentDao.Properties.Id).list();
        long numTotalSize = 0;
        for(Document doc : docEntries)
        {
            if(doc.getPath() == null || doc.getPath().isEmpty())
                continue;
            numTotalSize += doc.getSize();
        }
        totalSize = Document.sizeToString(numTotalSize);
        FolderInfoCache.sharedInstance().totalOfflineSize = totalSize;
    }
    public String getResult() { return this.totalSize; }
    @Override
    public boolean isCritical() { return true; }
}
