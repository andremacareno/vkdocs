package com.andremacareno.vkdocs.tasks;

import com.andremacareno.asynccore.BackgroundTask;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.vkdocs.objs.GalleryImageEntry;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;

/**
 * Created by andremacareno on 24/04/15.
 */
public class ConvertGalleryEntriesToInputMsgTask extends BackgroundTask {
    private ArrayList<GalleryImageEntry> entries;
    private final Map<Integer, GalleryImageEntry> idsToEntries;
    private Integer[] selection;
    private ArrayList<File> pathsOfImages = new ArrayList<>();
    public ConvertGalleryEntriesToInputMsgTask(ArrayList<GalleryImageEntry> entries, Map<Integer, GalleryImageEntry> idsToEntries, HashSet<Integer> selection)
    {
        this.entries = entries;
        this.idsToEntries = idsToEntries;
        this.selection = selection.toArray(new Integer[selection.size()]);
    }

    @Override
    public String getTaskKey() {
        return "convert_gallery_entries_to_inputMessageContent";
    }

    @Override
    public int getTaskFailedNotificationId() {
        return 0;
    }

    @Override
    public int getTaskCompletedNotificationId() {
        return NotificationCenter.didQuickSendModelsCreated;
    }

    @Override
    protected void work() throws Exception {
        for(int selectedId : selection)
        {
            synchronized(idsToEntries)
            {
                GalleryImageEntry entry = idsToEntries.get(selectedId);
                if(entry == null)
                    continue;
                File file = new File(entry.getPath());
                pathsOfImages.add(file);
            }
        }
    }
    public ArrayList<File> getPaths() {
        return this.pathsOfImages;
    }
}
