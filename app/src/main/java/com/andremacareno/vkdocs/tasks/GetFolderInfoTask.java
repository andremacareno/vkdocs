package com.andremacareno.vkdocs.tasks;

import android.util.Log;

import com.andremacareno.asynccore.BackgroundTask;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.vkdocs.AppLoader;
import com.andremacareno.vkdocs.BuildConfig;
import com.andremacareno.vkdocs.dao.DaoSession;
import com.andremacareno.vkdocs.dao.Document;
import com.andremacareno.vkdocs.dao.DocumentDao;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import de.greenrobot.dao.query.QueryBuilder;

/**
 * Created by Andrew on 07.05.2015.
 */
public class GetFolderInfoTask extends BackgroundTask {
    private final Object monitor = new Object();
    private final AtomicLong folder = new AtomicLong(0);

    private final AtomicLong folderSize = new AtomicLong(0);
    private final AtomicInteger filesCount = new AtomicInteger(0);


    public GetFolderInfoTask(long folder) {
        this.folder.set(folder);
    }

    @Override
    public String getTaskKey() {
        return String.format("get_folder%d_info", folder.get());
    }
    @Override
    public int getTaskFailedNotificationId() {
        return 0;
    }

    @Override
    public int getTaskCompletedNotificationId() {
        return NotificationCenter.didFolderInfoRetrieved;
    }

    @Override
    protected void work() throws Exception {
        try
        {
            synchronized(monitor)
            {
                DaoSession session = AppLoader.sharedInstance().daoMaster.newSession();
                DocumentDao docDao = session.getDocumentDao();
                QueryBuilder<Document> qb = docDao.queryBuilder();
                List<Document> docsInFolder = qb.where(DocumentDao.Properties.FolderId.eq(folder)).list();
                for(Document doc : docsInFolder)
                {
                    filesCount.incrementAndGet();
                    folderSize.addAndGet(doc.getSize());
                }
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            synchronized(monitor)
            {
                folderSize.set(0);
                filesCount.set(0);
            }
        }
        if(BuildConfig.DEBUG)
            Log.d("GetFolderInfo", String.format("folder %d: folderSize = %d, filesCount = %d", folder.get(), folderSize.get(), filesCount.get()));
    }
    public long getFolderSize() { synchronized(monitor) { return this.folderSize.get(); } }
    public int getFilesCount() { synchronized(monitor) { return this.filesCount.get(); } }
    public String getStringSize() { synchronized(monitor) { return Document.sizeToString(this.folderSize.get()); } }
    @Override
    public boolean isCritical() { return true; }
}
