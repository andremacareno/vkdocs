package com.andremacareno.vkdocs.tasks;

import android.os.Process;

import com.andremacareno.vkdocs.AppLoader;
import com.andremacareno.vkdocs.cache.BookCache;
import com.andremacareno.vkdocs.dao.DaoSession;
import com.andremacareno.vkdocs.dao.Document;
import com.andremacareno.vkdocs.dao.DocumentDao;
import com.andremacareno.vkdocs.dao.Folder;
import com.andremacareno.vkdocs.dao.FolderDao;

import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class DatabaseRemoveThread extends Thread {
    private static volatile DatabaseRemoveThread _instance;
    BlockingQueue<Object> tasks;
    private DatabaseRemoveThread()
    {
        tasks = new LinkedBlockingQueue<>();
    }
    @Override
    public void run()
    {
        Process.setThreadPriority(Process.THREAD_PRIORITY_LOWEST);
        while(!isInterrupted())
        {
            Object task;
            try {
                task = tasks.take();
                if(task instanceof Document)
                {
                    Document doc = (Document) task;
                    if(doc.getExt().toLowerCase().equals("epub"))
                        BookCache.sharedInstance().notifyBookRemoved(doc.getId());
                    DaoSession daoSession = AppLoader.sharedInstance().daoMaster.newSession();
                    DocumentDao documentDao = daoSession.getDocumentDao();
                    documentDao.delete((Document) task);
                }
                else if(task instanceof ArrayList)
                {
                    DaoSession daoSession = AppLoader.sharedInstance().daoMaster.newSession();
                    DocumentDao documentDao = daoSession.getDocumentDao();
                    documentDao.deleteInTx((ArrayList<Document>) task);
                }
                else if(task instanceof Folder)
                {
                    DaoSession daoSession = AppLoader.sharedInstance().daoMaster.newSession();
                    FolderDao folderDao = daoSession.getFolderDao();
                    folderDao.delete((Folder) task);
                }
            } catch (InterruptedException e) {
                return;
            }
            catch(Exception ignored) {}
        }
    }
    public void addTask(Document task) {
        tasks.add(task);
    }
    public void addTask(Folder task) {
        tasks.add(task);
    }
    public void addTask(ArrayList<Document> task) {
        tasks.add(task);
    }
    public static DatabaseRemoveThread getInstance()
    {
        if(_instance == null)
        {
            synchronized(DatabaseRemoveThread.class)
            {
                if(_instance == null) {
                    _instance = new DatabaseRemoveThread();
                    _instance.start();
                }
            }
        }
        return _instance;
    }
}