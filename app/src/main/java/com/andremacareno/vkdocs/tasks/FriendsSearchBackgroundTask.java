package com.andremacareno.vkdocs.tasks;

import com.andremacareno.asynccore.BackgroundTask;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.vk.sdk.api.model.VKApiUserFull;
import com.vk.sdk.api.model.VKList;

/**
 * Created by Andrew on 07.01.2016.
 */
public class FriendsSearchBackgroundTask extends BackgroundTask {
    private final VKList<VKApiUserFull> source;
    private final VKList<VKApiUserFull> results = new VKList<>();
    private final String query;
    public FriendsSearchBackgroundTask(VKList<VKApiUserFull> src, String query)
    {
        this.source = new VKList<>(src);
        this.query = query.toLowerCase();
    }
    @Override
    public String getTaskKey() {
        return String.format("friends_search_%s", query);
    }

    @Override
    public int getTaskFailedNotificationId() {
        return 0;
    }

    @Override
    public int getTaskCompletedNotificationId() {
        return NotificationCenter.didLocalSearchFinished;
    }

    @Override
    protected void work() throws Exception {
        for(VKApiUserFull user : source)
        {
            try
            {
                String fullName = String.format("%s %s", user.first_name, user.last_name).toLowerCase();
                if(user.first_name.toLowerCase().contains(query) || user.last_name.contains(query) || fullName.contains(query))
                    results.add(user);
            }
            catch(Exception ignored) {}
        }
    }
    @Override
    public boolean isCritical() { return true; }
    public VKList<VKApiUserFull> getResults() { return this.results; }
}
