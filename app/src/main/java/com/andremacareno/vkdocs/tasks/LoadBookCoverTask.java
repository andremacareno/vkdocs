package com.andremacareno.vkdocs.tasks;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.andremacareno.asynccore.BackgroundTask;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.vkdocs.AndroidUtilities;
import com.andremacareno.vkdocs.AppLoader;
import com.andremacareno.vkdocs.cache.ImageCache;
import com.andremacareno.vkdocs.cache.LoadImageTask;
import com.andremacareno.vkdocs.cache.RecyclingBitmapDrawable;

import nl.siegmann.epublib.domain.Book;

/**
 * Created by andremacareno on 16/08/15.
 */
public class LoadBookCoverTask extends BackgroundTask {
    private Book book;
    private final int reqWidth, reqHeight;
    public LoadBookCoverTask(Book book)
    {
        this.book = book;
        reqWidth = AndroidUtilities.dp(90);
        reqHeight = AndroidUtilities.dp(120);
    }
    @Override
    public String getTaskKey() {
        return String.format("load_book_cover_%s", book.getTitle());
    }

    @Override
    public int getTaskFailedNotificationId() {
        return NotificationCenter.didBookCoverRetrieveFailed;
    }

    @Override
    public int getTaskCompletedNotificationId() {
        return NotificationCenter.didBookCoverRetrieved;
    }

    @Override
    protected void work() throws Exception {
        byte[] newData = book.getCoverImage().getData();
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(newData, 0, newData.length, options);

        options.inSampleSize = LoadImageTask.calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;

        LoadImageTask.addInBitmapOptions(options, ImageCache.getInstance());

        Bitmap bmp = BitmapFactory.decodeByteArray(newData, 0, newData.length, options);

        ImageCache.getInstance().addBitmapToCache(book.getTitle(), new RecyclingBitmapDrawable(AppLoader.sharedInstance().getResources(), bmp));
    }
    @Override
    public Object getNotificationObject() { return getTaskKey(); }


}
