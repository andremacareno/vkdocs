package com.andremacareno.vkdocs;

import com.andremacareno.vkdocs.dao.Document;

/**
 * Created by Andrew on 08.01.2016.
 */
public interface DownloadDelegate {
    enum CancelReason {USER_CANCELLED, NETWORK_PROBLEM}
    String getDelegateKey();
    void didFileDownloadStarted(Document doc);
    void didFileProgressUpdated(Document doc);
    void didFileDownloadFinished(Document doc);
    void didFileDownloadCancelled(Document doc, CancelReason why);
}