package com.andremacareno.vkdocs.vk;

import android.util.Log;

import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.vkdocs.BuildConfig;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKAccessTokenTracker;

/**
 * Created by Andrew on 07.01.2016.
 */
public class VKAuthObserver extends VKAccessTokenTracker {
    public enum AuthState {AUTH_REQUIRED, AUTH_OK}
    private static final String TAG = "AuthObserver";
    @Override
    public void onVKAccessTokenChanged(VKAccessToken oldToken, VKAccessToken newToken) {
        if(newToken == null) {
            if(BuildConfig.DEBUG)
                Log.d(TAG, "Access token became invalid; broadcasting logging out event...");
            didLoggedOut();
        }
        else if(BuildConfig.DEBUG)
            Log.d(TAG, "Received new access token");
    }
    public void didLoggedOut()
    {
        NotificationCenter.getInstance().postNotification(NotificationCenter.didLoggedOut);
    }
    public static AuthState getAuthState()
    {
        return VKAccessToken.currentToken() == null ? AuthState.AUTH_REQUIRED : AuthState.AUTH_OK;
    }
}
