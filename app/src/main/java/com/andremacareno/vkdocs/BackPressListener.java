package com.andremacareno.vkdocs;

/**
 * Created by Andrew on 01.05.2015.
 */
public interface BackPressListener {
    void onBackPressed();
}
