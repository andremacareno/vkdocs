package com.andremacareno.vkdocs;

import com.andremacareno.vkdocs.dao.Document;

import java.util.HashMap;

/**
 * Created by Andrew on 07.01.2016.
 */
public class Constants {
    public static final String APP_PREFS = "app_prefs";
    public static final String AUTH_FRAGMENT = "auth_fragment";
    public static final String SHARE_WITH_FRIEND_FRAGMENT = "share_with_friend_fragment";
    public static final String VK_DOC_ID = "doc_id";
    public static final String VK_ORDER = "order";
    public static final String VK_ATTACHMENT = "attachment";
    public static final String IMAGE_CACHE_DIR = "thumbs";
    public static final String GALLERY_PHOTO_KEY = "gallery_photo_%d";
    public static final String ARG_DOC_ID = "doc_id";
    public static final String PREFS_AUTOSAVE = "autosave";
    public static final String PREFS_REDUCE_ANIMATIONS = "reduce_anims";
    public static final String PREFS_DELETE_WITHOUT_CONFIRMATION = "del_wo_confirmation";
    public static final String ARG_START_OFFLINE = "start_offline";
    public static final String PREFS_USER_ID = "auth_user_id";
    public static final String PREFS_FIRST_NAME = "auth_first_name";
    public static final String PREFS_LAST_NAME = "auth_last_name";
    public static final String PREFS_PHOTO = "auth_photo";
    public static final String ARG_BOOK_PATH = "book_path";


    public static final HashMap<String, Document.Type> extensions = new HashMap<String, Document.Type>() {{
        put("pdf", Document.Type.PDF);
        put("epub", Document.Type.BOOK);
        put("jpg", Document.Type.IMG);
        put("jpeg", Document.Type.IMG);
        put("gif", Document.Type.IMG);
        put("tiff", Document.Type.IMG);
        put("png", Document.Type.IMG);
    }};
    public static final HashMap<Document.Type, Integer> extensionsStrings = new HashMap<Document.Type, Integer>() {{
        put(Document.Type.PDF, R.string.pdf_document);
        put(Document.Type.BOOK, R.string.book);
        put(Document.Type.IMG, R.string.img);
        put(Document.Type.OTHER, R.string.other_docs);
    }};
}
