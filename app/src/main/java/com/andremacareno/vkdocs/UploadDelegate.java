package com.andremacareno.vkdocs;

import com.andremacareno.vkdocs.dao.Document;

/**
 * Created by Andrew on 08.01.2016.
 */
public interface UploadDelegate {
    String getDelegateKey();
    void didFileUploadStarted(Document doc);
    void didFileProgressUpdated(Document doc);
    void didFileUploadFinished(Document doc);
    void didFileUploadCancelled(Document doc, DownloadDelegate.CancelReason why);
}