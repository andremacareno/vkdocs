package com.andremacareno.vkdocs;

import com.andremacareno.vkdocs.dao.Document;

/**
 * Created by Andrew on 13.01.2016.
 */
public class Configuration {
    public static int AUTOSAVE_ALL = 0;
    public static int AUTOSAVE_EPUB = 1;
    public static int DISABLE_AUTOSAVE = 2;
    public boolean reducedAnimations;
    public boolean deleteWithoutConfirmation;
    public int autosave;
    public static int PACKAGE_SIZE_MOBILE_NETWORK = 4096;
    public static int PACKAGE_SIZE_WIFI = 8192;
    private static volatile Configuration _instance;
    private Configuration()
    {
        this.reducedAnimations = AppLoader.sharedInstance().sharedPreferences().getBoolean(Constants.PREFS_REDUCE_ANIMATIONS, false);
        this.deleteWithoutConfirmation = AppLoader.sharedInstance().sharedPreferences().getBoolean(Constants.PREFS_DELETE_WITHOUT_CONFIRMATION, false);
        this.autosave = AppLoader.sharedInstance().sharedPreferences().getInt(Constants.PREFS_AUTOSAVE, DISABLE_AUTOSAVE);
    }
    public static Configuration sharedInstance()
    {
        if(_instance == null)
        {
            synchronized(Configuration.class)
            {
                if(_instance == null)
                    _instance = new Configuration();
            }
        }
        return _instance;
    }
    public boolean shouldSave(Document doc)
    {
        try
        {
            if(autosave == DISABLE_AUTOSAVE)
                return false;
            else if(autosave == AUTOSAVE_ALL)
                return true;
            else if(autosave == AUTOSAVE_EPUB)
                return doc.getExt().toLowerCase().equals("epub");
        }
        catch(Exception ignored) { return false; }
        return false;
    }
}
