package com.andremacareno.vkdocs.controllers;

import android.util.Log;
import android.view.View;

import com.andremacareno.vkdocs.BuildConfig;
import com.andremacareno.vkdocs.listadapters.NewDocMenuAdapter;
import com.andremacareno.vkdocs.objs.BottomSheetAction;
import com.andremacareno.vkdocs.views.BottomSheetListView;
import com.andremacareno.vkdocs.views.NewDocView;

/**
 * Created by andremacareno on 17/04/15.
 */
public class NewDocListController extends ViewController{
    //statement variables
    private volatile String photo_path;
    private final String TAG = "AttachListController";
    private NewDocMenuAdapter adapter;
    private int chosenAction = BottomSheetAction.NOTHING;
    private NewDocView.MenuDelegate delegate;
    public NewDocListController(View v) {
        super(v);
        init();
    }

    private void init()
    {
        this.adapter = new NewDocMenuAdapter(this);
        initViewSettings();
    }
    @Override
    protected void onViewReplace() {
        if(getView() != null) {
            initViewSettings();
        }
    }
    public NewDocMenuAdapter getAdapter() { return this.adapter; }
    private void initViewSettings() {
        BottomSheetListView v = (BottomSheetListView) getView();
        v.setNewElementCount(adapter.getItemCount());
        v.setAdapter(adapter);
        v.setMotionEventSplittingEnabled(false);
    }

    @Override
    public void onFragmentPause() {
    }

    @Override
    public void onFragmentResume() {
    }

    @Override
    public void onFragmentDestroy() {
    }
    public void onQuickPhotoSendRequest()
    {
        this.chosenAction = BottomSheetAction.CHOOSE_FROM_GALLERY;
        if(delegate != null)
            delegate.onActionChoose();
    }
    public void didActionChosen(BottomSheetAction act)
    {
        if(BuildConfig.DEBUG)
            Log.d("NewDocListCtrl", delegate != null ? "delegate != null" : "delegate == null");
        this.chosenAction = act.getAction();
        if(delegate != null)
            delegate.onActionChoose();
    }
    public void continueStartIntent()
    {
        if(chosenAction == BottomSheetAction.NOTHING || delegate == null)
            return;
        else if(chosenAction == BottomSheetAction.CHOOSE_FROM_GALLERY)
        {
            chosenAction = BottomSheetAction.NOTHING;
            if(adapter.getQuickSendSelectionCount() == 0)
                delegate.chooseFileFromGallery();
            else
                delegate.sendRecentGallery();
        }
        else if(chosenAction == BottomSheetAction.CHOOSE_A_FILE)
        {
            chosenAction = BottomSheetAction.NOTHING;
            delegate.chooseFileFromAll();
        }
        else if(chosenAction == BottomSheetAction.NEW_FOLDER)
        {
            chosenAction = BottomSheetAction.NOTHING;
            delegate.createNewFolder();
        }
    }
    public void setDelegate(NewDocView.MenuDelegate delegateRef)
    {
        this.delegate = delegateRef;
    }
}
