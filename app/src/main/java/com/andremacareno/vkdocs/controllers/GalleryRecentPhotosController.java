package com.andremacareno.vkdocs.controllers;

import android.database.ContentObserver;
import android.os.Message;
import android.provider.MediaStore;
import android.view.View;

import com.andremacareno.asynccore.BackgroundTask;
import com.andremacareno.asynccore.OnTaskCompleteListener;
import com.andremacareno.vkdocs.AppLoader;
import com.andremacareno.screencore.PauseHandler;
import com.andremacareno.vkdocs.dao.Document;
import com.andremacareno.vkdocs.listadapters.GalleryImagesListAdapter;
import com.andremacareno.vkdocs.objs.GalleryImageEntry;
import com.andremacareno.vkdocs.tasks.ConvertGalleryEntriesToInputMsgTask;
import com.andremacareno.vkdocs.tasks.LoadImagesFromGalleryTask;
import com.andremacareno.vkdocs.views.RecentFromGalleryListView;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * Created by andremacareno on 17/04/15.
 */
public class GalleryRecentPhotosController extends ViewController{
    //statement variables
    private final String TAG = "GalleryRecent";
    private GalleryImagesListAdapter adapter;
    private ArrayList<GalleryImageEntry> entries = new ArrayList<>();
    private final Map<Integer, GalleryImageEntry> idsToEntries = Collections.synchronizedMap(new HashMap<Integer, GalleryImageEntry>());
    private PauseHandler photoEntryReceiver;
    private ContentObserver externalObserver, internalObserver;
    private volatile LoadImagesFromGalleryTask loadingTask;
    private GalleryImagesListAdapter.QuickSelectDelegate delegate;
    private Runnable refreshRunnable;
    public GalleryRecentPhotosController(View v) {
        super(v);
        init();
    }

    @Override
    public void onFragmentDestroy() {
        removeObservers();
    }

    private void init()
    {
        this.adapter = new GalleryImagesListAdapter(this, entries);
        this.photoEntryReceiver = new GalleryImgReceiver(this);
        refreshRunnable = new Runnable() {
            @Override
            public void run() {
                requestPhotos();
            }
        };
        externalObserver = new GalleryUpdateObserver();
        internalObserver = new GalleryUpdateObserver();
        try {
            AppLoader.sharedInstance().getApplicationContext().getContentResolver().registerContentObserver(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, false, externalObserver);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            AppLoader.sharedInstance().getApplicationContext().getContentResolver().registerContentObserver(MediaStore.Images.Media.INTERNAL_CONTENT_URI, false, internalObserver);
        } catch (Exception e) {
            e.printStackTrace();
        }
        initViewSettings();
    }
    public GalleryImagesListAdapter getAdapter() { return this.adapter; }
    private void initViewSettings() {
        RecentFromGalleryListView v = (RecentFromGalleryListView) getView();
        v.setAdapter(adapter);
        v.setMotionEventSplittingEnabled(false);
        if(entries.size() == 0)
            requestPhotos();
    }

    @Override
    public void onFragmentPause() {
        if(photoEntryReceiver != null)
            photoEntryReceiver.pause();
    }

    @Override
    public void onFragmentResume() {
        if(photoEntryReceiver != null)
            photoEntryReceiver.resume();
    }
    public void setDelegate(GalleryImagesListAdapter.QuickSelectDelegate delegateRef)
    {
        this.delegate = delegateRef;
        adapter.setDelegate(this.delegate);
    }

    public void removeObservers()
    {
        try {
            AppLoader.sharedInstance().getApplicationContext().getContentResolver().unregisterContentObserver(externalObserver);
        } catch (Exception e) {
        }
        try {
            AppLoader.sharedInstance().getApplicationContext().getContentResolver().unregisterContentObserver(internalObserver);
        } catch (Exception e) {
        }
    }

    private void requestPhotos()
    {
        if(loadingTask != null)
            loadingTask.cancel();
        loadingTask = new LoadImagesFromGalleryTask();
        loadingTask.onComplete(new OnTaskCompleteListener() {
            @Override
            public void taskComplete(BackgroundTask task) {
                if(((LoadImagesFromGalleryTask) task).isCancelled())
                    return;
                if(((LoadImagesFromGalleryTask) task).getEntries() == null)
                    return;
                try {
                    synchronized(((LoadImagesFromGalleryTask) task).getEntries())
                    {
                        if(!((LoadImagesFromGalleryTask) task).getEntries().isEmpty())
                        {
                            synchronized(idsToEntries)
                            {
                                idsToEntries.clear();
                                for(GalleryImageEntry e : ((LoadImagesFromGalleryTask) task).getEntries())
                                    idsToEntries.put(e.getThumbId(), e);
                            }
                            Message msg = Message.obtain(photoEntryReceiver);
                            msg.obj = ((LoadImagesFromGalleryTask) task).getEntries();
                            msg.sendToTarget();
                        }
                    }
                }
                catch(Exception e) { e.printStackTrace(); }
                finally {
                    loadingTask = null;
                }
            }
        });
        loadingTask.addToQueue();
    }
    public void sendSelection()
    {
        HashSet<Integer> selection = adapter.getSelectionIds();
        if(selection.size() == 0)
            return;
        ConvertGalleryEntriesToInputMsgTask task = new ConvertGalleryEntriesToInputMsgTask(entries, idsToEntries, selection);
        task.onComplete(new OnTaskCompleteListener() {
            @Override
            public void taskComplete(BackgroundTask task) {
                try {
                    if(delegate != null)
                        delegate.onTransmittingDocsStart();
                    Thread.sleep(300);
                    ConvertGalleryEntriesToInputMsgTask cast = (ConvertGalleryEntriesToInputMsgTask) task;
                    final ArrayList<File> files = cast.getPaths();
                    for(File f : files)
                    {
                        Document doc = new Document();
                        doc.markUploading(f);
                        if(delegate != null)
                            delegate.requestPerformQuickSend(doc, f);
                    }
                }
                catch(Exception e) { e.printStackTrace(); }
            }
        });
        adapter.clearSelection();
        task.addToQueue();
    }
    public static class GalleryImgReceiver extends PauseHandler
    {
        private WeakReference<GalleryRecentPhotosController> controllerRef;
        public GalleryImgReceiver(GalleryRecentPhotosController c)
        {
            this.controllerRef = new WeakReference<GalleryRecentPhotosController>(c);
        }

        @Override
        protected boolean storeMessage(Message message) {
            return true;
        }

        @Override
        protected void processMessage(Message message) {
            if(controllerRef.get() == null)
                return;
            try
            {
                ArrayList<GalleryImageEntry> newEntries = (ArrayList<GalleryImageEntry>) message.obj;
                controllerRef.get().entries.clear();
                controllerRef.get().entries.addAll(newEntries);
                controllerRef.get().adapter.notifyDataSetChanged();
            }
            catch(Exception e) { e.printStackTrace(); }
        }
    }
    private class GalleryUpdateObserver extends ContentObserver
    {
        public GalleryUpdateObserver() {
            super(null);
        }
        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
            AppLoader.applicationHandler.postDelayed(refreshRunnable, 2000);
        }
    }
}
