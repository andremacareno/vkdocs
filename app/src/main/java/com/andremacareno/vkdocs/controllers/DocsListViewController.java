package com.andremacareno.vkdocs.controllers;

import android.os.Message;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.andremacareno.asynccore.BackgroundTask;
import com.andremacareno.asynccore.OnTaskCompleteListener;
import com.andremacareno.asynccore.OnTaskFailureListener;
import com.andremacareno.asynccore.notificationcenter.Notification;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.asynccore.notificationcenter.NotificationObserver;
import com.andremacareno.screencore.PauseHandler;
import com.andremacareno.vkdocs.AppLoader;
import com.andremacareno.vkdocs.BuildConfig;
import com.andremacareno.vkdocs.Constants;
import com.andremacareno.vkdocs.R;
import com.andremacareno.vkdocs.cache.BookCache;
import com.andremacareno.vkdocs.cache.FileCache;
import com.andremacareno.vkdocs.cache.FolderInfoCache;
import com.andremacareno.vkdocs.dao.Document;
import com.andremacareno.vkdocs.dao.Folder;
import com.andremacareno.vkdocs.fragments.MainFragment;
import com.andremacareno.vkdocs.listadapters.DocsListAdapter;
import com.andremacareno.vkdocs.objs.DocListItemWrap;
import com.andremacareno.vkdocs.objs.DocsListSection;
import com.andremacareno.vkdocs.tasks.DatabaseRemoveThread;
import com.andremacareno.vkdocs.tasks.DatabaseWritingThread;
import com.andremacareno.vkdocs.tasks.GetFolderContentsTask;
import com.andremacareno.vkdocs.tasks.GetFoldersListTask;
import com.andremacareno.vkdocs.tasks.GetOfflineListTask;
import com.andremacareno.vkdocs.tasks.HandleDocsTask;
import com.andremacareno.vkdocs.tasks.LocalDocsSearchTask;
import com.andremacareno.vkdocs.views.DocsListView;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKBatchRequest;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Andrew on 07.01.2016.
 */
public class DocsListViewController extends ViewController {
    public enum Mode {ALL_DOCS, OFFLINE, FOLDERS_ONLY, FOLDER_CONTENTS, SEARCH_RESULTS}
    public interface DocsListDelegate
    {
        void didDocumentSelected(int position);
        void didDocumentLongPressed(int position);
        void didFolderMenuRequested(Folder folder);
        void loadMoreDocuments();
        void cancelUpload(int position);
        void didFolderSelected(Folder folder);
        void didSelectionCountChanged(int newCount);
    }

    private static final String TAG = "DocsListController";
    private int OFFLINE_REMOVE = 0;
    private int OFFLINE_INSERT = 1;
    private final AtomicBoolean preventLoading = new AtomicBoolean(false);
    private static final int ITEMS_COUNT = 10;
    private static final int FROM_SYNC = 0;
    private static final int NOT_FROM_SYNC = 1;
    private final AtomicInteger offset = new AtomicInteger(0);
    private final AtomicBoolean docsSync = new AtomicBoolean(false);                //true if sync requested by pull-to-refresh
    private DocsListDelegate delegate;
    private VKRequest.VKRequestListener getDocsListener, syncListener;
    private PauseHandler newDocsReceiver, addUploadingReceiver, uploadFailedReceiver, updateOfflineReceiver, newFolderReceiver;
    private NotificationObserver uploadFailedObserver, savedOfflineObserver, removedOfflineObserver, uploadFinishedObserver;

    private final ArrayList<DocListItemWrap> documents = new ArrayList<>();
    private final ArrayList<DocListItemWrap> onlyFolders = new ArrayList<>();
    private final ArrayList<DocListItemWrap> folderContent = new ArrayList<>();
    private final ArrayList<DocListItemWrap> searchResults = new ArrayList<>();
    private final TreeMap<Long, DocListItemWrap> uploading = new TreeMap<>();
    private DocsListAdapter adapter, searchAdapter;
    private WeakReference<MainFragment.MainFragmentDelegate> fragmentDelegate;
    private int foldersCount = 0;
    //offline mode
    private DocsListAdapter offlineAdapter, foldersListAdapter, folderContentsListAdapter;
    private final ArrayList<DocListItemWrap> offlineDocs = new ArrayList<>();
    private final Map<Long, DocListItemWrap> offlineMap = new HashMap<>();
    private Mode previousMode = null;
    private Mode currentMode = Mode.ALL_DOCS;

    private final AtomicBoolean offlineDataLoading = new AtomicBoolean(true);
    private final AtomicBoolean foldersLoading = new AtomicBoolean(true);
    private final Object monitor = new Object();


    private final ArrayList<Document> docsToMove = new ArrayList<>();
    private String currentSearchQuery = "";

    public DocsListViewController(View v, MainFragment.MainFragmentDelegate delegate) {
        super(v);
        this.fragmentDelegate = new WeakReference<MainFragment.MainFragmentDelegate>(delegate);
        init();
    }
    private void init()
    {
        initVars();
        initView();
        retrieveOfflineList();
        retrieveFoldersList();
    }
    private void initView()
    {
        if(getView() == null)
            return;
        DocsListView list = (DocsListView) getView();
        list.setAdapter(adapter);
    }
    @Override
    protected void onViewReplace()
    {
        initView();
    }
    @Override
    public void onFragmentPause() {
        newDocsReceiver.pause();
        addUploadingReceiver.pause();
        uploadFailedReceiver.pause();
        updateOfflineReceiver.pause();
        newFolderReceiver.pause();
    }

    @Override
    public void onFragmentResume() {
        newDocsReceiver.resume();
        addUploadingReceiver.resume();
        uploadFailedReceiver.resume();
        updateOfflineReceiver.resume();
        newFolderReceiver.resume();
    }

    @Override
    public void onFragmentDestroy() {
        NotificationCenter.getInstance().removeObserver(uploadFailedObserver);
        NotificationCenter.getInstance().removeObserver(savedOfflineObserver);
        NotificationCenter.getInstance().removeObserver(removedOfflineObserver);
        NotificationCenter.getInstance().removeObserver(uploadFinishedObserver);
    }
    private void initVars()
    {
        newDocsReceiver = new NewDocsReceiver(this);
        addUploadingReceiver = new AddUploadingReceiver(this);
        uploadFailedReceiver = new UploadFailedReceiver(this);
        updateOfflineReceiver = new UpdateOfflineReceiver(this);
        newFolderReceiver = new NewFolderReceiver(this);
        removedOfflineObserver = new NotificationObserver(NotificationCenter.didRemovedOffline) {
            @Override
            public void didNotificationReceived(Notification notification) {
                try
                {
                    Long docId = (Long) notification.getObject();
                    DocListItemWrap wrap;
                    synchronized(offlineMap)
                    {
                        wrap = offlineMap.get(docId);
                        offlineMap.remove(docId);
                    }
                    synchronized(offlineDocs)
                    {
                        if(wrap != null)
                        {
                            int index = offlineDocs.indexOf(wrap);
                            if(index >= 0)
                            {
                                offlineDocs.remove(index);
                                Message msg = Message.obtain(updateOfflineReceiver);
                                msg.what = index;
                                msg.arg1 = OFFLINE_REMOVE;
                                msg.sendToTarget();
                            }
                        }
                    }
                }
                catch(Exception ignored) {}
            }
        };
        savedOfflineObserver = new NotificationObserver(NotificationCenter.didSavedOffline) {
            @Override
            public void didNotificationReceived(Notification notification) {
                try
                {
                    Document offlineDoc = (Document) notification.getObject();
                    if(offlineDoc.getExt().toLowerCase().equals("epub"))
                        BookCache.sharedInstance().addBook(offlineDoc.getId());
                    DocListItemWrap wrap = FileCache.getInstance().getDocument(offlineDoc);
                    synchronized(offlineMap)
                    {
                        offlineMap.put(offlineDoc.getId(), wrap);
                    }
                    synchronized(offlineDocs)
                    {
                        offlineDocs.add(0, wrap);
                        Message msg = Message.obtain(updateOfflineReceiver);
                        msg.what = 0;
                        msg.arg1 = OFFLINE_INSERT;
                        msg.sendToTarget();
                    }
                }
                catch(Exception ignored) {}
            }
        };
        uploadFailedObserver = new NotificationObserver(NotificationCenter.didUploadFailed) {
            @Override
            public void didNotificationReceived(Notification notification) {
                try
                {
                    synchronized(uploading)
                    {
                        Long id = ((Document) notification.getObject()).getId();
                        if(BuildConfig.DEBUG)
                            Log.d(TAG, String.format("failed to upload document %d", id));
                        uploading.remove(id);
                    }
                }
                catch(Exception e) { e.printStackTrace(); }
                try
                {
                    Message msg = Message.obtain(uploadFailedReceiver);
                    msg.obj = notification.getObject();
                    msg.sendToTarget();
                }
                catch(Exception ignored) {}
            }
        };
        uploadFinishedObserver = new NotificationObserver(NotificationCenter.didUploadFinished) {
            @Override
            public void didNotificationReceived(Notification notification) {
                try
                {
                    synchronized(uploading)
                    {
                        Long id = (Long) notification.getObject();
                        if(BuildConfig.DEBUG)
                            Log.d(TAG, String.format("finished uploading document %d", id));
                        uploading.remove(id);
                    }
                }
                catch(Exception e) { e.printStackTrace(); }
            }
        };
        NotificationCenter.getInstance().addObserver(uploadFailedObserver);
        NotificationCenter.getInstance().addObserver(savedOfflineObserver);
        NotificationCenter.getInstance().addObserver(removedOfflineObserver);
        NotificationCenter.getInstance().addObserver(uploadFinishedObserver);
        getDocsListener = new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                if(BuildConfig.DEBUG)
                    Log.d(TAG, "getDocs: onComplete");
                if(BuildConfig.DEBUG && response != null && response.json != null) {
                    Log.d(TAG, "VKResponse.json toString() = ".concat(response.json.toString()));
                }
                synchronized(docsSync)
                {
                    if(docsSync.get())
                        return;
                }
                HandleDocsTask task = new HandleDocsTask(response);
                task.onComplete(new OnTaskCompleteListener() {
                    @Override
                    public void taskComplete(BackgroundTask task) {
                        HandleDocsTask cast = (HandleDocsTask) task;
                        final ArrayList<DocListItemWrap> newDocs = cast.getParsedDocs();
                        try
                        {
                            boolean shouldWait;
                            synchronized(offlineDataLoading)
                            {
                                synchronized(foldersLoading)
                                {
                                    shouldWait = offlineDataLoading.get() || foldersLoading.get();
                                }
                            }
                            if(shouldWait)
                            {
                                while(shouldWait)
                                {
                                    synchronized(monitor)
                                    {
                                        try
                                        {
                                            monitor.wait();
                                        }
                                        catch(Exception ignored) {}
                                    }
                                    synchronized(offlineDataLoading)
                                    {
                                        synchronized(foldersLoading)
                                        {
                                            shouldWait = offlineDataLoading.get() || foldersLoading.get();
                                        }
                                    }
                                }
                            }
                            synchronized(docsSync)
                            {
                                if(docsSync.get())
                                    return;
                            }
                            for(DocListItemWrap w : newDocs)
                            {
                                synchronized(offlineMap)
                                {
                                    if(w.getType() != DocListItemWrap.Type.DOCUMENT)
                                        continue;
                                    Document doc = w.castDoc();
                                    if(BuildConfig.DEBUG)
                                        Log.d(TAG, doc.toString());
                                    if(offlineMap.containsKey(doc.getId()))
                                    {
                                        DocListItemWrap wrap = offlineMap.get(doc.getId());
                                        if(wrap.getType() == DocListItemWrap.Type.DOCUMENT)
                                        {
                                            Document offlineDoc = wrap.castDoc();
                                            if(BuildConfig.DEBUG)
                                                Log.d(TAG, offlineDoc.toString());
                                            if(!offlineDoc.equals(doc))
                                                doc.markOffline(offlineDoc.getFolderId(), offlineDoc.getPath());
                                        }
                                    }
                                }
                            }
                            Message msg = Message.obtain(newDocsReceiver);
                            msg.arg1 = NOT_FROM_SYNC;
                            msg.obj = newDocs;
                            msg.sendToTarget();
                        }
                        finally
                        {
                            boolean tryToUnlock;
                            synchronized(docsSync)
                            {
                                tryToUnlock = !docsSync.get();
                            }
                            if(tryToUnlock)
                            {
                                synchronized (preventLoading) {
                                    synchronized(offset)
                                    {
                                        offset.addAndGet(newDocs.size());
                                        if (offset.get() < cast.getTotalCount())
                                            preventLoading.set(false);
                                    }
                                }
                            }
                        }
                    }
                });
                task.onFailure(new OnTaskFailureListener() {
                    @Override
                    public void taskFailed(BackgroundTask task) {
                        synchronized (preventLoading) {
                            preventLoading.set(false);
                        }
                    };
                });
                task.addToQueue();
            }

            @Override
            public void onError(VKError error) {
                if(BuildConfig.DEBUG)
                    Log.d(TAG, "getDocs: onError");
                synchronized(preventLoading)
                {
                    preventLoading.set(false);
                }
            }
        };
        delegate = new DocsListDelegate() {
            @Override
            public void didDocumentSelected(int position) {
                if(fragmentDelegate == null || fragmentDelegate.get() == null)
                    return;
                DocListItemWrap wrap = getDocument(position);
                if(wrap == null)
                    return;
                if(wrap.getType() == DocListItemWrap.Type.DOCUMENT)
                    fragmentDelegate.get().didOpenDocRequested(wrap.castDoc());
            }

            @Override
            public void didDocumentLongPressed(int position) {
                if(fragmentDelegate == null || fragmentDelegate.get() == null)
                    return;
                DocListItemWrap wrap = getDocument(position);
                if(wrap != null && wrap.getType() == DocListItemWrap.Type.DOCUMENT)
                    fragmentDelegate.get().didOpenContextMenuRequested(wrap.castDoc());
            }

            @Override
            public void didFolderMenuRequested(Folder folder) {
                fragmentDelegate.get().didOpenContextMenuRequested(folder);
            }


            @Override
            public void loadMoreDocuments() {
                synchronized(preventLoading)
                {
                    if(preventLoading.get()) {
                        if(BuildConfig.DEBUG)
                            Log.d(TAG, "preventLoading is set to true");
                        return;
                    }
                    preventLoading.set(true);
                }
                try
                {
                    synchronized(offset)
                    {
                        VKRequest loadDocsRequest = new VKRequest("docs.get", VKParameters.from(VKApiConst.COUNT, ITEMS_COUNT, VKApiConst.OFFSET, offset));
                        loadDocsRequest.attempts = 10;
                        loadDocsRequest.executeWithListener(getDocsListener);
                    }
                }
                catch(Exception ignored) {}
            }

            @Override
            public void cancelUpload(int position) {
                DocListItemWrap wrap = getDocument(position);
                if(wrap.getType() == DocListItemWrap.Type.DOCUMENT)
                {
                    Document doc = wrap.castDoc();
                    if(!doc.isUploading())
                        return;
                    synchronized(offset)
                    {
                        offset.decrementAndGet();
                    }
                    documents.remove(position);
                    adapter.notifyItemRemoved(position);
                }
            }

            @Override
            public void didFolderSelected(final Folder folder) {
                final long folderId = folder.getId();
                if(docsToMove.isEmpty()) {
                    previousMode = currentMode;
                    currentMode = Mode.FOLDER_CONTENTS;
                    try
                    {
                        fragmentDelegate.get().onViewModeChanged(currentMode);
                    }
                    catch(Exception ignored) {}
                    if(getView() == null)
                        return;
                    GetFolderContentsTask getContent = new GetFolderContentsTask(folderId);
                    getContent.onComplete(new OnTaskCompleteListener() {
                        @Override
                        public void taskComplete(BackgroundTask task) {
                            GetFolderContentsTask cast = (GetFolderContentsTask) task;
                            synchronized(folderContent)
                            {
                                folderContent.clear();
                                folderContent.addAll(cast.getFolderContents());
                            }
                            AppLoader.applicationHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    try
                                    {
                                        DocsListView list = (DocsListView) getView();
                                        list.setAdapter(folderContentsListAdapter);
                                        folderContentsListAdapter.notifyDataSetChanged();
                                    }
                                    catch(Exception ignored) {}
                                }
                            });
                        }
                    });
                    getContent.addToQueue();
                    try
                    {
                        fragmentDelegate.get().onFolderSelected(folder);
                    }
                    catch(Exception ignored) {}
                    return;
                }
                try
                {
                    for(final Document finalDoc : docsToMove)
                    {
                        try
                        {
                            if(BuildConfig.DEBUG)
                                Log.d(TAG, "trying to move to folder");
                            Long oldFolder = finalDoc.getFolderId();
                            if(FolderInfoCache.sharedInstance().checkMovingToSelf(finalDoc.getId(), folderId) ||  (oldFolder != null && folderId == oldFolder)) {
                                if(BuildConfig.DEBUG)
                                    Log.d(TAG, "file are already in folder");
                                continue;
                            }
                            if(oldFolder != null && oldFolder > 0)
                                FolderInfoCache.sharedInstance().didDocRemovedFromFolder(oldFolder, finalDoc);
                            FolderInfoCache.sharedInstance().didDocAddedToFolder(folderId, finalDoc);
                            finalDoc.setFolderId(folderId);
                            DatabaseWritingThread.getInstance().addTask(finalDoc);
                            if(BuildConfig.DEBUG)
                                Log.d(TAG, "done");
                        }
                        catch(Exception e) { e.printStackTrace(); }
                    }
                }
                finally
                {
                    docsToMove.clear();
                    usePreviousMode();
                    try
                    {
                        fragmentDelegate.get().onMoveFinished();
                    }
                    catch(Exception ignored) {}
                }
            }

            @Override
            public void didSelectionCountChanged(int newCount) {
                try
                {
                    fragmentDelegate.get().didSelectionCountChanged(newCount);
                }
                catch(Exception ignored) {}
            }
        };
        adapter = new DocsListAdapter(documents, delegate);
        offlineAdapter = new DocsListAdapter(offlineDocs, delegate);
        foldersListAdapter = new DocsListAdapter(onlyFolders, delegate);
        folderContentsListAdapter = new DocsListAdapter(folderContent, delegate);
        searchAdapter = new DocsListAdapter(searchResults, delegate);
    }
    private DocListItemWrap getDocument(int position)
    {
        try
        {
            if(currentMode == Mode.ALL_DOCS)
            {
                synchronized(documents)
                {
                    return documents.get(position);
                }
            }
            else if(currentMode == Mode.FOLDERS_ONLY)
            {
                synchronized(onlyFolders)
                {
                    return onlyFolders.get(position);
                }
            }
            else if(currentMode == Mode.OFFLINE)
            {
                synchronized(offlineDocs)
                {
                    return offlineDocs.get(position);
                }
            }
            else if(currentMode == Mode.FOLDER_CONTENTS)
            {
                synchronized(folderContent)
                {
                    return folderContent.get(position);
                }
            }
            else if(currentMode == Mode.SEARCH_RESULTS)
            {
                synchronized(searchResults)
                {
                    return searchResults.get(position);
                }
            }
            return null;
        }
        catch(Exception ignored) { return null; }
    }
    public void offlineAccessRevokeRequested(final Document doc)
    {
        doc.broadcastOfflineAccessRevoked();
        removeFromOffline(FileCache.getInstance().getDocument(doc));
    }
    public Mode getCurrentMode() { return this.currentMode; }
    public void deleteDocument(final Document doc)
    {
        try
        {
            DocListItemWrap wrap = FileCache.getInstance().getDocument(doc);
            if(currentMode == Mode.FOLDER_CONTENTS)
            {
                synchronized(folderContent)
                {
                    int index = folderContent.indexOf(wrap);
                    if(index >= 0)
                    {
                        folderContent.remove(index);
                        folderContentsListAdapter.notifyItemRemoved(index);
                    }
                }
            }
            synchronized(offset)
            {
                offset.decrementAndGet();
            }
            removeFromEverything(wrap);
            if(doc.getFolderId() != 0)
                FolderInfoCache.sharedInstance().didDocRemovedFromFolder(doc.getFolderId(), doc);
            DatabaseRemoveThread.getInstance().addTask(doc);
        }
        catch(Exception ignored) {}
        //doc.revokeOfflineAccess();
    }
    public void deleteSelected(final Runnable onFinish)
    {
        try
        {
            final ArrayList<DocListItemWrap> selection = new ArrayList<>(getCurrentlyUsedAdapter().getSelection());
            final ArrayList<Document> docs = new ArrayList<>();
            VKRequest[] requests = new VKRequest[selection.size()];
            int i = 0;
            for(DocListItemWrap wr : selection)
            {
                if(wr.getType() != DocListItemWrap.Type.DOCUMENT)
                    continue;
                docs.add(wr.castDoc());
                synchronized(offset)
                {
                    offset.decrementAndGet();
                }
                VKRequest loadDocsRequest = new VKRequest("docs.delete", VKParameters.from(VKApiConst.OWNER_ID, VKAccessToken.currentToken().userId, Constants.VK_DOC_ID, wr.castDoc().getId()));
                loadDocsRequest.attempts = 10;
                requests[i++] = loadDocsRequest;
            }
            VKBatchRequest del = new VKBatchRequest(requests);
            del.executeWithListener(new VKBatchRequest.VKBatchRequestListener() {
                @Override
                public void onComplete(VKResponse[] responses) {
                    super.onComplete(responses);
                    try {
                        DatabaseRemoveThread.getInstance().addTask(docs);
                        AppLoader.applicationHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                onFinish.run();
                                for (DocListItemWrap wr : selection) {
                                    if (wr.getType() != DocListItemWrap.Type.DOCUMENT)
                                        continue;
                                    try {
                                        if (currentMode == Mode.FOLDER_CONTENTS) {
                                            synchronized (folderContent) {
                                                int index = folderContent.indexOf(wr);
                                                if (index >= 0) {
                                                    folderContent.remove(index);
                                                    folderContentsListAdapter.notifyItemRemoved(index);
                                                }
                                            }
                                        }
                                        synchronized (offset) {
                                            offset.decrementAndGet();
                                        }
                                        removeFromEverything(wr);
                                        Document doc = wr.castDoc();
                                        if (BuildConfig.DEBUG)
                                            Log.d(TAG, doc.toString());
                                        if (doc.getFolderId() != null && doc.getFolderId() != 0L)
                                            FolderInfoCache.sharedInstance().didDocRemovedFromFolder(doc.getFolderId(), doc);
                                    } catch (Exception ignored) {
                                    }
                                }
                            }
                        });
                    } catch (Exception ignored) {
                    }
                }

                @Override
                public void onError(VKError error) {
                    super.onError(error);
                }
            });
        }
        catch(Exception ignored) {}
    }
    public void removeFromFolder(final Document doc)
    {
        FolderInfoCache.sharedInstance().didDocRemovedFromFolder(doc.getFolderId(), doc);
        doc.setFolderId(0L);
        DocListItemWrap wrap = FileCache.getInstance().getDocument(doc);
        DatabaseWritingThread.getInstance().addTask(doc);
        synchronized(folderContent)
        {
            int index = folderContent.indexOf(wrap);
            if(index >= 0)
            {
                folderContent.remove(index);
                folderContentsListAdapter.notifyItemRemoved(index);
            }
        }
    }
    public void deleteFolder(final Folder folder)
    {
        DocListItemWrap wrap = new DocListItemWrap(folder);
        removeFromEverything(wrap);
        DatabaseRemoveThread.getInstance().addTask(folder);
    }
    private void removeFromEverything(DocListItemWrap wrap)
    {
        removeFromMainList(wrap);
        removeFromOffline(wrap);
        removeFromFoldersList(wrap);
    }
    private void removeFromMainList(DocListItemWrap wrap)
    {
        synchronized(documents)
        {
            int index = documents.indexOf(wrap);
            if(index >= 0)
            {
                documents.remove(index);
                boolean done = false;
                if(wrap.getType() == DocListItemWrap.Type.DOCUMENT)
                {
                    int pos = documents.size() - 1;
                    DocListItemWrap lastItem = documents.get(pos);
                    if(lastItem.getType() == DocListItemWrap.Type.SECTION) {
                        done = true;
                        documents.remove(pos);
                        adapter.notifyItemRangeRemoved(pos-1, 2);
                    }
                }
                else if(wrap.getType() == DocListItemWrap.Type.FOLDER)
                {
                    if(foldersCount == 1)
                    {
                        done = true;
                        documents.remove(0);
                        adapter.notifyItemRangeRemoved(0, 2);
                    }
                    foldersCount--;
                }
                if(!done)
                    adapter.notifyItemRemoved(index);
            }
        }
    }
    private void removeFromOffline(DocListItemWrap wrap)
    {
        synchronized(offlineDocs)
        {
            int index = offlineDocs.indexOf(wrap);
            if(index >= 0)
            {
                offlineDocs.remove(index);
                offlineAdapter.notifyItemRemoved(index);
            }
        }
    }
    private void removeFromFoldersList(DocListItemWrap wrap)
    {
        synchronized(onlyFolders)
        {
            int index = onlyFolders.indexOf(wrap);
            if(index >= 0)
            {
                onlyFolders.remove(index);
                foldersListAdapter.notifyItemRemoved(index);
            }
        }
    }
    public void enableSelectionMode(DocListItemWrap initial)
    {
        getCurrentlyUsedAdapter().enableSelectionMode();
        getCurrentlyUsedAdapter().addToSelection(initial);
    }
    public void disableSelectionMode()
    {
        getCurrentlyUsedAdapter().disableSelectionMode();
    }
    public void updateTitle(final Document doc, final String newTitle)
    {
        final DocListItemWrap wrap = FileCache.getInstance().getDocument(doc);
        AppLoader.applicationHandler.post(new Runnable() {
            @Override
            public void run() {
                synchronized (documents) {
                    final int index = documents.indexOf(wrap);
                    if (index >= 0)
                        adapter.notifyItemChanged(index);
                }
                synchronized (offlineMap) {
                    if (wrap.getType() != DocListItemWrap.Type.DOCUMENT)
                        return;
                    DocListItemWrap offlineWrap = offlineMap.get(doc.getId());
                    if (offlineWrap == null)
                        return;
                    Document offline = offlineWrap.castDoc();
                    if (offline == null)
                        return;
                    synchronized (offlineDocs) {
                        int index = offlineDocs.indexOf(offlineWrap);
                        offline.setTitle(newTitle);
                        if (index >= 0)
                            offlineAdapter.notifyItemChanged(index);
                    }
                }
                synchronized (folderContent) {
                    final int index = folderContent.indexOf(wrap);
                    if (index >= 0)
                        folderContentsListAdapter.notifyItemChanged(index);
                }
                synchronized (searchResults) {
                    final int index = searchResults.indexOf(wrap);
                    if (index >= 0)
                        searchAdapter.notifyItemChanged(index);
                }
            }
        });
    }
    public void updateTitle(final Folder folder)
    {
        final DocListItemWrap wrap = new DocListItemWrap(folder);
        synchronized (documents) {
            final int index = documents.indexOf(wrap);
            if (index >= 0)
                adapter.notifyItemChanged(index);
        }
        synchronized(onlyFolders)
        {
            final int index = onlyFolders.indexOf(wrap);
            if (index >= 0)
                foldersListAdapter.notifyItemChanged(index);
        }
    }
    public void addFolder(final Folder folder)
    {
        DocListItemWrap wrap = new DocListItemWrap(folder);
        Message msg = Message.obtain(newFolderReceiver);
        msg.obj = wrap;
        msg.sendToTarget();
    }
    public void addUploadingDocument(final Document doc)
    {
        DocListItemWrap wrap = FileCache.getInstance().getDocument(doc);
        synchronized(uploading)
        {
            if(BuildConfig.DEBUG)
                Log.d(TAG, String.format("logging uploading doc %d", doc.getId()));
            uploading.put(doc.getId(), wrap);
        }
        Message msg = Message.obtain(addUploadingReceiver);
        msg.obj = wrap;
        msg.sendToTarget();
    }
    private static class NewDocsReceiver extends PauseHandler
    {
        private WeakReference<DocsListViewController> controllerRef;
        public NewDocsReceiver(DocsListViewController controller)
        {
            this.controllerRef = new WeakReference<DocsListViewController>(controller);
        }
        @Override
        protected boolean storeMessage(Message message) {
            return true;
        }

        @Override
        protected void processMessage(Message message) {
            if(message.obj == null || controllerRef.get() == null)
                return;
            try
            {
                if(message.arg1 == NOT_FROM_SYNC)
                {
                    ArrayList<DocListItemWrap> newDocs = (ArrayList<DocListItemWrap>) message.obj;
                    synchronized(controllerRef.get().documents)
                    {
                        int positionStart = controllerRef.get().documents.size();
                        if(!newDocs.isEmpty() && (controllerRef.get().documents.isEmpty() || controllerRef.get().documents.get(controllerRef.get().documents.size() - 1).getType() != DocListItemWrap.Type.DOCUMENT))
                        {
                            DocsListSection section = new DocsListSection(AppLoader.sharedInstance().getString(R.string.files));
                            DocListItemWrap wrap = new DocListItemWrap(section);
                            newDocs.add(0, wrap);
                        }
                        controllerRef.get().documents.addAll(newDocs);
                        controllerRef.get().adapter.notifyItemRangeInserted(positionStart, newDocs.size());
                        if(controllerRef.get().currentMode == Mode.ALL_DOCS)
                            controllerRef.get().fragmentDelegate.get().showEmptyView(controllerRef.get().documents.isEmpty());
                    }
                }
                else
                {
                    synchronized(controllerRef.get().documents)
                    {
                        if(BuildConfig.DEBUG)
                            Log.d(TAG, "receiving sync data");
                        controllerRef.get().documents.clear();
                        controllerRef.get().documents.addAll(controllerRef.get().onlyFolders);
                        DocsListSection filesSection = new DocsListSection(AppLoader.sharedInstance().getString(R.string.files));
                        DocListItemWrap filesSectionWrap = new DocListItemWrap(filesSection);
                        controllerRef.get().documents.add(filesSectionWrap);
                        synchronized(controllerRef.get().uploading)
                        {
                            if(!controllerRef.get().uploading.isEmpty())
                            {
                                controllerRef.get().documents.addAll(new ArrayList<DocListItemWrap>(controllerRef.get().uploading.values()));
                            }
                        }
                        controllerRef.get().documents.addAll((ArrayList<DocListItemWrap>) message.obj);
                    }
                    controllerRef.get().adapter.notifyDataSetChanged();
                }
            }
            catch(Exception e) { e.printStackTrace(); }
        }
    }
    public void showOfflineDocs()
    {
        if(this.currentMode == Mode.OFFLINE)
            return;
        this.previousMode = this.currentMode;
        this.currentMode = Mode.OFFLINE;
        try
        {
            fragmentDelegate.get().onViewModeChanged(currentMode);
        }
        catch(Exception ignored) {}
        if(getView() == null)
            return;
        if(BuildConfig.DEBUG)
            Log.d(TAG, "show offline");
        DocsListView list = (DocsListView) getView();
        list.setAdapter(offlineAdapter);
        offlineAdapter.notifyDataSetChanged();
        synchronized(offlineDataLoading)
        {
            if(offlineDataLoading.get())
                return;
        }
        synchronized(offlineDocs)
        {
            try
            {
                fragmentDelegate.get().showEmptyView(offlineDocs.isEmpty());
            }
            catch(Exception ignored) {}
        }
    }
    public void showAllDocs()
    {
        if(this.currentMode == Mode.ALL_DOCS)
            return;
        this.previousMode = this.currentMode;
        this.currentMode = Mode.ALL_DOCS;
        try
        {
            fragmentDelegate.get().onViewModeChanged(currentMode);
        }
        catch(Exception ignored) {}
        if(getView() == null)
            return;
        DocsListView list = (DocsListView) getView();
        list.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        try
        {
            synchronized(documents)
            {
                fragmentDelegate.get().showEmptyView(documents.isEmpty());
                if(documents.isEmpty() || documents.get(documents.size() - 1).getType() != DocListItemWrap.Type.DOCUMENT)
                {
                    startSync();
                }
            }
        }
        catch(Exception ignored) {}
    }

    public void retrieveOfflineList()
    {
        GetOfflineListTask offlineTask = new GetOfflineListTask();
        offlineTask.onComplete(new OnTaskCompleteListener() {
            @Override
            public void taskComplete(BackgroundTask task) {
                GetOfflineListTask cast = (GetOfflineListTask) task;
                synchronized(offlineDocs)
                {
                    offlineDocs.addAll(cast.getOfflineDocs());
                }
                synchronized(offlineMap)
                {
                    offlineMap.putAll(cast.getMap());
                }
                synchronized(offlineDataLoading)
                {
                    offlineDataLoading.set(false);
                }
                synchronized(monitor)
                {
                    monitor.notifyAll();
                }
                try
                {
                    if(currentMode == Mode.OFFLINE)
                        fragmentDelegate.get().showEmptyView(cast.getOfflineDocs().isEmpty());
                }
                catch(Exception ignored) {}
                AppLoader.applicationHandler.post(new Runnable() {
                    @Override
        public void run() {
                        offlineAdapter.notifyDataSetChanged();
                    }
                });
            }
        });
        offlineTask.onFailure(new OnTaskFailureListener() {
            @Override
            public void taskFailed(BackgroundTask task) {
                synchronized (offlineDataLoading) {
                    offlineDataLoading.set(false);
                }
                synchronized (monitor) {
                    monitor.notifyAll();
                }
            }
        });
        offlineTask.addToQueue();
    }
    public void startSync()
    {
        synchronized(docsSync)
        {
            if(docsSync.get())
                return;
            docsSync.set(true);
        }
        synchronized(preventLoading)
        {
            preventLoading.set(true);
        }
        if(syncListener == null)
        {
            syncListener = new VKRequest.VKRequestListener() {
                @Override
                public void onComplete(VKResponse response) {
                    if(BuildConfig.DEBUG && response != null && response.json != null) {
                        Log.d(TAG, "VKResponse.json toString() = ".concat(response.json.toString()));
                    }
                    HandleDocsTask task = new HandleDocsTask(response);
                    task.onComplete(new OnTaskCompleteListener() {
                        @Override
                        public void taskComplete(BackgroundTask task) {
                            if(BuildConfig.DEBUG)
                                Log.d(TAG, "sync response handled");
                            HandleDocsTask cast = (HandleDocsTask) task;
                            final ArrayList<DocListItemWrap> newDocs = cast.getParsedDocs();
                            try
                            {
                                boolean shouldWait;
                                synchronized(offlineDataLoading)
                                {
                                    synchronized(foldersLoading)
                                    {
                                        shouldWait = offlineDataLoading.get() || foldersLoading.get();
                                        if(BuildConfig.DEBUG)
                                            Log.d(TAG, String.format("shouldWait = %s", Boolean.toString(shouldWait)));
                                    }
                                }
                                if(shouldWait)
                                {

                                    while(shouldWait)
                                    {
                                        synchronized(monitor)
                                        {
                                            try
                                            {
                                                monitor.wait();
                                            }
                                            catch(Exception ignored) {}
                                        }
                                        synchronized(offlineDataLoading)
                                        {
                                            synchronized(foldersLoading)
                                            {
                                                shouldWait = offlineDataLoading.get() || foldersLoading.get();
                                            }
                                        }
                                    }
                                }
                                for(DocListItemWrap w : newDocs)
                                {
                                    synchronized(offlineMap)
                                    {
                                        if(w.getType() != DocListItemWrap.Type.DOCUMENT)
                                            continue;
                                        Document doc = w.castDoc();
                                        if(BuildConfig.DEBUG)
                                            Log.d(TAG, doc.toString());
                                        if(offlineMap.containsKey(doc.getId()))
                                        {
                                            DocListItemWrap wrap = offlineMap.get(doc.getId());
                                            if(wrap.getType() == DocListItemWrap.Type.DOCUMENT)
                                            {
                                                Document offlineDoc = wrap.castDoc();
                                                if(BuildConfig.DEBUG)
                                                    Log.d(TAG, offlineDoc.toString());
                                                if(!offlineDoc.equals(doc))
                                                    doc.markOffline(offlineDoc.getFolderId(), offlineDoc.getPath());
                                            }
                                        }
                                    }
                                }
                                if(BuildConfig.DEBUG)
                                    Log.d(TAG, "sending message");
                                Message msg = Message.obtain(newDocsReceiver);
                                msg.obj = newDocs;
                                msg.arg1 = FROM_SYNC;
                                msg.sendToTarget();
                            }
                            catch(Exception e) { e.printStackTrace(); }
                            finally
                            {
                                synchronized(docsSync)
                                {
                                    docsSync.set(false);
                                }
                                synchronized (preventLoading) {
                                    preventLoading.set(false);
                                }
                                synchronized(offset)
                                {
                                    offset.set(newDocs.size());
                                }
                                try
                                {
                                    fragmentDelegate.get().onSyncFinished(true);
                                    fragmentDelegate.get().showEmptyView(newDocs.isEmpty());
                                }
                                catch(Exception ignored) {}
                            }
                        }
                    });
                    task.onFailure(new OnTaskFailureListener() {
                        @Override
                        public void taskFailed(BackgroundTask task) {
                            synchronized(docsSync)
                            {
                                docsSync.set(false);
                            }
                            synchronized (preventLoading) {
                                preventLoading.set(false);
                            }
                            try
                            {
                                fragmentDelegate.get().onSyncFinished(false);
                            }
                            catch(Exception ignored) {}
                        };
                    });
                    task.addToQueue();
                }

                @Override
                public void onError(VKError error) {
                    synchronized(preventLoading)
                    {
                        preventLoading.set(false);
                    }
                    try
                    {
                        fragmentDelegate.get().onSyncFinished(false);
                    }
                    catch(Exception ignored) {}
                }
            };
        }
        VKRequest sync = new VKRequest("docs.get", VKParameters.from(VKApiConst.COUNT, ITEMS_COUNT, VKApiConst.OFFSET, 0));
        sync.executeWithListener(syncListener);
    }
    public void retrieveFoldersList()
    {
        GetFoldersListTask foldersTask = new GetFoldersListTask();
        foldersTask.onComplete(new OnTaskCompleteListener() {
            @Override
            public void taskComplete(BackgroundTask task) {
                GetFoldersListTask cast = (GetFoldersListTask) task;
                List<DocListItemWrap> folders = cast.getOfflineFolders();
                foldersCount += folders.size();
                if(!folders.isEmpty())
                {
                    DocListItemWrap section = new DocListItemWrap(new DocsListSection(AppLoader.sharedInstance().getString(R.string.folders)));
                    folders.add(0, section);
                }
                synchronized(onlyFolders)
                {
                    onlyFolders.addAll(0, folders);
                }
                synchronized(documents)
                {
                    documents.addAll(0, folders);
                }

                /*synchronized (offlineDocs) {
                    offlineDocs.addAll(0, folders);
                }*/
                synchronized (foldersLoading) {
                    foldersLoading.set(false);
                }
                synchronized (monitor) {
                    monitor.notifyAll();
                }
                AppLoader.applicationHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        foldersListAdapter.notifyDataSetChanged();
                        adapter.notifyDataSetChanged();
                        //offlineAdapter.notifyDataSetChanged();
                    }
                });
            }
        });
        foldersTask.onFailure(new OnTaskFailureListener() {
            @Override
            public void taskFailed(BackgroundTask task) {
                synchronized (foldersLoading) {
                    foldersLoading.set(false);
                }
                synchronized (monitor) {
                    monitor.notifyAll();
                }
            }
        });
        foldersTask.addToQueue();
    }
    public void moveSelection()
    {
        try
        {
            HashSet<DocListItemWrap> selection = getCurrentlyUsedAdapter().getSelection();
            for(DocListItemWrap wr : selection)
            {
                if(wr.getType() != DocListItemWrap.Type.DOCUMENT)
                    continue;
                docsToMove.add(wr.castDoc());
            }
            onMoveRequested();
        }
        catch(Exception e) { e.printStackTrace(); }
    }
    public void showOnlyFolders(Document docToMove)
    {
        docsToMove.clear();
        docsToMove.add(docToMove);
        onMoveRequested();
    }
    private void onMoveRequested()
    {
        if(this.previousMode != Mode.FOLDERS_ONLY)
            this.previousMode = this.currentMode;
        this.currentMode = Mode.FOLDERS_ONLY;
        try
        {
            fragmentDelegate.get().onViewModeChanged(currentMode);
        }
        catch(Exception ignored) {}
        if(getView() == null)
            return;
        DocsListView list = (DocsListView) getView();
        list.setAdapter(foldersListAdapter);
        foldersListAdapter.notifyDataSetChanged();
    }
    public void usePreviousMode()
    {
        if(this.previousMode == null)
            return;
        if(this.currentMode == Mode.FOLDERS_ONLY)
            docsToMove.clear();
        this.currentMode = this.previousMode;
        try
        {
            fragmentDelegate.get().onViewModeChanged(currentMode);
        }
        catch(Exception ignored) {}
        this.previousMode = this.currentMode == Mode.FOLDER_CONTENTS ? Mode.ALL_DOCS : null;
        if(getView() == null)
            return;
        DocsListView list = (DocsListView) getView();
        RecyclerView.Adapter ad = getCurrentlyUsedAdapter();
        list.setAdapter(ad);
        ad.notifyDataSetChanged();
    }
    private DocsListAdapter getCurrentlyUsedAdapter()
    {
        return this.currentMode == Mode.ALL_DOCS ? adapter : this.currentMode == Mode.OFFLINE ? offlineAdapter : this.currentMode == Mode.FOLDER_CONTENTS ? folderContentsListAdapter : foldersListAdapter;
    }
    public void enableSearchMode()
    {
        if(this.currentMode == Mode.SEARCH_RESULTS)
            return;
        this.previousMode = this.currentMode;
        this.currentMode = Mode.SEARCH_RESULTS;
        try
        {
            fragmentDelegate.get().onViewModeChanged(currentMode);
        }
        catch(Exception ignored) {}
        if(getView() == null)
            return;
        DocsListView list = (DocsListView) getView();
        list.setAdapter(searchAdapter);
        searchAdapter.notifyDataSetChanged();
    }
    public void newQuery(final String query)
    {
        if(currentMode != Mode.SEARCH_RESULTS)
            return;
        currentSearchQuery = query;
        LocalDocsSearchTask localSearch = new LocalDocsSearchTask(previousMode == Mode.ALL_DOCS ? documents : previousMode == Mode.OFFLINE ? offlineDocs : folderContent, query);
        localSearch.onComplete(new OnTaskCompleteListener() {
            @Override
            public void taskComplete(BackgroundTask task) {
                try {
                    if (currentMode != Mode.SEARCH_RESULTS)
                        return;
                    if (!query.equals(currentSearchQuery))
                        return;
                    final ArrayList<DocListItemWrap> resultsList = ((LocalDocsSearchTask) task).getResults();
                    AppLoader.applicationHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            synchronized (searchResults) {
                                searchResults.clear();
                                searchResults.addAll(resultsList);
                                searchAdapter.notifyDataSetChanged();
                            }
                        }
                    });
                } catch (Exception ignored) {
                }
            }
        });
        localSearch.addToQueue();
    }
    public void clearSearchData()
    {
        this.currentSearchQuery = "";
        synchronized(searchResults)
        {
            searchResults.clear();
        }
        searchAdapter.notifyDataSetChanged();
    }

    private static class AddUploadingReceiver extends PauseHandler
    {
        private WeakReference<DocsListViewController> controllerRef;
        public AddUploadingReceiver(DocsListViewController controller)
        {
            this.controllerRef = new WeakReference<DocsListViewController>(controller);
        }
        @Override
        protected boolean storeMessage(Message message) {
            return true;
        }

        @Override
        protected void processMessage(Message message) {
            if(message.obj == null || controllerRef.get() == null)
                return;
            try
            {
                int firstVisible = ((LinearLayoutManager)((DocsListView) controllerRef.get().getView()).getLayoutManager()).findFirstVisibleItemPosition();
                int lastVisible = ((LinearLayoutManager)((DocsListView) controllerRef.get().getView()).getLayoutManager()).findLastVisibleItemPosition();
                if(firstVisible > lastVisible)
                    firstVisible = lastVisible;
                DocListItemWrap doc = (DocListItemWrap) message.obj;
                int index;
                synchronized(controllerRef.get().onlyFolders)
                {
                    index = controllerRef.get().onlyFolders.size();
                }
                synchronized(controllerRef.get().documents)
                {
                    boolean sectionAdded = false;
                    if(doc != null && controllerRef.get().documents.isEmpty())
                    {
                        DocsListSection section = new DocsListSection(AppLoader.sharedInstance().getString(R.string.files));
                        DocListItemWrap wrap = new DocListItemWrap(section);
                        controllerRef.get().documents.add(index, wrap);
                        sectionAdded = true;
                    }
                    controllerRef.get().documents.add(index + 1, doc);
                    if(!sectionAdded)
                        controllerRef.get().adapter.notifyItemInserted(index + 1);
                    else
                        controllerRef.get().adapter.notifyItemRangeInserted(index, 2);
                    synchronized(controllerRef.get().offset)
                    {
                        controllerRef.get().offset.incrementAndGet();
                    }
                    if(firstVisible <= 10)
                        ((DocsListView) controllerRef.get().getView()).smoothScrollToPosition(index);
                }
            }
            catch(Exception ignored) {}
        }
    }
    private static class UploadFailedReceiver extends PauseHandler
    {
        private WeakReference<DocsListViewController> controllerRef;
        public UploadFailedReceiver(DocsListViewController controller)
        {
            this.controllerRef = new WeakReference<DocsListViewController>(controller);
        }
        @Override
        protected boolean storeMessage(Message message) {
            return true;
        }

        @Override
        protected void processMessage(Message message) {
            if(message.obj == null || controllerRef.get() == null)
                return;
            try
            {
                Document doc = (Document) message.obj;
                DocListItemWrap docWrap = FileCache.getInstance().getDocument(doc);
                controllerRef.get().removeFromMainList(docWrap);
                Toast.makeText(AppLoader.sharedInstance().getApplicationContext(), String.format(AppLoader.sharedInstance().getString(R.string.error_uploading_document), doc.getTitle().concat(".".concat(doc.getExt()))), Toast.LENGTH_LONG).show();
            }
            catch(Exception e) { e.printStackTrace(); }
        }
    }
    private static class UpdateOfflineReceiver extends PauseHandler
    {
        private WeakReference<DocsListViewController> controllerRef;
        public UpdateOfflineReceiver(DocsListViewController controller)
        {
            this.controllerRef = new WeakReference<DocsListViewController>(controller);
        }
        @Override
        protected boolean storeMessage(Message message) {
            return true;
        }

        @Override
        protected void processMessage(Message message) {
            if(message.what < 0 || controllerRef.get() == null)
                return;
            try
            {
                int action = message.arg1;
                int index = message.what;
                if(action == controllerRef.get().OFFLINE_REMOVE)
                    controllerRef.get().offlineAdapter.notifyItemRemoved(index);
                else if(action == controllerRef.get().OFFLINE_INSERT)
                    controllerRef.get().offlineAdapter.notifyItemInserted(index);
            }
            catch(Exception ignored) {}
        }
    }
    private static class NewFolderReceiver extends PauseHandler
    {
        private WeakReference<DocsListViewController> controllerRef;
        public NewFolderReceiver(DocsListViewController controller)
        {
            this.controllerRef = new WeakReference<DocsListViewController>(controller);
        }
        @Override
        protected boolean storeMessage(Message message) {
            return true;
        }

        @Override
        protected void processMessage(Message message) {
            try
            {
                DocListItemWrap f = (DocListItemWrap) message.obj;
                if(controllerRef.get().foldersCount == 0)
                {
                    DocListItemWrap section = new DocListItemWrap(new DocsListSection(AppLoader.sharedInstance().getString(R.string.folders)));
                    synchronized(controllerRef.get().documents)
                    {
                        controllerRef.get().documents.add(0, f);
                        controllerRef.get().documents.add(0, section);
                        controllerRef.get().adapter.notifyItemRangeInserted(0, 2);
                    }
                    synchronized(controllerRef.get().onlyFolders)
                    {
                        controllerRef.get().onlyFolders.add(0, f);
                        controllerRef.get().onlyFolders.add(0, section);
                        controllerRef.get().foldersListAdapter.notifyItemRangeInserted(0, 2);
                    }
                }
                else
                {
                    synchronized(controllerRef.get().documents)
                    {
                        controllerRef.get().documents.add(controllerRef.get().foldersCount+1, f);
                        controllerRef.get().adapter.notifyItemInserted(controllerRef.get().foldersCount + 1);
                    }
                    synchronized(controllerRef.get().onlyFolders)
                    {
                        controllerRef.get().onlyFolders.add(controllerRef.get().foldersCount+1, f);
                        controllerRef.get().foldersListAdapter.notifyItemInserted(controllerRef.get().foldersCount + 1);
                    }
                }
                ((DocsListView) controllerRef.get().getView()).scrollToPosition(controllerRef.get().foldersCount);
                controllerRef.get().foldersCount++;
            }
            catch(Exception ignored) {}
        }
    }

}
