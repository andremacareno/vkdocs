package com.andremacareno.vkdocs.controllers;

import android.os.Message;
import android.util.Log;
import android.view.View;

import com.andremacareno.asynccore.BackgroundTask;
import com.andremacareno.asynccore.OnTaskCompleteListener;
import com.andremacareno.screencore.PauseHandler;
import com.andremacareno.vkdocs.AndroidUtilities;
import com.andremacareno.vkdocs.AppLoader;
import com.andremacareno.vkdocs.BuildConfig;
import com.andremacareno.vkdocs.fragments.ShareWithFriendFragment;
import com.andremacareno.vkdocs.listadapters.FriendsListAdapter;
import com.andremacareno.vkdocs.objs.CurrentUser;
import com.andremacareno.vkdocs.tasks.FriendsSearchBackgroundTask;
import com.andremacareno.vkdocs.views.FriendsListView;
import com.andremacareno.vkdocs.views.ShareReceiverListView;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiUserFull;
import com.vk.sdk.api.model.VKList;
import com.vk.sdk.api.model.VKUsersArray;

import java.lang.ref.WeakReference;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by Andrew on 07.01.2016.
 */
public class FriendsListViewController extends ViewController {
    public interface FriendsListDelegate
    {
        void loadMoreFriends();
    }
    public static final int STATUS_SENDING = 0;
    public static final int STATUS_SENT = 1;
    public static final int STATUS_FAILED = 2;
    private static final String TAG = "FriendsListController";
    private final AtomicBoolean preventLoading = new AtomicBoolean(false);
    private static final int ITEMS_COUNT = 20;
    private volatile int offset = 0;

    private FriendsListDelegate delegate;
    private VKRequest.VKRequestListener getFriendsListener, msgSendListener;
    private PauseHandler newFriendsReceiver;

    private final VKList<VKApiUserFull> allFriends = new VKList<>();
    private final VKList<VKApiUserFull> friends = new VKList<>();
    private FriendsListAdapter adapter;
    private final long docId;
    private ShareWithFriendFragment.FragmentDelegate fragmentDelegate;
    private String currentSearchQuery = "";
    public FriendsListViewController(View v, long docId, ShareWithFriendFragment.FragmentDelegate delegateRef) {
        super(v);
        this.docId = docId;
        this.fragmentDelegate = delegateRef;
        init();
    }
    private void init()
    {
        initVars();
        initView();
    }
    private void initView()
    {
        if(getView() == null)
            return;
        FriendsListView list = (FriendsListView) getView();
        list.setAdapter(adapter);
    }
    @Override
    protected void onViewReplace()
    {
        initView();
    }
    @Override
    public void onFragmentPause() {
        newFriendsReceiver.pause();
    }

    @Override
    public void onFragmentResume() {
        newFriendsReceiver.resume();
    }

    @Override
    public void onFragmentDestroy() {

    }
    public void setHeaderListViewRef(ShareReceiverListView view)
    {
        if(adapter != null)
            adapter.setViewRef(view);
    }
    private void initVars()
    {
        newFriendsReceiver = new NewFriendsReceiver(this);
        getFriendsListener = new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                if(BuildConfig.DEBUG && response != null && response.json != null) {
                    Log.d(TAG, "VKResponse.json toString() = ".concat(response.json.toString()));
                }
                if(BuildConfig.DEBUG && response != null && response.parsedModel != null && response.parsedModel instanceof VKList)
                    Log.d(TAG, "parsedModel is ready");
                try
                {
                    VKList<VKApiUserFull> friendList = (VKList<VKApiUserFull>) response.parsedModel;

                    synchronized (preventLoading) {
                        offset += friendList.size();
                        if (offset < friendList.getCount())
                            preventLoading.set(false);
                    }
                    Message msg = Message.obtain(newFriendsReceiver);
                    msg.obj = friendList;
                    msg.sendToTarget();
                }
                catch(Exception ignored)
                {
                    this.onError(null);
                }
            }

            @Override
            public void onError(VKError error) {
                synchronized(preventLoading)
                {
                    preventLoading.set(false);
                }
            }
        };
        msgSendListener = new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                if(BuildConfig.DEBUG && response != null)
                    Log.d("FriendsListControl", response.responseString);
                if(fragmentDelegate != null)
                    fragmentDelegate.onStatusChanged(STATUS_SENT);
            }

            @Override
            public void onError(VKError error) {
                if(BuildConfig.DEBUG)
                    Log.d("FriendsListControl", error.toString());
                if(fragmentDelegate != null)
                    fragmentDelegate.onStatusChanged(STATUS_FAILED);
            }
        };
        delegate = new FriendsListDelegate() {
            @Override
            public void loadMoreFriends() {
                if(!currentSearchQuery.isEmpty())
                    return;
                synchronized(preventLoading)
                {
                    if(preventLoading.get()) {
                        return;
                    }
                    preventLoading.set(true);
                }
                try
                {
                    VKRequest loadFriendsRequest = new VKRequest("execute.getActiveFriends", VKParameters.from(VKApiConst.FIELDS, AndroidUtilities.isLowDPIScreen() ? "photo_50" : "photo_100", VKApiConst.COUNT, ITEMS_COUNT, VKApiConst.OFFSET, offset), VKUsersArray.class);
                    loadFriendsRequest.attempts = 10;
                    loadFriendsRequest.executeWithListener(getFriendsListener);
                }
                catch(Exception ignored) {}
            }
        };
        adapter = new FriendsListAdapter(friends, delegate);
    }
    public void send(String ids)
    {
        try
        {
            if(fragmentDelegate != null)
                fragmentDelegate.onStatusChanged(STATUS_SENDING);
            VKRequest req = new VKRequest("messages.send", VKParameters.from("user_ids", ids, "message", "", "attachment", String.format("doc%d_%d", CurrentUser.getInstance().getId(), docId)));
            req.attempts = 10;
            req.executeWithListener(msgSendListener);
        }
        catch(Exception ignored) {}
    }
    public void newQuery(final String query)
    {
        currentSearchQuery = query;
        FriendsSearchBackgroundTask localSearch = new FriendsSearchBackgroundTask(allFriends, query);
        localSearch.onComplete(new OnTaskCompleteListener() {
            @Override
            public void taskComplete(BackgroundTask task) {
                try {
                    if (!query.equals(currentSearchQuery))
                        return;
                    final VKList<VKApiUserFull> resultsList = ((FriendsSearchBackgroundTask) task).getResults();
                    AppLoader.applicationHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            synchronized (friends) {
                                friends.clear();
                                friends.addAll(resultsList);
                                adapter.notifyDataSetChanged();
                            }
                        }
                    });
                } catch (Exception ignored) {
                }
            }
        });
        localSearch.addToQueue();
    }

    public void clearSearchData()
    {
        this.currentSearchQuery = "";
        synchronized(friends)
        {
            friends.clear();
            friends.addAll(allFriends);
        }
        adapter.notifyDataSetChanged();
    }

    private static class NewFriendsReceiver extends PauseHandler
    {
        private WeakReference<FriendsListViewController> controllerRef;
        public NewFriendsReceiver(FriendsListViewController controller)
        {
            this.controllerRef = new WeakReference<FriendsListViewController>(controller);
        }
        @Override
        protected boolean storeMessage(Message message) {
            return true;
        }

        @Override
        protected void processMessage(Message message) {
            if(message.obj == null || controllerRef.get() == null)
                return;
            try
            {
                VKList<VKApiUserFull> newFriends = (VKList<VKApiUserFull>) message.obj;
                synchronized(controllerRef.get().friends)
                {
                    int positionStart = controllerRef.get().friends.size();
                    if(controllerRef.get().currentSearchQuery.isEmpty()) {
                        controllerRef.get().friends.addAll(newFriends);
                        controllerRef.get().adapter.notifyItemRangeInserted(positionStart, newFriends.size());
                    }
                    controllerRef.get().allFriends.addAll(newFriends);
                }
            }
            catch(Exception e) { e.printStackTrace(); }
        }
    }

}
