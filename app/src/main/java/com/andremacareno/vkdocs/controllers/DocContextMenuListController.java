package com.andremacareno.vkdocs.controllers;

import android.view.View;

import com.andremacareno.vkdocs.AppLoader;
import com.andremacareno.vkdocs.DownloadState;
import com.andremacareno.vkdocs.R;
import com.andremacareno.vkdocs.cache.FileCache;
import com.andremacareno.vkdocs.dao.Document;
import com.andremacareno.vkdocs.dao.Folder;
import com.andremacareno.vkdocs.listadapters.DocContextMenuAdapter;
import com.andremacareno.vkdocs.objs.BottomSheetAction;
import com.andremacareno.vkdocs.views.BottomSheetListView;
import com.andremacareno.vkdocs.views.DocActionsView;

import java.util.ArrayList;

/**
 * Created by andremacareno on 17/04/15.
 */
public class DocContextMenuListController extends ViewController{
    public enum WhereOpened {MAIN_SCREEN, OFFLINE_SCREEN, FOLDER, SEARCH};
    private final String TAG = "ContextMenuControl";
    private int chosenAction = BottomSheetAction.NOTHING;
    private DocContextMenuAdapter adapter;
    private DocActionsView.MenuDelegate delegate;
    private Document attachedDoc;
    private Folder attachedFolder;
    private final BottomSheetAction SHARE = new BottomSheetAction(BottomSheetAction.SHARE_WITH_A_FRIEND, R.drawable.ic_share, AppLoader.sharedInstance().getString(R.string.share_with_a_friend));
    private final BottomSheetAction MOVE_TO_FOLDER =new BottomSheetAction(BottomSheetAction.MOVE_TO_FOLDER, R.drawable.ic_folder, AppLoader.sharedInstance().getString(R.string.move_to_folder));
    private final BottomSheetAction REMOVE_FROM_FOLDER =new BottomSheetAction(BottomSheetAction.REMOVE_FROM_FOLDER, R.drawable.ic_folder, AppLoader.sharedInstance().getString(R.string.remove_from_folder));
    private final BottomSheetAction MAKE_OFFLINE = new BottomSheetAction(BottomSheetAction.MAKE_OFFLINE, R.drawable.ic_smartphone_big, AppLoader.sharedInstance().getString(R.string.make_offline));
    private final BottomSheetAction REVOKE_OFFLINE = new BottomSheetAction(BottomSheetAction.REVOKE_OFFLINE, R.drawable.ic_smartphone_big, AppLoader.sharedInstance().getString(R.string.revoke_offline));
    private final BottomSheetAction RENAME = new BottomSheetAction(BottomSheetAction.RENAME, R.drawable.ic_edit, AppLoader.sharedInstance().getString(R.string.rename));
    private final BottomSheetAction DELETE = new BottomSheetAction(BottomSheetAction.DELETE, R.drawable.ic_del, AppLoader.sharedInstance().getString(R.string.delete));
    private final BottomSheetAction SELECT = new BottomSheetAction(BottomSheetAction.SELECT, R.drawable.ic_select, AppLoader.sharedInstance().getString(R.string.select));
    private final ArrayList<BottomSheetAction> docActions = new ArrayList<BottomSheetAction>() {{
        add(SHARE);
        add(MOVE_TO_FOLDER);
        add(MAKE_OFFLINE);
        add(SELECT);
        add(RENAME);
        add(DELETE);
    }};
    private final ArrayList<BottomSheetAction> folderActions = new ArrayList<BottomSheetAction>() {{
        add(RENAME);
        add(DELETE);
    }};
    private final ArrayList<BottomSheetAction> actions = new ArrayList<BottomSheetAction>() {{
        add(SHARE);
        add(MOVE_TO_FOLDER);
        add(MAKE_OFFLINE);
        add(RENAME);
        add(DELETE);
    }};
    private final ArrayList<BottomSheetAction> openedInFolder = new ArrayList<BottomSheetAction>() {{
        add(SHARE);
        add(REMOVE_FROM_FOLDER);
        add(MAKE_OFFLINE);
        add(SELECT);
        add(RENAME);
        add(DELETE);
    }};
    private final ArrayList<BottomSheetAction> openedInOffline = new ArrayList<BottomSheetAction>() {{
        add(SHARE);
        add(REVOKE_OFFLINE);
    }};
    private final ArrayList<BottomSheetAction> openedInSearch = new ArrayList<BottomSheetAction>() {{
        add(SHARE);
        add(RENAME);
        add(MAKE_OFFLINE);
        add(DELETE);
    }};
    public DocContextMenuListController(View v) {
        super(v);
        init();
    }
    private void init()
    {
        this.adapter = new DocContextMenuAdapter(this, actions);
        initViewSettings();
    }
    @Override
    protected void onViewReplace() {
        if(getView() != null) {
            initViewSettings();
        }
    }
    public DocContextMenuAdapter getAdapter() { return this.adapter; }
    private void initViewSettings() {
        BottomSheetListView v = (BottomSheetListView) getView();
        v.setNewElementCount(actions.size());
        v.setAdapter(adapter);
        v.setMotionEventSplittingEnabled(false);
    }

    @Override
    public void onFragmentPause() {
    }

    @Override
    public void onFragmentResume() {
    }
    @Override
    public void onFragmentDestroy() {

    }

    public void didActionChosen(BottomSheetAction act)
    {
        this.chosenAction = act.getAction();
        if(delegate != null && chosenAction != BottomSheetAction.RENAME)
            delegate.onActionChoose();
        else
        {
            if(delegate != null && attachedDoc != null)
                delegate.didRenameRequested(attachedDoc);
            else if(delegate != null && attachedFolder != null)
                delegate.didRenameRequested(attachedFolder);
        }
    }
    public void continueProcessing()
    {
        if(delegate != null && attachedDoc != null && chosenAction != BottomSheetAction.NOTHING)
        {
            if(chosenAction == BottomSheetAction.SHARE_WITH_A_FRIEND)
                delegate.didShareWithFriendRequested(attachedDoc);
            else if(chosenAction == BottomSheetAction.MOVE_TO_FOLDER)
                delegate.didMoveToFolderRequested(attachedDoc);
            else if(chosenAction == BottomSheetAction.REMOVE_FROM_FOLDER)
                delegate.didRemoveFromFolderRequested(attachedDoc);
            else if(chosenAction == BottomSheetAction.MAKE_OFFLINE)
                delegate.didOfflineAccessRequested(attachedDoc);
            else if(chosenAction == BottomSheetAction.REVOKE_OFFLINE)
                delegate.didRevokeOfflineAccessRequested(attachedDoc);
            else if(chosenAction == BottomSheetAction.DELETE)
                delegate.didRemoveRequested(attachedDoc);
            else if(chosenAction == BottomSheetAction.SELECT)
                delegate.onSelectionModeRequested(FileCache.getInstance().getDocument(attachedDoc));
        }
        else if(delegate != null && attachedFolder != null && chosenAction != BottomSheetAction.NOTHING)
        {
            if(chosenAction == BottomSheetAction.DELETE)
                delegate.didRemoveRequested(attachedFolder);
        }
        chosenAction = BottomSheetAction.NOTHING;
        attachedDoc = null;
    }

    public void setDelegate(DocActionsView.MenuDelegate delegateRef)
    {
        this.delegate = delegateRef;
    }
    public void attachDocument(Document doc, WhereOpened whereOpened)
    {
        this.attachedDoc = doc;
        this.attachedFolder = null;
        if(delegate == null)
            return;
        actions.clear();
        if(whereOpened == WhereOpened.MAIN_SCREEN)
            actions.addAll(docActions);
        else if(whereOpened == WhereOpened.FOLDER)
            actions.addAll(openedInFolder);
        else if(whereOpened == WhereOpened.OFFLINE_SCREEN)
            actions.addAll(openedInOffline);
        else if(whereOpened == WhereOpened.SEARCH)
            actions.addAll(openedInSearch);
        if(doc.getDownloadState() == DownloadState.DOWNLOADED && whereOpened != WhereOpened.OFFLINE_SCREEN)
        {
            actions.remove(2);
            actions.add(2, REVOKE_OFFLINE);
        }
        if(getView() != null && getView() instanceof BottomSheetListView)
        {
            ((BottomSheetListView) getView()).setNewElementCount(actions.size());
        }
        adapter.notifyDataSetChanged();
    }
    public void attachFolder(Folder f)
    {
        this.attachedDoc = null;
        this.attachedFolder = f;
        if(delegate == null)
            return;
        actions.clear();
        actions.addAll(folderActions);
        if(getView() != null && getView() instanceof BottomSheetListView)
        {
            ((BottomSheetListView) getView()).setNewElementCount(actions.size());
        }
        adapter.notifyDataSetChanged();
    }
    public Document getAttachedDocument() { return this.attachedDoc; }
}
