package com.andremacareno.vkdocs.controllers;

import android.view.View;

/**
 * Created by Andrew on 07.01.2016.
 */
public abstract class ViewController {
    private View view;
    public ViewController(View v)
    {
        this.view = v;
    }
    protected View getView() { return this.view; }
    public void replaceView(View v) {
        if(v == this.view)
            return;
        this.view = v;
        onViewReplace();
    }
    protected void onViewReplace() { }
    public abstract void onFragmentPause();
    public abstract void onFragmentResume();
    public abstract void onFragmentDestroy();
}