package com.andremacareno.vkdocs.controllers;


import com.andremacareno.vkdocs.views.ActionView;

import java.lang.ref.WeakReference;

/**
 * Created by Andrew on 13.05.2015.
 */
public abstract class ActionViewController {
    protected WeakReference<ActionView> actionView;
    public void setActionView(ActionView v)
    {
        this.actionView = new WeakReference<ActionView>(v);
        this.onActionViewSet();
    }
    public ActionView getActionView() { return this.actionView.get(); }
    public abstract void onFragmentPaused();
    public abstract void onFragmentResumed();
    public abstract void onActionViewRemoved();
    protected void onActionViewSet() {};
}
