package com.andremacareno.vkdocs.controllers;

import android.util.Log;
import android.view.View;

import com.andremacareno.asynccore.BackgroundTask;
import com.andremacareno.asynccore.OnTaskCompleteListener;
import com.andremacareno.vkdocs.AppLoader;
import com.andremacareno.vkdocs.BuildConfig;
import com.andremacareno.vkdocs.listadapters.BooksDashboardAdapter;
import com.andremacareno.vkdocs.objs.BookWrap;
import com.andremacareno.vkdocs.tasks.LoadBooksDashboardTask;
import com.andremacareno.vkdocs.views.BooksGridView;

import java.util.ArrayList;

/**
 * Created by Andrew on 07.01.2016.
 */
public class BooksGridController extends ViewController {
    private final ArrayList<BookWrap> books = new ArrayList<>();
    private BooksDashboardAdapter adapter;
    private final BooksDashboardAdapter.DashboardDelegate delegate;
    private final Runnable updRunnable = new Runnable() {
        @Override
        public void run() {
            synchronized(books)
            {
                if(delegate != null)
                    delegate.controlEmptyView(books.isEmpty());
            }
            adapter.notifyDataSetChanged();
        }
    };
    public BooksGridController(View v, BooksDashboardAdapter.DashboardDelegate delegate) {
        super(v);
        this.delegate = delegate;
        init();
    }
    private void init()
    {
        initView();
        loadList();
    }
    private void initView()
    {
        if(getView() == null)
            return;
        BooksGridView list = (BooksGridView) getView();
        if(adapter == null)
            adapter = new BooksDashboardAdapter(books, delegate);
        list.setAdapter(adapter);
    }
    @Override
    protected void onViewReplace()
    {
        initView();
    }
    @Override
    public void onFragmentPause() {
    }

    @Override
    public void onFragmentResume() {
    }

    @Override
    public void onFragmentDestroy() {
    }
    private void loadList()
    {
        LoadBooksDashboardTask t = new LoadBooksDashboardTask();
        t.onComplete(new OnTaskCompleteListener() {
            @Override
            public void taskComplete(BackgroundTask task) {
                LoadBooksDashboardTask cast = (LoadBooksDashboardTask) task;
                synchronized(books)
                {
                    if(BuildConfig.DEBUG)
                        Log.d("BooksGrid", String.format("count = %d", cast.getResults().size()));
                    books.addAll(cast.getResults());
                }
                AppLoader.applicationHandler.post(updRunnable);
            }
        });
        t.addToQueue();
    }

}
