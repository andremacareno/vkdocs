package com.andremacareno.vkdocs;

import android.animation.Animator;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.andremacareno.asynccore.BackgroundTask;
import com.andremacareno.asynccore.OnTaskCompleteListener;
import com.andremacareno.asynccore.OnTaskFailureListener;
import com.andremacareno.asynccore.notificationcenter.Notification;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.asynccore.notificationcenter.NotificationObserver;
import com.andremacareno.screencore.BaseScreen;
import com.andremacareno.screencore.ScreenManager;
import com.andremacareno.vkdocs.cache.FolderInfoCache;
import com.andremacareno.vkdocs.fragments.AuthFragment;
import com.andremacareno.vkdocs.fragments.MainFragment;
import com.andremacareno.vkdocs.objs.CurrentUser;
import com.andremacareno.vkdocs.tasks.AppInitTask;
import com.andremacareno.vkdocs.views.BottomSheet;
import com.andremacareno.vkdocs.views.NavDrawerLayout;
import com.andremacareno.vkdocs.views.NavDrawerListener;
import com.andremacareno.vkdocs.views.NavDrawerView;
import com.andremacareno.vkdocs.vk.VKAuthObserver;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiUserFull;
import com.vk.sdk.api.model.VKList;

public class VKDocsMainActivity extends AppCompatActivity implements BackPressProcessor {

    private volatile boolean offlineWork = false;

    private NotificationObserver authObserver;
    private BackPressListener backPressInterceptor;
    private FloatingActionButton fab;

    //navdrawer
    private NavDrawerLayout navDrawerLayout;
    private NavDrawerView navigationView;


    //bottom sheet
    private final int bottomSheetColorFrom = Color.argb(0, 0, 0, 0);
    private final int bottomSheetColorTo = Color.argb(190, 0, 0, 0);
    private boolean bottomSheetDismissed = false;
    private NavDrawerListener selectionListener;
    private final DecelerateInterpolator slideUpInterpolator = new DecelerateInterpolator(1.7f);
    private final AccelerateInterpolator slideDownInterpolator = new AccelerateInterpolator(1.7f);
    private final NotificationObserver connectionObserver = new NotificationObserver(NotificationCenter.didNetworkConnectionStateChanged) {
        @Override
        public void didNotificationReceived(Notification notification) {
            try
            {
                offlineWork = !((boolean) notification.getObject());
                if(BuildConfig.DEBUG)
                    Log.d("MainActivity", String.format("offlineWork set to %s", Boolean.valueOf(offlineWork)));
            }
            catch(Exception ignored) {}
        }
    };
    private final Animator.AnimatorListener slideDownListener = new Animator.AnimatorListener() {
        @Override
        public void onAnimationStart(Animator animator) {
        }

        @Override
        public void onAnimationEnd(Animator animator) {
            if(attachedBottomSheet != null) {
                attachedBottomSheet.onClose(bottomSheetDismissed);
                bottomSheetContainer.removeView((View) attachedBottomSheet);
                bottomSheetContainer.setVisibility(View.GONE);
                attachedBottomSheet = null;
            }
            bottomSheetDismissed = false;
        }

        @Override
        public void onAnimationCancel(Animator animator) {
        }

        @Override
        public void onAnimationRepeat(Animator animator) {

        }
    };
    private FloatingActionButton.Behavior fabDefaultBehavior = null;
    private BottomSheet attachedBottomSheet = null;
    private FrameLayout bottomSheetContainer;
    private boolean processMenuButton = true;
    private final Runnable onLogOutRunnable = new Runnable() {
        @Override
        public void run() {
            loggingOut = true;
            performLogOut();
            loggingOut = false;
        }
    };
    private boolean loggingOut = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        authObserver = new NotificationObserver(NotificationCenter.didLoggedOut) {
            @Override
            public void didNotificationReceived(Notification notification) {
                try
                {
                    runOnUiThread(onLogOutRunnable);
                }
                catch(Exception ignored) {}
            }
        };
        NotificationCenter.getInstance().addObserver(authObserver);
        NotificationCenter.getInstance().addObserver(connectionObserver);
        setContentView(R.layout.main);
        fab = (FloatingActionButton) findViewById(R.id.newDocumentFab);
        FrameLayout container = (FrameLayout) findViewById(R.id.container);
        navDrawerLayout = (NavDrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavDrawerView) findViewById(R.id.navigation);
        bottomSheetContainer = (FrameLayout) findViewById(R.id.bottom_sheet_container);
        bottomSheetContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDismissed = true;
                slideDownBottomSheet();
            }
        });
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(final MenuItem menuItem) {
                //drawerLayout.closeDrawer(GravityCompat.START);
                //TODO uncomment
                int actionToSend;
                switch (menuItem.getItemId()) {
                    case R.id.drawer_settings:
                        actionToSend = NavDrawerView.GOTO_SETTINGS;
                        break;
                    case R.id.drawer_offline:
                        actionToSend = NavDrawerView.GOTO_OFFLINE;
                        break;
                    case R.id.drawer_files:
                        actionToSend = NavDrawerView.GOTO_FILES;
                        break;
                    case R.id.drawer_books:
                        actionToSend = NavDrawerView.GOTO_BOOKS;
                        break;
                    default:
                        return true;
                }
                selectionListener.setAction(actionToSend);
                NotificationCenter.getInstance().postNotification(NotificationCenter.didNavDrawerActionSelected, actionToSend);
                return true;
            }
        });
        selectionListener = new NavDrawerListener(new Runnable() {
            @Override
            public void run() {
                closeNavDrawer();
            }
        }, new Runnable() {
            @Override
            public void run() {
                if(navigationView != null)
                    navigationView.refreshAvatar();
            }
        });
        NotificationCenter.getInstance().addObserver(selectionListener);
        navDrawerLayout.setDrawerListener(selectionListener);
        ScreenManager.getInstance().attachActivity(this, container);
        if(!Configuration.sharedInstance().reducedAnimations)
        {
            ScreenManager.getInstance().setOpenAnimation(AnimationUtils.loadAnimation(this, R.anim.scr_slide_up));
            ScreenManager.getInstance().setCloseAnimation(AnimationUtils.loadAnimation(this, R.anim.scr_slide_down));
        }
        if(savedInstanceState == null)
        {
            loadInitialFragment();
        }
    }
    public void enableNavDrawer()
    {
        if(navDrawerLayout != null)
            navDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }
    public void disableNavDrawer()
    {
        if(navDrawerLayout != null)
            navDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }
    public void closeNavDrawer()
    {
        if(navDrawerLayout != null)
            navDrawerLayout.closeDrawers();
    }
    public void showNavDrawer()
    {
        if(navDrawerLayout != null && navigationView != null)
            navDrawerLayout.openDrawer(navigationView);
    }
    private void loadInitialFragment()
    {
        if(VKAuthObserver.getAuthState() == VKAuthObserver.AuthState.AUTH_OK) {
            onAccessToken(VKAccessToken.currentToken());
            //loadMainFragment();
        }
        else
            loadAuthFragment(false);
    }
    private void loadMainFragment()
    {
        BaseScreen fr = new MainFragment();
        ScreenManager.getInstance().addScreen(fr, false);
    }
    private void loadOfflineFragment()
    {
        BaseScreen fr = new MainFragment();
        Bundle args = new Bundle();
        args.putBoolean(Constants.ARG_START_OFFLINE, true);
        fr.setArguments(args);
        ScreenManager.getInstance().addScreen(fr, false);
    }
    private void loadAuthFragment(boolean dueToLogout)
    {
        AuthFragment fr = new AuthFragment();
        ScreenManager.getInstance().addScreen(fr, false);
        /*fr.setRetainInstance(false);
        if(dueToLogout)
            ft.replace(R.id.container, fr, Constants.AUTH_FRAGMENT).commitAllowingStateLoss();
        else
            ft.add(R.id.container, fr, Constants.AUTH_FRAGMENT).commitAllowingStateLoss();*/
    }
    public void performLogOut()
    {
        AppLoader.sharedInstance().sharedPreferences().edit().clear().apply();
        ScreenManager.getInstance().clearWithBackStack();
        loadAuthFragment(true);
    }
    public void hideFAB()
    {
        CoordinatorLayout.LayoutParams lp = (CoordinatorLayout.LayoutParams) fab.getLayoutParams();
        if(fabDefaultBehavior == null)
            fabDefaultBehavior = (FloatingActionButton.Behavior) (lp.getBehavior());
        lp.setBehavior(null);
        fab.setLayoutParams(lp);
        fab.setVisibility(View.GONE);
    }
    public void showFAB()
    {
        if(fabDefaultBehavior == null)
            return;
        CoordinatorLayout.LayoutParams lp = (CoordinatorLayout.LayoutParams) fab.getLayoutParams();
        lp.setBehavior(fabDefaultBehavior);
        fab.setLayoutParams(lp);
        fab.setVisibility(View.VISIBLE);
    }
    @Override
    public void onDestroy()
    {
        super.onDestroy();
        try
        {
            ScreenManager.getInstance().onActivityDestroyed();
            if(bottomSheetContainer != null)
                bottomSheetContainer.removeAllViews();
            if(selectionListener != null)
                NotificationCenter.getInstance().removeObserver(selectionListener);
            NotificationCenter.getInstance().addObserver(connectionObserver);
            selectionListener = null;
        }
        catch(Exception ignored) {}
    }

    public FloatingActionButton getFAB() { return this.fab; }

//auth processing
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
                onAccessToken(res);
            }

            @Override
            public void onError(VKError error) {
                finish();
            }
        }))
        {
            super.onActivityResult(requestCode, resultCode, data);
            BaseScreen presenting = ScreenManager.getInstance().getPresentingScreen();
            if (presenting != null) {
                presenting.onActivityResult(requestCode, resultCode, data);
            }
        }
    }
    private void onLoginError(final VKApiUserFull meInfo)
    {
        offlineWork = true;
        AppLoader.applicationHandler.post(new Runnable() {
            @Override
            public void run() {
                if (meInfo != null)
                    initApp(meInfo);
                else {
                    Toast.makeText(getApplicationContext(), R.string.error_logging_in, Toast.LENGTH_LONG).show();
                    finish();
                }
            }
        });
    }
    @Override
    public void onResume()
    {
        super.onResume();
        AppLoader.sharedInstance().startConnectionChangeBroadcasting();
        ScreenManager.getInstance().onResume();
        FolderInfoCache.sharedInstance().queryFinishReceiver.resume();
    }
    public void onPause()
    {
        super.onPause();
        AppLoader.sharedInstance().stopConnectionChangeBroadcasting();
        ScreenManager.getInstance().onPause();
        FolderInfoCache.sharedInstance().queryFinishReceiver.pause();
    }
    //back button processing
    @Override
    public void onBackPressed()
    {
        if(navDrawerLayout != null && navDrawerLayout.isDrawerOpen(navigationView)) {
            navDrawerLayout.closeDrawers();
            return;
        }
        if(attachedBottomSheet != null)
        {
            bottomSheetDismissed = true;
            slideDownBottomSheet();
            return;
        }
        if(backPressInterceptor != null)
            backPressInterceptor.onBackPressed();
        else if(ScreenManager.getInstance().haveBackStackEntries())
            processBackButton();
        else
            super.onBackPressed();
    }
    public void showBottomSheet(BottomSheet bottomSheet)
    {
        if(bottomSheet == null)
            return;
        if(attachedBottomSheet != null)
            return;
        this.attachedBottomSheet = bottomSheet;
        bottomSheetContainer.setVisibility(View.VISIBLE);
        FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) ((View) bottomSheet).getLayoutParams();
        lp.gravity = Gravity.BOTTOM;
        bottomSheetContainer.addView((View) attachedBottomSheet);
        slideUpBottomSheet();
    }
    private void slideUpBottomSheet()
    {
        if(attachedBottomSheet == null)
            return;
        fab.hide();
        ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), bottomSheetColorFrom, bottomSheetColorTo);
        ObjectAnimator animator = ObjectAnimator.ofFloat(attachedBottomSheet, "yFraction", 1.0f, 0).setDuration(Configuration.sharedInstance().reducedAnimations ? 0 : 250);
        animator.setInterpolator(slideUpInterpolator);
        colorAnimation.setInterpolator(slideUpInterpolator);
        colorAnimation.setDuration(Configuration.sharedInstance().reducedAnimations ? 0 : 250);
        colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                if (bottomSheetContainer != null)
                    bottomSheetContainer.setBackgroundColor((int) animator.getAnimatedValue());
            }
        });
        colorAnimation.start();
        animator.start();
    }
    public void slideDownBottomSheet()
    {
        if(attachedBottomSheet == null)
            return;
        ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), bottomSheetColorTo, bottomSheetColorFrom);
        ObjectAnimator animator = ObjectAnimator.ofFloat(attachedBottomSheet, "yFraction", 0f, 1.0f).setDuration(Configuration.sharedInstance().reducedAnimations ? 0 : 250);
        animator.setInterpolator(slideUpInterpolator);
        colorAnimation.setInterpolator(slideUpInterpolator);
        colorAnimation.setDuration(Configuration.sharedInstance().reducedAnimations ? 0 : 250);
        colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                if (bottomSheetContainer != null)
                    bottomSheetContainer.setBackgroundColor((int) animator.getAnimatedValue());
            }
        });
        colorAnimation.start();
        animator.addListener(slideDownListener);
        animator.setInterpolator(slideDownInterpolator);
        animator.start();
    }
    public void performGoBack() {
        if(backPressInterceptor != null)
            backPressInterceptor.onBackPressed();
        else
            processBackButton();
    }
    private void processBackButton()
    {
        ScreenManager.getInstance().popBackStack();
    }
    public void removeBackPressInterceptor(BackPressListener listener)
    {
        if(listener == null)
            return;
        if(listener == this.backPressInterceptor)
            this.backPressInterceptor = null;
    }
    public void interceptBackPress(BackPressListener interceptor) {
        this.backPressInterceptor = interceptor;
    }
    private void onAccessToken(VKAccessToken res)
    {
        VKRequest.VKRequestListener getMeHandler = new VKRequest.VKRequestListener() {
            private void fallbackInit()
            {
                try
                {
                    VKApiUserFull meInfo = new VKApiUserFull();
                    if(!fillFromPrefs(meInfo)) {
                        onLoginError(null);
                        return;
                    }
                    onLoginError(meInfo);
                }
                catch(Exception ignored) { onLoginError(null); }
            }
            private boolean fillFromPrefs(VKApiUserFull obj)
            {
                try
                {
                    SharedPreferences prefs = AppLoader.sharedInstance().sharedPreferences();
                    if(!prefs.contains(Constants.PREFS_USER_ID))
                        return false;
                    if(!prefs.contains(Constants.PREFS_FIRST_NAME))
                        return false;
                    if(!prefs.contains(Constants.PREFS_LAST_NAME))
                        return false;
                    if(!prefs.contains(Constants.PREFS_PHOTO))
                        return false;
                    obj.id = prefs.getInt(Constants.PREFS_USER_ID, 0);
                    obj.first_name = prefs.getString(Constants.PREFS_FIRST_NAME, "");
                    obj.last_name = prefs.getString(Constants.PREFS_LAST_NAME, "");
                    if(AndroidUtilities.isLowDPIScreen())
                        obj.photo_50 = prefs.getString(Constants.PREFS_PHOTO, "");
                    else
                        obj.photo_100 = prefs.getString(Constants.PREFS_PHOTO, "");
                    return true;
                }
                catch(Exception ignored) { return false; }
            }
            @Override
            public void onComplete(VKResponse response) {
                offlineWork = false;
                try
                {
                    VKList<VKApiUserFull> meWrap = (VKList<VKApiUserFull>) response.parsedModel;
                    VKApiUserFull meInfo = meWrap.isEmpty() ? null : meWrap.get(0);
                    if(meInfo == null) {
                        meInfo = new VKApiUserFull();
                        if(!fillFromPrefs(meInfo)) {
                            onLoginError(null);
                            return;
                        }
                    }
                    else
                    {
                        SharedPreferences prefs = AppLoader.sharedInstance().sharedPreferences();
                        SharedPreferences.Editor prefEdit = prefs.edit();
                        prefEdit.putInt(Constants.PREFS_USER_ID, meInfo.id);
                        prefEdit.putString(Constants.PREFS_FIRST_NAME, meInfo.first_name);
                        prefEdit.putString(Constants.PREFS_LAST_NAME, meInfo.last_name);
                        prefEdit.putString(Constants.PREFS_PHOTO, AndroidUtilities.isLowDPIScreen() ? meInfo.photo_50 : meInfo.photo_100);
                        prefEdit.apply();
                    }
                    initApp(meInfo);
                }
                catch(Exception e) {
                    e.printStackTrace();
                    fallbackInit();
                }
            }

            @Override
            public void onError(VKError error) {
                fallbackInit();
            }
        };
        if(!res.accessToken.isEmpty())
        {
            if(BuildConfig.DEBUG)
                Log.d("MainActivity", "requesting info about self");
            VKRequest getMeRequest = VKApi.users().get(VKParameters.from(VKApiConst.FIELDS, AndroidUtilities.isLowDPIScreen() ? "photo_50" : "photo_100"));
            getMeRequest.attempts = 10;
            getMeRequest.executeWithListener(getMeHandler);
        }
    }
    private void initApp(final VKApiUserFull meInfo)
    {
        AppInitTask initDB = new AppInitTask();
        initDB.onComplete(new OnTaskCompleteListener() {
            @Override
            public void taskComplete(BackgroundTask task) {
                AppLoader.applicationHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        try
                        {
                            if(BuildConfig.DEBUG)
                                Log.d("MainActivity", String.format("me = %s", meInfo.toString()));
                            CurrentUser.createInstance(meInfo.id, String.format("%s %s", meInfo.first_name, meInfo.last_name), AndroidUtilities.isLowDPIScreen() ? meInfo.photo_50 : meInfo.photo_100);
                            navigationView.initHeader();
                            if(offlineWork)
                                loadOfflineFragment();
                            else
                                loadMainFragment();
                        }
                        catch(Exception ignored) {}
                    }
                });
            }
        });
        initDB.onFailure(new OnTaskFailureListener() {
            @Override
            public void taskFailed(BackgroundTask task) {
                AppLoader.applicationHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Toast.makeText(VKDocsMainActivity.this, R.string.error_init_db, Toast.LENGTH_LONG).show();
                            finish();
                        } catch (Exception ignored) {
                        }
                    }
                });
            }
        });
        initDB.addToQueue();
    }
    public void disableMenuButtonProcessing(boolean disable)
    {
        this.processMenuButton = !disable;
    }
    public boolean checkOfflineWork() { return this.offlineWork; }
    public boolean isLoggingOut() { return this.loggingOut; }
    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        final int keycode = event.getKeyCode();
        final int action = event.getAction();
        if (keycode == KeyEvent.KEYCODE_MENU && action == KeyEvent.ACTION_UP && processMenuButton) {
            if(navigationView != null && navDrawerLayout.getDrawerLockMode(navigationView) != DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
            {
                if(navDrawerLayout.isDrawerOpen(navigationView))
                    navDrawerLayout.closeDrawer(navigationView);
                else
                    navDrawerLayout.openDrawer(navigationView);
            }
            return true; // consume the key press
        }
        return super.dispatchKeyEvent(event);
    }
}
