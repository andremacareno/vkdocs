package com.andremacareno.vkdocs.network;

import com.andremacareno.vkdocs.dao.Document;

/**
 * Created by andremacareno on 11/03/16.
 */
public interface DocumentManagerTask {
    void pauseIfNeed();
    void unpause();
    void cancel();
    long getTaskId();
    Document getBoundDoc();
}
