package com.andremacareno.vkdocs.network;

import android.support.v4.util.LongSparseArray;
import android.util.Log;

import com.andremacareno.vkdocs.BuildConfig;
import com.andremacareno.vkdocs.DownloadState;
import com.andremacareno.vkdocs.TempDownloadDelegate;
import com.andremacareno.vkdocs.dao.Document;

import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by Andrew on 09.01.2016.
 */
public class DocumentLoadManager {
    private static volatile DocumentLoadManager _instance;
    public final LongSparseArray<DocumentManagerTask> runningTasks = new LongSparseArray<>();
    public final AtomicBoolean paused = new AtomicBoolean(false);
    public final AtomicLong taskId = new AtomicLong(0);
    private ExecutorService threadPool;
    private DocumentLoadManager() {
        final int threadCount = Runtime.getRuntime().availableProcessors() == 0 ? 4 : Runtime.getRuntime().availableProcessors();
        this.threadPool = Executors.newFixedThreadPool(threadCount);
    }
    public static DocumentLoadManager sharedInstance() {
        if(_instance == null)
        {
            synchronized(DocumentLoadManager.class)
            {
                if(_instance == null)
                    _instance = new DocumentLoadManager();
            }
        }
        return _instance;
    }
    public void requestDownload(Document doc)
    {
        if(doc == null)
            throw new NullPointerException("doc == null");
        if(doc.getDownloadState() == DownloadState.DOWNLOADING || doc.getDownloadState() == DownloadState.DOWNLOADED || doc.isUploading())
            return;
        synchronized(taskId)
        {
            threadPool.submit(new DocDownloadWorker(doc.getUrl(), doc, taskId.getAndIncrement()));
        }
    }
    public void requestTemporaryDownload(Document doc, TempDownloadDelegate delegate)
    {
        if(doc == null || delegate == null)
            throw new NullPointerException("doc == null || delegate == null");
        if(doc.getDownloadState() == DownloadState.DOWNLOADING || doc.getDownloadState() == DownloadState.DOWNLOADED || doc.isUploading())
            return;
        synchronized(taskId)
        {
            threadPool.submit(new DocTmpDownloadWorker(doc.getUrl(), doc, taskId.getAndIncrement(), delegate));
        }
    }
    public void requestUpload(File f, Document doc, String uploadUrl)
    {
        if(doc == null)
            throw new NullPointerException("doc == null");
        if(!doc.isUploading()) {
            if(BuildConfig.DEBUG)
                Log.d("LoadManager", "isUploading == false");
            return;
        }
        synchronized(taskId)
        {
            if(BuildConfig.DEBUG)
                Log.d("LoadManager", "task submit");
            threadPool.submit(new DocUploadWorker(f, uploadUrl, doc, taskId.getAndIncrement()));
        }
    }
    public void pause()
    {
        synchronized(paused)
        {
            paused.set(true);
        }
    }
    public void unpause()
    {
        synchronized(paused)
        {
            paused.set(false);
        }
        unpauseRunning();
    }
    private void unpauseRunning()
    {
        try
        {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    synchronized(runningTasks)
                    {
                        for(int i = 0; i < runningTasks.size(); i++)
                        {
                            try
                            {
                                DocumentManagerTask task = runningTasks.get(runningTasks.keyAt(i));
                                task.unpause();
                            }
                            catch(Exception ignored) {}
                        }
                    }
                }
            }).start();
        }
        catch(Exception e) { e.printStackTrace(); }
    }
    public void didTaskStarted(DocumentManagerTask task)
    {
        synchronized(runningTasks)
        {
            runningTasks.put(task.getTaskId(), task);
        }
    }
    public void didTaskFinished(long id)
    {
        synchronized(runningTasks)
        {
            runningTasks.remove(id);
        }
    }
    public void cancelLoad(final Document doc)
    {
        try
        {
            if(BuildConfig.DEBUG)
                Log.d("DocLoadManager", "received cancel request");
            new Thread(new Runnable() {
                @Override
                public void run() {
                    synchronized(runningTasks)
                    {
                        long idToRemove = -1;
                        for(int i = 0; i < runningTasks.size(); i++)
                        {
                            try
                            {
                                DocumentManagerTask task = runningTasks.get(runningTasks.keyAt(i));
                                if(BuildConfig.DEBUG)
                                    Log.d("DocLoadManager", String.format("task docId = %d; docId = %d", task.getBoundDoc().getId(), doc.getId()));
                                if(task.getBoundDoc().getId().equals(doc.getId())) {
                                    idToRemove = task.getTaskId();
                                    task.cancel();
                                    break;
                                }
                            }
                            catch(Exception ignored) {}
                        }
                        if(idToRemove != -1)
                            didTaskFinished(idToRemove);
                    }
                }
            }).start();
        }
        catch(Exception ignored) {}
    }

}
