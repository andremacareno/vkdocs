package com.andremacareno.vkdocs.network;

import android.os.Process;
import android.util.Log;

import com.andremacareno.vkdocs.AppLoader;
import com.andremacareno.vkdocs.BuildConfig;
import com.andremacareno.vkdocs.Configuration;
import com.andremacareno.vkdocs.DownloadDelegate;
import com.andremacareno.vkdocs.TempDownloadDelegate;
import com.andremacareno.vkdocs.dao.Document;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by Andrew on 09.01.2016.
 */
public class DocTmpDownloadWorker implements Runnable, DocumentManagerTask {
    private String stringUrl;
    private String pathToSave;
    private String offlinePathToSave;
    private final String cacheDir;
    private final String offlineCacheDir;
    private WeakReference<Document> docRef;
    private final Object pauseObject = new Object();
    private final long id;
    private final AtomicBoolean cancelled = new AtomicBoolean(false);
    private TempDownloadDelegate progressDialogDelegate;
    public DocTmpDownloadWorker(String url, Document documentRef, long taskId, TempDownloadDelegate progressDialogDelegate)
    {
        this.progressDialogDelegate = progressDialogDelegate;
        this.cacheDir = AppLoader.sharedInstance().tmpCacheLocation();
        this.offlineCacheDir = AppLoader.sharedInstance().docsCacheLocation();
        this.stringUrl = url;
        this.pathToSave = cacheDir.concat(documentRef.getId().toString()).concat(".".concat(documentRef.getExt()));
        this.offlinePathToSave = offlineCacheDir.concat(documentRef.getId().toString()).concat(".".concat(documentRef.getExt()));
        docRef = new WeakReference<Document>(documentRef);
        this.id = taskId;
    }
    @Override
    public void run() {
        Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
        boolean publishProgress = docRef != null && docRef.get() != null;
        InputStream input = null;
        OutputStream output = null;
        HttpURLConnection connection = null;
        DocumentLoadManager.sharedInstance().didTaskStarted(this);
        boolean success = false;
        int attemptsCount = 5;
        do {
            try {
                pauseIfNeed();
                URL url = new URL(stringUrl);
                connection = (HttpURLConnection) url.openConnection();
                connection.setReadTimeout(10000);
                connection.setConnectTimeout(5000);
                connection.connect();
                if(Configuration.sharedInstance().shouldSave(docRef.get()))
                {
                    File cacheFolder = new File(offlineCacheDir);
                    if(!cacheFolder.exists())
                        cacheFolder.mkdir();
                    output = new FileOutputStream(offlinePathToSave);
                }
                else
                {
                    File cacheFolder = new File(cacheDir);
                    if(!cacheFolder.exists())
                        cacheFolder.mkdir();
                    File f = new File(pathToSave);
                    if(f.exists() && !f.isDirectory()) {
                        if(BuildConfig.DEBUG)
                            Log.d("TmpDownload", "found file in tmp directory; no need to download again");
                        success = true;
                        break;
                    }
                    output = new FileOutputStream(pathToSave);
                }
                //Status check
                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK)
                    throw new IllegalStateException("Expected 200 OK status, got" + connection.getResponseCode());

                //if > 0, use for progress publishing (if needed)
                int fileLength = connection.getContentLength();

                // download buffers
                input = connection.getInputStream();
                boolean onWifi = AppLoader.sharedInstance().isConnectedToWifi();
                byte data[] = new byte[onWifi ? Configuration.PACKAGE_SIZE_WIFI : Configuration.PACKAGE_SIZE_MOBILE_NETWORK];
                long ready = 0;
                float previousProgress = 0f;
                int count;
                while ((count = input.read(data)) != -1) {
                    synchronized(cancelled)
                    {
                        if(cancelled.get())
                            throw new UnsupportedOperationException();
                    }
                    ready += count;
                    float currentProgress = (float) ready / fileLength;
                    if (fileLength > 0 && publishProgress && (currentProgress - previousProgress) > 0.05f && progressDialogDelegate != null && docRef != null && docRef.get() != null) {
                        progressDialogDelegate.didFileProgressUpdated(docRef.get(), currentProgress);
                        previousProgress = currentProgress;
                    }
                    output.write(data, 0, count);
                }
                success = true;
            } catch (Exception ignored) {
                synchronized(cancelled)
                {
                    if(cancelled.get()) {
                        progressDialogDelegate.didFileDownloadCancelled(docRef.get(), DownloadDelegate.CancelReason.USER_CANCELLED);
                        break;
                    }
                }
                success = false;
            } finally {
                try {
                    if (output != null) {
                        output.close();
                    }
                    if (input != null)
                        input.close();
                } catch (IOException ignored) {
                }
                if (connection != null)
                    connection.disconnect();
            }
            attemptsCount--;
        } while(!success && attemptsCount > 0);
        try
        {
            DocumentLoadManager.sharedInstance().didTaskFinished(this.id);
            synchronized(cancelled)
            {
                if(cancelled.get()) {
                    progressDialogDelegate.didFileDownloadCancelled(docRef.get(), DownloadDelegate.CancelReason.USER_CANCELLED);
                    return;
                }
            }

            if(success) {
                String path = Configuration.sharedInstance().shouldSave(docRef.get()) ? offlinePathToSave : pathToSave;
                progressDialogDelegate.didFileDownloadFinished(docRef.get(), new File(path));
                if(Configuration.sharedInstance().shouldSave(docRef.get()))
                    docRef.get().broadcastDownloadFinished(path);
            }
            else
                progressDialogDelegate.didFileDownloadCancelled(docRef.get(), DownloadDelegate.CancelReason.NETWORK_PROBLEM);
        }
        catch(Exception ignored) {}
    }
    @Override
    public void pauseIfNeed()
    {
        while(DocumentLoadManager.sharedInstance().paused.get())
        {
            synchronized(pauseObject)
            {
                try
                {
                    pauseObject.wait();
                }
                catch(Exception ignored) { break;}
            }
        }
    }
    @Override
    public void unpause() {
        synchronized(pauseObject)
        {
            synchronized (pauseObject)
            {
                pauseObject.notifyAll();
            }
        }
    }

    @Override
    public void cancel() {
        synchronized(cancelled)
        {
            cancelled.set(true);
        }
    }

    @Override
    public long getTaskId() { return this.id; }

    @Override
    public Document getBoundDoc() {
        return docRef != null ? docRef.get() : null;
    }
}
