package com.andremacareno.vkdocs.network;

import android.os.Process;

import com.andremacareno.vkdocs.AppLoader;
import com.andremacareno.vkdocs.Configuration;
import com.andremacareno.vkdocs.DownloadDelegate;
import com.andremacareno.vkdocs.dao.Document;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by Andrew on 09.01.2016.
 */
public class DocDownloadWorker implements Runnable, DocumentManagerTask {
    private String stringUrl;
    private String pathToSave;
    private final String cacheDir;
    private WeakReference<Document> docRef;
    private final Object pauseObject = new Object();
    private final long id;
    private final AtomicBoolean cancelled = new AtomicBoolean(false);
    public DocDownloadWorker(String url, Document documentRef, long taskId)
    {
        this.cacheDir = AppLoader.sharedInstance().docsCacheLocation();
        this.stringUrl = url;
        this.pathToSave = cacheDir.concat(documentRef.getId().toString()).concat(".".concat(documentRef.getExt()));
        docRef = new WeakReference<Document>(documentRef);
        this.id = taskId;
    }
    @Override
    public void run() {
        android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
        boolean publishProgress = docRef != null && docRef.get() != null;
        InputStream input = null;
        OutputStream output = null;
        HttpURLConnection connection = null;
        DocumentLoadManager.sharedInstance().didTaskStarted(this);
        boolean success = false;
        int attemptsCount = 5;
        do {
            try {
                pauseIfNeed();
                URL url = new URL(stringUrl);
                if(docRef != null && docRef.get() != null)
                    docRef.get().broadcastDownloadStarted();
                connection = (HttpURLConnection) url.openConnection();
                connection.setReadTimeout(10000);
                connection.setConnectTimeout(5000);
                connection.connect();

                //Status check
                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK)
                    throw new IllegalStateException("Expected 200 OK status, got" + connection.getResponseCode());

                //if > 0, use for progress publishing (if needed)
                int fileLength = connection.getContentLength();

                // download buffers
                input = connection.getInputStream();
                File cacheFolder = new File(cacheDir);
                if(!cacheFolder.exists())
                    cacheFolder.mkdir();
                output = new FileOutputStream(pathToSave);
                boolean onWifi = AppLoader.sharedInstance().isConnectedToWifi();
                byte data[] = new byte[onWifi ? Configuration.PACKAGE_SIZE_WIFI : Configuration.PACKAGE_SIZE_MOBILE_NETWORK];
                long ready = 0;
                float previousProgress = 0f;
                int count;
                while ((count = input.read(data)) != -1) {
                    synchronized(cancelled)
                    {
                        if(cancelled.get())
                            throw new UnsupportedOperationException();
                    }
                    ready += count;
                    float currentProgress = (float) ready / fileLength;
                    if (fileLength > 0 && publishProgress && (currentProgress - previousProgress) > 0.05f && docRef != null && docRef.get() != null) {
                        docRef.get().broadcastFileProgressUpdated(ready, fileLength);
                        previousProgress = currentProgress;
                    }
                    output.write(data, 0, count);
                }
                success = true;
            } catch (Exception ignored) {
                synchronized(cancelled)
                {
                    if(cancelled.get()) {
                        docRef.get().broadcastFileOperationCancelled(DownloadDelegate.CancelReason.USER_CANCELLED);
                        break;
                    }
                }
                success = false;
            } finally {
                try {
                    if (output != null) {
                        output.close();
                    }
                    if (input != null)
                        input.close();
                } catch (IOException ignored) {
                }
                if (connection != null)
                    connection.disconnect();
            }
            attemptsCount--;
        } while(!success && attemptsCount > 0);
        try
        {
            synchronized(cancelled)
            {
                if(cancelled.get()) {
                    docRef.get().broadcastFileOperationCancelled(DownloadDelegate.CancelReason.USER_CANCELLED);
                    return;
                }
            }

            if(success)
                docRef.get().broadcastDownloadFinished(pathToSave);
            else
                docRef.get().broadcastFileOperationCancelled(DownloadDelegate.CancelReason.NETWORK_PROBLEM);
            DocumentLoadManager.sharedInstance().didTaskFinished(this.id);
        }
        catch(Exception ignored) {}
    }
    @Override
    public void pauseIfNeed()
    {
        while(DocumentLoadManager.sharedInstance().paused.get())
        {
            synchronized(pauseObject)
            {
                try
                {
                    pauseObject.wait();
                }
                catch(Exception ignored) { break;}
            }
        }
    }
    @Override
    public void unpause() {
        synchronized(pauseObject)
        {
            synchronized (pauseObject)
            {
                pauseObject.notifyAll();
            }
        }
    }

    @Override
    public void cancel() {
        synchronized(cancelled)
        {
            cancelled.set(true);
        }
    }

    @Override
    public long getTaskId() { return this.id; }

    @Override
    public Document getBoundDoc() {
        return docRef != null ? docRef.get() : null;
    }
}
