package com.andremacareno.vkdocs.network;

import android.os.Process;
import android.util.Log;
import android.util.Pair;
import android.webkit.MimeTypeMap;

import com.andremacareno.vkdocs.AppLoader;
import com.andremacareno.vkdocs.BuildConfig;
import com.andremacareno.vkdocs.Configuration;
import com.andremacareno.vkdocs.DownloadDelegate;
import com.andremacareno.vkdocs.FilenameUtils;
import com.andremacareno.vkdocs.dao.Document;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by Andrew on 09.01.2016.
 */
public class DocUploadWorker implements Runnable, DocumentManagerTask {
    private static final String VK_BOUNDARY = "Boundary(======VK_SDK_%d======)";
    private static final String TAG = "DocUploadWorker";
    private File file;
    private String uploadUrl;
    private final String cacheDir;
    private WeakReference<Document> docRef;
    private final Object pauseObject = new Object();
    private final long id;
    private final String fileName, fileFullName, fileExtension;
    private final boolean publishProgress;
    private final AtomicBoolean cancelled = new AtomicBoolean(false);
    public DocUploadWorker(File fileToUl, String uploadUrl, Document documentRef, long taskId)
    {
        this.cacheDir = AppLoader.sharedInstance().docsCacheLocation();
        this.file = fileToUl;
        this.uploadUrl = uploadUrl;
        docRef = new WeakReference<Document>(documentRef);
        this.id = taskId;
        fileFullName = file.getName();
        fileName = FilenameUtils.removeExtension(file.getName());
        fileExtension = FilenameUtils.getExtension(file.getName());
        publishProgress = docRef != null && docRef.get() != null;
    }
    @Override
    public void run() {
        Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
        DocumentLoadManager.sharedInstance().didTaskStarted(this);
        boolean success;
        String response = "";
        FileInputStream reader = null;
        InputStream is = null;
        OutputStream os = null;
        ByteArrayOutputStream responseOS = null;
        int attemptsCount = 5;
        do {
            try {
                String boundary = String.format(Locale.US, VK_BOUNDARY, new Random().nextInt());
                pauseIfNeed();
                if(BuildConfig.DEBUG)
                    Log.d(TAG, String.format("upload_url = %s; fileName = %s; fullFileName = %s; fileExtension = %s, mimeType = %s", uploadUrl, fileName, fileFullName, fileExtension, MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension)));
                URL url = new URL(uploadUrl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setReadTimeout(5000);
                connection.setConnectTimeout(10000);
                connection.setRequestMethod("POST");
                connection.setUseCaches(false);
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setRequestProperty("User-Agent", "VK Docs App");
                connection.setRequestProperty("Connection", "Keep-Alive");
                Pair<String, String> contentType = getContentType(boundary);
                connection.setRequestProperty(contentType.first, contentType.second);
                long contentLength = getContentLength(boundary);
                connection.setRequestProperty("Content-Length", contentLength + "");
                boolean isWifi = AppLoader.sharedInstance().isConnectedToWifi();
                final byte[] fileBuffer = new byte[isWifi ? Configuration.PACKAGE_SIZE_WIFI : Configuration.PACKAGE_SIZE_MOBILE_NETWORK];
                connection.setChunkedStreamingMode(isWifi ? Configuration.PACKAGE_SIZE_WIFI : Configuration.PACKAGE_SIZE_MOBILE_NETWORK);
                os = connection.getOutputStream();
                if(BuildConfig.DEBUG)
                    Log.d(TAG, getBoundaryHead(boundary));
                os.write(getBoundaryHead(boundary).getBytes("UTF-8"));
                if(BuildConfig.DEBUG)
                    Log.d(TAG, getContentDisposition());
                os.write(getContentDisposition().getBytes("UTF-8"));
                if(BuildConfig.DEBUG)
                    Log.d(TAG, getFileContentType());
                os.write(getFileContentType().getBytes("UTF-8"));
                reader = new FileInputStream(file);
                int bytesRead = 0;
                int bytesTotal = 0;
                float previousProgress = 0f;
                final long fileSize = file.length();
                while ((bytesRead = reader.read(fileBuffer)) != -1) {
                    synchronized(cancelled)
                    {
                        if(cancelled.get())
                            throw new UnsupportedOperationException();
                    }
                    bytesTotal += bytesRead;
                    float currentProgress = (float) bytesTotal / fileSize;
                    if (publishProgress && (currentProgress - previousProgress) > 0.05f && docRef != null && docRef.get() != null) {
                        if(BuildConfig.DEBUG)
                            Log.d(TAG, String.format("broadcasting progress; currentProgress = %4.2f", currentProgress));
                        docRef.get().broadcastFileProgressUpdated(bytesTotal, fileSize);
                        previousProgress = currentProgress;
                    }
                    os.write(fileBuffer, 0, bytesRead);
                }
                synchronized(cancelled)
                {
                    if(cancelled.get()) {
                        success = false;
                        break;
                    }
                }
                os.write(getBoundaryEnd(boundary).getBytes("UTF-8"));
                os.flush();
                if(BuildConfig.DEBUG)
                    Log.d(TAG, "writeTo finished");
                connection.connect();
                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    throw new IOException(String.format("%s failed with HTTP status: %d",
                            uploadUrl, connection.getResponseCode()));
                }
                is = connection.getInputStream();
                responseOS = new ByteArrayOutputStream();
                byte[] buffer = new byte[1024];

                while ((bytesRead = is.read(buffer)) != -1) {
                    responseOS.write(buffer, 0, bytesRead);
                }
                response = new String(responseOS.toByteArray());
                JSONObject responseJSON = new JSONObject(response);
                success = !responseJSON.has("error");
                if(!success)
                {
                    is.close();
                    os.close();
                    responseOS.close();
                    reader.close();
                }
            }
            catch (Exception e) {
                success = false;
                synchronized(cancelled)
                {
                    if(cancelled.get()) {
                        docRef.get().broadcastFileOperationCancelled(DownloadDelegate.CancelReason.USER_CANCELLED);
                        break;
                    }
                }
            }
            attemptsCount--;
        } while(!success && attemptsCount > 0);
        try
        {
            synchronized(cancelled)
            {
                if(cancelled.get()) {
                    docRef.get().broadcastFileOperationCancelled(DownloadDelegate.CancelReason.USER_CANCELLED);
                    return;
                }
            }
            if(!response.isEmpty() && BuildConfig.DEBUG)
                Log.d(TAG, String.format("server response: %s", response));
            JSONObject responseJSON = new JSONObject(response);
            if(success)
                saveDocument(docRef.get(), responseJSON);
            else
                docRef.get().broadcastFileOperationCancelled(DownloadDelegate.CancelReason.NETWORK_PROBLEM);
            DocumentLoadManager.sharedInstance().didTaskFinished(this.id);
        }
        catch(Exception ignored) {}
        finally
        {
            try
            {
                if(is != null)
                    is.close();
                if(os != null)
                    os.close();
                if(responseOS != null)
                    responseOS.close();
                if(reader != null)
                    reader.close();
            }
            catch(Exception ignored) {}
        }
    }
    @Override
    public void pauseIfNeed()
    {
        while(DocumentLoadManager.sharedInstance().paused.get())
        {
            synchronized(pauseObject)
            {
                try
                {
                    pauseObject.wait();
                }
                catch(Exception ignored) { break;}
            }
        }
    }
    @Override
    public void unpause() {
        synchronized(pauseObject)
        {
            synchronized (pauseObject) {
                pauseObject.notifyAll();
            }
        }
    }

    @Override
    public long getTaskId() {
        return this.id;
    }

    private void saveDocument(final Document doc, JSONObject file) throws Exception {
        VKRequest req = new VKRequest("docs.save", VKParameters.from("file", file.getString("file"), "title", fileName));
        req.attempts = 10;
        req.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                try {
                    JSONObject docInfo = response.json.getJSONArray("response").getJSONObject(0);
                    doc.broadcastUploadFinished(docInfo);
                    String pathToSave = cacheDir.concat(doc.getId().toString()).concat(".".concat(doc.getExt()));
                    copyFile(DocUploadWorker.this.file, new File(pathToSave));
                    if(BuildConfig.DEBUG)
                        Log.d(TAG, String.format("changed path: %s", pathToSave));
                    doc.updatePath(pathToSave);
                }
                catch(Exception ignored) {}
            }
            @Override
            public void onError(VKError error) {
                try
                {
                    doc.broadcastFileOperationCancelled(DownloadDelegate.CancelReason.NETWORK_PROBLEM);
                }
                catch(Exception ignored) {}
            }
        });
    }
    private static boolean copyFile(File sourceFile, File destFile) throws IOException {
        FileChannel source = null;
        FileChannel destination = null;

        try {
            if(!destFile.exists()) {
                destFile.createNewFile();
            }
            source = new FileInputStream(sourceFile).getChannel();
            destination = new FileOutputStream(destFile).getChannel();
            destination.transferFrom(source, 0, source.size());
        }
        catch(Exception e) { return false; }
        finally {
            if(source != null) {
                source.close();
            }
            if(destination != null) {
                destination.close();
            }
        }
        return true;
    }


    private long getContentLength(String boundary) {
        long length = 0;
        length += file.length();
        length += getBoundaryHead(boundary).length();
        length += getContentDisposition().length();
        length += getFileContentType().length();
        length += getBoundaryEnd(boundary).length();
        return length;
    }

    private Pair<String, String> getContentType(String boundary) {
        return new Pair<>("Content-Type", String.format("multipart/form-data; boundary=%s", boundary));
    }
    private String getBoundaryHead(String boundary)
    {
        return String.format("--%s\r\n", boundary);
    }
    private String getContentDisposition()
    {
        return String.format("Content-Disposition: form-data; name=\"%s\"; filename=\"%s\"\r\n", "file", fileFullName);
    }
    private String getFileContentType()
    {
        return String.format("Content-Type: %s\r\n\r\n", MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension));
    }

    private String getBoundaryEnd(String boundary) {
        return String.format("\r\n--%s--\r\n", boundary);
    }
    @Override
    public void cancel() {
        synchronized(cancelled)
        {
            cancelled.set(true);
        }
    }

    @Override
    public Document getBoundDoc() {
        return docRef != null ? docRef.get() : null;
    }
}
