package com.andremacareno.vkdocs.network;

import android.util.Log;

import com.andremacareno.vkdocs.AppLoader;
import com.andremacareno.vkdocs.BuildConfig;
import com.andremacareno.vkdocs.dao.Document;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.docs.VKUploadDocRequest;

import org.json.JSONObject;

import java.io.File;

/**
 * Created by andremacareno on 11/03/16.
 */
public class UploadRequestHandler extends VKUploadDocRequest {
    private final VKRequestListener getUploadServerHandler;
    private Runnable onError, onStartUpload;
    private Document boundDoc;
    public UploadRequestHandler(File docFile, Document docObj, Runnable errorRunnable, Runnable startUploadRunnable) {
        super(docFile);
        this.boundDoc = docObj;
        this.onError = errorRunnable;
        this.onStartUpload = startUploadRunnable;
        getUploadServerHandler = new VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                try
                {
                    JSONObject json = response.json;
                    if(BuildConfig.DEBUG)
                        Log.d("UploadRequest", json.toString());
                    String uploadURL = json.getJSONObject("response").getString("upload_url");
                    if(onStartUpload != null)
                        AppLoader.applicationHandler.post(onStartUpload);
                    DocumentLoadManager.sharedInstance().requestUpload(mDoc, boundDoc, uploadURL);
                }
                catch(Exception e) {
                    e.printStackTrace();
                    onError(null);
                }
            }

            @Override
            public void onError(VKError error) {
                try
                {
                    if(onError != null)
                        AppLoader.applicationHandler.post(onError);
                }
                catch(Exception ignored) {}
            }
        };
    }

    public void performUpload()
    {
        VKRequest getServerReq = getServerRequest();
        getServerReq.executeWithListener(getUploadServerHandler);
    }
}
