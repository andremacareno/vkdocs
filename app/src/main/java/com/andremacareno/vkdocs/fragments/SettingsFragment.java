package com.andremacareno.vkdocs.fragments;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.andremacareno.asynccore.BackgroundTask;
import com.andremacareno.asynccore.OnTaskCompleteListener;
import com.andremacareno.vkdocs.AndroidUtilities;
import com.andremacareno.vkdocs.AppLoader;
import com.andremacareno.vkdocs.Configuration;
import com.andremacareno.vkdocs.Constants;
import com.andremacareno.vkdocs.R;
import com.andremacareno.vkdocs.VKDocsMainActivity;
import com.andremacareno.vkdocs.cache.BookCache;
import com.andremacareno.vkdocs.cache.FileCache;
import com.andremacareno.vkdocs.cache.FolderInfoCache;
import com.andremacareno.vkdocs.cache.ImageCache;
import com.andremacareno.vkdocs.dao.DaoSession;
import com.andremacareno.vkdocs.objs.CurrentUser;
import com.andremacareno.vkdocs.tasks.ComputeOfflineSizeTask;
import com.andremacareno.vkdocs.views.ActionView;
import com.andremacareno.vkdocs.views.actionviews.SettingsActionView;
import com.andremacareno.vkdocs.views.settings.SettingsSingleLineItem;
import com.andremacareno.vkdocs.views.settings.SettingsToggleItem;
import com.andremacareno.vkdocs.views.settings.SettingsTwoLineItem;
import com.vk.sdk.VKSdk;

import java.io.File;

/**
 * Created by andremacareno on 17/03/16.
 */
public class SettingsFragment extends VKDocsBaseScreen {
    private ScrollView container = null;

    private SettingsTwoLineItem offlineInfo;
    private SettingsTwoLineItem autoDownload;
    private SettingsToggleItem reduceAnims;
    private SettingsToggleItem deleteConfirmation;
    private SettingsSingleLineItem logout;
    private int selection = 0;
    private final Runnable changeBackgroundRunnable = new Runnable() {
        @Override
        public void run() {
            try
            {
                container.setBackgroundColor(0x00000000);
            }
            catch(Exception ignored) {}
        }
    };

    @Override
    public boolean hasFAB() {
        return false;
    }

    @Override
    public void init() {
        try
        {
            String totalOffline = FolderInfoCache.sharedInstance().totalOfflineSize;
            if(totalOffline != null)
                offlineInfo.setSubtitle(totalOffline);
            else
            {
                offlineInfo.setSubtitle(R.string.computing);
                ComputeOfflineSizeTask t = new ComputeOfflineSizeTask();
                t.onComplete(new OnTaskCompleteListener() {
                    @Override
                    public void taskComplete(BackgroundTask task) {
                        final String size = ((ComputeOfflineSizeTask) task).getResult();
                        try
                        {
                            AppLoader.applicationHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    try
                                    {
                                        offlineInfo.setSubtitle(size);
                                    }
                                    catch(Exception ignored) {}
                                }
                            });
                        }
                        catch(Exception ignored) {}
                    }
                });
                t.addToQueue();
            }
        }
        catch(Exception ignored) {}
    }

    @Override
    public ActionView getActionView() {
        SettingsActionView av = new SettingsActionView(getContext());
        av.backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });
        return av;
    }

    @Override
    public View getContentView() {

        container = new ScrollView(getContext());
        container.setBackgroundColor(0xFFFFFFFF);
        container.setFillViewport(true);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, AndroidUtilities.dp(56));
        LinearLayout.LayoutParams twoLineLP = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, AndroidUtilities.dp(74));
        LinearLayout ll = new LinearLayout(getContext());
        ll.setLayoutParams(new ScrollView.LayoutParams(ScrollView.LayoutParams.MATCH_PARENT, ScrollView.LayoutParams.WRAP_CONTENT));
        ll.setOrientation(LinearLayout.VERTICAL);
        offlineInfo = new SettingsTwoLineItem(getContext());
        autoDownload = new SettingsTwoLineItem(getContext());
        reduceAnims = new SettingsToggleItem(getContext());
        deleteConfirmation = new SettingsToggleItem(getContext());
        logout = new SettingsSingleLineItem(getContext());
        offlineInfo.setLayoutParams(twoLineLP);
        offlineInfo.setSubtitleColor(0xFF757575);
        autoDownload.setLayoutParams(twoLineLP);
        autoDownload.setSubtitleColor(0xFF5181B8);
        reduceAnims.setLayoutParams(lp);
        deleteConfirmation.setLayoutParams(lp);
        logout.setLayoutParams(lp);
        logout.setTextColor(0xFFE15868);
        logout.setText(R.string.log_out);
        offlineInfo.setTitle(R.string.saved_offline);
        autoDownload.setTitle(R.string.save_on_preview);
        updateAutosaveSubtitle();
        updateReduceAnimations(true);
        updateDeleteWithoutConfirmation(true);
        reduceAnims.setTitle(R.string.reduce_animations);
        deleteConfirmation.setTitle(R.string.del_without_confirm);
        ll.addView(offlineInfo);
        ll.addView(autoDownload);
        ll.addView(reduceAnims);
        ll.addView(deleteConfirmation);
        ll.addView(logout);
        container.addView(ll);

        autoDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAutosaveAlert();
            }
        });
        reduceAnims.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateReduceAnimations(false);
            }
        });
        deleteConfirmation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateDeleteWithoutConfirmation(false);
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showLogoutConfirm();
            }
        });
        return container;
    }

    private void showAutosaveAlert()
    {

        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
        adb.setCancelable(true);
        adb.setSingleChoiceItems(new CharSequence[]{getResources().getString(R.string.autosave_all), getResources().getString(R.string.autosave_epub), getResources().getString(R.string.autosave_disable)}, Configuration.sharedInstance().autosave, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                selection = i;
            }
        });
        adb.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                try
                {
                    AppLoader.sharedInstance().sharedPreferences().edit().putInt(Constants.PREFS_AUTOSAVE, selection).apply();
                    Configuration.sharedInstance().autosave = selection;
                    updateAutosaveSubtitle();
                }
                catch(Exception ignored) {}
                //dialogInterface.dismiss();
            }
        });
        adb.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //dialogInterface.dismiss();
            }
        });
        adb.create().show();
    }

    private void updateAutosaveSubtitle()
    {
        int autosave = Configuration.sharedInstance().autosave;
        if(autosave == Configuration.DISABLE_AUTOSAVE)
            autoDownload.setSubtitle(R.string.autosave_disable);
        else if(autosave == Configuration.AUTOSAVE_EPUB)
            autoDownload.setSubtitle(R.string.autosave_epub);
        else if(autosave == Configuration.AUTOSAVE_ALL)
            autoDownload.setSubtitle(R.string.autosave_all);
    }
    private void updateReduceAnimations(boolean init)
    {
        boolean reduceAnimations = Configuration.sharedInstance().reducedAnimations;
        reduceAnims.setSwitchCondition(init ? reduceAnimations : !reduceAnimations);
        if(init)
            return;
        Configuration.sharedInstance().reducedAnimations = !reduceAnimations;
        AppLoader.sharedInstance().sharedPreferences().edit().putBoolean(Constants.PREFS_REDUCE_ANIMATIONS, !reduceAnimations).apply();
    }
    private void updateDeleteWithoutConfirmation(boolean init)
    {
        boolean delWithoutConfirmation = Configuration.sharedInstance().deleteWithoutConfirmation;
        deleteConfirmation.setSwitchCondition(init ? delWithoutConfirmation : !delWithoutConfirmation);
        if(init)
            return;
        Configuration.sharedInstance().deleteWithoutConfirmation = !delWithoutConfirmation;
        AppLoader.sharedInstance().sharedPreferences().edit().putBoolean(Constants.PREFS_DELETE_WITHOUT_CONFIRMATION, !delWithoutConfirmation).apply();
    }
    public void showLogoutConfirm()
    {
        new AlertDialog.Builder(getActivity())
                .setTitle(R.string.log_out)
                .setMessage(R.string.logout_confirm)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        performLogOut();
                    }
                })
                .setNegativeButton(R.string.no, null)
                .setCancelable(true)
                .show();
    }
    @Override
    public void onDestroy() {

    }

    @Override
    public int getScreenId() {
        return VKDocsBaseScreen.SETTINGS;
    }
    @Override
    public void onOpenAnimationFinished()
    {
        if(container != null)
            container.postDelayed(changeBackgroundRunnable, 100);
    }
    @Override
    public void onCloseAnimationStart()
    {
        if(container != null) container.setBackgroundColor(0xFFFFFFFF);
    }

    private void performLogOut()
    {
        try
        {
            if(((VKDocsMainActivity) getActivity()).checkOfflineWork())
            {
                Toast.makeText(getContext(), R.string.cant_log_out, Toast.LENGTH_LONG).show();
                return;
            }
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try
                    {
                        DaoSession session = AppLoader.sharedInstance().daoMaster.newSession();
                        session.getDocumentDao().deleteAll();
                        session.getFolderDao().deleteAll();
                    }
                    catch(Exception e) { e.printStackTrace(); }
                    try
                    {
                        File docs = new File(AppLoader.sharedInstance().docsCacheLocation());
                        File tmp = new File(AppLoader.sharedInstance().docsCacheLocation());
                        if(docs.exists() && docs.isDirectory())
                            for(File f : docs.listFiles())
                                f.delete();
                        if(tmp.exists() && tmp.isDirectory())
                            for(File f : tmp.listFiles())
                                f.delete();
                    }
                    catch(Exception e) { e.printStackTrace(); }
                    try
                    {
                        FolderInfoCache.sharedInstance().clear();
                    }
                    catch(Exception e) { e.printStackTrace(); }
                    try
                    {
                        ImageCache.getInstance().clearCache();
                    }
                    catch(Exception e) { e.printStackTrace(); }
                    try
                    {
                        FileCache.getInstance().clear();
                    }
                    catch(Exception e) { e.printStackTrace(); }
                    try
                    {
                        CurrentUser.didLoggedOut();
                    }
                    catch(Exception e) { e.printStackTrace(); }
                    try
                    {
                        BookCache.sharedInstance().clear();
                    }
                    catch(Exception e) { e.printStackTrace(); }
                }
            }).start();
            VKSdk.logout();
            ((VKDocsMainActivity) getActivity()).performLogOut();
        }
        catch(Exception ignored) {}
    }
}
