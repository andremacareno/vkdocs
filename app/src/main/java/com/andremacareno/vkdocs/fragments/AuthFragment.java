package com.andremacareno.vkdocs.fragments;

import android.view.View;

import com.andremacareno.vkdocs.views.ActionView;
import com.vk.sdk.VKSdk;

/**
 * Created by Andrew on 07.01.2016.
 */
public class AuthFragment extends VKDocsBaseScreen {


    @Override
    public void init() {
        VKSdk.login(getActivity(), "docs", "offline", "messages");
    }

    @Override
    public ActionView getActionView() {
        return null;
    }

    @Override
    public View getContentView() {
        return null;
    }

    @Override
    public void onDestroy() {
    }

    @Override
    public int getScreenId() {
        return VKDocsBaseScreen.AUTH;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public boolean hasFAB() {
        return false;
    }
}
