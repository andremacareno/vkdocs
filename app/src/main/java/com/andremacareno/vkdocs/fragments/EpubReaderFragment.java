package com.andremacareno.vkdocs.fragments;


import android.view.View;
import android.widget.TextView;

import com.andremacareno.vkdocs.Constants;
import com.andremacareno.vkdocs.reader.BookView;
import com.andremacareno.vkdocs.views.ActionView;
import com.andremacareno.vkdocs.views.actionviews.ReaderActionView;

import nl.siegmann.epublib.domain.Book;

/**
 * Created by Andrew on 01.02.2016.
 */
public class EpubReaderFragment extends VKDocsBaseScreen {
    private ReaderActionView av;
    private Book book;

    private TextView title;
    private TextView pageIndicator;

    private BookView bookView;

    @Override
    public boolean hasFAB() {
        return false;
    }

    @Override
    public void onResume()
    {
        super.onResume();
    }
    @Override
    public void onPause()
    {
        super.onPause();
    }

    @Override
    public void init() {
        if(getArguments() == null || !getArguments().containsKey(Constants.ARG_BOOK_PATH)) {
            return;
        }
        try
        {
            String path = getArguments().getString(Constants.ARG_BOOK_PATH);
            /*File f = new File(path);
            InputStream epubInputStream = new FileInputStream(f);*/
            bookView.setFileName(path);
            bookView.init();
            /*book = (new EpubReader()).readEpub(epubInputStream);
            String strTitle = book.getTitle().isEmpty() ? getResources().getString(R.string.untitled) : book.getTitle();
            title.setText(strTitle);
            pageIndicator.setText(String.valueOf(book.getSpine().size()));*/
        }
        catch(Exception e) {
            e.printStackTrace();
            return;
        }
    }
    @Override
    public ActionView getActionView() {
        if(av == null)
        {
            av = new ReaderActionView(getContext());
            av.backButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    goBack();
                }
            });
        }
        return av;
    }

    @Override
    public View getContentView() {
        /*LinearLayout ll = new LinearLayout(getContext());
        ll.setOrientation(LinearLayout.VERTICAL);
        ll.setBackgroundColor(0xFFF5F5F5);
        LinearLayout.LayoutParams titleLP = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        titleLP.gravity = Gravity.CENTER;
        title = new TextView(getContext());
        title.setTextColor(0xFF5181B8);
        title.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
        title.setLayoutParams(titleLP);

        pageIndicator = new TextView(getContext());
        pageIndicator.setTextColor(0xFFABAF83);
        pageIndicator.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);
        pageIndicator.setLayoutParams(titleLP);

        ll.addView(title);
        ll.addView(pageIndicator);*/
        bookView = new BookView(getContext());
        return bookView;
        //return ll;
    }

    @Override
    public void onDestroy()
    {

    }
    @Override
    public int getScreenId() {
        return VKDocsBaseScreen.SHARE_WITH_FRIEND;
    }
}
