package com.andremacareno.vkdocs.fragments;

import com.andremacareno.screencore.BaseScreen;
import com.andremacareno.vkdocs.VKDocsMainActivity;

/**
 * Created by andremacareno on 14/03/16.
 */
public abstract class VKDocsBaseScreen extends BaseScreen {
    static int totalScreens = 0;
    static final int AUTH = totalScreens++;
    static final int MAIN = totalScreens++;
    static final int SHARE_WITH_FRIEND = totalScreens++;
    static final int SETTINGS = totalScreens++;
    static final int BOOKS = totalScreens++;
    static final int READER = totalScreens++;

    @Override
    public void onCreate()
    {
        try
        {
            if(hasFAB())
                ((VKDocsMainActivity) getActivity()).showFAB();
            else
                ((VKDocsMainActivity) getActivity()).hideFAB();
        }
        catch(Exception ignored) {}
    }
    @Override
    protected void goBack()
    {
        try
        {
            ((VKDocsMainActivity) getActivity()).performGoBack();
        }
        catch(Exception ignored) {}
    }
    public abstract boolean hasFAB();
}
