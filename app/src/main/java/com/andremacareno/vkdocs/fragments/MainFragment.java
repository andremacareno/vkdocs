package com.andremacareno.vkdocs.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andremacareno.asynccore.BackgroundTask;
import com.andremacareno.asynccore.OnTaskCompleteListener;
import com.andremacareno.asynccore.OnTaskFailureListener;
import com.andremacareno.asynccore.notificationcenter.Notification;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.asynccore.notificationcenter.NotificationObserver;
import com.andremacareno.screencore.BaseScreen;
import com.andremacareno.screencore.ScreenManager;
import com.andremacareno.vkdocs.AndroidUtilities;
import com.andremacareno.vkdocs.AppLoader;
import com.andremacareno.vkdocs.BackPressListener;
import com.andremacareno.vkdocs.BuildConfig;
import com.andremacareno.vkdocs.Configuration;
import com.andremacareno.vkdocs.Constants;
import com.andremacareno.vkdocs.DownloadDelegate;
import com.andremacareno.vkdocs.DownloadState;
import com.andremacareno.vkdocs.R;
import com.andremacareno.vkdocs.TempDownloadDelegate;
import com.andremacareno.vkdocs.VKDocsMainActivity;
import com.andremacareno.vkdocs.controllers.ActionViewController;
import com.andremacareno.vkdocs.controllers.DocContextMenuListController;
import com.andremacareno.vkdocs.controllers.DocsListViewController;
import com.andremacareno.vkdocs.controllers.GalleryRecentPhotosController;
import com.andremacareno.vkdocs.controllers.NewDocListController;
import com.andremacareno.vkdocs.dao.DaoSession;
import com.andremacareno.vkdocs.dao.Document;
import com.andremacareno.vkdocs.dao.DocumentDao;
import com.andremacareno.vkdocs.dao.Folder;
import com.andremacareno.vkdocs.listadapters.GalleryImagesListAdapter;
import com.andremacareno.vkdocs.network.DocumentLoadManager;
import com.andremacareno.vkdocs.network.UploadRequestHandler;
import com.andremacareno.vkdocs.objs.BottomSheetAction;
import com.andremacareno.vkdocs.objs.CurrentUser;
import com.andremacareno.vkdocs.objs.DocListItemWrap;
import com.andremacareno.vkdocs.tasks.DatabaseWritingThread;
import com.andremacareno.vkdocs.tasks.GalleryImagePathTask;
import com.andremacareno.vkdocs.tasks.NewFolderTask;
import com.andremacareno.vkdocs.views.ActionView;
import com.andremacareno.vkdocs.views.DocActionsView;
import com.andremacareno.vkdocs.views.DocsListView;
import com.andremacareno.vkdocs.views.DrawerArrowDrawable;
import com.andremacareno.vkdocs.views.NavDrawerView;
import com.andremacareno.vkdocs.views.NewDocView;
import com.andremacareno.vkdocs.views.NewFolderConfirmView;
import com.andremacareno.vkdocs.views.SelectionInfoView;
import com.andremacareno.vkdocs.views.actionviews.FileScreenActionView;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import java.io.File;
import java.util.List;

/**
 * Created by Andrew on 07.01.2016.
 */
public class MainFragment extends VKDocsBaseScreen {
    public interface MainFragmentDelegate
    {
        public void didOpenContextMenuRequested(Document doc);
        public void didOpenContextMenuRequested(Folder folder);
        public void didOpenDocRequested(Document doc);
        void onFolderSelected(Folder folder);
        void onViewModeChanged(DocsListViewController.Mode mode);
        void didSelectionCountChanged(int newCount);
        void onMoveFinished();
        void onSyncFinished(boolean success);
        void showEmptyView(boolean show);
    }
    private SwipeRefreshLayout container;
    private SelectionInfoView selectionMenu;
    private DocsListView listView;
    private DocsListViewController listController;
    private View.OnClickListener fabClickListener;
    private GalleryImagePathTask galleryImagePathTask;
    private TextView emptyView;
    //new doc menu
    private boolean restoreNewDocMenu = false;
    private boolean restoreDocContextMenu = false;
    private NewDocView newDocMenu;
    private NewDocView.MenuDelegate newDocMenuDelegate;
    private GalleryImagesListAdapter.QuickSelectDelegate quickSelectDelegate;
    private GalleryRecentPhotosController recentPhotosController;
    private NewDocListController attachListController;
    //doc selection menu
    private DocActionsView docActionsBottomSheet;
    private DocContextMenuListController docActionsController;
    private DocActionsView.MenuDelegate contextMenuActionsDelegate;

    private MainFragmentDelegate fragmentDelegate;
    //action view
    private FileScreenActionView actionView;
    private FileScreenActionViewController actionViewController;
    private NotificationObserver drawerSelectionObserver;
    private Animation selectionMenuShow, selectionMenuHide;
    private Animation.AnimationListener selectionShowListener, selectionHideListener;
    private SelectionInfoView.SelectionInfoDelegate selectionDelegate;
    private final Runnable showKeyboardRunnable = new Runnable() {
        @Override
        public void run() {
            try
            {
                if(actionView != null)
                    AndroidUtilities.showKeyboard(actionView.searchEditText);
            }
            catch(Exception ignored) {}
        }
    };
    private final Runnable hideKeyboardRunnable = new Runnable() {
        @Override
        public void run() {
            try
            {
                if(actionView != null)
                    AndroidUtilities.hideKeyboard(actionView.searchEditText);
            }
            catch(Exception ignored) {}
        }
    };
    private final Runnable syncFailedRunnable = new Runnable() {
        @Override
        public void run() {
            try
            {
                Toast.makeText(getContext(), R.string.sync_unaccessible_network_unreachable, Toast.LENGTH_LONG).show();
                container.setRefreshing(false);
            }
            catch(Exception ignored) {}
        }
    };
    private final Runnable syncFinishedRunnable = new Runnable() {
        @Override
        public void run() {
            try
            {
                container.setRefreshing(false);
            }
            catch(Exception ignored) {}
        }
    };
    private final Runnable showEmptyViewRunnable = new Runnable() {
        @Override
        public void run() {
            try
            {
                emptyView.setVisibility(View.VISIBLE);
            }
            catch(Exception ignored) {}
        }
    };
    private final Runnable hideEmptyViewRunnable = new Runnable() {
        @Override
        public void run() {
            try
            {
                emptyView.setVisibility(View.GONE);
            }
            catch(Exception ignored) {}
        }
    };
    private final BackPressListener folderBackPress = new BackPressListener() {
        @Override
        public void onBackPressed() {
            try
            {
                if(listController != null) {
                    listController.usePreviousMode();
                    if(selectionMode)
                    {
                        listController.disableSelectionMode();
                        selectionMode = false;
                    }
                }
                ((VKDocsMainActivity) getActivity()).removeBackPressInterceptor(this);
            }
            catch(Exception ignored) {}
        }
    };
    private final SwipeRefreshLayout.OnRefreshListener pullToRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            try
            {
                if(((VKDocsMainActivity) getActivity()).checkOfflineWork())
                {
                    Toast.makeText(getContext(), R.string.sync_unaccessible_network_unreachable, Toast.LENGTH_LONG).show();
                    container.setRefreshing(false);
                }
                else
                {
                    container.setRefreshing(true);
                    listController.startSync();
                }
            }
            catch(Exception ignored) {}
        }
    };

    //search mode
    private DrawerArrowDrawable arrowDrawable;
    private boolean searchMode = false;
    private float parameter = 0.0f;
    private static Handler flipHandler = new Handler(Looper.getMainLooper());
    private static Handler searchReqHandler = new Handler(Looper.getMainLooper());
    private BackPressListener searchModeBackPressInterceptor;
    private Runnable flipRunnable = new Runnable() {
        @Override
        public void run() {
            if(parameter < 0.0f) {
                parameter = 0.0f;
                return;
            }
            else if(parameter > 1.0f)
            {
                parameter = 1.0f;
                return;
            }
            arrowDrawable.setFlip(!searchMode);
            parameter += searchMode ? 0.08 : -0.08;
            if(arrowDrawable != null)
                arrowDrawable.setParameter(parameter);
            if(actionView != null)
                actionView.drawerButton.setImageDrawable(arrowDrawable);
            flipHandler.postDelayed(flipRunnable, 10);
        }
    };
    //selection mode
    private boolean selectionMode = false;
    private BackPressListener selectionModeBackPress = new BackPressListener() {
        @Override
        public void onBackPressed() {
            try
            {
                if(selectionMenu.getAnimation() != null && !selectionMenu.getAnimation().hasEnded())
                    return;
                selectionMenu.startAnimation(selectionMenuHide);
            }
            catch(Exception ignored) {}
        }
    };

    @Override
    public void onCreate()
    {
        super.onCreate();
        selectionMenuShow = AnimationUtils.loadAnimation(getContext(), R.anim.scr_slide_up);
        selectionShowListener = new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                selectionMenu.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        };
        selectionDelegate = new SelectionInfoView.SelectionInfoDelegate() {
            private void performDelete()
            {
                try
                {
                    final ProgressDialog pd = new ProgressDialog(getActivity());
                    pd.setIndeterminate(true);
                    pd.setCancelable(false);
                    pd.setMessage(getResources().getString(R.string.removing));
                    pd.show();
                    if(listController != null) {
                        listController.deleteSelected(new Runnable() {
                            @Override
                            public void run() {
                                try
                                {
                                    pd.dismiss();
                                    listController.disableSelectionMode();
                                    selectionMode = false;
                                    selectionModeBackPress.onBackPressed();
                                }
                                catch(Exception ignored) {}
                            }
                        });
                    }
                }
                catch(Exception ignored) {}
            }
            @Override
            public void onDeleteRequested() {
                if(Configuration.sharedInstance().deleteWithoutConfirmation)
                    performDelete();
                else
                {
                    new AlertDialog.Builder(getActivity())
                            .setTitle(R.string.confirm_delete)
                            .setMessage(R.string.cant_be_undone)
                            .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    performDelete();
                                }
                            })
                            .setNegativeButton(R.string.no, null)
                            .setCancelable(false)
                            .show();
                }
            }

            @Override
            public void onMoveToFolderRequested() {
                if(listController != null)
                    listController.moveSelection();
                selectionModeBackPress.onBackPressed();
            }
        };
        selectionMenuShow.setAnimationListener(selectionShowListener);
        selectionMenuHide = AnimationUtils.loadAnimation(getContext(), R.anim.selection_slide_down);
        selectionHideListener = new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                try
                {
                    selectionMenu.setVisibility(View.GONE);
                    listController.disableSelectionMode();
                    selectionMode = false;
                    if(listController.getCurrentMode() == DocsListViewController.Mode.ALL_DOCS || listController.getCurrentMode() == DocsListViewController.Mode.OFFLINE) {
                        ((VKDocsMainActivity) getActivity()).enableNavDrawer();
                        ((VKDocsMainActivity) getActivity()).showFAB();
                        ((VKDocsMainActivity) getActivity()).removeBackPressInterceptor(selectionModeBackPress);
                    }
                    else if(listController.getCurrentMode() == DocsListViewController.Mode.FOLDERS_ONLY) {
                        selectionMode = true;
                        ((VKDocsMainActivity) getActivity()).interceptBackPress(folderBackPress);
                    }
                }
                catch(Exception ignored) {}
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        };
        selectionMenuHide.setAnimationListener(selectionHideListener);
        arrowDrawable = new DrawerArrowDrawable(getResources());
        searchModeBackPressInterceptor = new BackPressListener() {
            @Override
            public void onBackPressed() {
                disableSearchMode();
            }
        };
        drawerSelectionObserver = new NotificationObserver(NotificationCenter.didNavDrawerClosed) {
            @Override
            public void didNotificationReceived(Notification notification) {
                try
                {
                    int action = (int) notification.getObject();
                    if(listController != null)
                    {
                        if(((VKDocsMainActivity) getActivity()).checkOfflineWork() && action == NavDrawerView.GOTO_FILES && listController.getCurrentMode() != DocsListViewController.Mode.ALL_DOCS)
                        {
                            Toast.makeText(getContext(), R.string.files_unaccessible_network_unreachable, Toast.LENGTH_LONG).show();
                            return;
                        }
                        if(action == NavDrawerView.GOTO_OFFLINE)
                            listController.showOfflineDocs();
                        else if(action == NavDrawerView.GOTO_FILES)
                            listController.showAllDocs();
                        else if(action == NavDrawerView.GOTO_SETTINGS)
                            openSettings();
                        else if(action == NavDrawerView.GOTO_BOOKS)
                            openBooksDashboard();
                    }
                }
                catch(Exception ignored) {}
            }
        };
        NotificationCenter.getInstance().addObserver(drawerSelectionObserver);
        fragmentDelegate = new MainFragmentDelegate() {
            @Override
            public void didOpenContextMenuRequested(Document doc) {
                showContextMenu(doc);
            }

            @Override
            public void didOpenContextMenuRequested(Folder folder) {
                showContextMenu(folder);
            }

            @Override
            public void didOpenDocRequested(final Document doc) {
                if(doc.getDownloadState() == DownloadState.DOWNLOADING) {
                    Toast.makeText(getContext(), R.string.wait_for_saving, Toast.LENGTH_SHORT).show();
                    return;
                }
                else if(doc.getDownloadState() == DownloadState.DOWNLOADED)
                {
                    if(doc.getExt().toLowerCase().equals("epub"))
                        openEpub(doc.getPath());
                    else
                        openDocument(new File(doc.getPath()), MimeTypeMap.getSingleton().getMimeTypeFromExtension(doc.getExt()));
                }
                else
                {
                    final ProgressDialog pd = new ProgressDialog(getActivity());
                    pd.setMessage(getResources().getString(R.string.loading));
                    pd.setIndeterminate(true);
                    //pd.setMax(10000);
                    pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    //pd.setProgress(0);
                    pd.setCancelable(false);
                    pd.setButton(DialogInterface.BUTTON_POSITIVE, getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            try {
                                pd.dismiss();
                                DocumentLoadManager.sharedInstance().cancelLoad(doc);
                            } catch (Exception ignored) {
                            }
                        }
                    });
                    pd.show();
                    TempDownloadDelegate delegate = new TempDownloadDelegate() {
                        @Override
                        public void didFileProgressUpdated(Document d, final float currentProgress) {
                            /*if(doc != null && d != null && doc.getId().equals(d.getId())) {
                                try
                                {
                                    AppLoader.applicationHandler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                if(BuildConfig.DEBUG)
                                                    Log.d("MainFragment", String.format("progress = %d", Math.round(currentProgress * 10000)));
                                                pd.setProgress(Math.round(currentProgress * 10000));
                                            } catch (Exception ignored) {
                                            }
                                        }
                                    });
                                }
                                catch(Exception ignored) {}
                            }*/
                        }

                        @Override
                        public void didFileDownloadFinished(final Document d, final File tmpPath) {
                            try
                            {
                                AppLoader.applicationHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            pd.dismiss();
                                            if(d.getExt().toLowerCase().equals("epub"))
                                                openEpub(tmpPath.getPath());
                                            else
                                                openDocument(tmpPath, MimeTypeMap.getSingleton().getMimeTypeFromExtension(d.getExt()));
                                        } catch (Exception ignored) {
                                        }
                                    }
                                });
                            }
                            catch(Exception ignored) {}
                        }

                        @Override
                        public void didFileDownloadCancelled(Document doc, DownloadDelegate.CancelReason why) {
                            if(why == DownloadDelegate.CancelReason.NETWORK_PROBLEM)
                            {
                                AppLoader.applicationHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            pd.dismiss();
                                            Toast.makeText(getContext(), R.string.cant_open_document, Toast.LENGTH_SHORT).show();
                                        } catch (Exception ignored) {
                                        }
                                    }
                                });
                            }
                        }
                    };
                    DocumentLoadManager.sharedInstance().requestTemporaryDownload(doc, delegate);
                }
            }

            @Override
            public void onFolderSelected(Folder folder) {
                try
                {
                    ((VKDocsMainActivity) getActivity()).interceptBackPress(folderBackPress);
                    actionView.setTitle(folder.getFolderName());
                }
                catch(Exception ignored) {}
            }

            @Override
            public void onViewModeChanged(DocsListViewController.Mode mode) {
                try
                {
                    if(mode == DocsListViewController.Mode.ALL_DOCS) {
                        emptyView.setText(R.string.files_empty);
                        ((VKDocsMainActivity) getActivity()).enableNavDrawer();
                        ((VKDocsMainActivity) getActivity()).showFAB();
                        actionView.setTitle(R.string.files);
                        container.setEnabled(true);
                    }
                    else {
                        ((VKDocsMainActivity) getActivity()).hideFAB();
                        if(mode == DocsListViewController.Mode.OFFLINE) {
                            emptyView.setText(R.string.offline_empty);
                            ((VKDocsMainActivity) getActivity()).enableNavDrawer();
                            actionView.setTitle(R.string.offline);
                        }
                        else
                            ((VKDocsMainActivity) getActivity()).disableNavDrawer();
                        container.setEnabled(false);
                    }

                    if(mode == DocsListViewController.Mode.FOLDER_CONTENTS)
                        ((VKDocsMainActivity) getActivity()).interceptBackPress(folderBackPress);
                }
                catch(Exception ignored) {}
            }

            @Override
            public void didSelectionCountChanged(int newCount) {
                try
                {
                    if(newCount == 0)
                    {
                        selectionModeBackPress.onBackPressed();
                    }
                    else
                        selectionMenu.updateSelectionCount(newCount);
                }
                catch(Exception ignored) {}
            }

            @Override
            public void onMoveFinished() {
                try
                {
                    ((VKDocsMainActivity) getActivity()).removeBackPressInterceptor(folderBackPress);
                    listController.disableSelectionMode();
                    selectionMode = false;
                }
                catch(Exception ignored) {}
            }

            @Override
            public void onSyncFinished(boolean success) {
                try
                {
                    AppLoader.applicationHandler.post(success ? syncFinishedRunnable : syncFailedRunnable);
                }
                catch(Exception ignored) {}
            }

            @Override
            public void showEmptyView(boolean show) {
                try
                {
                    AppLoader.applicationHandler.post(show ? showEmptyViewRunnable : hideEmptyViewRunnable);
                }
                catch(Exception ignored) {}
            }
        };
        newDocMenuDelegate = new NewDocView.MenuDelegate() {
            private void newFolder(String folderName)
            {
                if(folderName != null && !folderName.isEmpty())
                {
                    Folder folderObj = new Folder();
                    folderObj.setFolderName(folderName);
                    DatabaseWritingThread.getInstance().addTask(folderObj);
                    NewFolderTask t = new NewFolderTask(folderObj);
                    t.onComplete(new OnTaskCompleteListener() {
                        @Override
                        public void taskComplete(BackgroundTask task) {
                            Folder updatedFolder = ((NewFolderTask) task).getResult();
                            if(BuildConfig.DEBUG)
                                Log.d("MainFragment", String.format("new folder id: %d", updatedFolder.getId()));
                            if(listController != null)
                                listController.addFolder(updatedFolder);
                        }
                    });
                    t.addToQueue();
                }
            }
            @Override
            public void createNewFolder() {
                AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
                final NewFolderConfirmView confirmView = new NewFolderConfirmView(getContext());
                adb.setView(confirmView);
                adb.setTitle(R.string.folder_name);
                adb.setPositiveButton(R.string.ok, null);
                final AlertDialog dialog = adb.create();
                dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialogInterface) {
                        Button b = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
                        b.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View view) {
                                String folderName = confirmView.getEnteredTitle().trim();
                                if(folderName.isEmpty())
                                    return;
                                AndroidUtilities.hideKeyboard(confirmView.getEditText());
                                newFolder(folderName);
                                dialog.dismiss();
                            }
                        });
                    }
                });
                dialog.show();
                confirmView.getEditText().setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                        if (i == EditorInfo.IME_ACTION_DONE) {
                            String folderName = confirmView.getEnteredTitle().trim();
                            if(folderName.isEmpty())
                                return true;
                            AndroidUtilities.hideKeyboard(confirmView.getEditText());
                            newFolder(folderName);
                            dialog.dismiss();
                            return true;
                        }
                        return false;
                    }
                });
                confirmView.showKeyboard();
            }

            @Override
            public void sendRecentGallery() {
                if(recentPhotosController != null)
                    recentPhotosController.sendSelection();
            }

            @Override
            public void chooseFileFromGallery() {
                performGalleryIntentLaunch();
            }

            @Override
            public void chooseFileFromAll() {
                chooseFile();
            }

            @Override
            public void onBottomSheetClose(boolean dismissed) {
                restoreNewDocMenu = false;
                if(dismissed && recentPhotosController != null)
                    recentPhotosController.getAdapter().clearSelection();
                attachListController.continueStartIntent();
            }

            @Override
            public void onActionChoose() {
                try
                {
                    ((VKDocsMainActivity) getActivity()).slideDownBottomSheet();
                }
                catch(Exception e) { e.printStackTrace(); }
            }
        };
        contextMenuActionsDelegate = new DocActionsView.MenuDelegate() {
            @Override
            public void didShareWithFriendRequested(Document doc) {
                shareDocWithFriend(doc);
            }

            @Override
            public void didMoveToFolderRequested(Document doc) {
                if(listController != null)
                    listController.showOnlyFolders(doc);
                try
                {
                    ((VKDocsMainActivity) getActivity()).interceptBackPress(folderBackPress);
                }
                catch(Exception ignored) {}
            }

            @Override
            public void didRemoveFromFolderRequested(Document doc) {
                if(listController != null)
                    listController.removeFromFolder(doc);
            }

            @Override
            public void didOfflineAccessRequested(Document doc) {
                DocumentLoadManager.sharedInstance().requestDownload(doc);
            }

            @Override
            public void didRevokeOfflineAccessRequested(Document doc) {
                try
                {
                    if(listController != null)
                        listController.offlineAccessRevokeRequested(doc);
                    doc.revokeOfflineAccess();
                }
                catch(Exception ignored) {}
            }

            @Override
            public void didRemoveRequested(final Document doc) {
                if(Configuration.sharedInstance().deleteWithoutConfirmation)
                    performDeleteDocument(doc);
                else
                {
                    new AlertDialog.Builder(getActivity())
                            .setTitle(R.string.confirm_delete)
                            .setMessage(R.string.cant_be_undone)
                            .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    performDeleteDocument(doc);
                                }
                            })
                            .setNegativeButton(R.string.no, null)
                            .setCancelable(false)
                            .show();
                }
            }
            private void performDeleteDocument(final Document doc)
            {
                VKRequest.VKRequestListener onRemoveListener = new VKRequest.VKRequestListener() {
                    @Override
                    public void onComplete(VKResponse response) {
                        AppLoader.applicationHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                if(listController != null)
                                    listController.deleteDocument(doc);
                            }
                        });
                    }
                    @Override
                    public void onError(VKError error) {
                        AppLoader.applicationHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(AppLoader.sharedInstance().getApplicationContext(), R.string.error_removing_document, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                };
                VKRequest loadDocsRequest = new VKRequest("docs.delete", VKParameters.from(VKApiConst.OWNER_ID, VKAccessToken.currentToken().userId, Constants.VK_DOC_ID, doc.getId()));
                loadDocsRequest.attempts = 10;
                loadDocsRequest.executeWithListener(onRemoveListener);
            }
            private void performDeleteFolder(Folder folder)
            {
                if(listController != null)
                    listController.deleteFolder(folder);
            }

            @Override
            public void didRemoveRequested(final Folder folder) {
                if(Configuration.sharedInstance().deleteWithoutConfirmation)
                    performDeleteFolder(folder);
                else
                {
                    new AlertDialog.Builder(getActivity())
                            .setTitle(R.string.confirm_delete)
                            .setMessage(R.string.cant_be_undone)
                            .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    performDeleteFolder(folder);
                                    performDeleteFolder(folder);
                                }
                            })
                            .setNegativeButton(R.string.no, null)
                            .setCancelable(false)
                            .show();
                }
            }

            @Override
            public void onBottomSheetClose(boolean dismissed) {
                if(!dismissed && docActionsController != null)
                    docActionsController.continueProcessing();
                restoreDocContextMenu = false;
            }
            @Override
            public void onActionChoose()
            {
                try
                {
                    ((VKDocsMainActivity) getActivity()).slideDownBottomSheet();
                }
                catch(Exception ignored) {}
            }

            @Override
            public void didRenameRequested(Document doc) {
                try
                {
                    getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
                }
                catch(Exception ignored) {}
                docActionsBottomSheet.onRenameEnabled(doc);
            }

            @Override
            public void didRenameRequested(Folder folder) {
                try
                {
                    getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
                }
                catch(Exception ignored) {}
                docActionsBottomSheet.onRenameEnabled(folder);
            }

            @Override
            public void onRenameDonePressed(final Document doc, final String newTitle) {
                try
                {
                    getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                    VKRequest req = new VKRequest("docs.edit", VKParameters.from("owner_id", CurrentUser.getInstance().getId(), "doc_id", doc.getId(), "title", newTitle));
                    req.executeWithListener(new VKRequest.VKRequestListener() {
                        @Override
                        public void onComplete(VKResponse response) {
                            super.onComplete(response);
                            doc.setTitle(newTitle);
                            if(listController != null)
                                listController.updateTitle(doc, newTitle);
                            try
                            {
                                DaoSession daoSession = AppLoader.sharedInstance().daoMaster.newSession();
                                DocumentDao documentDao = daoSession.getDocumentDao();
                                documentDao.update(doc);
                            }
                            catch(Exception ignored) {}
                        }

                        @Override
                        public void onError(VKError error) {
                            super.onError(error);
                            if(BuildConfig.DEBUG)
                                Log.d("MainFragment", error.toString());
                            try
                            {
                                AppLoader.applicationHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getContext(), R.string.error_rename_document, Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                            catch(Exception ignored) {}
                        }
                    });
                }
                catch(Exception ignored) {}
                ((VKDocsMainActivity)getActivity()).slideDownBottomSheet();
            }

            @Override
            public void onRenameDonePressed(final Folder folder, final String newTitle) {
                try
                {
                    getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                    folder.setFolderName(newTitle);
                    DatabaseWritingThread.getInstance().addTask(folder);
                    if(listController != null)
                        listController.updateTitle(folder);
                }
                catch(Exception ignored) {}
                ((VKDocsMainActivity)getActivity()).slideDownBottomSheet();
            }

            @Override
            public void onSelectionModeRequested(DocListItemWrap initialSelection) {
                ((VKDocsMainActivity) getActivity()).disableNavDrawer();
                if(listController != null) {
                    ((VKDocsMainActivity) getActivity()).hideFAB();
                    listController.enableSelectionMode(initialSelection);
                    selectionMode = true;
                    ((VKDocsMainActivity) getActivity()).interceptBackPress(selectionModeBackPress);
                    selectionMenu.startAnimation(selectionMenuShow);
                    selectionMenu.updateSelectionCount(1);
                }
            }
        };
        quickSelectDelegate = new GalleryImagesListAdapter.QuickSelectDelegate() {

            @Override
            public void requestPerformQuickSend(Document doc, File file) {
                if(listController != null)
                    listController.addUploadingDocument(doc);
                upload(file, doc);
            }

            @Override
            public void onTransmittingDocsStart() {
                AppLoader.applicationHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        attachListController.onQuickPhotoSendRequest();
                    }
                });
            }

            @Override
            public void didQuickSendSelectionCountUpdated(int newCount) {
                attachListController.getAdapter().updateQuickSendSelectionCount(newCount);
            }
        };
    }

    @Override
    public boolean hasFAB() {
        return true;
    }
    @Override
    public void onResume()
    {
        super.onResume();
        try
        {
            ((VKDocsMainActivity) getActivity()).enableNavDrawer();
        }
        catch(Exception e) { e.printStackTrace(); }
        if(listController != null) {
            listController.onFragmentResume();
            listController.replaceView(listView);
        }
        if(restoreNewDocMenu)
            showNewDocMenu();
        else if(restoreDocContextMenu)
        {
            createDocContextMenu();
            if(docActionsBottomSheet != null && docActionsController.getAttachedDocument() != null)
            {
                docActionsBottomSheet.title.setText(docActionsController.getAttachedDocument().getTitle());
                openDocContextMenu();
            }
        }
        if(searchMode)
            enableSearchMode();
    }
    @Override
    public void onPause()
    {
        super.onPause();
        try
        {
            ((VKDocsMainActivity) getActivity()).disableNavDrawer();
        }
        catch(Exception e) { e.printStackTrace(); }
        if(listController != null)
            listController.onFragmentPause();
    }

    @Override
    public void init() {
        boolean offline = getArguments() != null ? getArguments().getBoolean(Constants.ARG_START_OFFLINE, false) : false;
        galleryImagePathTask = new GalleryImagePathTask();
        fabClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showNewDocMenu();
            }
        };
        ((VKDocsMainActivity) getActivity()).getFAB().setOnClickListener(fabClickListener);
        listController = new DocsListViewController(listView, fragmentDelegate);
        if(offline)
            listController.showOfflineDocs();
        try
        {
            ((VKDocsMainActivity) getActivity()).enableNavDrawer();
        }
        catch(Exception ignored) {}
    }

    @Override
    public ActionView getActionView() {
        if(actionView == null) {
            actionView = new FileScreenActionView(getContext());
            actionView.drawerButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(actionViewController != null && !searchMode) {
                        if(!selectionMode)
                            actionViewController.didNavDrawerButtonClicked();
                    }
                    else
                        goBack();
                }
            });
            actionView.searchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(actionViewController != null && !selectionMode)
                        actionViewController.didSearchButtonClicked();
                }
            });
            arrowDrawable.setStrokeColor(0xFFFFFFFF);
            actionView.drawerButton.setImageDrawable(arrowDrawable);
            actionView.setController(getActionViewController());
        }
        return actionView;
    }

    @Override
    public View getContentView() {
        RelativeLayout rl = new RelativeLayout(getContext());
        ViewGroup.LayoutParams fullscreenLP = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        int padding = AndroidUtilities.dp(16);
        emptyView = new TextView(getContext());
        emptyView.setTextColor(0xFF999999);
        emptyView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);
        emptyView.setGravity(Gravity.CENTER);
        emptyView.setPadding(padding, padding, padding, padding);
        emptyView.setVisibility(View.GONE);
        emptyView.setLayoutParams(fullscreenLP);
        container = new SwipeRefreshLayout(getContext());
        container.setOnRefreshListener(pullToRefreshListener);
        container.setColorSchemeColors(0xFF5181B8);
        listView = new DocsListView(getContext());
        listView.setLayoutParams(fullscreenLP);
        container.addView(listView);
        RelativeLayout.LayoutParams pullToRefreshLP = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        pullToRefreshLP.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        pullToRefreshLP.addRule(RelativeLayout.ABOVE, R.id.selection_menu);

        selectionMenu = new SelectionInfoView(getContext());
        selectionMenu.setId(R.id.selection_menu);
        selectionMenu.setVisibility(View.GONE);
        selectionMenu.setDelegate(selectionDelegate);
        RelativeLayout.LayoutParams selectionLP = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, AndroidUtilities.dp(52));
        selectionLP.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        selectionMenu.setLayoutParams(selectionLP);

        rl.addView(container);
        rl.addView(selectionMenu);
        rl.addView(emptyView);

        return rl;
    }

    public ActionViewController getActionViewController() {
        if(actionViewController == null)
            actionViewController = new FileScreenActionViewController();
        return actionViewController;
    }

    @Override
    public void onDestroy()
    {
        try
        {
            if(((VKDocsMainActivity) getActivity()).isLoggingOut())
            {
                AppLoader.sharedInstance().daoMaster.getDatabase().close();
                AppLoader.sharedInstance().daoMaster = null;
            }
        }
        catch(Exception ignored) {}
        if(listController != null) {
            listController.onFragmentDestroy();
            listController = null;
        }
        newDocMenu = null;
        NotificationCenter.getInstance().removeObserver(drawerSelectionObserver);
    }

    @Override
    public int getScreenId() {
        return VKDocsBaseScreen.MAIN;
    }

    public void shareDocWithFriend(Document doc)
    {
        if(doc.getId() >= 2000000000) {
            Toast.makeText(getContext(), String.format(getResources().getString(R.string.wait_for_upload), doc.getTitle()), Toast.LENGTH_SHORT).show();
            return;
        }
        Bundle args = new Bundle();
        args.putLong(Constants.ARG_DOC_ID, doc.getId());
        BaseScreen scr = new ShareWithFriendFragment();
        scr.setArguments(args);
        ScreenManager.getInstance().addScreen(scr, true);
    }
    private void openSettings()
    {
        BaseScreen scr = new SettingsFragment();
        ScreenManager.getInstance().addScreen(scr, true);
    }
    private void openBooksDashboard()
    {
        BaseScreen scr = new BooksDashboardScreen();
        ScreenManager.getInstance().addScreen(scr, true);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        try
        {

            if(resultCode == Activity.RESULT_OK)
            {
                if(requestCode == BottomSheetAction.CHOOSE_A_FILE)
                {
                    final Uri uri = data.getData();
                    if(uri != null)
                    {
                        if(Build.VERSION.SDK_INT >= 19)
                        {
                            final int takeFlags = data.getFlags()
                                    & (Intent.FLAG_GRANT_READ_URI_PERMISSION
                                    | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                            // Check for the freshest data.
                            //noinspection ResourceType
                            AppLoader.sharedInstance().getContentResolver().takePersistableUriPermission(uri, takeFlags);
                        }
                        try
                        {
                            File f = new File(uri.getPath());
                            Document doc = new Document();
                            doc.markUploading(f);
                            if(listController != null)
                                listController.addUploadingDocument(doc);
                            upload(f, doc);
                        }
                        catch(Exception e) { e.printStackTrace(); }
                    }
                }
                else if(requestCode == BottomSheetAction.CHOOSE_FROM_GALLERY)
                {
                    Uri uri = data.getData();
                    if(uri != null)
                    {
                        if(Build.VERSION.SDK_INT >= 19)
                        {
                            final int takeFlags = data.getFlags()
                                    & (Intent.FLAG_GRANT_READ_URI_PERMISSION
                                    | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                            // Check for the freshest data.
                            //noinspection ResourceType
                            AppLoader.sharedInstance().getContentResolver().takePersistableUriPermission(uri, takeFlags);
                        }
                        galleryImagePathTask.setLink(uri);
                        galleryImagePathTask.onFailure(new OnTaskFailureListener() {
                            @Override
                            public void taskFailed(BackgroundTask task) {
                                AppLoader.applicationHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getContext(), R.string.error_picking_from_gallery, Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                        });
                        galleryImagePathTask.onComplete(new OnTaskCompleteListener() {
                            @Override
                            public void taskComplete(BackgroundTask task) {
                                final String path = ((GalleryImagePathTask) task).getImagePath();
                                if (path != null) {
                                    try {
                                        final File f = new File(path);
                                        final Document doc = new Document();
                                        doc.markUploading(f);
                                        if (listController != null)
                                            listController.addUploadingDocument(doc);
                                        upload(f, doc);
                                    }
                                    catch (Exception e) { e.printStackTrace(); }
                                }
                            }
                        });
                        galleryImagePathTask.addToQueue();
                    }

                }
            }
        }
        catch(Exception ignored) { AppLoader.applicationHandler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getContext(), R.string.error_picking_file, Toast.LENGTH_LONG).show();
            }
        }); }
    }
    private void showNewDocMenu()
    {
        if(newDocMenu == null)
        {
            newDocMenu = new NewDocView(getContext());
            FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            newDocMenu.setLayoutParams(lp);
            newDocMenu.setDelegate(newDocMenuDelegate);
        }
        if(attachListController == null)
        {
            attachListController = new NewDocListController(newDocMenu.attachActionsListView);
            newDocMenu.attachActionsListView.setController(attachListController);
            attachListController.setDelegate(newDocMenuDelegate);
        }
        else
        {
            newDocMenu.attachActionsListView.setController(attachListController);
            attachListController.replaceView(newDocMenu.attachActionsListView);
        }
        if(recentPhotosController == null) {
            recentPhotosController = new GalleryRecentPhotosController(newDocMenu.recentFromGallery);
            recentPhotosController.setDelegate(quickSelectDelegate);
        }
        else
            recentPhotosController.replaceView(newDocMenu.recentFromGallery);
        try
        {
            restoreNewDocMenu = true;
            ((VKDocsMainActivity) getActivity()).showBottomSheet(newDocMenu);
        }
        catch(Exception e) { e.printStackTrace(); }
    }
    private void createDocContextMenu()
    {
        if(docActionsBottomSheet == null)
        {
            docActionsBottomSheet = new DocActionsView(getContext());
            FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            docActionsBottomSheet.setLayoutParams(lp);
            docActionsBottomSheet.setDelegate(contextMenuActionsDelegate);
        }
        if(docActionsController == null)
        {
            docActionsController = new DocContextMenuListController(docActionsBottomSheet.attachActionsListView);
            docActionsController.setDelegate(contextMenuActionsDelegate);
            docActionsBottomSheet.attachActionsListView.setController(docActionsController);
        }
        else
            docActionsController.replaceView(docActionsBottomSheet.attachActionsListView);
    }
    private void openDocContextMenu()
    {
        try
        {
            restoreDocContextMenu = true;
            ((VKDocsMainActivity) getActivity()).showBottomSheet(docActionsBottomSheet);
        }
        catch(Exception e) { e.printStackTrace(); }
    }
    public void showContextMenu(Document doc)
    {
        DocsListViewController.Mode listMode = listController == null ? null : listController.getCurrentMode();
        if(listMode == DocsListViewController.Mode.FOLDERS_ONLY || listMode == null)
            return;
        createDocContextMenu();
        if(docActionsController != null)
        {
            DocContextMenuListController.WhereOpened whereOpened = listMode == DocsListViewController.Mode.ALL_DOCS ? DocContextMenuListController.WhereOpened.MAIN_SCREEN : listMode == DocsListViewController.Mode.FOLDER_CONTENTS ? DocContextMenuListController.WhereOpened.FOLDER : listMode == DocsListViewController.Mode.SEARCH_RESULTS ? DocContextMenuListController.WhereOpened.SEARCH : DocContextMenuListController.WhereOpened.OFFLINE_SCREEN;
            docActionsController.attachDocument(doc, whereOpened);
            docActionsBottomSheet.title.setText(doc.getTitle());
            openDocContextMenu();
        }
    }
    public void showContextMenu(Folder folder)
    {
        createDocContextMenu();
        if(docActionsController != null)
        {
            docActionsController.attachFolder(folder);
            docActionsBottomSheet.title.setText(folder.getFolderName());
            openDocContextMenu();
        }
    }
    private void performGalleryIntentLaunch()
    {
        if (Build.VERSION.SDK_INT < 19){
            Intent galleryIntent = new Intent();
            galleryIntent.setType("image/*");
            galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(galleryIntent, BottomSheetAction.CHOOSE_FROM_GALLERY);
        } else {
            Intent galleryIntent = new Intent();
            galleryIntent.setType("image/*");
            galleryIntent.setAction(Intent.ACTION_OPEN_DOCUMENT);
            galleryIntent.addCategory(Intent.CATEGORY_OPENABLE);
            startActivityForResult(galleryIntent, BottomSheetAction.CHOOSE_FROM_GALLERY);
        }
    }
    private void chooseFile()
    {
        if (Build.VERSION.SDK_INT < 19){
            Intent galleryIntent = new Intent();
            galleryIntent.setType("*/*");
            galleryIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(galleryIntent, BottomSheetAction.CHOOSE_A_FILE);
        } else {
            Intent galleryIntent = new Intent();
            galleryIntent.setType("*/*");
            galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
            galleryIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            galleryIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            startActivityForResult(galleryIntent, BottomSheetAction.CHOOSE_A_FILE);
        }
    }

    private void upload(File path, Document docObj)
    {
        UploadRequestHandler uploadReq = new UploadRequestHandler(path, docObj, null, null);
        uploadReq.performUpload();
    }
    private void openEpub(String path)
    {
        /*Bundle args = new Bundle();
        args.putString(Constants.ARG_BOOK_PATH, path);
        BaseScreen scr = new EpubReaderFragment();
        scr.setArguments(args);
        ScreenManager.getInstance().addScreen(scr, true);*/
        boolean showAlert = false;
        try
        {
            Uri uriPath = Uri.parse(path);
            Intent fileIntent = new Intent(Intent.ACTION_VIEW);
            fileIntent.setDataAndType(uriPath, MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(path)));
            List<ResolveInfo> intents = getActivity().getPackageManager().queryIntentActivities(fileIntent, PackageManager.MATCH_DEFAULT_ONLY);
            if(intents.isEmpty())
                showAlert = true;
            else
                getActivity().startActivity(fileIntent);
        }
        catch(Exception ignored) { Toast.makeText(getActivity().getApplicationContext(), R.string.no_app_found, Toast.LENGTH_SHORT).show(); }
        if(showAlert)
        {
            AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
            adb.setCancelable(true);
            adb.setMessage(R.string.no_epub_reader);
            adb.setPositiveButton(R.string.got_it, null);
            adb.show();
        }
    }
    private void openDocument(File path, String mimeType)
    {
        try
        {
            Uri uriPath = Uri.fromFile(path);
            Intent fileIntent = new Intent(Intent.ACTION_VIEW);
            fileIntent.setDataAndType(uriPath, mimeType);
            List<ResolveInfo> intents = getActivity().getPackageManager().queryIntentActivities(fileIntent, PackageManager.MATCH_DEFAULT_ONLY);
            if(intents.isEmpty())
                Toast.makeText(getActivity().getApplicationContext(), R.string.no_app_found, Toast.LENGTH_SHORT).show();
            else
                getActivity().startActivity(fileIntent);
        }
        catch(Exception ignored) { Toast.makeText(getActivity().getApplicationContext(), R.string.no_app_found, Toast.LENGTH_SHORT).show(); }
    }
    private void enableSearchMode()
    {
        if(searchMode == false)
        {
            flipHandler.removeCallbacksAndMessages(null);
            parameter = 0.0f;
            flipHandler.post(flipRunnable);
        }
        searchMode = true;
        if(actionView == null) {
            return;
        }
        listController.enableSearchMode();
        ((VKDocsMainActivity) getActivity()).disableNavDrawer();
        actionView.searchButton.setVisibility(View.GONE);
        actionView.title.setVisibility(View.GONE);
        actionView.searchEditText.setVisibility(View.VISIBLE);
        actionView.searchEditText.addTextChangedListener(actionViewController.textWatcher);
        ((VKDocsMainActivity) getActivity()).disableMenuButtonProcessing(true);
        ((VKDocsMainActivity) getActivity()).hideFAB();
        ((VKDocsMainActivity) getActivity()).interceptBackPress(searchModeBackPressInterceptor);
        actionView.searchEditText.requestFocus();
        AppLoader.applicationHandler.postDelayed(showKeyboardRunnable, 200);
        searchReqHandler.removeCallbacksAndMessages(null);
    }
    private void disableSearchMode()
    {
        if(searchMode)
        {
            flipHandler.removeCallbacksAndMessages(null);
            parameter = 1.0f;
            flipHandler.post(flipRunnable);
        }
        searchMode = false;
        if(actionView == null || actionViewController == null) {
            return;
        }
        AppLoader.applicationHandler.postDelayed(hideKeyboardRunnable, 200);
        actionView.searchEditText.removeTextChangedListener(actionViewController.textWatcher);
        actionView.searchEditText.setVisibility(View.GONE);
        actionView.searchButton.setVisibility(View.VISIBLE);
        actionView.title.setVisibility(View.VISIBLE);
        actionView.searchEditText.setText("");
        listController.clearSearchData();
        listController.usePreviousMode();
        if(listController.getCurrentMode() == DocsListViewController.Mode.ALL_DOCS)
            ((VKDocsMainActivity) getActivity()).showFAB();
        ((VKDocsMainActivity) getActivity()).disableMenuButtonProcessing(false);
        ((VKDocsMainActivity) getActivity()).removeBackPressInterceptor(searchModeBackPressInterceptor);
    }
    private class FileScreenActionViewController extends ActionViewController
    {
        public TextWatcher textWatcher;
        public FileScreenActionViewController()
        {
            super();
            textWatcher = new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    try
                    {
                        String query = editable.toString().trim();
                        if(!query.isEmpty() && !query.startsWith(" ") && !query.endsWith(" ") && listController != null)
                            listController.newQuery(query);
                    }
                    catch(Exception ignored) {}
                }
            };
        }
        @Override
        public void onFragmentPaused() {
        }

        @Override
        public void onFragmentResumed() {
        }

        @Override
        public void onActionViewRemoved() {
        }
        public void didNavDrawerButtonClicked()
        {
            try
            {
                ((VKDocsMainActivity) getActivity()).showNavDrawer();
            }
            catch(Exception ignored) {}
        }
        public void didSearchButtonClicked()
        {
            enableSearchMode();
        }
    }
}
