package com.andremacareno.vkdocs.fragments;


import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.view.ContextThemeWrapper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.andremacareno.vkdocs.AndroidUtilities;
import com.andremacareno.vkdocs.AppLoader;
import com.andremacareno.vkdocs.BuildConfig;
import com.andremacareno.vkdocs.Constants;
import com.andremacareno.vkdocs.R;
import com.andremacareno.vkdocs.controllers.FriendsListViewController;
import com.andremacareno.vkdocs.views.ActionView;
import com.andremacareno.vkdocs.views.FastScroller;
import com.andremacareno.vkdocs.views.FriendsListView;
import com.andremacareno.vkdocs.views.ShareReceiverListView;
import com.andremacareno.vkdocs.views.actionviews.ShareScreenActionView;

/**
 * Created by Andrew on 01.02.2016.
 */
public class ShareWithFriendFragment extends VKDocsBaseScreen {
    public interface FragmentDelegate
    {
        void onStatusChanged(int status);
    }
    private FriendsListView listView;
    private FriendsListViewController listController;
    private long documentId = 0;
    private ShareScreenActionView actionView;
    private FragmentDelegate fragmentDelegate;
    private FastScroller fastScroller;
    private FloatingActionButton fab;
    ShareReceiverListView.SelectionDelegate selectionDelegate;
    private final Runnable changeBackgroundRunnable = new Runnable() {
        @Override
        public void run() {
            try
            {
                listView.setBackgroundColor(0x00000000);
                listController = new FriendsListViewController(listView, documentId, fragmentDelegate);
                listController.setHeaderListViewRef(actionView.selection);
                fastScroller.setRecyclerView(listView);
            }
            catch(Exception ignored) {}
        }
    };

    @Override
    public boolean hasFAB() {
        return false;
    }

    @Override
    public void onResume()
    {
        super.onResume();
        if(listController != null) {
            listController.onFragmentResume();
            listController.replaceView(listView);
        }
    }
    @Override
    public void onPause()
    {
        super.onPause();
        if(listController != null)
            listController.onFragmentPause();
    }

    @Override
    public void init() {
        if(getArguments() == null) {
            if(BuildConfig.DEBUG)
                Log.d("ShareWithFriend", "return");
            return;
        }
        fragmentDelegate = new FragmentDelegate() {
            private final Runnable sendingRunnable = new Runnable() {
                @Override
                public void run() {
                    try
                    {
                        Toast.makeText(AppLoader.sharedInstance().getApplicationContext(), R.string.sending, Toast.LENGTH_SHORT).show();
                    }
                    catch(Exception ignored) {}
                }
            };
            private final Runnable sentRunnable = new Runnable() {
                @Override
                public void run() {
                    try
                    {
                        Toast.makeText(AppLoader.sharedInstance().getApplicationContext(), R.string.sent, Toast.LENGTH_SHORT).show();
                        goBack();
                    }
                    catch(Exception ignored) {}
                }
            };
            private final Runnable failedRunnable = new Runnable() {
                @Override
                public void run() {
                    try
                    {
                        Toast.makeText(AppLoader.sharedInstance().getApplicationContext(), R.string.share_failed, Toast.LENGTH_SHORT).show();
                    }
                    catch(Exception ignored) {}
                }
            };
            @Override
            public void onStatusChanged(final int status) {
                try
                {
                    AppLoader.applicationHandler.post(status == FriendsListViewController.STATUS_SENDING ? sendingRunnable : status == FriendsListViewController.STATUS_FAILED ? failedRunnable : sentRunnable);
                }
                catch(Exception ignored) {}
            }
        };
        documentId = getArguments().getLong(Constants.ARG_DOC_ID);
        if(BuildConfig.DEBUG)
            Log.d("ShareWithFriend", String.format("documentId = %d", documentId));
    }
    @Override
    protected void goBack()
    {
        try
        {
            actionView.selection.clearSelection();
            super.goBack();
        }
        catch(Exception ignored) {}
    }
    @Override
    public ActionView getActionView() {
        if(actionView == null)
        {
            actionView = new ShareScreenActionView(getContext());
            actionView.backButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    goBack();
                }
            });
            selectionDelegate = new ShareReceiverListView.SelectionDelegate() {
                @Override
                public void didSelectionCountChanged(int selectionCount) {
                    if(selectionCount == 0)
                        fab.hide();
                    else
                        fab.show();
                }
            };
            actionView.selection.setDelegate(selectionDelegate);
            actionView.searchEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    try
                    {
                        String query = editable.toString().trim();
                        if(!query.isEmpty() && !query.startsWith(" ") && !query.endsWith(" ") && listController != null)
                            listController.newQuery(query);
                        else if(query.isEmpty())
                            listController.clearSearchData();
                    }
                    catch(Exception ignored) {}
                }
            });
        }
        return actionView;
    }
    @Override
    public FloatingActionButton getFabBetweenViews()
    {
        fab = new FloatingActionButton(new ContextThemeWrapper(getContext(), R.style.WhiteFabTheme));
        fab.setRippleColor(0x40BEBEBE);
        fab.setImageResource(R.drawable.ic_share);
        fab.setColorFilter(new PorterDuffColorFilter(ContextCompat.getColor(getContext(), R.color.bottom_sheet_mask), PorterDuff.Mode.SRC_ATOP));
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try
                {
                    String ids = actionView.selection.getIds();
                    if(ids.isEmpty())
                        return;
                    if(listController != null)
                        listController.send(ids);
                }
                catch(Exception ignored) {}
            }
        });
        fab.hide();
        return fab;
    }
    @Override
    public View getContentView() {
        FrameLayout container = new FrameLayout(getContext());
        FrameLayout.LayoutParams fastScrollLP = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.MATCH_PARENT);
        FrameLayout.LayoutParams rvLP = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
        int margin = AndroidUtilities.dp(8);
        fastScrollLP.setMargins(0, 0, margin, 0);
        fastScrollLP.gravity = Gravity.RIGHT;
        fastScroller = new FastScroller(getContext());
        listView = new FriendsListView(getContext());
        listView.setBackgroundColor(0xFFFFFFFF);
        fastScroller.setLayoutParams(fastScrollLP);
        listView.setLayoutParams(rvLP);
        listView.setVerticalScrollBarEnabled(false);
        container.addView(listView);
        container.addView(fastScroller);
        return container;
    }

    @Override
    public void onDestroy()
    {
        if(listController != null) {
            listController.onFragmentDestroy();
            listController = null;
        }
    }
    @Override
    public void onOpenAnimationFinished()
    {
        listView.postDelayed(changeBackgroundRunnable, 100);
    }
    @Override
    public void onCloseAnimationStart()
    {
        listView.setBackgroundColor(0xFFFFFFFF);
    }

    @Override
    public int getScreenId() {
        return VKDocsBaseScreen.SHARE_WITH_FRIEND;
    }
}
