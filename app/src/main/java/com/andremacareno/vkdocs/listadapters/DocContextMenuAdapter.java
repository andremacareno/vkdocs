package com.andremacareno.vkdocs.listadapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.andremacareno.vkdocs.AndroidUtilities;
import com.andremacareno.vkdocs.controllers.DocContextMenuListController;
import com.andremacareno.vkdocs.objs.BottomSheetAction;
import com.andremacareno.vkdocs.views.BottomSheetHolder;
import com.andremacareno.vkdocs.views.BottomSheetItem;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class DocContextMenuAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<BottomSheetAction> actions;
    private WeakReference<DocContextMenuListController> controller;
    public DocContextMenuAdapter(DocContextMenuListController c, ArrayList<BottomSheetAction> actions)
    {
        this.controller = new WeakReference<DocContextMenuListController>(c);
        this.actions = actions;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = new BottomSheetItem(parent.getContext());
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, AndroidUtilities.dp(52));
        view.setLayoutParams(lp);
        return new BottomSheetHolder(view) {
            @Override
            public void onClick(View view) {
                try
                {
                    int position = getAdapterPosition();
                    BottomSheetAction act = actions.get(position);
                    controller.get().didActionChosen(act);
                }
                catch(Exception ignored) {}
            }
        };
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        BottomSheetAction act = actions.get(position);
        ((BottomSheetHolder) holder).item.fill(act);
    }

    @Override
    public int getItemCount() {
        return actions.size();
    }
}
