package com.andremacareno.vkdocs.listadapters;

import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.andremacareno.vkdocs.AndroidUtilities;
import com.andremacareno.vkdocs.R;
import com.andremacareno.vkdocs.cache.ImageLoader;
import com.andremacareno.vkdocs.objs.BookWrap;
import com.andremacareno.vkdocs.views.RecyclingImageView;

import java.util.ArrayList;

import nl.siegmann.epublib.domain.Book;

public class BooksDashboardAdapter extends RecyclerView.Adapter<BooksDashboardAdapter.ViewHolder> {
    private final int padding;
    public interface DashboardDelegate
    {
        void onBookSelected(BookWrap book);
        void controlEmptyView(boolean show);
    }
    private final ArrayList<BookWrap> books;
    private DashboardDelegate delegate;
    public BooksDashboardAdapter(ArrayList<BookWrap> data, DashboardDelegate delegate)
    {
        this.delegate = delegate;
        this.books = data; this.padding = AndroidUtilities.dp(12);
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = new RecyclingImageView(parent.getContext());
        if(Build.VERSION.SDK_INT >= 21)
            view.setElevation(AndroidUtilities.dp(3));
        view.setBackgroundResource(R.drawable.list_selector);
        view.setPadding(padding, padding/2, padding, padding/2);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

    }

    @Override
    public void onViewAttachedToWindow(ViewHolder holder) {
        int position = holder.getAdapterPosition();
        if(position >= 0)
        {
            Book book;
            synchronized(books)
            {
                book = books.get(position).book;
                ImageLoader.getInstance().loadBookCover(book, holder.img);
            }
        }
    }

    @Override
    public int getItemCount() {
        synchronized(books)
        {
            return books.size();
        }
    }
    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public RecyclingImageView img;
        public ViewHolder(View itemView) {
            super(itemView);
            this.img = (RecyclingImageView) itemView;
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(delegate != null)
                    {
                        int position = getAdapterPosition();
                        if(position >= 0)
                        {
                            synchronized(books)
                            {
                                delegate.onBookSelected(books.get(position));
                            }
                        }
                    }
                }
            });
        }
    }
}
