package com.andremacareno.vkdocs.listadapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.andremacareno.vkdocs.AndroidUtilities;
import com.andremacareno.vkdocs.cache.ImageLoader;
import com.andremacareno.vkdocs.controllers.GalleryRecentPhotosController;
import com.andremacareno.vkdocs.dao.Document;
import com.andremacareno.vkdocs.objs.GalleryImageEntry;
import com.andremacareno.vkdocs.views.GalleryImageItem;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashSet;


/**
 * Created by Andrew on 01.05.2015.
 */
public class GalleryImagesListAdapter extends RecyclerView.Adapter<GalleryImagesListAdapter.ViewHolder> {
    public interface QuickSelectDelegate
    {
        void requestPerformQuickSend(Document doc, File file);
        void onTransmittingDocsStart();
        void didQuickSendSelectionCountUpdated(int newCount);

    }
    private ArrayList<GalleryImageEntry> entries;
    private HashSet<Integer> selectedIds = new HashSet<Integer>();
    private QuickSelectDelegate delegate;
    private WeakReference<GalleryRecentPhotosController> controller;
    public GalleryImagesListAdapter(GalleryRecentPhotosController c, ArrayList<GalleryImageEntry> data)
    {
        setHasStableIds(true);
        this.controller = new WeakReference<GalleryRecentPhotosController>(c);
        this.entries = data;
    }
    public void setDelegate(QuickSelectDelegate delegateRef) { this.delegate = delegateRef; }
    @Override
    public long getItemId(int position)
    {
        return entries.get(position).getThumbId();
    }
    @Override
    public GalleryImagesListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = new GalleryImageItem(parent.getContext());
        v.setPadding(AndroidUtilities.dp(4), AndroidUtilities.dp(4), AndroidUtilities.dp(4), AndroidUtilities.dp(4));
        v.setLayoutParams(new LinearLayout.LayoutParams(AndroidUtilities.dp(104), AndroidUtilities.dp(104)));
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(GalleryImagesListAdapter.ViewHolder holder, int position) {
        GalleryImageEntry entry = entries.get(position);
        if(selectedIds.contains(entry.getThumbId()))
            holder.item.markSelected(false);
        else
            holder.item.markUnselected(false);
    }
    @Override
    public void onViewAttachedToWindow(ViewHolder holder) {
        int position = holder.getAdapterPosition();
        if(position < 0)
            return;
        GalleryImageEntry entry = entries.get(position);
        ImageLoader.getInstance().loadGalleryThumb(entry, holder.item.getImageView());
    }

    @Override
    public int getItemCount() {
        return entries.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public GalleryImageItem item;
        public ViewHolder(View itemView) {
            super(itemView);
            this.item = (GalleryImageItem) itemView;
            item.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(controller.get() != null)
                toggleSelection(getAdapterPosition(), this.item);
        }
    }
    private void toggleSelection(int position, GalleryImageItem itm)
    {
        if(position < 0)
            return;
        GalleryImageEntry entry = entries.get(position);
        if(selectedIds.contains(entry.getThumbId()))
        {
            selectedIds.remove(entry.getThumbId());
            itm.markUnselected(true);
            if(delegate != null)
                delegate.didQuickSendSelectionCountUpdated(selectedIds.size());
        }
        else
        {
            selectedIds.add(entry.getThumbId());
            itm.markSelected(true);
            if(delegate != null)
                delegate.didQuickSendSelectionCountUpdated(selectedIds.size());
        }
    }
    public HashSet<Integer> getSelectionIds()
    {
        return this.selectedIds;
    }
    public void clearSelection()
    {
        selectedIds.clear();
        if(delegate != null)
            delegate.didQuickSendSelectionCountUpdated(0);
        notifyItemRangeChanged(0, entries.size());
    }
}
