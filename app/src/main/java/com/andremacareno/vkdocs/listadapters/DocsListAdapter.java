package com.andremacareno.vkdocs.listadapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.andremacareno.vkdocs.AndroidUtilities;
import com.andremacareno.vkdocs.BuildConfig;
import com.andremacareno.vkdocs.Configuration;
import com.andremacareno.vkdocs.DownloadState;
import com.andremacareno.vkdocs.controllers.DocsListViewController;
import com.andremacareno.vkdocs.dao.Document;
import com.andremacareno.vkdocs.dao.Folder;
import com.andremacareno.vkdocs.network.DocumentLoadManager;
import com.andremacareno.vkdocs.objs.DocListItemWrap;
import com.andremacareno.vkdocs.views.DocumentView;
import com.andremacareno.vkdocs.views.ListSectionHeader;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by Andrew on 07.01.2016.
 */
public class DocsListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int SECTION = 0;
    private static final int DOC = 1;
    private static final int FOLDER = 2;
    private final ArrayList<DocListItemWrap> docListRef;
    private DocsListViewController.DocsListDelegate delegateRef;
    private final HashSet<DocListItemWrap> selectedDocs = new HashSet<>();
    private boolean selectionMode = false;
    public DocsListAdapter(ArrayList<DocListItemWrap> list, DocsListViewController.DocsListDelegate delegate)
    {
        this.docListRef = list;
        this.delegateRef = delegate;
    }
    @Override
    public int getItemViewType(int position)
    {
        try
        {
            DocListItemWrap itm;
            synchronized(docListRef)
            {
                itm = docListRef.get(position);
            }
            if(itm.getType() == DocListItemWrap.Type.SECTION)
                return SECTION;
            if(itm.getType() == DocListItemWrap.Type.DOCUMENT)
                return DOC;
            return FOLDER;
        }
        catch(Exception ignored) { return DOC;}
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == SECTION)
        {
            View v = new ListSectionHeader(parent.getContext());
            v.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, AndroidUtilities.dp(48)));
            return new SectionListHolder(v);
        }
        View v = new DocumentView(parent.getContext());
        v.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, AndroidUtilities.dp(72)));
        return new DocsListHolder(v);
    }

    @Override
    public int getItemCount() {
        synchronized(docListRef)
        {
            if(docListRef.isEmpty() && delegateRef != null)
                delegateRef.loadMoreDocuments();
            return docListRef.size();
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
    }
    @Override
    public void onViewAttachedToWindow(RecyclerView.ViewHolder viewHolder)
    {
        int position = viewHolder.getAdapterPosition();
        if(position < 0)
            return;
        synchronized(docListRef)
        {
            DocListItemWrap itm = docListRef.get(position);
            if(itm.getType() == DocListItemWrap.Type.DOCUMENT) {
                if(selectionMode) {
                    if(selectedDocs.contains(itm))
                        ((DocsListHolder) viewHolder).docView.check(false);
                    else
                        ((DocsListHolder) viewHolder).docView.uncheck(false);
                }
                else
                    ((DocsListHolder) viewHolder).docView.uncheck(false);
                ((DocsListHolder) viewHolder).docView.bindDocument(itm.castDoc());
            }
            else if(itm.getType() == DocListItemWrap.Type.FOLDER) {
                ((DocsListHolder) viewHolder).docView.bindFolder(itm.castFolder());
            }
            else if(itm.getType() == DocListItemWrap.Type.SECTION)
                ((SectionListHolder) viewHolder).header.setTitle(itm.castSection().getTitle());
            if(docListRef.size() - position <= 8 && delegateRef != null)
                delegateRef.loadMoreDocuments();
        }
    }
    @Override
    public void onViewDetachedFromWindow(RecyclerView.ViewHolder viewHolder)
    {
        int position = viewHolder.getAdapterPosition();
        if(position < 0)
            return;
        synchronized(docListRef)
        {
            try
            {
                DocListItemWrap wrap = docListRef.get(position);
                if(wrap.getType() == DocListItemWrap.Type.DOCUMENT)
                {
                    ((DocsListHolder) viewHolder).docView.unbindDoc(wrap.castDoc());
                }
            }
            catch(Exception ignored) {}
        }
    }
    public void enableSelectionMode()
    {
        selectionMode = true;
        notifyDataSetChanged();
    }
    public void disableSelectionMode()
    {
        selectionMode = false;
        selectedDocs.clear();
        notifyDataSetChanged();
    }
    public HashSet<DocListItemWrap> getSelection() { return this.selectedDocs; }
    public void removeFromSelection(DocListItemWrap wrap) {
        selectedDocs.remove(wrap);
        if(delegateRef != null)
            delegateRef.didSelectionCountChanged(selectedDocs.size());
    }
    public boolean isSelectionModeEnabled() { return this.selectionMode; }
    private void select(DocListItemWrap wrap)
    {
        selectedDocs.add(wrap);
        if(delegateRef != null)
            delegateRef.didSelectionCountChanged(selectedDocs.size());
    }
    public void addToSelection(DocListItemWrap wrap)
    {
        selectedDocs.add(wrap);
        synchronized(docListRef)
        {
            int index = docListRef.indexOf(wrap);
            if(index >= 0)
                notifyItemChanged(index);
        }
    }
    private void toggle(DocListItemWrap wrap, DocumentView docView)
    {
        if(selectedDocs.contains(wrap)) {
            removeFromSelection(wrap);
            try
            {
                docView.uncheck(!Configuration.sharedInstance().reducedAnimations);
            }
            catch(Exception ignored) {}
        }
        else {
            select(wrap);
            try
            {
                docView.check(!Configuration.sharedInstance().reducedAnimations);
            }
            catch(Exception ignored) {}
        }
    }
    class DocsListHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener
    {
        public DocumentView docView;
        public DocsListHolder(View itemView) {
            super(itemView);
            this.docView = (DocumentView) itemView;
            this.docView.setOnClickListener(this);
            this.docView.setOnLongClickListener(this);
            this.docView.getOverflowMenu().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    try
                    {
                        synchronized (docListRef)
                        {
                            DocListItemWrap itm = docListRef.get(position);
                            if(itm.getType() == DocListItemWrap.Type.DOCUMENT)
                            {
                                Document doc = itm.castDoc();
                                if(doc.isUploading()) {
                                    delegateRef.cancelUpload(position);
                                    DocumentLoadManager.sharedInstance().cancelLoad(doc);
                                }
                                else if(doc.getDownloadState() == DownloadState.DOWNLOADING)
                                    DocumentLoadManager.sharedInstance().cancelLoad(doc);
                                else
                                    delegateRef.didDocumentLongPressed(position);
                            }
                            else if(itm.getType() == DocListItemWrap.Type.FOLDER)
                            {
                                Folder folder = itm.castFolder();
                                if(folder != null && delegateRef != null)
                                    delegateRef.didFolderMenuRequested(folder);
                            }
                        }
                    }
                    catch(Exception ignored) {}
                }
            });
        }
        @Override
        public void onClick(View v)
        {
            int position = getAdapterPosition();
            DocListItemWrap wrap;
            synchronized(docListRef)
            {
                wrap = docListRef.get(position);
                if(wrap.getType() == DocListItemWrap.Type.FOLDER && delegateRef != null) {
                    if(selectionMode)
                        return;
                    delegateRef.didFolderSelected(wrap.castFolder());
                    return;
                }
            }
            if(delegateRef != null && position >= 0) {
                if(selectionMode) {
                    toggle(wrap, this.docView);
                }
                else
                    delegateRef.didDocumentSelected(position);
            }
        }

        @Override
        public boolean onLongClick(View view) {
            if(BuildConfig.DEBUG)
                Log.d("DocsListAdapter", String.format("long clicked view: %s", view.toString()));
            int position = getAdapterPosition();
            if(delegateRef != null && position >= 0) {
                synchronized(docListRef)
                {
                    DocListItemWrap wrap = docListRef.get(position);
                    if(wrap.getType() == DocListItemWrap.Type.FOLDER) {
                        if(selectionMode)
                            return false;
                        delegateRef.didFolderMenuRequested(wrap.castFolder());
                        return true;
                    }
                    else if(wrap.getType() == DocListItemWrap.Type.DOCUMENT)
                    {
                        if(!selectionMode)
                            delegateRef.didDocumentLongPressed(position);
                        else {
                            toggle(wrap, this.docView);
                        }
                        return true;
                    }
                }
            }
            return true;
        }
    }
    class SectionListHolder extends RecyclerView.ViewHolder
    {
        public ListSectionHeader header;
        public SectionListHolder(View itemView) {
            super(itemView);
            this.header = (ListSectionHeader) itemView;
        }
    }
}
