package com.andremacareno.vkdocs.listadapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.andremacareno.vkdocs.AndroidUtilities;
import com.andremacareno.vkdocs.BuildConfig;
import com.andremacareno.vkdocs.controllers.FriendsListViewController;
import com.andremacareno.vkdocs.views.FastScroller;
import com.andremacareno.vkdocs.views.FriendsListItem;
import com.andremacareno.vkdocs.views.ShareReceiverListView;
import com.vk.sdk.api.model.VKApiUserFull;
import com.vk.sdk.api.model.VKList;

import java.lang.ref.WeakReference;

/**
 * Created by Andrew on 07.01.2016.
 */
public class FriendsListAdapter extends RecyclerView.Adapter<FriendsListAdapter.FriendsListHolder> implements FastScroller.FastScrollHelper {
    public interface FriendsAdapterDelegate
    {
        public void onRemoveFromTopList(VKApiUserFull user);
    }
    private final VKList<VKApiUserFull> friendsListRef;
    private FriendsListViewController.FriendsListDelegate delegateRef;
    private final SparseArray<Character> positionToChar = new SparseArray<>();
    private WeakReference<ShareReceiverListView> viewRef;
    private FriendsAdapterDelegate delegate;
    private final SparseArray<FriendsListItem> visible = new SparseArray<>();
    public FriendsListAdapter(VKList<VKApiUserFull> list, FriendsListViewController.FriendsListDelegate delegate)
    {
        this.friendsListRef = list;
        this.delegateRef = delegate;
    }
    public void setViewRef(ShareReceiverListView view)
    {
        if(delegate == null)
            delegate = new FriendsAdapterDelegate() {
                @Override
                public void onRemoveFromTopList(VKApiUserFull user) {
                    try
                    {
                        FriendsListItem itm = visible.get(user.id);
                        if(BuildConfig.DEBUG)
                            Log.d("FriendsAdapter", String.format("unchecking %d", user.id));
                        if(itm != null) {
                            if(BuildConfig.DEBUG)
                                Log.d("FriendsAdapter", "performing unchecking");
                            itm.uncheck(true);
                        }
                    }
                    catch(Exception ignored) {}
                }
            };
        view.setBottomAdapterDelegate(delegate);
        this.viewRef = new WeakReference<ShareReceiverListView>(view);
    }
    @Override
    public FriendsListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = new FriendsListItem(parent.getContext());
        v.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, AndroidUtilities.dp(72)));
        return new FriendsListHolder(v);
    }

    @Override
    public int getItemCount() {
        synchronized(friendsListRef)
        {
            if(friendsListRef.isEmpty() && delegateRef != null)
                delegateRef.loadMoreFriends();
            return friendsListRef.size();
        }
    }

    @Override
    public void onBindViewHolder(FriendsListHolder holder, int position) {
    }
    @Override
    public void onViewDetachedFromWindow(FriendsListHolder viewHolder)
    {
        int position = viewHolder.getAdapterPosition();
        if(position < 0)
            return;
        synchronized(friendsListRef) {
            VKApiUserFull itm = friendsListRef.get(position);
            try {
                if(BuildConfig.DEBUG)
                    Log.d("FriendsAdapter", String.format("removing %d", itm.id));
                visible.remove(itm.id);
                VKApiUserFull prevItm = position == 0 ? null : friendsListRef.get(position - 1);
                viewHolder.item.insertHeaderIfNeed(itm, prevItm);
            } catch (Exception ignored) {}
        }
    }
    @Override
    public void onViewAttachedToWindow(FriendsListHolder viewHolder)
    {
        int position = viewHolder.getAdapterPosition();
        if(position < 0)
            return;
        synchronized(friendsListRef)
        {
            VKApiUserFull itm = friendsListRef.get(position);
            try
            {
                if(BuildConfig.DEBUG)
                    Log.d("FriendsAdapter", String.format("adding %d", itm.id));
                visible.put(itm.id, viewHolder.item);
                VKApiUserFull prevItm = position == 0 ? null : friendsListRef.get(position - 1);
                viewHolder.item.insertHeaderIfNeed(itm, prevItm);
            }
            catch(Exception ignored) {}
            viewHolder.item.fill(itm);
            if(isSelected(position))
                viewHolder.item.check(false);
            else
                viewHolder.item.uncheck(false);

            if(friendsListRef.size() - position <= 8 && delegateRef != null)
                delegateRef.loadMoreFriends();
        }
    }

    @Override
    public char getCharIndex(int position) {
        try
        {
            Character chr = positionToChar.get(position);
            if(chr == null) {
                synchronized(friendsListRef)
                {
                    VKApiUserFull itm = friendsListRef.get(position);
                    chr = !itm.first_name.isEmpty() ? itm.first_name.charAt(0) : itm.last_name.charAt(0);
                    positionToChar.put(position, chr);
                }
            }
            return chr;
        }
        catch(Exception ignored) { return 'A'; }
    }

    @Override
    public int getItemHeight() {
        return AndroidUtilities.dp(72);
    }

    private void selectItem(int position)
    {
        try
        {
            synchronized(friendsListRef)
            {
                viewRef.get().add(friendsListRef.get(position));
            }
        }
        catch(Exception ignored) {}
    }
    private void unselectItem(int position)
    {
        try
        {
            synchronized(friendsListRef)
            {
                viewRef.get().remove(friendsListRef.get(position).id);
            }
        }
        catch(Exception ignored) {}
    }

    public void toggleSelection(int position)
    {
        if(isSelected(position))
            unselectItem(position);
        else
            selectItem(position);
    }

    private boolean isSelected(int position)
    {
        try
        {
            synchronized(friendsListRef)
            {
                int userId = friendsListRef.get(position).id;
                return viewRef.get().isSelected(userId);
            }
        }
        catch(Exception ignored) { return false; }
    }

    class FriendsListHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        public FriendsListItem item;
        public FriendsListHolder(View itemView) {
            super(itemView);
            this.item = (FriendsListItem) itemView;
            this.item.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {
            try
            {
                int i = getAdapterPosition();
                if(i >= 0)
                {
                    if(isSelected(i))
                        item.uncheck(true);
                    else
                        item.check(true);
                    toggleSelection(i);
                }
            }
            catch(Exception ignored) {}
        }

        /*@Override
        public void onClick(View v)
        {
            int position = getAdapterPosition();
            if(delegateRef != null && position >= 0)
                delegateRef.didFriendSelected(position);
        }*/
    }
}
