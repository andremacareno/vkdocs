package com.andremacareno.vkdocs.listadapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.andremacareno.vkdocs.AndroidUtilities;
import com.andremacareno.vkdocs.AppLoader;
import com.andremacareno.vkdocs.R;
import com.andremacareno.vkdocs.controllers.NewDocListController;
import com.andremacareno.vkdocs.objs.BottomSheetAction;
import com.andremacareno.vkdocs.views.BottomSheetHolder;
import com.andremacareno.vkdocs.views.BottomSheetItem;

import java.lang.ref.WeakReference;

/**
 * Created by Andrew on 01.05.2015.
 */
public class NewDocMenuAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private BottomSheetAction[] actions;
    private int quickSendSelectionCount = 0;
    private WeakReference<NewDocListController> controller;
    public NewDocMenuAdapter(NewDocListController c)
    {
        this.controller = new WeakReference<NewDocListController>(c);
        this.actions = new BottomSheetAction[] {
                 new BottomSheetAction(BottomSheetAction.CHOOSE_FROM_GALLERY, R.drawable.ic_library_add_black_48dp, AppLoader.sharedInstance().getString(R.string.choose_from_gallery)),
                 new BottomSheetAction(BottomSheetAction.CHOOSE_A_FILE, R.drawable.ic_attach, AppLoader.sharedInstance().getString(R.string.choose_a_file)),
                 new BottomSheetAction(BottomSheetAction.NEW_FOLDER, R.drawable.ic_new_folder, AppLoader.sharedInstance().getString(R.string.new_folder)),
        };
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = new BottomSheetItem(parent.getContext());
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, AndroidUtilities.dp(52));
        view.setLayoutParams(lp);
        return new BottomSheetHolder(view) {
            @Override
            public void onClick(View view) {
                try
                {
                    int position = getAdapterPosition();
                    BottomSheetAction act = actions[position];
                    controller.get().didActionChosen(act);
                }
                catch(Exception ignored) {}
            }
        };
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        BottomSheetAction act = actions[position];
        if(position == 0)
        {
            if(quickSendSelectionCount == 0)
                act.setText(AppLoader.sharedInstance().getResources().getString(R.string.choose_from_gallery));
            else if(quickSendSelectionCount == 1)
                act.setText(AppLoader.sharedInstance().getResources().getString(R.string.quicksend_one_photo));
            else
                act.setText(String.format(AppLoader.sharedInstance().getResources().getString(R.string.quicksend_two_or_more), quickSendSelectionCount));
        }
        ((BottomSheetHolder) holder).item.fill(act);
    }

    @Override
    public int getItemCount() {
        return actions.length;
    }

    public void updateQuickSendSelectionCount(int newCount)
    {
        this.quickSendSelectionCount = newCount;
        notifyItemChanged(0);
    }
    public int getQuickSendSelectionCount() { return this.quickSendSelectionCount; }
}
