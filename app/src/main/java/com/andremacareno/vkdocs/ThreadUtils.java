package com.andremacareno.vkdocs;

import com.andremacareno.asynccore.BackgroundTaskManager;
import com.andremacareno.vkdocs.network.DocumentLoadManager;

/**
 * Created by andremacareno on 09/03/16.
 */
public class ThreadUtils {
    public static void pauseAll()
    {
        try
        {
            BackgroundTaskManager.sharedInstance().pauseThread();
            DocumentLoadManager.sharedInstance().pause();
        }
        catch(Exception ignored) {}
    }
    public static void unpauseAll()
    {
        try
        {
            BackgroundTaskManager.sharedInstance().unpauseThread();
            DocumentLoadManager.sharedInstance().unpause();
        }
        catch(Exception ignored) {}
    }
}
