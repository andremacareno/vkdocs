package com.andremacareno.screencore;

import android.app.Activity;
import android.os.Message;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;

import com.andremacareno.vkdocs.BuildConfig;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;

/**
 * Created by andremacareno on 14/03/16.
 */
public class ScreenManager {

    //stack of views
    private static final String TAG = "ScreenManager";
    private final Deque<BaseScreen> backStack = new ArrayDeque<>();
    private final SparseArray<BaseScreen> idsToScreen = new SparseArray<>();
    private ViewGroup container;
    private BaseScreen presentingScreen = null;

    private Activity activity = null;
    private PauseHandler newScreenReceiver;
    private Animation openAnimation, closeAnimation;
    private boolean animating = false;

    private static volatile ScreenManager _instance = null;
    private ScreenManager() {
        newScreenReceiver = new NewScreenReceiver();
    }
    public static ScreenManager getInstance()
    {
        if(_instance == null)
        {
            synchronized(ScreenManager.class)
            {
                if(_instance == null)
                    _instance = new ScreenManager();
            }
        }
        return _instance;
    }
    public void addScreen(BaseScreen scr, boolean addPreviousToBackStack)
    {
        if(animating)
            return;
        BaseScreen check = idsToScreen.get(scr.getScreenId());
        if(check != null) {
            if(BuildConfig.DEBUG)
                Log.d(TAG, "return");
            return;
        }
        Message msg = Message.obtain(newScreenReceiver);
        msg.obj = scr;
        msg.arg1 = addPreviousToBackStack ? 1 : 0;
        msg.sendToTarget();
    }
    public void attachActivity(Activity activityRef, ViewGroup container)
    {
        if(BuildConfig.DEBUG)
            Log.d(TAG, String.format("attachActivity: %s", activityRef == null ? "null" : activityRef.toString()));
        this.activity = activityRef;
        this.container = container;
        try
        {
            this.container.removeAllViews();
            Iterator<BaseScreen> screenIterator = backStack.descendingIterator();
            while(screenIterator.hasNext())
            {
                BaseScreen scr = screenIterator.next();
                if(BuildConfig.DEBUG)
                    Log.d(TAG, String.format("reattaching screen %s", scr.toString()));
                scr.reattachToActivity(activity);
                View v = scr.getView(container);
                if(v != null)
                {
                    v.setVisibility(View.GONE);
                    if(v.getParent() != null && v.getParent() == container)
                        continue;
                    else if(v.getParent() != null)
                        ((ViewGroup) v.getParent()).removeView(v);
                    container.addView(v);
                }
            }
            if(presentingScreen != null)
            {
                View v = presentingScreen.getView(container);
                if(v != null)
                {
                    v.setVisibility(View.VISIBLE);
                    if(v.getParent() != null && v.getParent() != container)
                    {
                        ((ViewGroup) v.getParent()).removeView(v);
                    }
                    if(BuildConfig.DEBUG)
                        Log.d(TAG, "reattaching presenting screen");
                    container.addView(v);
                }
                presentingScreen.reattachToActivity(activity);
            }
        }
        catch(Exception e) { e.printStackTrace(); }
    }
    public void onActivityDestroyed()
    {
        try
        {
            container.removeAllViews();
        }
        catch(Exception ignored) {}
    }
    public void onPause()
    {
        try
        {
            newScreenReceiver.pause();
            if(presentingScreen != null)
                presentingScreen.onPause();
        }
        catch(Exception ignored) {}
    }
    public void onResume()
    {
        try
        {
            newScreenReceiver.resume();
            if(presentingScreen != null)
                presentingScreen.onResume();
        }
        catch(Exception ignored) {}
    }
    public void setOpenAnimation(Animation anim)
    {
        this.openAnimation = anim;
    }
    public void setCloseAnimation(Animation anim)
    {
        this.closeAnimation = anim;
    }
    public Activity getActivity() { return this.activity; }
    public BaseScreen getPresentingScreen() { return this.presentingScreen; }
    public void popBackStack()
    {
        try
        {
            if(animating)
                return;
            if(presentingScreen != null)
                presentingScreen.onPause();
            BaseScreen prevScr = backStack.pop();
            if(prevScr == null)
                return;
            View v = prevScr.getView(container);
            Animation.AnimationListener listener = null;
            final View presentingView = container.getChildAt(container.getChildCount() - 1);
            final BaseScreen removingScreen = presentingScreen;
            if(v != null) {
                if(closeAnimation != null)
                    listener = new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                            animating = true;
                            removingScreen.onCloseAnimationStart();
                            View viewUnder = container.getChildAt(container.getChildCount() - 2);
                            if(viewUnder != null)
                                viewUnder.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            container.removeView(presentingView);
                            if(removingScreen != null)
                                removingScreen.onDestroy();
                            animating = false;
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    };
            }
            if(presentingScreen != null) {
                if(closeAnimation != null)
                {
                    closeAnimation.setAnimationListener(listener);
                    presentingView.startAnimation(closeAnimation);
                }
                else
                {
                    animating = true;
                    presentingScreen.onCloseAnimationStart();
                    View viewUnder = container.getChildAt(container.getChildCount() - 2);
                    if(viewUnder != null)
                        viewUnder.setVisibility(View.VISIBLE);
                    container.removeView(presentingView);
                    presentingScreen.onDestroy();
                    animating = false;
                }
            }
            idsToScreen.delete(presentingScreen.getScreenId());
            presentingScreen = prevScr;
            prevScr.onResume();
        }
        catch(Exception ignored) {}
    }
    public void clearWithBackStack()
    {
        try
        {
            idsToScreen.clear();
            if(presentingScreen != null)
            {
                presentingScreen.onPause();
                presentingScreen.onDestroy();
            }
            synchronized(backStack)
            {
                BaseScreen scr;
                container.removeAllViews();
                while((scr = backStack.pop()) != null)
                {
                    scr.onDestroy();
                }
            }
        }
        catch(Exception ignored) {}
    }
    public boolean haveBackStackEntries() {
        if(BuildConfig.DEBUG)
            Log.d(TAG, String.format("back stack entries count: %d", backStack.size()));
        return backStack.size() > 0;
    }
    private static class NewScreenReceiver extends PauseHandler
    {

        @Override
        protected boolean storeMessage(Message message) {
            return true;
        }

        @Override
        protected void processMessage(Message message) {
            try
            {
                final BaseScreen scr = (BaseScreen) message.obj;
                ScreenManager.getInstance().idsToScreen.put(scr.getScreenId(), scr);
                boolean addToBackStack = message.arg1 == 1;
                boolean hidePreviousScreen = ScreenManager.getInstance().presentingScreen != null && addToBackStack;
                if(hidePreviousScreen) {
                    if (BuildConfig.DEBUG)
                        Log.d(TAG, "adding to back stack");
                    ScreenManager.getInstance().backStack.push(ScreenManager.getInstance().presentingScreen);
                    ScreenManager.getInstance().presentingScreen.onPause();
                }
                else if(ScreenManager.getInstance().presentingScreen != null)
                {
                    try
                    {
                        ScreenManager.getInstance().presentingScreen.onPause();
                        ScreenManager.getInstance().presentingScreen.onDestroy();
                        View previousView = ScreenManager.getInstance().container.getChildAt(ScreenManager.getInstance().container.getChildCount() - 1);
                        ScreenManager.getInstance().container.removeView(previousView);
                    }
                    catch(Exception e) { e.printStackTrace(); }
                }
                ScreenManager.getInstance().presentingScreen = scr;
                View v = scr.getView(ScreenManager.getInstance().container);
                Animation.AnimationListener listener = null;
                if(v != null) {
                    if(hidePreviousScreen) {
                        if(BuildConfig.DEBUG)
                            Log.d(TAG, "hiding previous view: ".concat(ScreenManager.getInstance().container.getChildAt(ScreenManager.getInstance().container.getChildCount() - 1).toString()));
                        if(ScreenManager.getInstance().openAnimation == null)
                            ScreenManager.getInstance().container.getChildAt(ScreenManager.getInstance().container.getChildCount() - 1).setVisibility(View.GONE);
                        else
                        {
                            final View previousView = ScreenManager.getInstance().container.getChildAt(ScreenManager.getInstance().container.getChildCount() - 1);
                            listener = new Animation.AnimationListener() {
                                @Override
                                public void onAnimationStart(Animation animation) {
                                    ScreenManager.getInstance().animating = true;
                                }

                                @Override
                                public void onAnimationEnd(Animation animation) {
                                    if(previousView != null)
                                        previousView.setVisibility(View.GONE);
                                    scr.onOpenAnimationFinished();
                                    ScreenManager.getInstance().animating = false;
                                }

                                @Override
                                public void onAnimationRepeat(Animation animation) {

                                }
                            };
                        }
                    }
                    ScreenManager.getInstance().container.addView(v);
                    if(listener != null && ScreenManager.getInstance().openAnimation != null) {
                        ScreenManager.getInstance().openAnimation.setAnimationListener(listener);
                        v.startAnimation(ScreenManager.getInstance().openAnimation);
                    }
                    else
                        scr.onOpenAnimationFinished();
                }
                scr.init();
            }
            catch(Exception ignored) {}
        }
    }
}
