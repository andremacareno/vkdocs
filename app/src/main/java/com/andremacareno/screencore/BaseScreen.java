package com.andremacareno.screencore;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.andremacareno.vkdocs.AndroidUtilities;
import com.andremacareno.vkdocs.BuildConfig;
import com.andremacareno.vkdocs.R;
import com.andremacareno.vkdocs.views.ActionView;

/**
 * Created by andremacareno on 14/03/16.
 */
public abstract class BaseScreen {
    private View v;
    private Bundle args;
    private ActionView actionView;
    private View contentView;
    private View fab;
    private static final FrameLayout.LayoutParams containerLP = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
    private static final RelativeLayout.LayoutParams actionViewLP = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
    private static final RelativeLayout.LayoutParams contentViewLP = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
    public BaseScreen()
    {
        onCreate();
    }
    static {{
        actionViewLP.addRule(RelativeLayout.ALIGN_PARENT_TOP);
    }
    }
    public final View getView(ViewGroup container)
    {
        /*if(v != null && v.getParent() != container)
        {
            v = null;
            actionView = null;
            contentView = null;
            fab = null;
        }*/
        try
        {
            if(v != null)
                return v;
            if(actionView == null)
                actionView = getActionView();
            if(contentView == null)
                contentView = getContentView();
            if(actionView != null && Build.VERSION.SDK_INT >= 21 && actionView.drawShadow())
                actionView.setElevation(AndroidUtilities.dp(6));
            if(actionView != null && actionView.getParent() != null)
                ((ViewGroup) actionView.getParent()).removeView(actionView);
            if(contentView != null && contentView.getParent() != null)
                ((ViewGroup) contentView.getParent()).removeView(contentView);

            if(actionView == null && contentView == null)
                return null;
            fab = getFabBetweenViews();
            if(actionView == null) {
                if(BuildConfig.DEBUG)
                    Log.d("BaseScreen", "show only contentView");
                this.v = contentView;
                contentView.setLayoutParams(containerLP);
                return contentView;
            }
            else
            {
                RelativeLayout fragmentContainer = new RelativeLayout(container.getContext());
                fragmentContainer.setLayoutParams(containerLP);
                actionView.setLayoutParams(actionViewLP);
                actionView.setId(R.id.action_view);
                contentViewLP.addRule(RelativeLayout.BELOW, R.id.action_view);
                contentView.setLayoutParams(contentViewLP);
                //contentView.setBackgroundColor(0xFFFFFFFF);
                fragmentContainer.addView(actionView);
                fragmentContainer.addView(contentView);
                if(fab != null)
                {
                    RelativeLayout.LayoutParams fabLP = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                    fabLP.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                    fabLP.addRule(RelativeLayout.ALIGN_BOTTOM, R.id.action_view);
                    fabLP.bottomMargin = -AndroidUtilities.dp(48);
                    fabLP.rightMargin = AndroidUtilities.dp(20);
                    fab.setLayoutParams(fabLP);
                    fragmentContainer.addView(fab);
                }
                this.v = fragmentContainer;
                return fragmentContainer;
            }
        }
        catch(Exception e) { e.printStackTrace(); return getContentView(); }
    }

    public void onResume()
    {
        try
        {
            if(actionView != null)
                actionView.getController().onFragmentResumed();
        }
        catch(Exception ignored) {}
    }
    public void onPause()
    {
        try
        {
            if(actionView != null) {
                actionView.getController().onFragmentPaused();
            }
        }
        catch(Exception ignored) {}
    }
    public void reattachToActivity(Activity act)
    {
        if(BuildConfig.DEBUG)
            Log.d("BaseScreen", "changed activity");
        init();
    }
    public void startActivityForResult(Intent intent, int requestCode) {
        Activity activityRef = ScreenManager.getInstance().getActivity();
        if(activityRef != null) {
            onPause();
            activityRef.startActivityForResult(intent, requestCode);
        }
    }

    public Activity getActivity() { return ScreenManager.getInstance().getActivity(); }
    public Context getContext() {
        Activity activityRef = ScreenManager.getInstance().getActivity();
        return activityRef == null ? null : activityRef.getApplicationContext();
    }
    protected Resources getResources() {
        Activity activityRef = ScreenManager.getInstance().getActivity();
        return activityRef == null ? null : activityRef.getResources();
    }
    public void setArguments(Bundle bundle) { this.args = bundle; }
    protected Bundle getArguments() { return this.args; }
    public abstract void init();
    public abstract ActionView getActionView();
    public abstract View getContentView();
    public abstract void onDestroy();
    public abstract int getScreenId();
    public abstract void onCreate();
    protected abstract void goBack();
    public FloatingActionButton getFabBetweenViews()
    {
        return null;
    }
    public void onOpenAnimationFinished() {}
    public void onCloseAnimationStart() {}
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        onResume();
    }
    /*protected void goBack()
    {
        try
        {
            ((VKDocsMainActivity) getActivity()).performGoBack();
        }
        catch(Exception ignored) {}
    }
    public abstract boolean hasFAB();*/
}
